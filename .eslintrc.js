module.exports = {
  "extends": "airbnb-base",
  "env": {
    "node": true,
    "mocha": true
  },
  "rules": {
    "func-names": "off",
    "prefer-arrow-callback": "off",
    "padded-blocks": "off",
    "comma-dangle": "off",
    "no-console": "off",
  }
};
