const vorpal = require('../bin');

async function test(type, region, env) {
  try {
    vorpal.execSync('loglevel debug');
    await vorpal.exec(`connect --type ${type} --region ${region} --env ${env}`);
    await vorpal.exec('machine send currentState fnsp 1 --toKeyVal');
    await vorpal.exec('user send stateSet fnsp 5 --toKeyVal');
    const [message] = await vorpal.exec('machine receive --topic command');
    if (!(message && message.msg === 'STATE-SET' && message.data && message.data.fnsp === '0005')) {
      throw new Error('No or wrong message received');
    }
    vorpal.logger.info('Test passed');
  } catch (e) {
    vorpal.logger.error(e);
    vorpal.logger.error('Test failed');
  } finally {
    vorpal.execSync('exit');
  }
}

test('520', 'row', 'dev004').then(vorpal.logger.info).catch(vorpal.logger.error);
