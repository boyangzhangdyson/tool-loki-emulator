FROM 689381822103.dkr.ecr.eu-west-1.amazonaws.com/infra-node-js-base-image-docker:10.11.0_11 as base

ENV BUILDHOME=/opt/dyson/loki-build
ENV RUNHOME=/opt/dyson/loki

# Build
FROM base as build
WORKDIR $BUILDHOME
RUN apk update && apk --no-cache add python make g++
COPY package.json package-lock.json $BUILDHOME/
RUN npm install

# Run
FROM base as run
WORKDIR $RUNHOME
COPY --from=build $BUILDHOME/ $RUNHOME
COPY . .

ENTRYPOINT [ "npm", "run" ]
CMD ["start"]
