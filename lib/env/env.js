const testData = require('../../testData.js');

/**
 * Return the PKI cloud grade, given an environment
 *
 * @param {string} region The name of the region, for example `row`
 * @param {string} environmentName The name of the environment, for example `si`
 * @returns {string} The resulting grade
 */
function getCloudGrade(region, environment) {
  let pkiCloudGrade = '';

  // In China region, prod and stage are considered different pki cloud grades
  // In ROW region, prod and stage are considered prod cloud grades
  if (region === 'cn') {
    pkiCloudGrade = `pki.${region}.${['prod', 'stage'].includes(environment) ? environment : 'dev'}`;
  } else {
    pkiCloudGrade = `pki.${region}.${['prod', 'stage'].includes(environment) ? 'prod' : 'dev'}`;
  }
  return pkiCloudGrade;
}

/**
 * Return the base URL that should be used to communicate with a given environment
 *
 * @param {string} region The name of the region, for example `row`
 * @param {string} environmentName The name of the environment, for example `si`
 * @returns {object} The base url for a calling App AND base url for a calling product
 */
function getPublicCloudBaseUrl(region, environment) {
  return {
    product: `https://api.cp${environment === 'prod' ? '' : environment}.dyson.${region === 'row' ? 'com' : region}`,
    //TODO: dev004 does not have appapi set up in api gateway yet
    app: `https://${environment === 'dev004' ? '' : 'app'}api.cp${environment === 'prod' ? '' : environment}.dyson.${region === 'row' ? 'com' : region}`
  };
}

/**
 * Return the base URL that should be used to communicate with a specific private API
 * in a given environment given environment
 *
 * @param {string} region The name of the region, for example `row`
 * @param {string} environmentName The name of the environment, for example `si`
 * @param {string} environmentName The prefix pointing to a specific service, for example `lec-mcs`
 * @returns {string} The base url
 */
function getInternalCloudBaseUrl(region, environment, servicePrefix) {
  return `https://${servicePrefix}.cp${environment === 'prod' ? '' : environment}.dyson.${region === 'row' ? 'com' : region}`;
}

/**
 * Return the URL that should be used to call the Time Service in a given environment
 *
 * @param {string} region The name of the region, for example `row`
 * @param {string} environment The name of the environment, for example `si`
 * @returns {string} The time site url
 */
function getTimeSiteUrl(region, environment) {
  // Note HTTP is important as the time server is used to figure out the time (required for TLS!)
  return `http://time.cp${environment === 'prod' ? '' : environment}.dyson.${region === 'row' ? 'com' : region}`;
}

/**
 * Returns any test data for a given environment
 *
 * @param {any} environmentTag The name of the environment, for example `row.si`
 * @param {any} { testDataSource = testData } Dependency injection for tests
 * @returns {any} The result, or false if no data exists
 */
function getTestData(environmentTag, { testDataSource = {} }) {
  return Object.assign({}, ...Object.keys(testDataSource)
    .map(objectName => ({ [objectName]: testDataSource[objectName][environmentTag] })));
}

/**
 * Returns the configuration for the requested environment
 *
 * @param {any} {
 *   requestedLinkEnv = null,
 *   testDataSource = testData
 * } options
 * @returns {object}
 */
function getCurrentEnvironment({
  requestedLinkEnv = null,
  testDataSource = testData,
  envProvider = process.env
} = {}) {
  const linkEnv = requestedLinkEnv || envProvider.LINK_ENV;

  if (!linkEnv) {
    throw new Error('Unable to read/interpret environment variable LINK_ENV. Please check README.md for usage information');
  }
  const [region, environmentName] = linkEnv.split('.');

  return {
    cloud: {
      region,
      environmentName,
      linkEnv,
      publicBaseUrl: getPublicCloudBaseUrl(region, environmentName),
      timeSiteUrl: getTimeSiteUrl(region, environmentName),
      grade: getCloudGrade(region, environmentName),
      testData: getTestData(linkEnv, { testDataSource })
    }
  };
}

// Slightly funky looking module.exports so we can get
// testable functions, and maintain the default export
module.exports = {
  // The "actual" environment information
  ...getCurrentEnvironment(),

  // The getCurrentEnvironment function for testing
  getCurrentEnvironment,
  getPublicCloudBaseUrl,
  getInternalCloudBaseUrl,
  getTimeSiteUrl,
  getCloudGrade,
};
