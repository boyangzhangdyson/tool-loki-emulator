const env = require('./env');

const ipv4 = 4;

module.exports = {
  env,
  ipv4,
};
