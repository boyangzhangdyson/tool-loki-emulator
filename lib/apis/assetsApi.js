const request = require('../helpers/request');

const { env } = require('../env');

function cleanHistory(account, passwordHash, serial, locale) {
  const options = {
    method: 'GET',
    url: `${env.cloud.publicBaseUrl.app}/v1/assets/devices/${serial}/cleanhistory?culture=${locale}&timeZone=Europe/London`,
    headers: {
      Authorization: `Basic ${Buffer.from(`${account}:${passwordHash}`).toString('base64')}`
    },
  };
  return request(options);
}

function mandatoryUpdate(account, passwordHash, serial, version) {
  const options = {
    method: 'GET',
    url: `${env.cloud.publicBaseUrl.app}/v1/assets/devices/${serial}/versions/${version}`,
    headers: {
      Authorization: `Basic ${Buffer.from(`${account}:${passwordHash}`).toString('base64')}`
    },
  };

  return request(options);
}

function setPendingRelease(account, passwordHash, serial) {
  const options = {
    method: 'POST',
    url: `${env.cloud.publicBaseUrl.app}/v1/assets/devices/${serial}/pendingrelease`,
    headers: {
      Authorization: `Basic ${Buffer.from(`${account}:${passwordHash}`).toString('base64')}`
    },
  };

  return request(options);
}

function getPendingRelease(account, passwordHash, serial) {
  const options = {
    method: 'GET',
    url: `${env.cloud.publicBaseUrl.app}/v1/assets/devices/${serial}/pendingrelease`,
    headers: {
      Authorization: `Basic ${Buffer.from(`${account}:${passwordHash}`).toString('base64')}`
    },
  };

  return request(options);
}

function getCurrentRelease(account, passwordHash, serial) {
  const options = {
    method: 'GET',
    url: `${env.cloud.publicBaseUrl.app}/v1/assets/devices/${serial}/currentrelease`,
    headers: {
      Authorization: `Basic ${Buffer.from(`${account}:${passwordHash}`).toString('base64')}`
    },
  };

  return request(options);
}

module.exports = {
  cleanHistory,
  mandatoryUpdate,
  setPendingRelease,
  getPendingRelease,
  getCurrentRelease
};
