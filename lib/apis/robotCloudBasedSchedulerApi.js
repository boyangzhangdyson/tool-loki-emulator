const request = require('../helpers/request');
const { env } = require('../env/index');

function putSchedule(account, passwordHash, serial, schedule) {
  const options = {
    method: 'PUT',
    url: `${env.cloud.publicBaseUrl.app}/v1/scheduler/devices/${serial}/schedule`,
    headers: {
      Authorization: `Basic ${Buffer.from(`${account}:${passwordHash}`).toString('base64')}`,
      'Content-Type': 'application/json'
    },
    body: schedule,
  };
  return request(options);
}

function getSchedule(account, passwordHash, serial) {
  const options = {
    method: 'GET',
    url: `${env.cloud.publicBaseUrl.app}/v1/scheduler/devices/${serial}/schedule`,
    headers: {
      Authorization: `Basic ${Buffer.from(`${account}:${passwordHash}`).toString('base64')}`,
      'Content-Type': 'application/json'
    },
  };
  return request(options);
}

module.exports = {
  putSchedule,
  getSchedule
};
