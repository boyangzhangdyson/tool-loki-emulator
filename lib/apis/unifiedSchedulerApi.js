const request = require('../helpers/request');
const { env } = require('../env/index');

function putSchedule(account, passwordHash, type, serial, schedule) {
  const options = {
    method: 'PUT',
    url: `${env.cloud.publicBaseUrl.app}/v1/unifiedscheduler/${serial}/events?productType=${type}`,
    headers: {
      Authorization: `Basic ${Buffer.from(`${account}:${passwordHash}`).toString('base64')}`,
      'Content-Type': 'application/json'
    },
    body: schedule,
  };
  return request(options);
}

function getSchedule(account, passwordHash, type, serial) {
  const options = {
    method: 'GET',
    url: `${env.cloud.publicBaseUrl.app}/v1/unifiedscheduler/${serial}/events?productType=${type}`,
    headers: {
      Authorization: `Basic ${Buffer.from(`${account}:${passwordHash}`).toString('base64')}`,
      'Content-Type': 'application/json'
    },
  };
  return request(options);
}

function getScheduleBin(machine) {
  const { cert, key } = machine.machineIdentity.certificates;
  const options = {
    method: 'GET',
    url: `${env.cloud.publicBaseUrl.product}/v1/unifiedscheduler/${machine.serial}/schedule.bin`,
    json: false,
    encoding: null,
    agentOptions: {
      cert,
      key
    }
  };
  return request(options);
}

module.exports = {
  putSchedule,
  getSchedule,
  getScheduleBin
};
