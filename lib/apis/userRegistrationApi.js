const moment = require('moment-timezone');
const debug = require('debug')('lib:apis:userRegistration');
const { env } = require('../env');
const request = require('../helpers/request');

function registerUser(userOptions) {
  return request({
    method: 'POST',
    url: `${env.cloud.publicBaseUrl.app}/v2/userregistration/register?culture=${userOptions.locale}&country=${userOptions.country}`,
    headers: {
      'Content-Type': 'application/json'
    },
    body: {
      ContactPermitted: false,
      Email: userOptions.email,
      FirstName: userOptions.firstName,
      LastName: userOptions.lastName,
      Password: userOptions.password,
      Honorific: userOptions.honorific
    },
  });
}

function authenticateUser(userOptions) {
  return request({
    method: 'POST',
    url: `${env.cloud.publicBaseUrl.app}/v1/userregistration/authenticate?country=${userOptions.country}`,
    headers: {
      'Content-Type': 'application/json'
    },
    body: {
      Email: userOptions.email,
      Password: userOptions.password,
    },
  }).catch((err) => {
    debug(`Failed to authenticate user ${userOptions.email}`);
    throw err;
  });
}

function claimOwnership(account, passwordHash, serial, country) {
  const timezone = 'Europe/London';
  const ownershipDate = moment().tz(timezone).format('YYYY-MM-DD');
  const requestObject = {
    method: 'POST',
    url: `${env.cloud.publicBaseUrl.app}/v1/userregistration/ownership?serial=${serial}&country=${country}`,
    headers: {
      Authorization: `Basic ${Buffer.from(`${account}:${passwordHash}`).toString('base64')}`,
      'Content-Type': 'application/json'
    },
    body: `${ownershipDate}`,
  };
  return request(requestObject);
}

function getOwnership(user, serial, country) {
  const requestObject = {
    method: 'GET',
    url: `${env.cloud.publicBaseUrl.app}/v1/userregistration/ownership?serial=${serial}&country=${country}`,
    headers: {
      Authorization: user.authToken,
      'Content-Type': 'application/json'
    },
  };
  return request(requestObject);
}

module.exports = {
  registerUser,
  authenticateUser,
  claimOwnership,
  getOwnership
};
