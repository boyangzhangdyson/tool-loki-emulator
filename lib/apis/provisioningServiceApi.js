const request = require('../helpers/request');
const { env } = require('../env');

function manifest(apiVersion, account, passwordHash) {
  const options = {
    method: 'GET',
    url: `${env.cloud.publicBaseUrl.app}/${apiVersion}/provisioningservice/manifest`,
    headers: {
      Authorization: `Basic ${Buffer.from(`${account}:${passwordHash}`).toString('base64')}`
    },
  };
  return request(options);
}

function removeManifest(user, serial) {
  const options = {
    method: 'DELETE',
    url: `${env.cloud.publicBaseUrl.app}/v1/provisioningservice/manifest/${serial}`,
    headers: {
      Authorization: user.authToken
    },
  };
  return request(options);
}

function manifestForSerial(apiVersion, user, serial) {
  return this.manifest(apiVersion, user.account, user.passwordHash)
    .then(res => res.body)
    .then((userManifest) => {
      const matchingManifest = userManifest.filter(entry => entry.Serial === serial)[0];
      return matchingManifest;
    });
}

// Despite the name, this endpoint requires an object in the form {Name, AutoUpdate}
function updateDevice(user, serial, newMachineName, autoUpdate = true) {
  const options = {
    method: 'PUT',
    url: `${env.cloud.publicBaseUrl.app}/v1/provisioningservice/devices/${serial}`,
    headers: {
      Authorization: user.authToken
    },
    body: {
      Name: newMachineName,
      AutoUpdate: autoUpdate
    },
  };
  return request(options);
}

module.exports = {
  manifest,
  removeManifest,
  manifestForSerial,
  updateDevice
};
