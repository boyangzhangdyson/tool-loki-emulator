const request = require('../helpers/request');
const { env } = require('../env');

function cleanMap(account, passwordHash, serial, cleanId) {
  const options = {
    method: 'GET',
    url: `${env.cloud.publicBaseUrl.app}/v1/mapvisualizer/devices/${serial}/map/${cleanId}`,
    headers: {
      Authorization: `Basic ${Buffer.from(`${account}:${passwordHash}`).toString('base64')}`
    },
  };
  return request(options);
}

module.exports = {
  cleanMap
};
