const request = require('../helpers/request');

const { env } = require('../env');

function provision(pairingToken, serial, user) {
  const options = {
    method: 'POST',
    url: `${env.cloud.publicBaseUrl.product}/v1/lec/${serial}/provision`,
    headers: {
      Authorization: user.authToken,
      'X-Dyson-PairingToken': pairingToken
    },
  };
  return request(options);
}

module.exports = {
  provision
};
