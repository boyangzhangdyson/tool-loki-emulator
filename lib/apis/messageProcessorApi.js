const request = require('../helpers/request');
const { env } = require('../env');

function connectionStatus({ account, passwordHash }, serial) {
  const options = {
    method: 'GET',
    url: `${env.cloud.publicBaseUrl.app}/v1/messageprocessor/devices/${serial}/connectionstatus`,
    headers: {
      Authorization: `Basic ${Buffer.from(`${account}:${passwordHash}`).toString('base64')}`
    },
  };
  return request(options);
}

module.exports = {
  connectionStatus
};
