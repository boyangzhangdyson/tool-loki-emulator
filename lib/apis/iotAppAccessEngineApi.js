const debug = require('debug')('lib:apis:iotAppAccessEngineApi');

const request = require('../helpers/request');
const { env } = require('../env');

function iotRoleCredentials(serial, guid, passwordHash) {

  return request({
    method: 'POST',
    url: `${env.cloud.publicBaseUrl.app}/v1/authorize/iot-role-credentials`,
    body: {
      serial
    },
    auth: {
      user: guid,
      password: passwordHash,
      sendImmediately: true
    },
  })
    .catch((err) => {
      debug(`Failed to request IAAE credentials ${err}`);
      throw err;
    });
}

module.exports = {
  iotRoleCredentials
};
