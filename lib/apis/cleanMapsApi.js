const request = require('../helpers/request');
const { env } = require('../env');

function readCleanMaps({ authToken }, serial) {
  const options = {
    method: 'GET',
    url: `${env.cloud.publicBaseUrl.app}/v1/${serial}/clean-maps`,
    headers: {
      Authorization: `${authToken}`
    },
  };
  return request(options);
}

module.exports = {
  readCleanMaps
};
