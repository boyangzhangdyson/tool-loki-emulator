const request = require('../helpers/request');
const { env } = require('../env');

const getPersistentMapMetadata = (user, serial) => {
  const options = {
    method: 'GET',
    url: `${env.cloud.publicBaseUrl.app}/v1/app/${serial}/persistent-map-metadata`,
    headers: {
      Authorization: `${user.authToken}`,
      'Content-Type': 'application/json'
    },
  };
  return request(options).then(res => res.body);
};

const putZonesDefinition = (user, serial, mapId, zonesDefinition) => {
  const options = {
    method: 'PUT',
    url: `${env.cloud.publicBaseUrl.app}/v1/app/${serial}/zones-definitions/${mapId}`,
    body: zonesDefinition,
    headers: {
      Authorization: `${user.authToken}`,
      'Content-Type': 'application/json'
    },
  };
  return request(options).then(res => res.body);
};

const deleteZonesDefinition = (user, serial, mapId) => {
  const options = {
    method: 'DELETE',
    url: `${env.cloud.publicBaseUrl.app}/v1/app/${serial}/zones-definitions/${mapId}`,
    headers: {
      Authorization: `${user.authToken}`,
      'Content-Type': 'application/json'
    },
  };
  return request(options).then(res => res.body);
};

const getZonesDefinition = (machine, mapId) => {
  const { cert, key, ca } = machine.machineIdentity.certificates;

  const options = {
    method: 'GET',
    url: `${env.cloud.publicBaseUrl.product}/v1/robot/${machine.serial}/zones-definitions/${mapId}`,
    headers: {
      'Content-Type': 'application/json',
    },
    agentOptions: {
      key,
      cert,
      ca: ca.awsIot
    },
  };
  return request(options).then(res => res.body);
};

const getPersistentMapManifest = (machine) => {
  const { cert, key, ca } = machine.machineIdentity.certificates;

  const options = {
    method: 'GET',
    url: `${env.cloud.publicBaseUrl.product}/v1/robot/${machine.serial}/persistent-map-manifest`,
    headers: {
      'Content-Type': 'application/json',
    },
    agentOptions: {
      key,
      cert,
      ca: ca.awsIot
    },
  };
  return request(options).then(res => res.body);
};

const getPersistentMapById = (user, serial, mapId) => {
  const options = {
    method: 'GET',
    url: `${env.cloud.publicBaseUrl.app}/v1/app/${serial}/persistent-maps/${mapId}`,
    headers: {
      Authorization: `${user.authToken}`,
      'Content-Type': 'application/json'
    },
    simple: false,
  };
  return request(options);
};

const putPersistentMap = (machine, persistentMap) => {
  const { cert, key, ca } = machine.machineIdentity.certificates;

  if (machine.machineIdentity.type !== '276') {
    return Promise.reject(new Error('Persistent Map can only be sent by Robot(276)'));
  }

  const options = {
    method: 'PUT',
    url: `${env.cloud.publicBaseUrl.product}/v1/robot/${machine.serial}/persistent-maps/${persistentMap.id}/${persistentMap.version}`,
    headers: {
      'Content-Type': 'application/json',
    },
    agentOptions: {
      key,
      cert,
      ca: ca.awsIot
    },
    body: persistentMap,
  };
  return request(options).then(res => res.body);
};

const deletePersistentMap = (user, serial, mapId) => {
  const options = {
    method: 'DELETE',
    url: `${env.cloud.publicBaseUrl.app}/v1/app/${serial}/persistent-maps/${mapId}`,
    headers: {
      Authorization: `${user.authToken}`,
      'Content-Type': 'application/json'
    },
  };
  return request(options).then(res => res.body);
};

module.exports = {
  getPersistentMapMetadata,
  getPersistentMapById,
  getPersistentMapManifest,
  putPersistentMap,
  deletePersistentMap,
  putZonesDefinition,
  deleteZonesDefinition,
  getZonesDefinition
};
