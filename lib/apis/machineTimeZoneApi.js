const request = require('../helpers/request');
const { env } = require('../env/index');

const getTimeZone = (user, serial) => {
  const options = {
    method: 'GET',
    url: `${env.cloud.publicBaseUrl.app}/v1/machine/${serial}/timezone`,
    headers: {
      Authorization: `Basic ${Buffer.from(`${user.account}:${user.passwordHash}`).toString('base64')}`,
      'Content-Type': 'application/json'
    },
  };
  return request(options);
};

const setTimeZone = (user, serial, timeZone) => {
  const options = {
    method: 'PUT',
    url: `${env.cloud.publicBaseUrl.app}/v1/machine/${serial}/timezone`,
    headers: {
      Authorization: `Basic ${Buffer.from(`${user.account}:${user.passwordHash}`).toString('base64')}`,
      'Content-Type': 'application/json'
    },
    body: timeZone,
  };
  return request(options);
};

module.exports = {
  getTimeZone,
  setTimeZone,
};
