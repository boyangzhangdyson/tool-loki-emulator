const request = require('../helpers/request');
const { env } = require('../env/index');

function getScheduleBin(machine) {
  const { cert, key } = machine.machineIdentity.certificates;
  const options = {
    method: 'GET',
    url: `${env.cloud.publicBaseUrl.product}/v1/productscheduler/devices/${machine.serial}/schedule.bin`,
    json: false,
    encoding: null,
    agentOptions: {
      key,
      cert
    },
  };
  return request(options);
}

function checkScheduleSyncStatus(account, passwordHash, serial) {
  const options = {
    method: 'GET',
    url: `${env.cloud.publicBaseUrl.app}/v1/productscheduler/devices/${serial}`,
    headers: {
      Authorization: `Basic ${Buffer.from(`${account}:${passwordHash}`).toString('base64')}`,
      'Content-Type': 'application/json'
    }
  };
  return request(options);
}

module.exports = {
  getScheduleBin,
  checkScheduleSyncStatus
};
