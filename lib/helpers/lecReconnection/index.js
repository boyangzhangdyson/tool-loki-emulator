const request = require('../request');
const { env } = require('../../../lib/env');

async function reverify(user, machine, payload2) {
  // Send payload 2 to reverify endpoint and retrieve payload 3
  const reverifyRequestOptions = {
    uri: `${env.cloud.publicBaseUrl.app}/v1/lec/${machine.serial}/reverify`,
    headers: {
      Authorization: user.authToken,
    },
    body: {
      challenge: payload2
    },
    method: 'POST',
    agentOptions: {
      key: machine.machineIdentity.certificates.key,
      cert: machine.machineIdentity.certificates.cert,
      ca: machine.machineIdentity.certificates.ca.link
    },
  };
  let reverifyResult;
  try {
    reverifyResult = await request(reverifyRequestOptions);
  } catch (err) {
    throw new Error(`Reverify request failed whilst attempting to re-connect.\n    ${err}`);
  }
  return reverifyResult.body.payload;
}

module.exports = { reverify };
