const debug = require('debug')('helpers:pki');
const util = require('util');
const { env } = require('../../env');
const config = require('../../../config.json');
const request = require('../request');

async function updateLecKeys(serial, lecKeys) {
  const baseUrl = env.getInternalCloudBaseUrl(env.cloud.region, env.cloud.environmentName, 'lec-mcs');
  const ingestEndpoint = `${baseUrl}/import/ingest`;
  const requestBody = [Object.assign({ Serial: serial }, lecKeys)];
  debug(`Making request to ingest endpoint of Machine Certificates Service at ${ingestEndpoint} with the following object: ${util.inspect(JSON.stringify(requestBody), false, null)}`);

  try {
    const updateResult = await request({
      uri: ingestEndpoint,
      headers: {
        'X-Dyson-ImportAuthCode': config.machineCertificate.importAuthCode[env.cloud.environmentName],
        'Content-Type': 'application/json'
      },
      body: requestBody,
      method: 'POST',
    });
    debug(`Request response:\n${util.inspect(JSON.stringify(updateResult), false, null)}`);

    if (updateResult.failedCertificates) {
      console.log('Some certificates failed to be imported. Next steps might fail.');
    }
    return updateResult;
  } catch (err) {
    throw new Error(`The request to the ingest endpoint failed. Endpoint might not be reachable or import auth code is wrong. Error:\n${err}`);
  }
}

module.exports = {
  updateLecKeys
};
