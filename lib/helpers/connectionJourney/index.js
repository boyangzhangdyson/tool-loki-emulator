const lecOnly = require('./lecOnly');
const hecOnly = require('./hecOnly');
const lecHec = require('./lecHec');

const perform = (machine, user, otaOperation = false) => {
  switch (machine.machineIdentity.connectionJourneyType) {
    case 'lecOnly':
      return lecOnly.perform(machine, user);
    case 'hecOnly':
      return hecOnly.perform(machine, user, otaOperation);
    case 'lecHec':
      return lecHec.perform(machine, user, otaOperation);
    default:
      throw new Error(`Unknown connection journey type: ${machine.machineIdentity.connectionJourneyType}`);
  }
};

module.exports = {
  perform
};
