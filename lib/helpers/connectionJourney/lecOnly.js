const {
  hello
} = require('../../../lib/messages/machineMessages');
const lecAuth = require('./lecAuth');
// Workflow:
//   LEC payload exchange
//   Provision LEC machine

async function perform(machine, user) {
  // Perform common actions
  const pairingToken = await lecAuth(user, machine);
  await user.lecProvision(machine, pairingToken);
  await user.connect(machine);
  await user.publish(hello(), machine);
  await user.useMachine(machine);
}

module.exports = {
  perform
};
