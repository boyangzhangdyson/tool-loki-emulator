const {
  hello,
  otaHello,
  avu,
} = require('../../../lib/messages/machineMessages');
const lecAuth = require('./lecAuth');
// Workflow:
//   LEC payload exchange
//   Provision machine
//   Connect to broker
//   Send HELLO message
//   Send AVU message

async function perform(machine, user, otaOperation = false) {
  // Perform common actions
  await lecAuth(user, machine);
  await machine.provision();
  await machine.connect();
  if (otaOperation) {
    await machine.publish(otaHello());
  } else {
    await machine.publish(hello());
  }
  await machine.publish(avu(user.account));
}

module.exports = {
  perform
};
