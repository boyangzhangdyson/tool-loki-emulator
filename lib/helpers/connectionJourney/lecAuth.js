const crypto = require('crypto');
const LecCrypto = require('../elliptic-curve-crypto');
const config = require('../../../config.json');
const request = require('../request');
const { env } = require('../../../lib/env');

// Workflow as follows:
//   Retrieve Payload1 from public API endpoint using machine apiAuthToken
//   Decrypt Payload1 using machine public key and cloud private key
//     (different keys per machine stored in DynamoDB)
//   Generate and encrypt Payload2
//   Request Payload3 using Payload2 as challenge
//   Decrypt Payload3 to get LongTermKey (LTK)
//   Return PairingToken

async function perform(user, machine) {
  // Initialise the Machine Crypto Instance and apiAuthToken
  // All test machines share the same machinePublicKey, cloudPrivateKey and apiAuthToken
  // for the sake of simplicity (values stored in DynamoDB)
  // This should be moved into a property of machineIdentity
  const machineCryptoInstance = new LecCrypto({
    privateKey: Buffer.from(config.lecKeys.MachinePrivateKey, 'hex'),
    publicKey: Buffer.from(config.lecKeys.CloudPublicKey, 'hex')
  });
  const commonApiAuthCode = config.lecKeys.ApiAuthCode;

  // Retrieve Payload 1
  const authRequestOptions = {
    uri: `${env.cloud.publicBaseUrl.app}/v1/lec/${machine.serial}/auth`,
    headers: {
      'X-Dyson-ApiAuthCode': commonApiAuthCode,
      Authorization: user.authToken
    },
    method: 'POST',
    agentOptions: {
      ca: machine.machineIdentity.certificates.ca.link
    },
  };
  let authResult;
  try {
    authResult = await request(authRequestOptions);
  } catch (err) {
    throw new Error(`Auth request failed during connection journey for serial '${machine.serial}'.\n    ${err}`);
  }
  const lecPayload1 = authResult.body.payload;

  // Validate Payload 1
  const decryptedPayload1 = Buffer.from(machineCryptoInstance.decrypt(lecPayload1, 'hex', 2), 'hex');

  // Generate Payload
  const machineRandomNumber = crypto.randomBytes(16);
  const payload2Contents = Buffer.concat([decryptedPayload1, machineRandomNumber]).toString('hex');
  const [payload2] = machineCryptoInstance.encrypt(payload2Contents, 'hex', Buffer.from([0x00, 0x00]));
  const lecPayload2 = payload2;

  // Retrieve Payload 3
  const verifyRequestOptions = {
    uri: `${env.cloud.publicBaseUrl.app}/v1/lec/${machine.serial}/verify`,
    headers: {
      'X-Dyson-ApiAuthCode': commonApiAuthCode,
      Authorization: user.authToken
    },
    body: {
      challenge: lecPayload2
    },
    method: 'POST',
    agentOptions: {
      ca: machine.machineIdentity.certificates.ca.link
    },
  };
  let verifyResult;
  try {
    verifyResult = await request(verifyRequestOptions);
  } catch (err) {
    throw new Error(`Verify request failed during connection journey.\n    ${err}`);
  }
  const lecPayload3 = verifyResult.body.payload;

  // Validate Payload 3
  const payload3 = machineCryptoInstance.decrypt(lecPayload3, 'hex', 2, 32);
  const returnedChallengeRandomNumber = payload3.slice(0, 16);
  const returnedLtk = payload3.slice(16, 32).toString('hex');

  // Quick check on the challenge
  if (returnedChallengeRandomNumber.toString('hex') !== machineRandomNumber.toString('hex')) {
    throw new Error('Machine challenge did not match');
  }

  const ltkRequestOptions = {
    uri: `${env.cloud.publicBaseUrl.app}/v1/lec/${machine.serial}/ltk`,
    headers: {
      Authorization: user.authToken
    },
    method: 'GET',
    agentOptions: {
      ca: machine.machineIdentity.certificates.ca.link
    },
  };
  let ltkResult;
  try {
    ltkResult = await request(ltkRequestOptions);
  } catch (err) {
    throw new Error(`LTK request failed during connection journey.\n    ${err}`);
  }

  // Quick check on the LTK
  if (returnedLtk !== ltkResult.body.ltk.toLowerCase()) {
    throw new Error('Guessed LTK from Payload3 does not match with the returned LTK from API');
  }

  return verifyResult.body.pairingToken;
}

module.exports = perform;
