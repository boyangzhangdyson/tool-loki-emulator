const {
  hello,
  otaHello,
  avu,
} = require('../../../lib/messages/machineMessages');

async function perform(machine, user, otaOperation = false) {
  await machine.provision();
  await machine.connect();
  if (otaOperation) {
    await machine.publish(otaHello());
  } else {
    await machine.publish(hello());
  }
  await machine.publish(avu(user.account));
}

module.exports = {
  perform
};
