const debug = require('debug')('helpers:poll');
const util = require('util');

exports.poll = (action, duration, condition = () => true) => {
  let interval;
  let timeout;
  let lastResult;
  let numberOfCalls = 0;
  let numberUnsuccessful = 0;
  let stopProcessing = false;

  return new Promise((resolve, reject) => {
    debug(`Registering periodical poll function..\n    Action: ${action.toString()}\n    Condition: ${condition.toString()}`);

    interval = setInterval(() => {
      numberOfCalls += 1;
      debug(`[Attempt #${numberOfCalls}] Executing action and comparing result with the condition.`);

      action()
        .then((actionResult) => {
          // Do not do anything if any other callback has set stopProcessing to true
          // to avoid polluting callbacks and logs
          if (stopProcessing) return false;

          lastResult = actionResult;
          numberUnsuccessful += 1;
          if (!condition(actionResult)) {
            debug('Poll attempt failed. Condition not met.');
            return false;
          }
          // Success!
          stopProcessing = true;
          debug('Poll attempt succeded. Condition met.');

          clearInterval(interval);
          clearTimeout(timeout);
          return resolve(actionResult);
        })
        .catch((err) => {
          lastResult = err;
          numberUnsuccessful += 1;
          debug(`Poll attempt failed with an exception: ${err}`);
        });
    }, 1000);
    timeout = setTimeout(() => {
      clearInterval(interval);
      stopProcessing = true;
      return reject(new Error(`Reached global timeout while retrying an action after ${numberOfCalls} call(s) and ${numberUnsuccessful} unsuccessful result(s).\nAction was:\n  ${action.toString()}.\nCondition was:\n  ${condition.toString()}.\nLast result was:\n  ${util.inspect(lastResult, { breakLength: 80 })}`));
    }, duration);
  });
};
