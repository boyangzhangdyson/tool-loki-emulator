const util = require('util');
const moment = require('moment-timezone');
const debug = require('debug');
const NTPClient = require('@destinationstransfers/ntp');
const { getRandomPowerMode, getPowerModeMapping } = require('../../messages/powerModes');
const { getEventSetting, getLegacyEventSetting } = require('../../schedule/scheduleSetting');
const { robotCloudBasedScheduler, unifiedScheduler, ecScheduleBin } = require('../../apis');
const { machineTimeZone } = require('../../apis');
const { sleep } = require('../sleep');
const ScheduleBinDeSerializer = require('./scheduleBinDeserializer');
const ScheduleBinValidator = require('./scheduleBinSettingsValidator');

// One time sync with Dyson NTP servers (do not rely on system date)
async function syncMomentWithDyson() {
  const networkTime = await NTPClient.getNetworkTime({ server: 'ntp.dyson.com' });
  const currentTimeOffset = moment(+new Date()).diff(moment(networkTime));
  moment.now = function () {
    return +new Date() - currentTimeOffset;
  };
}

const scheduleDebug = debug('features:robot-schedules:start');
const userDebug = debug('lib:user');

const LEGACY = 'LEGACY';
const MINUTES_IN_FUTURE_TO_SCHEDULE_CLEAN_FOR = '1';
let timezone = 'Europe/London';

let expectedPowerModeMapping;
let sentScheduleForToday;

const emptyLegacySchedule = () => ({
  TimeZone: timezone,
  Days: []
});

const emptySchedule = () => ({
  Enabled: true,
  Events: []
});

const defaultLegacySchedule = ({
  scheduleDate,
  vaccumPowerMode,
  timeZone
}) => {
  const schedule = {
    TimeZone: timeZone,
    Days: []
  };
  for (let i = 0; i < 7; i += 1) {
    schedule.Days.push({
      DayOfWeek: i,
      Time: scheduleDate.format('HH:mm:00'),
      Enabled: true,
      Repeated: false,
      PowerMode: vaccumPowerMode
    });
  }
  return schedule;
};

const defaultSchedule = ({
  scheduleDate,
  vaccumPowerMode,
  currentCleaningMode
}) => {
  const schedule = {
    Enabled: true,
    Events: [
      {
        GroupId: 1,
        Days: [0, 1, 2, 3, 4, 5, 6],
        StartTime: scheduleDate.format('HH:mm:00'),
        WeeklyRepeat: false,
        Enabled: true,
      }
    ]
  };

  if (typeof vaccumPowerMode !== 'undefined') {
    if (!schedule.Events[0].Settings) schedule.Events[0].Settings = {};
    schedule.Events[0].Settings.PowerMode = vaccumPowerMode;
  }
  if (currentCleaningMode) {
    if (!schedule.Events[0].Settings) schedule.Events[0].Settings = {};
    schedule.Events[0].Settings.CurrentCleaningMode = currentCleaningMode;
  }

  return schedule;
};


const defaultLegacyEcSchedule = settings => ({
  Days: [0, 1, 2, 3, 4, 5, 6],
  Start: '09:00:00',
  End: '12:00:00',
  Repeat: 'true',
  Settings: settings
});

const defaultEcSchedule = settings => ({
  Enabled: true,
  Events: [
    {
      GroupId: 1,
      Days: [0, 1, 2, 3, 4, 5, 6],
      StartTime: '09:00:00',
      WeeklyRepeat: false,
      Enabled: true,
      Settings: settings
    },
    {
      GroupId: 1,
      Days: [0, 1, 2, 3, 4, 5, 6],
      StartTime: '12:00:00',
      WeeklyRepeat: false,
      Enabled: true,
      Settings: settings
    }
  ]
});

const putSchedule = async (user, machine, useLegacy, schedule) => {
  if (!['N223', '276', '520', '438', '527', '475'].includes(machine.machineIdentity.type)) {
    throw new Error(`The tests do not have support for the schedule type used by '${machine.machineIdentity.type}' machines`);
  }

  let putRequestFunction;
  let putRequestParameters;

  if (useLegacy) {
    putRequestFunction = robotCloudBasedScheduler.putSchedule;
    putRequestParameters = [user.account, user.passwordHash, machine.serial, schedule];
  } else {
    putRequestFunction = unifiedScheduler.putSchedule;
    putRequestParameters = [
      user.account, user.passwordHash, machine.type, machine.serial, schedule
    ];
  }


  // Try to read scheduled time for debug
  try {
    const currentDay = parseInt(moment().tz(timezone).weekday(), 10);
    sentScheduleForToday = useLegacy
      ? schedule.Days.filter(day => day.DayOfWeek === currentDay)[0].Time
      : schedule.Events[0].StartTime;

    userDebug(`Time is ${moment().tz(timezone)}. Sending schedule for ${sentScheduleForToday}.`);
  } catch (err) { /* Do nothing */ }

  try {
    const res = await putRequestFunction(...putRequestParameters);
    userDebug(`PUT Schedule Response: ${util.inspect(JSON.stringify(res), false, null)}\n`);
    return res;
  } catch (err) {
    userDebug('Error putting schedule');
    throw new Error(err);
  }
};

const getSchedule = async (user, machine, useLegacy) => {

  if (!['N223', '276', '475', '520', '438'].includes(machine.machineIdentity.type)) {
    throw new Error(`The tests do not have support for the schedule type used by '${machine.machineIdentity.type}' machines`);
  }

  let getRequestFunction;
  let getRequestParameters;

  if (useLegacy) {
    getRequestFunction = robotCloudBasedScheduler.getSchedule;
    getRequestParameters = [user.account, user.passwordHash, machine.serial];
  } else {
    getRequestFunction = unifiedScheduler.getSchedule;
    getRequestParameters = [user.account, user.passwordHash, machine.type, machine.serial];
  }

  try {
    const res = await getRequestFunction(...getRequestParameters);
    userDebug(`GET Schedule Response: ${util.inspect(JSON.stringify(res), false, null)}\n`);
    return res;
  } catch (err) {
    userDebug('Error getting schedule');
    throw new Error(err);
  }
};

const getScheduleBin = async (machine) => {
  if (!['520', '438', '475'].includes(machine.machineIdentity.type)) {
    throw new Error(`The tests do not have support for the schedule bin download used by '${machine.machineIdentity.type}' machines`);
  }

  let getRequestFunction;

  // Gen 1 machine requests separate endpoint to get schedule bin
  if (machine.machineIdentity.type === '475') {
    getRequestFunction = ecScheduleBin.getScheduleBin;
  } else {
    getRequestFunction = unifiedScheduler.getScheduleBin;
  }

  try {
    const res = await getRequestFunction(machine);
    userDebug(`GET Schedule Bin Response: ${util.inspect(JSON.stringify(res), false, null)}\n`);
    return res;
  } catch (err) {
    userDebug('Error Downloading ScheduleBin');
    throw new Error(err);
  }
};

const checkScheduleSyncStatus = async (user, machine) => {
  if (machine.machineIdentity.type !== '475') {
    throw new Error(`The tests do not have support for the schedule type used by '${machine.machineIdentity.type}' machines`);
  }

  try {
    const res = await ecScheduleBin.checkScheduleSyncStatus(user.account,
      user.passwordHash, machine.serial);
    userDebug(`GET Schedule Status: ${JSON.stringify(res)}\n`);
    return res;
  } catch (err) {
    userDebug('Error getting schedule status');
    throw new Error(err);
  }
};

function create(user, schedulerType) {
  // First things first
  syncMomentWithDyson();
  const useLegacy = schedulerType === LEGACY;

  return {
    putEmptySchedule: () => {
      const schedule = useLegacy ? emptyLegacySchedule() : emptySchedule();
      return putSchedule(user, user.currentMachine, useLegacy, schedule);
    },
    putDefaultSchedule: async ({
      timeZone = 'Europe/London',
      currentCleaningMode,
      vaccumPowerMode
    } = {}) => {
      timezone = timeZone;
      const nowDate = moment().tz(timezone);
      const scheduleDate = nowDate.clone().add(MINUTES_IN_FUTURE_TO_SCHEDULE_CLEAN_FOR, 'm');

      expectedPowerModeMapping = getPowerModeMapping(user.currentMachine.type, vaccumPowerMode);

      if (!useLegacy) {
        // Legacy Scheduler updates machine timezone during schedule updates.
        // Unified Scheduler does not update machine timezone during schedule updates.
        // Hence, calling Machine Service Timezone API to update timezone alone.
        await machineTimeZone.setTimeZone(
          user,
          user.currentMachine.serial,
          { timezone }
        );
        await sleep(3); // KCK-21475
      }

      const schedule = useLegacy
        ? defaultLegacySchedule({
          scheduleDate,
          vaccumPowerMode,
          timeZone
        })
        : defaultSchedule({
          scheduleDate,
          vaccumPowerMode,
          currentCleaningMode
        });

      return putSchedule(user, user.currentMachine, useLegacy, schedule);
    },
    getSchedule: () => getSchedule(user, user.currentMachine, useLegacy),
    getExpectedPowerModeMapping: () => expectedPowerModeMapping,
    timeToWaitForStartMessage: () => {
      const currentMoment = moment().tz(timezone);
      const scheduledDateAndTime = moment.tz(`${currentMoment.format('YYYY-MM-DD')} ${sentScheduleForToday}`, timezone);
      const waitSeconds = scheduledDateAndTime.diff(moment().tz(timezone), 'seconds');
      scheduleDebug(`Waiting until time is ${scheduledDateAndTime.format('HH:mm:ss')} (in Machine's Timezone '${timezone}' when the schedule should be triggered). Total waiting time = ${waitSeconds} seconds ...`);
      return waitSeconds;
    },
    isEmptySchedule: (schedule) => {

      const productType = user.currentMachine.type;
      if (useLegacy) {
        return schedule.body.Days && schedule.body.Days.length === 0;
      }

      if (['520', '438', '475'].includes(productType)) {
        return schedule.body.events && schedule.body.events.length === 0
          && schedule.body.serial === user.currentMachine.serial;
      }

      return schedule.body.events && schedule.body.events.length === 0;
    },
    putDefaultEcSchedule: async () => {
      const schedule = useLegacy
        ? defaultLegacyEcSchedule(getLegacyEventSetting())
        : defaultEcSchedule(getEventSetting(user.currentMachine.type));

      return putSchedule(user, user.currentMachine, useLegacy, schedule);
    },
    getScheduleBin: () => getScheduleBin(user.currentMachine),
    getDeserializerBin: scheduleBinResult => new ScheduleBinDeSerializer(scheduleBinResult)
      .deserialize(user.currentMachine.type),
    getScheduleSettings: async () => {
      const schedule = useLegacy
        ? defaultLegacyEcSchedule(getLegacyEventSetting())
        : defaultEcSchedule(getEventSetting(user.currentMachine.type));

      return schedule;
    },
    getRandomPowerMode,
    verifySchedulerBinSettings: (scheduleBinSettings, scheduleSettings) => new ScheduleBinValidator(
      scheduleBinSettings, scheduleSettings, user.currentMachine.type
    ).validate(),
    checkScheduleSyncStatus: () => checkScheduleSyncStatus(user, user.currentMachine)
  };
}

module.exports = { create };
