class ScheduleBinSettingsValidator {

  constructor(scheduleBinSettings, scheduleSettings, productType = '520') {
    this.scheduleBinSettings = scheduleBinSettings;
    this.scheduleSettings = scheduleSettings;
    this.productType = productType;
    this.isValid = true;
  }

  validate() {
    switch (this.productType) {
      case '520':
      case '438':
      case '527':
        for (let i = 0; i < this.scheduleSettings.Events[0].Days.length * 2; i += 2) {
          this.isValid = this.isValid && this.verifyECSettings(
            this.scheduleBinSettings.events[i],
            this.scheduleBinSettings.events[i + 1],
            this.scheduleSettings.Events[0].Settings
          );
        }
        return this.isValid;
      case '475':
        for (let i = 1; i < this.scheduleSettings.Events[0].Days.length + 1; i += 2) {
          this.isValid = this.isValid && this.verifyECSettings(
            this.scheduleBinSettings.events[i],
            this.scheduleBinSettings.events[i],
            this.scheduleSettings.Events[0].Settings
          );
        }
        return this.isValid;
      default:
        return false;
    }
  }

  verifyECSettings(startEvent, endEvent, settings) {
    // Here we need to check whether each settings are matching or not
    let result = true;
    if (this.productType === '475') {
      result = result && startEvent.fmod === settings.fmod.padEnd(4, '~');
      result = result && startEvent.sltm === settings.sltm.padEnd(4, '~');
    } else {
      result = result && startEvent.fdir === settings.fdir.padEnd(4, '~');
      result = result && startEvent.auto === settings.auto.padEnd(4, '~');
    }
    result = result && startEvent.fnsp === settings.fnsp.toString().padStart(4, '0');
    result = result && startEvent.oson === settings.oson.padEnd(4, '~');
    result = result && startEvent.nmod === settings.nmod.padEnd(4, '~');
    if (['475', '527'].includes(this.productType)) {
      result = result && startEvent.hmod === settings.hmod.padEnd(4, '~');
      result = result && startEvent.hmax === settings.hmax.toString().padStart(4, '0');
    } else {
      result = result && startEvent.hmod === undefined;
      result = result && startEvent.hmax === undefined;
    }
    if (this.productType !== '475') {
      result = result && startEvent.fpwr === 'ON~~' && endEvent.fpwr === 'OFF~';
    }

    return result;
  }
}

module.exports = ScheduleBinSettingsValidator;
