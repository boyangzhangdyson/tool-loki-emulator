// Used from SLT https://stash.dyson.global.corp/projects/CTCLD/repos/cld-unifiedscheduler-webapi/browse/AcceptanceTests/helpers/scheduleBinDeserializer.js
// For deserializer gen1 machine:
// https://confluence.dyson.global.corp/display/CLD/Unified+Scheduler+Service+-+Initial+Planning?preview=%2F101318841%2F108673262%2FSchedule+Bin+Format+Calculation+v2.0.xlsx

function BinStruct(name, numBytes, type) {
  return { name, numBytes, type };
}

const HEADER_STRUCT = [
  // Header //
  new BinStruct('scheduleFileFormatVersion', 2, 'int'),
  new BinStruct('padding', 6, 'padding'),
  new BinStruct('scheduleVersion', 6, 'int'), // unix epoch time of schedule update time utc
  new BinStruct('padding', 2, 'padding') // javascript only support up to 6 bytes.,
];

const GEN_1_HEADER_STRUCT = [
  // Gen 1 Header //
  new BinStruct('Init', 2, 'int'),
  new BinStruct('scheduleVersion', 2, 'int')
];

const SCHEDULE_TIME_STRUCT = [
  // Schedule Timestamp //
  new BinStruct('year', 2, 'int'),
  new BinStruct('month', 1, 'int'),
  new BinStruct('day', 1, 'int'),
  new BinStruct('hour', 1, 'int'),
  new BinStruct('minute', 1, 'int'),
  new BinStruct('second', 1, 'int'),
  new BinStruct('padding', 1, 'padding'),
];

const DST_STRUCT = [
  // Daylight Saving Settings //
  new BinStruct('timeZoneId', 2, 'int'),
  new BinStruct('timeZoneOffset', 2, 'int'),
  new BinStruct('ruleVersion', 2, 'int'),
  new BinStruct('startMonth', 1, 'int'),
  new BinStruct('startRule', 1, 'int'),
  new BinStruct('startDate', 1, 'int'),
  new BinStruct('startDayOfWeek', 1, 'int'),
  new BinStruct('startTime', 1, 'int'),
  new BinStruct('adjustment', 1, 'int'),
  new BinStruct('endMonth', 1, 'int'),
  new BinStruct('endRule', 1, 'int'),
  new BinStruct('endDate', 1, 'int'),
  new BinStruct('endDayOfWeek', 1, 'int'),
  new BinStruct('endTime', 1, 'int'),
  new BinStruct('padding', 7, 'padding'),
];

const GEN_1_DST_STRUCT = [
  // Gen 1 Daylight Saving Settings //
  new BinStruct('timeZoneId', 2, 'int'),
  new BinStruct('timeZoneOffset', 2, 'int'),
  new BinStruct('ruleVersion', 2, 'int'),
  new BinStruct('startMonth', 1, 'int'),
  new BinStruct('startRule', 1, 'int'),
  new BinStruct('startDate', 1, 'int'),
  new BinStruct('startDayOfWeek', 1, 'int'),
  new BinStruct('startTime', 1, 'int'),
  new BinStruct('adjustment', 1, 'int'),
  new BinStruct('endMonth', 1, 'int'),
  new BinStruct('endRule', 1, 'int'),
  new BinStruct('endDate', 1, 'int'),
  new BinStruct('endDayOfWeek', 1, 'int'),
  new BinStruct('endTime', 1, 'int'),
  new BinStruct('padding', 1, 'padding'),
];

const EVENT_COUNT_STRUCT = [
  // Events Count //
  new BinStruct('eventCount', 2, 'int'),
  new BinStruct('padding', 2, 'padding'),
];

const GEN_1_EVENT_COUNT_STRUCT = [
  // Gen 1 Events Count //
  new BinStruct('eventCount', 1, 'int'),
  new BinStruct('padding', 1, 'padding'),
];

const EVENT_SCHEDULE_STRUCT = [
  // Event Schedule //
  new BinStruct('eventStartTime', 2, 'int'),
  new BinStruct('weeklyRepeat', 1, 'boolean'),
  new BinStruct('groupId', 1, 'int'),
];

const GEN_1_EVENT_SCHEDULE_STRUCT = [
  // Gen 1 Event Schedule //
  new BinStruct('eventStartTime', 2, 'int'),
  new BinStruct('eventEndTime', 2, 'int'),
  new BinStruct('weeklyRepeat', 1, 'boolean'),
  new BinStruct('eventId', 1, 'int'),
];

const EC_EVENT_SETTINGS_STRUCT = [
  // Event Settings //
  new BinStruct('fnspKey', 4, 'string'),
  new BinStruct('fnspVal', 4, 'string'),
  new BinStruct('osonKey', 4, 'string'),
  new BinStruct('osonVal', 4, 'string'),
  new BinStruct('nmodKey', 4, 'string'),
  new BinStruct('nmodVal', 4, 'string'),
  new BinStruct('ffocKey', 4, 'string'),
  new BinStruct('ffocVal', 4, 'string'),
  new BinStruct('hmaxKey', 4, 'string'),
  new BinStruct('hmaxVal', 4, 'string'),
  new BinStruct('hmodKey', 4, 'string'),
  new BinStruct('hmodVal', 4, 'string'),
  new BinStruct('fpwrKey', 4, 'string'),
  new BinStruct('fpwrVal', 4, 'string'),
  new BinStruct('fdirKey', 4, 'string'),
  new BinStruct('fdirVal', 4, 'string'),
  new BinStruct('autoKey', 4, 'string'),
  new BinStruct('autoVal', 4, 'string'),
  new BinStruct('osalKey', 4, 'string'),
  new BinStruct('osalVal', 4, 'string'),
  new BinStruct('osauKey', 4, 'string'),
  new BinStruct('osauVal', 4, 'string'),
  new BinStruct('ancpKey', 4, 'string'),
  new BinStruct('ancpVal', 4, 'string'),
  new BinStruct('spare1Key', 4, 'string'),
  new BinStruct('spare1Val', 4, 'string'),
  new BinStruct('spare2Key', 4, 'string'),
  new BinStruct('spare2Val', 4, 'string'),
  new BinStruct('spare3Key', 4, 'string'),
  new BinStruct('spare3Val', 4, 'string')

];

const GEN_1_EC_EVENT_SETTINGS_STRUCT = [
  // Gen 1 EC Event Settings //
  new BinStruct('fmodKey', 4, 'string'),
  new BinStruct('fmodVal', 4, 'string'),
  new BinStruct('fnspKey', 4, 'string'),
  new BinStruct('fnspVal', 4, 'string'),
  new BinStruct('osonKey', 4, 'string'),
  new BinStruct('osonVal', 4, 'string'),
  new BinStruct('nmodKey', 4, 'string'),
  new BinStruct('nmodVal', 4, 'string'),
  new BinStruct('ffocKey', 4, 'string'),
  new BinStruct('ffocVal', 4, 'string'),
  new BinStruct('hmaxKey', 4, 'string'),
  new BinStruct('hmaxVal', 4, 'string'),
  new BinStruct('hmodKey', 4, 'string'),
  new BinStruct('hmodVal', 4, 'string'),
  new BinStruct('sltmKey', 4, 'string'),
  new BinStruct('sltmVal', 4, 'string'),
  new BinStruct('spare1Key', 4, 'string'),
  new BinStruct('spare1Val', 4, 'string'),
  new BinStruct('spare2Key', 4, 'string'),
  new BinStruct('spare2Val', 4, 'string')
];

const LIGHT_EVENT_SETTINGS_STRUCT = [
  // Event Settings //
  new BinStruct('etKey', 2, 'string'),
  new BinStruct('etVal', 2, 'string')
];

class ScheduleBinDeSerializer {

  /**
   * Creates an instance of ScheduleBinDeSerializer
   * @param {Buffer} buf Schedule bin to deserialize
   */
  constructor(buf) {
    this.buf = buf;
    this.ptr = 0;
    this.output = {};
  }

  /**
   * Return a slice of bytes and move the cursor by the same number
   *
   * @param {number} numBytes number of bytes to read
   * @returns {Buffer} slice of buffer
   */
  read(numBytes) {
    const slice = this.buf.slice(this.ptr, this.ptr + numBytes);
    this.ptr += numBytes;

    return slice;
  }

  dumpSection(sectionStruct) {
    const arr = [];
    sectionStruct.forEach((section) => {
      if (section.type === 'padding') {
        this.read(section.numBytes);
        return;
      }

      const slice = this.read(section.numBytes);

      let data;
      switch (section.type) {
        case 'int':
          data = slice.readUIntLE(0, slice.byteLength);
          break;
        case 'string':
          data = slice.toString('ascii');
          break;
        case 'boolean':
          data = slice.readUIntLE(0, 1) === 1;
          break;
        default:
          throw new Error(`Unrecognized type ${section.type} encountered`);
      }

      arr.push({ key: section.name, value: data });
    });
    return arr;
  }

  dumpHeader(productType) {
    let headers;
    if (productType === '475') {
      headers = this.dumpSection(GEN_1_HEADER_STRUCT);
    } else {
      headers = this.dumpSection(HEADER_STRUCT);
    }
    headers.forEach((x) => {
      this.output[x.key] = x.value;
    });
  }

  dumpScheduleTime() {
    const [year, month, day, hour, min, sec] = this.dumpSection(SCHEDULE_TIME_STRUCT).map(x => x.value.toString().padStart(2, '0'));
    this.output.scheduleUpdateTime = `${year}-${month}-${day}T${hour}:${min}:${sec}`;
  }

  dumpDST(productType) {
    let dst;
    if (productType === '475') {
      dst = this.dumpSection(GEN_1_DST_STRUCT);
    } else {
      dst = this.dumpSection(DST_STRUCT);
    }
    this.output.dst = {};
    dst.forEach((x) => {
      this.output.dst[x.key] = x.value;
    });
  }

  dumpEventCount(productType) {
    let eventCount;
    if (productType === '475') {
      eventCount = this.dumpSection(GEN_1_EVENT_COUNT_STRUCT);
    } else {
      eventCount = this.dumpSection(EVENT_COUNT_STRUCT);
    }
    eventCount.forEach((x) => {
      this.output[x.key] = x.value;
    });
  }

  dumpIndivEventSchedule(productType) {
    const obj = {};
    let eventSchedule;
    if (productType === '475') {
      eventSchedule = this.dumpSection(GEN_1_EVENT_SCHEDULE_STRUCT);
    } else {
      eventSchedule = this.dumpSection(EVENT_SCHEDULE_STRUCT);
    }
    eventSchedule.forEach((x) => {
      obj[x.key] = x.value;
    });
    return obj;
  }

  dumpIndivEventSettings(productType) {
    const obj = {};
    let eventSettings;
    switch (productType) {
      case '552':
        eventSettings = this.dumpSection(LIGHT_EVENT_SETTINGS_STRUCT)
          .filter(s => s.value !== '\u0000\u0000');
        break;
      case '475':
        eventSettings = this.dumpSection(GEN_1_EC_EVENT_SETTINGS_STRUCT)
          .filter(s => s.value !== '\u0000\u0000\u0000\u0000');
        break;
      default:
        eventSettings = this.dumpSection(EC_EVENT_SETTINGS_STRUCT)
          .filter(s => s.value !== '\u0000\u0000\u0000\u0000');
        break;
    }

    for (let i = 0; i < eventSettings.length; i += 2) {
      obj[eventSettings[i].value] = eventSettings[i + 1].value;
    }
    return obj;
  }

  /**
   * Deserialize into a JSON representation of the schedule bin file
   */
  deserialize(productType = '520') {
    this.dumpHeader(productType);
    this.dumpScheduleTime();
    this.dumpDST(productType);
    this.dumpEventCount(productType);

    this.output.events = [];
    for (let n = 0; n < this.output.eventCount; n += 1) {
      const event = Object.assign(this.dumpIndivEventSchedule(productType),
        this.dumpIndivEventSettings(productType));
      this.output.events.push(event);
    }
    return this.output;
  }
}


module.exports = ScheduleBinDeSerializer;
