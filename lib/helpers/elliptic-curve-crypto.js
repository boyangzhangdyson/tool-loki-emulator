const crypto = require('crypto');
const aesjs = require('aes-js');
const curve = require('curve25519-n');

class ECCrypto {

  constructor({ privateKey, publicKey }) {
    this.privateKey = privateKey;
    this.publicKey = publicKey;
    this.payload = [];
    this.hashLength = 32;
  }

  encrypt(
    message = null,
    encoding = 'hex',
    preamble = Buffer.from([0x01, 0x00])
  ) {
    const payload = [];

    const ephemPriv = crypto.randomBytes(32);
    curve.makeSecretKey(ephemPriv);

    const ephemPub = curve.derivePublicKey(ephemPriv);

    const sharedSecret = curve.deriveSharedSecret(ephemPriv, this.publicKey);
    const encKey1 = this.hkdf2(16, sharedSecret);

    const cloudChall = message
      ? Buffer.from(message, encoding)
      : crypto.randomBytes(16);
    const iv = Buffer.alloc(16, 0x00);

    // eslint-disable-next-line
    const cbcCrypto = new aesjs.ModeOfOperation.cbc(encKey1, iv);
    const encrypted = cbcCrypto.encrypt(cloudChall);

    const mac = Buffer.from(ECCrypto.sha256(encKey1, encrypted));

    if (process.env.DEBUG === 'true') {
      console.log('ENCRYPT');
      console.log(`Challenge:           ${cloudChall.toString('hex')}`);
      console.log(`Shared Secret:       ${sharedSecret.toString('hex')}`);
      console.log(`Derived Key:         ${encKey1.toString('hex')}`);
      console.log(`Ephemeral PubKey:    ${ephemPub.toString('hex')}`);
      console.log(`Ephemeral PrivKey:   ${ephemPriv.toString('hex')}`);
      console.log(`Encrypted Challenge: ${aesjs.utils.hex.fromBytes(encrypted)}`);
      console.log(`MAC:                 ${mac.toString('hex')}`);
    }

    payload.push(preamble);
    payload.push(Buffer.from(ephemPub));
    payload.push(Buffer.from(encrypted));
    payload.push(Buffer.from(mac));

    if (process.env.DEBUG === 'true') {
      console.log('');
      console.log('RESULT:');
      console.log(Buffer.concat(payload).toString(encoding));
    }

    return [
      Buffer.concat(payload).toString(encoding),
      cloudChall,
      sharedSecret
    ];
  }

  decrypt(
    message,
    encoding = 'hex',
    startBytes = 0,
    payloadBytes = 16,
    secret = ''
  ) {
    curve.makeSecretKey(this.privateKey);

    const encryptedMessage = Buffer.from(message, encoding);

    // Unpack the first 32 bytes (ECIES public key) FIRST BYTE IS MESSAGE TYPE
    const payloadEphemPublic = encryptedMessage.slice(
      startBytes,
      startBytes + 32
    );
    const payload = encryptedMessage.slice(
      startBytes + 32,
      startBytes + 32 + payloadBytes
    );
    const challengeMac = encryptedMessage.slice(
      startBytes + 32 + payloadBytes,
      startBytes + 32 + payloadBytes + 95
    );

    const sharedSecret = secret
      ? Buffer.from(secret)
      : curve.deriveSharedSecret(this.privateKey, payloadEphemPublic);

    const iv = Buffer.alloc(16, 0x00);

    const encKey = this.hkdf2(16, sharedSecret);

    // eslint-disable-next-line
    const aesCbc = new aesjs.ModeOfOperation.cbc(encKey, iv);
    const decryptedBytes = aesCbc.decrypt(payload);

    const decryptedText = aesjs.utils.hex.fromBytes(decryptedBytes);

    const mac = ECCrypto.sha256(Buffer.from(encKey), payload);

    if (mac.toString('hex') !== challengeMac.toString('hex')) {
      throw new Error('Generated MAC and Challenge MAC do not match');
    }

    if (process.env.DEBUG === 'true') {
      console.log('');
      console.log('INCOMING:');
      console.log(Buffer.from(message, encoding).toString('hex'));
    }

    if (process.env.DEBUG === 'true') {
      console.log('DECRYPT');
      console.log(`Ephem:               ${payloadEphemPublic.toString('hex')}`);
      console.log(`Payload:             ${payload.toString('hex')}`);
      console.log(`MAC (from Payload):  ${challengeMac.toString('hex')}`);
      console.log(`MAC (G/O Payload):   ${mac.toString('hex')}`);
      console.log(`Secret:              ${sharedSecret.toString('hex')}`);
      console.log(`Derived Key:         ${encKey.toString('hex')}`);
      console.log(`Decrypted:           ${decryptedText.toString()}`);
      console.log();
    }

    return Buffer.from(decryptedBytes);
  }

  // eslint-disable-next-line
  hkdf2(length, ikm, salt = Buffer.from(''), info = Buffer.from('')) {
    const prk = crypto.createHmac('sha256', salt).update(ikm).digest();
    const okm = [];

    let t = Buffer.from([]);

    const range = Math.ceil(length / prk.length);

    // eslint-disable-next-line
    for (let i = 0; i < range; i++) {
      const val = Buffer.from([1 + i]);
      const newData = Buffer.concat([t, info, val]);
      const newT = crypto.createHmac('sha256', prk).update(newData).digest();
      t = newT;
      okm.push(t);
    }

    return Buffer.concat(okm).slice(0, length);
  }

  static sha256(salt, data) {
    return crypto.createHmac('sha256', salt).update(data).digest();
  }

}

module.exports = ECCrypto;
