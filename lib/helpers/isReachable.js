const net = require('net');
const debug = require('debug')('helpers:isReachable');

exports.isReachable = (port, host) => new Promise((resolve) => {
  net.createConnection(port, host).on('connect', () => {
    resolve(true);
  }).on('error', function (err) {
    debug(`Host ${host} at port ${port} is not reachable.\n${err}`);
    resolve(false);
  });
});
