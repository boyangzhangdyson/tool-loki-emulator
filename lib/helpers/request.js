const request = require('request-promise');
const util = require('util');
const uuid = require('uuid/v4');
const genericDebug = require('debug')('helpers:request');
const advancedDebug = require('debug')('helpers:advanced-request');
const { ipv4 } = require('../env');
const globalRegistry = require('./registry.js');

function getExtendedOptions(extraOptions) {
  // Use a correlation-id in the form: lokiffff-xxxx-xxxx-xxxx-xxxxxxxxxxxx
  const baseOptions = {
    json: true,
    strictSSL: false, // TODO: We shouldn't be disabling this. Needs investigation.
    family: ipv4, // Fixes weird behavior within request-promise module.
    resolveWithFullResponse: true,
    headers: {
      'X-Dyson-CorrelationId': uuid().replace(/^(.*?)-/gi, 'lokiffff-'),
    }
  };

  // Not doing deep merge to avoid using more libraries and introduce issues
  // Merging the 'headers' property separately
  return Object.assign(
    {},
    baseOptions,
    extraOptions,
    { headers: Object.assign(baseOptions.headers, extraOptions.headers) }
  );
}

function dysonRequest(options) {
  const extendedOptions = getExtendedOptions(options);
  // Store the stack trace to provide useful context if the async request fails
  const objectWithStackTrace = {};
  Error.captureStackTrace(objectWithStackTrace);

  // Log and save request
  genericDebug(`[${extendedOptions.method}] ${extendedOptions.url || extendedOptions.uri}`);
  advancedDebug(`Making an HTTP request [${extendedOptions.method}] to ${extendedOptions.url || extendedOptions.uri} with the following options:\n${util.inspect(extendedOptions, { breakLength: 80 })}`);
  const cucumberWorld = globalRegistry.get('world', this);
  if (cucumberWorld) cucumberWorld.lastRequest = extendedOptions;

  // Log and save response
  function logAndSaveResult(result) {
    // The 'result' object can be a proper response
    // or an error containing a failed response
    // or an error containing no response
    let responseObject;
    if (result.error) {
      responseObject = result.response ? result.response : {};
    } else {
      responseObject = result;
    }

    const logResponseObject = (({ statusCode, body }) => (
      ({ statusCode, body })
    ))(responseObject);

    if (cucumberWorld) cucumberWorld.lastResponse = logResponseObject;
    advancedDebug(`Got response:\n ${util.inspect(logResponseObject, { breakLength: 80 })}`);
    return result;
  }

  // Save the response and throw a new error if failed
  // (with origin in this file to masquerade 'request' module internals)
  return request(extendedOptions)
    .then(response => logAndSaveResult(response))
    .catch((err) => {
      logAndSaveResult(err);
      const errorToRaise = new Error(err);
      errorToRaise.stack = objectWithStackTrace.stack;
      throw errorToRaise;
    });
}

module.exports = dysonRequest;
