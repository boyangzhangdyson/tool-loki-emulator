const msleep = timeoutInMs => new Promise((resolve) => { setTimeout(resolve, timeoutInMs); });

const sleep = timeoutInSeconds => msleep(timeoutInSeconds * 1000);

module.exports = {
  msleep,
  sleep
};
