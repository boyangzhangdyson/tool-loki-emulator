/* eslint-disable global-require */
module.exports = {
  skusForN223: require('./skusN223'),
  skusFor520: require('./skus520'),
  skusFor527: require('./skus527'),
  skusFor276: require('./skus276'),
  skusFor552: require('./skus552'),
  skusFor475: require('./skus475'),
  skusFor358: require('./skus358')
};
