const util = require('util');
const awsIot = require('aws-iot-device-sdk');
const mqtt = require('mqtt');
const debug = require('debug')('lib:user');
const assert = require('assert');
const machineDefinitions = require('../machine/machineDefinitions');
const { getserverCertificates } = require('./lib/certificates/certificates');
const lecReconnection = require('../helpers/lecReconnection');
const { poll } = require('../../lib/helpers/poll');
const {
  messageProcessor,
  userRegistration,
  assets,
  mapVisualizer,
  manifest,
  iotAppAccessEngine,
  lecProvisioning,
  cleanMaps
} = require('../apis');

class User {
  constructor(userOptions = {
    account: undefined,
    email: undefined,
    password: undefined,
    passwordHash: undefined,
    country: undefined,
    locale: undefined,
    firstName: undefined,
    lastName: undefined,
    honorific: undefined,
    contactChoice: undefined,
    linkAppVersion: undefined,
  }) {
    Object.keys(userOptions).forEach((key) => {
      assert.notEqual(userOptions[key], undefined);
      this[key] = userOptions[key];
    });
    this.machines = [];

    // If existing user, assume it is logged in by default
    if (this.account) {
      debug(`User with email ${this.email} seems to have an account already assigned, so the user is already registered. Generating authToken so the user is logged in by default`);
      this.authToken = `Basic ${Buffer.from(`${this.account}:${this.passwordHash}`).toString('base64')}`;
    }
  }

  register() {
    return userRegistration.registerUser(this)
      .then((res) => {
        this.account = res.body.Account;
        this.passwordHash = res.body.Password;
        this.authToken = res.headers.authorization;

        debug(`New User Created ${util.inspect(JSON.stringify(res), false, null)}\n`);
        return res;
      });
  }

  signIn() {
    return userRegistration.authenticateUser(this)
      .then((res) => {
        this.account = res.body.Account;
        this.passwordHash = res.body.Password;
        this.authToken = res.headers.authorization;

        debug(`Existing User is successfully Verified ${util.inspect(JSON.stringify(res), false, null)}\n`);
        return res;
      });

  }

  addToMachinesList(machine) {
    this.machines.push(machine);
  }

  async useMachine(machine, autoUpdate = true) {
    if (!this.machines.includes(machine)) {
      this.addToMachinesList(machine);
    }
    // Give the machine a default name as it happens in real world
    await poll(
      () => manifest.updateDevice(this, machine.serial, 'Loki Test Machine', autoUpdate),
      10000,
      res => res.statusCode === 204
    );
    this.currentMachine = machine;
    return this.connect();
  }

  readConnectionStatus(machine = this.currentMachine) {
    return messageProcessor.connectionStatus(this, machine.serial)
      .then((res) => {
        debug(`Connection status response: ${util.inspect(JSON.stringify(res.body), false, null)}\n`);
        return Promise.resolve(res.body);
      })
      .catch((err) => {
        debug(`Error reading connection status: ${util.inspect(JSON.stringify(err), false, null)}\n`);
        return Promise.reject(err);
      });
  }

  verifyMachineAssociation(machine) {
    this.apiVersion = (this.linkAppVersion === '4.0') ? 'v1' : 'v2';
    return manifest.manifest(this.apiVersion, this.account, this.passwordHash)
      .then((res) => {
        debug(`User manifest query response:  ${util.inspect(JSON.stringify(res.body), false, null)}\n`);
        if (!res.body) {
          return Promise.reject(new Error('Empty HTTP body from Manifest API'));
        }
        let matched = false;
        matched = Boolean(res.body.find(
          item => item.Serial === machine.serial
            && (item.Active === true || item.Active === undefined)
        ));
        return Promise.resolve(matched);
      })
      .catch((err) => {
        debug(`Error getting user manifest: ${util.inspect(JSON.stringify(err), false, null)}\n`);
        return Promise.reject(err);
      });
  }

  async removeOwnership(machine = this.currentMachine) {
    await manifest.removeManifest(this, machine.serial);
  }

  iotRoleCredentials(machine) {
    return iotAppAccessEngine.iotRoleCredentials(machine.serial, this.account, this.passwordHash)
      .then((res) => {
        debug('Got App Access Engine role credentials for AWS Iot\n');
        return res.body;
      })
      .catch((err) => {
        debug(`Error obtaining App access engine role credentials: ${util.inspect(JSON.stringify(err), false, null)}\n`);
        return Promise.reject(err);
      });
  }

  readCleanHistory(machine = this.currentMachine) {
    return assets.cleanHistory(this.account, this.passwordHash, machine.serial, this.locale)
      .then((res) => {
        debug(`Clean history Response: ${util.inspect(JSON.stringify(res.body), false, null)}\n`);
        return res.body;
      })
      .catch((err) => {
        debug(`Error reading Clean history: ${util.inspect(JSON.stringify(err), false, null)}\n`);
        return Promise.reject(err);
      });
  }

  readCleanMap(machine = this.currentMachine, cleanId) {
    let cleanDetails;
    // Multiple versions of read clean map support (futuristic) in the app.
    // Defaults to read from map vis service.
    if (this.linkAppVersion === '4.5') {
      cleanDetails = cleanMaps.readCleanMaps(this, machine.serial)
        .then((res) => {
          debug(`Received a Maps Response ${util.inspect(JSON.stringify(res.body), false, null)}`);
          return res.body;
        })
        .catch((err) => {
          debug(`Error reading Clean Map: ${util.inspect(JSON.stringify(err), false, null)}\n`);
          return Promise.reject(err);
        });
    } else {
      cleanDetails = mapVisualizer.cleanMap(this.account,
        this.passwordHash,
        machine.serial,
        cleanId)
        .then((res) => {
          debug('Received a Map Response (png)');
          return res.body;
        })
        .catch((err) => {
          debug(`Error reading Clean Map: ${util.inspect(JSON.stringify(err), false, null)}\n`);
          return err;
        });

    }
    return cleanDetails;
  }

  connect(machine = this.currentMachine) {
    this.apiVersion = (this.linkAppVersion === '4.0') ? 'v1' : 'v2';
    return manifest.manifest(this.apiVersion, this.account, this.passwordHash)
      .then((res) => {
        const userManifest = res.body;
        const matchingManifest = userManifest.find(element => element.Serial === machine.serial);
        debug(`matchingManifest = ${util.inspect(JSON.stringify(matchingManifest), false, null)}`);

        if (!matchingManifest) {
          return Promise.reject(new Error(`Trying to connect to Machine ${machine.serial} which is not present in user manifest `));
        }

        let connectionType = matchingManifest.ConnectionType;
        // If the answer came from v1, there is no connectionType
        // Guess it from product type
        if (!connectionType) {
          connectionType = machineDefinitions.get(m => m.type === matchingManifest.ProductType).provisioningType === 'awsIot' ? 'wss' : 'mqtts';
        }

        if (connectionType === 'wss') {
          return this.iotRoleCredentials(machine)
            .then((appAccessEngineCredentials) => {
              if (!appAccessEngineCredentials.endpoint) {
                throw new Error('Obtaining App access engine role credentials failed - no endpoint returned');
              }
              debug(`AWS IoT App Access Credentials Response = ${util.inspect(JSON.stringify(appAccessEngineCredentials), false, null)}`);
              return new Promise((resolve, reject) => {
                this.mqttClient = awsIot.device({
                  host: appAccessEngineCredentials.endpoint,
                  clientId: `Loki-App-${Math.floor((Math.random() * 100000000) % 100000)}`, // Unique clientId to support concurrent broker connection
                  protocol: 'wss',
                  accessKeyId: appAccessEngineCredentials.iamCredentials.accessKeyId,
                  secretKey: appAccessEngineCredentials.iamCredentials.secretAccessKey,
                  sessionToken: appAccessEngineCredentials.iamCredentials.sessionToken
                });

                this.mqttClient.on('connect', () => resolve(this));
                this.mqttClient.on('error', () => reject(new Error('Aws IoT MQTT Connection Failed.')));
                this.mqttClient.on('offline', () => reject(new Error('Aws IoT MQTT Connection disconnected. Issues in client end.')));
              });
            })
            .catch((err) => {
              debug(`Error reading iot app access credentials: ${util.inspect(err, false, null)}\n`);
              return Promise.reject(err);
            });
        } if (connectionType === 'mqtts') {

          return this.readConnectionStatus(machine)
            .then(connectionResponse => new Promise((resolve, reject) => {
              const brokerConnectionTimeoutInSec = 30;
              const mqttOptions = {
                rejectUnauthorized: false,
                keepalive: 60,
                protocolId: 'MQIsdp',
                protocolVersion: 3,
                reconnectPeriod: 1000,
                clean: true,
                protocol: 'mqtts',
                secureProtocol: 'TLSv1_2_client_method',
                ciphers: 'AES256-SHA256',
                ca: getserverCertificates().ca.link,
                clientId: `Loki-App-${Math.floor((Math.random() * 100000000) % 100000)}`, // Unique clientId to support concurrent broker connection
                //TODO: dev004 don't have appbroker1.cpdev004.dyson.com
                host: connectionResponse.BrokerHostName.includes('dev004') ? 'broker1.cpdev004.dyson.com' : connectionResponse.BrokerHostName,
                port: connectionResponse.BrokerPort,
                username: this.account,
                password: this.passwordHash,
              };
              debug(`RabbitMq MQTT client in App connecting with ${JSON.stringify(mqttOptions)}`);
              this.mqttClient = mqtt.connect(mqttOptions);

              setTimeout(function () {
                return reject(new Error(`Broker connection attempt timed out in App after ${brokerConnectionTimeoutInSec} ms`));
              }, brokerConnectionTimeoutInSec * 1000);

              this.mqttClient.on('connect', () => resolve(this));

              this.mqttClient.on('error', (error) => {
                debug(`RabbitMq MQTT client in App received error: ${error}`);
                return reject(new Error('RabbitMq Mqtt Connection Failed in App.'));
              });

              this.mqttClient.on('offline', () => {
                debug('RabbitMq MQTT client connection went offline in App.');
              });

              this.mqttClient.on('end', () => {
                debug('RabbitMq MQTT client connection ended in App.');
              });

              this.mqttClient.on('close', () => {
                debug('RabbitMq MQTT client connection closed in App.');
                return reject(new Error('App Disconnected from broker.'));
              });

            }))
            .catch((err) => {
              debug(`Error reading connection status: ${util.inspect(err, false, null)}\n`);
              return Promise.reject(err);
            });
        }
        return Promise.reject(new Error(`Unknown connectionType '${connectionType}' received in user manifest for ${machine.serial}. Hence not able to choose broker connection type in the App`));

      })
      .catch((err) => {
        debug(`Error reading user manifest while determining app broker connection type : ${util.inspect(JSON.stringify(err), false, null)}\n`);
        return Promise.reject(err);
      });
  }

  async publish(messageFn, identity = this) {
    const {
      topic,
      payload
    } = messageFn(identity);
    // If publishing a message on behalf the app
    // Ensure the machine is connected before publishing anything
    // Wait if necessary for it to come online
    // TODO: Compare this to the app's behavior
    if (identity === this) {
      const readConnectionStatus = () => this.readConnectionStatus(this.currentMachine);
      const additionalSuccessConditions = connectionStatus => connectionStatus.Status === 'connected';
      const reconnectionTimeoutInSeconds = 5;
      try {
        await poll(
          readConnectionStatus,
          reconnectionTimeoutInSeconds * 1000,
          additionalSuccessConditions
        );
      } catch (err) {
        throw new Error('Refused to publish message because machine is not connected.');
      }
    }
    await new Promise((resolve, reject) => {
      if (!this.mqttClient) {
        debug(`The identity that tried to publish message before a connection is established is : ${identity}`);
        reject(new Error(`App tries to publish message to operate machine ${this.currentMachine} before connection to Remote Broker is established`));
      }
      this.mqttClient.publish(topic, JSON.stringify(payload), (err) => {
        if (err) { reject(err); } else { debug('Message published successfully'); resolve(); }
      });
    });
  }

  takeOwnership(machine = this.currentMachine) {
    return userRegistration.claimOwnership(
      this.account,
      this.passwordHash,
      machine.serial,
      this.country
    )
      .then((res) => {
        debug(`POST claim Ownership Response: ${util.inspect(JSON.stringify(res.body), false, null)}\n`);
        return res.body;
      })
      .catch((err) => {
        debug(`Irrespective of error response: ${util.inspect(JSON.stringify(err), false, null)} User successfully claimed ownership of the Machine`);
        debug('** This is a tactical change introduced in cloud services to acheive GDPR compliance.');
        // Cloud always completes ownership journey sucessfully, irrespective of sitecore response.
        // This is a tactical change in Provisioning service Web API as per KCK-19593/KCK-19591
        return Promise.resolve();
      });
  }

  async lecProvision(machine, pairingToken) {
    try {
      return await lecProvisioning.provision(pairingToken, machine.serial, this);
    } catch (err) {
      throw new Error(`Error calling LEC Provisioning, error response: ${util.inspect(JSON.stringify(err), false, null)}`);
    }
  }

  lecReconnect(machine) {
    this.reverifyPayload2 = machine.userReconnectNotKnown();
  }

  async lecReverify(machine) {
    this.reverifyPayload3 = await lecReconnection.reverify(this, machine, this.reverifyPayload2);
  }

  lecValidateReverify(machine) {
    machine.validateUserReverification(this.reverifyPayload3);
  }

  readManifest(machine) {
    this.apiVersion = (this.linkAppVersion === '4.0') ? 'v1' : 'v2';
    return manifest.manifestForSerial(
      this.apiVersion,
      this,
      machine.serial
    );
  }

  async renameMachine(machine, newMachineName) {
    await manifest.updateDevice(this, machine.serial, newMachineName);
  }

  mandatoryUpdate(machine = this.currentMachine, version) {
    return assets.mandatoryUpdate(this.account, this.passwordHash, machine.serial, version)
      .then((res) => {
        debug(`Get mandatory Upgrade Response: ${util.inspect(JSON.stringify(res.body), false, null)}\n`);
        return res;
      })
      .catch((err) => {
        debug(`Error reading Get mandatory Upgrade: ${util.inspect(JSON.stringify(err), false, null)}\n`);
        return Promise.reject(err);
      });
  }

  getPendingRelease(machine = this.currentMachine) {
    return assets.getPendingRelease(this.account, this.passwordHash, machine.serial)
      .then((res) => {
        debug(`Check manual firmware upgrade : ${util.inspect(JSON.stringify(res.body), false, null)}\n`);
        return res;
      })
      .catch((err) => {
        debug(`Error reading manual firmware upgrade: ${util.inspect(JSON.stringify(err), false, null)}\n`);
        return Promise.reject(err);
      });
  }

  setPendingRelease(machine = this.currentMachine) {
    return assets.setPendingRelease(this.account, this.passwordHash, machine.serial)
      .then((res) => {
        debug(`Post manual firmware upgrade : ${util.inspect(JSON.stringify(res.body), false, null)}\n`);
        return res;
      })
      .catch((err) => {
        debug(`Error reading manual firmware upgrade: ${util.inspect(JSON.stringify(err), false, null)}\n`);
        return Promise.reject(err);
      });
  }

  getCurrentRelease(machine = this.currentMachine) {
    return assets.getCurrentRelease(this.account, this.passwordHash, machine.serial)
      .then((res) => {
        debug(`Check current release version : ${util.inspect(JSON.stringify(res.body), false, null)}\n`);
        return res;
      })
      .catch((err) => {
        debug(`Error reading current release version: ${util.inspect(JSON.stringify(err), false, null)}\n`);
        return Promise.reject(err);
      });
  }
}

module.exports = User;
