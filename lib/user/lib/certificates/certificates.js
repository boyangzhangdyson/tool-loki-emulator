const fs = require('fs');
const path = require('path');
const { env } = require('../../../env');

const pathToCerts = path.join(__dirname, '..', '..', '..', '..', 'certs');

const getserverCertificates = () => {
  const pkiGrade = env.getCurrentEnvironment().cloud.grade;
  const serverCertRootCaAbsPathForIot = `${pathToCerts}/server-certs/cp.root.ca/${pkiGrade}/amazonCA/AWS_ROOTCA.crt.pem`;
  const serverCertRootCaAbsPathForLink = `${pathToCerts}/server-certs/cp.root.ca/${pkiGrade}/dysonCA/DysonConnectedProductsRootCA.crt`;
  return {
    ca: {
      link: fs.readFileSync(`${serverCertRootCaAbsPathForLink}`),
      awsIot: fs.readFileSync(`${serverCertRootCaAbsPathForIot}`)
    }
  };
};

module.exports = {
  getserverCertificates,
};
