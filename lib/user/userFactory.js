const User = require('./user');
const { env } = require('../env');
const existingUsers = require('../../testData').users;
const { poll } = require('../../lib/helpers/poll');

const usersStore = [];
const ordinalMap = ['first', 'second', 'third', 'fourth', 'fifth'];

const create = async ({
  type = 'existing',
  countryCode = env.cloud.linkEnv.split('.')[0] === 'row' ? 'GB' : 'CN',
  ordinal = 'first',
  linkAppVersion = '4.0'
}) => {
  const baseOptions = {
    country: 'GB',
    email: `LokiUser${Math.floor(Math.random() * 100000)}_${Math.floor(Math.random() * 10000000)}@dyson.com`,
    password: 'qwerty123',
    locale: 'en-GB',
    firstName: 'Loki',
    lastName: 'Odinson',
    honorific: 'Mr',
    contactChoice: false,
    linkAppVersion,
  };
  let user = null;

  if (type === 'new') {
    // Quick check for TEST_DATA_MODE
    // Fail if no new users are meant to be created
    if (process.env.TEST_DATA_MODE === 'existing') throw new Error('No new users are meant to be created because TEST_DATA_MODE=existing');

    user = new User(baseOptions);
  } else {
    // Throw error if there is already an user in the specified place
    if (usersStore[ordinalMap.indexOf(ordinal)]) {
      throw new Error(`A '${ordinal}' user has been already defined in this test`);
    }

    // Throw error if we don't have existing users for the given environment
    if (!existingUsers[env.cloud.linkEnv]) {
      throw new Error(`Trying to use an existing user but there are no existing users configured for environment '${env.cloud.linkEnv}' (at /testUsers.json)`);
    }

    // Get a random user from the list of available users
    // distinct than users stored in the userStore
    const nonRepeatingUserConditions = pickedUser => !usersStore.map(u => u.email)
      .includes(pickedUser.email);
    const filteredExistingUsers = existingUsers[env.cloud.linkEnv]
      .filter(u => u.country === countryCode);

    // Throw error if we don't have a user defined for this environment and country
    if (filteredExistingUsers.length === 0) {
      throw new Error(`No existing users were found for environment '${env.cloud.linkEnv}' and country '${countryCode}' (at /testUsers.json)`);
    }

    const existingUser = await poll(
      () => {
        //TODO:
        // const randomPositionUser = Math.floor(
        //   (Math.random() * 10000) % filteredExistingUsers.length
        // );
        const randomPositionUser = 0;
        return Promise.resolve(filteredExistingUsers[randomPositionUser]);
      },
      30 * 1000,
      nonRepeatingUserConditions
    );

    const existingUserOptions = Object.assign({}, baseOptions, existingUser);
    user = new User(existingUserOptions);
  }

  usersStore[ordinalMap.indexOf(ordinal)] = user;
  return user;
};

const getUser = (ordinal) => {
  const place = ordinal || 'first';
  if (!usersStore[ordinalMap.indexOf(place)]) throw new Error(`Cannot find ${place} user`);
  return usersStore[ordinalMap.indexOf(place)];
};

const cleanUsers = () => { usersStore.length = 0; };
module.exports = {
  create,
  getUser,
  cleanUsers,
};
