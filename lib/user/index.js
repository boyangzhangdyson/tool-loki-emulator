const User = require('./user');
const userFactory = require('./userFactory');

module.exports = {
  User,
  userFactory
};
