const fs = require('fs');
const path = require('path');
const debug = require('debug')('lib:machineFactory');

const { env } = require('../env');
const machineDefinitions = require('./machineDefinitions');
const Machine = require('./machine');
const CapabilitiesRegistry = require('./CapabilitiesRegistry');
const provisioningFunctionSelector = require('./lib/provisioning');
const mqttClientSelector = require('./lib/mqtt');
const { getCertificates, getMachineCertificateGroup } = require('./lib/certificates/certificates');
const config = require('../../config.json');
const pkiHelper = require('../helpers/pki');
const globalRegistry = require('../../lib/helpers/registry');

const getTopicsWithTypeAndSerial = ({ type, topics }, serial) => {
  const machineTopics = Object.assign({}, topics);

  Object.keys(machineTopics).forEach((topic) => {
    machineTopics[topic] = `${type}/${serial}/${machineTopics[topic]}`;
  });
  return machineTopics;
};


const create = async (matcherFn, isOtaMachine = false) => {
  const machineDefinition = machineDefinitions.get(matcherFn);
  if (machineDefinition === undefined) {
    return Promise.reject(new Error('No Machine Definition exists for MachineType. Check /lib/machine/machineDefinitions.js'));
  }

  const certificatePath = path.resolve(__dirname, '../../certs/client-certs/', env.cloud.grade, getMachineCertificateGroup(isOtaMachine));
  const serialMatcher = new RegExp(`^${machineDefinition.skus.join('|')}`);
  const validSerials = fs.readdirSync(certificatePath)
    .filter(item => serialMatcher.test(item)) // serial is a valid machine sku
    .map(item => `${item.split('.')[0].split('_')[0]}`) // remove extension and optional type after underscore
    .filter((item, index, self) => self.indexOf(item) === index); // distinct

  if (validSerials.length === 0) return Promise.reject(new Error(`Could not find a machine of required type '${machineDefinition.type}' in the certs folder`));
  //TODO:
  // const randomIndex = Math.floor(Math.random() * validSerials.length);
  const randomIndex = 0;
  const serial = validSerials[randomIndex];
  const gradedSerial = `${serial}A`;

  // TODO: Should this object just extend machineDefinition?
  const machineIdentity = {
    serial: gradedSerial,
    ungradedSerial: serial,
    type: machineDefinition.type,
    provisioningType: machineDefinition.provisioningType,
    connectionJourneyType: machineDefinition.connectionJourneyType,
    topics: getTopicsWithTypeAndSerial(machineDefinition, gradedSerial),
    certificates: getCertificates(
      serial, machineDefinition.connectionJourneyType,
      machineDefinition.provisioningType, isOtaMachine
    )
  };

  // Assign capabilities based on the machineDefinition
  const capabilitiesRegistry = new CapabilitiesRegistry(machineIdentity);
  capabilitiesRegistry.add('mqttClient', mqttClientSelector.get(machineDefinition.provisioningType));
  capabilitiesRegistry.add('provisioningFunction', provisioningFunctionSelector.get(machineDefinition.provisioningType));

  // Append capabilitiesRegistry to the machineIdentity
  machineIdentity.capabilitiesRegistry = capabilitiesRegistry;

  const newMachine = await new Machine(machineIdentity, capabilitiesRegistry);

  // If the product is LEC capable, try to update the LEC keys so we can talk to the Cloud
  // If the "update keys" action fails, carry on expecting keys are fine
  // Only update the keys in non production environments
  if (!globalRegistry.get('disable_ingest')
      && ['lecHec', 'lecOnly'].includes(machineIdentity.connectionJourneyType)
      && env.cloud.environmentName !== 'prod') {
    try {
      await pkiHelper.updateLecKeys(machineIdentity.ungradedSerial, config.lecKeys);
    } catch (err) {
      console.log(`There was an error updating LEC keys for machine '${machineIdentity.ungradedSerial}'.\nExecution will continue but keys might be wrong in the cloud side`);
      debug(err);
    }
  }

  return newMachine;
};

module.exports = {
  create
};
