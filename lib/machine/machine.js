const crypto = require('crypto');
const LecCrypto = require('../helpers/elliptic-curve-crypto');
const config = require('../../config.json');

class Machine {

  constructor(machineIdentity, capabilitiesRegistry) {
    this.machineIdentity = machineIdentity;
    this.capabilitiesRegistry = capabilitiesRegistry;

    this.serial = this.machineIdentity.serial;
    this.ungradedSerial = this.machineIdentity.ungradedSerial;
    this.type = machineIdentity.type;
    this.topics = machineIdentity.topics;

    if (this.machineIdentity.connectionJourneyType === 'lecOnly') {
      this.machineCryptoInstance = new LecCrypto({
        privateKey: Buffer.from(config.lecKeys.MachinePrivateKey, 'hex'),
        publicKey: Buffer.from(config.lecKeys.CloudPublicKey, 'hex')
      });
    }
  }

  updateCapabilities(newCapabilities) {
    Object.keys(newCapabilities).forEach((capabilityName) => {
      this.capabilitiesRegistry.add(capabilityName, newCapabilities[capabilityName]);
    });
  }

  provision() {
    let payload;
    // Machines expected to be upgraded via OTA from rabbitMQ to AWS Iot will send
    // additional payload on provision to help diagnose provisioning issues
    if (['438', '520'].includes(this.type)) {
      payload = {
        firmwareVersion: 'END-TO-END-TESTS',
        brokerConnectionAttempts: 1,
        lastAttemptedBrokerUrl: 'ignoreme.com',
        lastSuccessfulBrokerUrl: 'ignoreme.com'
      };
    }
    return this.capabilitiesRegistry.get('provisioningFunction').provision(this.machineIdentity.serial, this.machineIdentity.certificates, payload)
      .then((response) => {
        this.connection = response.body;
        return this;
      });
  }

  connect(timeout) {
    this.mqttClient = this.capabilitiesRegistry.get('mqttClient');
    return this.mqttClient.connect(this.connection, timeout);
  }

  disconnect() {
    return this.mqttClient && this.mqttClient.connected
      ? this.mqttClient.disconnect()
      : Promise.resolve();
  }

  publish(messageFn) {
    const {
      topic,
      payload
    } = messageFn(this.machineIdentity);
    return this.mqttClient.publish(topic, payload);
  }

  subscribe(topic = this.machineIdentity.topics.command) {
    return this.mqttClient.subscribe(topic);
  }

  addMessageListener(handler) {
    this.mqttClient.addListener('message', handler);
  }

  hasReceivedMessage(messageFn) {
    return this.getReceivedMessage(messageFn) !== undefined;
  }

  getReceivedMessage(messageFn) {
    return this.mqttClient.getReceivedMessage(messageFn);
  }

  getAllReceivedMessages(messageFn) {
    return this.mqttClient.getAllReceivedMessages(messageFn);
  }

  getAllReceivedMessagesForTopic(topic = this.machineIdentity.topics.command) {
    return this.mqttClient.getAllReceivedMessagesForTopic(topic);
  }

  clearAllReceivedMessages() {
    return this.mqttClient.clearAllReceivedMessages();
  }

  run(scenario) {
    return scenario(this);
  }

  userReconnectNotKnown() {
    if (this.machineIdentity.connectionJourneyType !== 'lecOnly') {
      throw new Error('Machine is not LEC-only - the user cannot reconnect');
    }

    // Generate Special Payload 2 to send to user to specify user
    // not found and reverification required
    const payloadPrefix = Buffer.from('00000000000000000000000000000000', 'hex');
    this.machineRandomNumber = crypto.randomBytes(16);
    const payload2Contents = Buffer.concat([payloadPrefix, this.machineRandomNumber]).toString('hex');
    const [payload2] = this.machineCryptoInstance.encrypt(payload2Contents, 'hex', Buffer.from([0x00, 0x00]));
    return payload2;
  }

  validateUserReverification(payload3) {
    if (this.machineIdentity.connectionJourneyType !== 'lecOnly') {
      throw new Error('Machine is not LEC-only - the machine cannot validate reverification');
    }

    // Validate Reverify Payload 3
    const decryptedPayload3 = this.machineCryptoInstance.decrypt(payload3, 'hex', 2, 32);
    const returnedChallengeRandomNumber = decryptedPayload3.slice(0, 16);

    // Quick check on the challenge
    if (returnedChallengeRandomNumber.toString('hex') !== this.machineRandomNumber.toString('hex')) {
      throw new Error('Machine Reverification challenge did not match');
    }
  }

}

module.exports = Machine;
