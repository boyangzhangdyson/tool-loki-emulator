const debug = require('debug')('lib:provisioning');

const { validateTimeSiteResponse } = require('./timeSite');
const { env } = require('../../../env');
const request = require('../../../helpers/request');

async function provision(serial, { cert, key, ca }, payload) {

  // We need to call the time site first, to simulate the machine gathering
  // the correct time. TLS calls cannot function without the correct time
  // and the provisioning service is behind TLS.
  await validateTimeSiteResponse();


  // If we get here, we've validated the response from the time site was valid
  // and that if we were a machine, we would have sync'd our clocks
  const options = {
    method: 'PUT',
    url: `${env.cloud.publicBaseUrl.product}/v1/machine/${serial}/`,
    agentOptions: {
      key,
      cert,
      ca: ca.awsIot
    },
    headers: [
      {
        name: 'accept',
        value: 'application/json'
      }
    ],
  };

  // 520/438 machines will pass an additional connection information payload.
  if (payload) {
    options.headers.push({
      name: 'content-type',
      value: 'application/json'
    });
    options.body = JSON.stringify(payload);
  }

  debug(`Performing provisioning request to '${options.url}'`);
  return request(options);
}

module.exports = {
  provision
};
