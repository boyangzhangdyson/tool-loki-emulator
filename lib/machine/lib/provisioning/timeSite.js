const assert = require('assert');
const debug = require('debug')('lib:time-site');
const request = require('../../../helpers/request');

const { env } = require('../../../env');

/**
 * Get the time from the time site and validate it
 * @returns Promise<void>
 */
async function validateTimeSiteResponse() {
  const options = {
    method: 'POST',
    url: `${env.cloud.timeSiteUrl}`,
  };

  debug(`Performing request to the time site '${options.url}'`);

  // TODO: Use a shared request function which can set sensible defaults and centralises logging
  const response = await request(options);

  assert.deepEqual(response.statusCode, 200, 'Expected a 200 response code from the time site.');
  assert.ok(response.headers.date, 'Expected a date header to be returned from the time site');

  debug('Time site responded in the expected way');
}

exports.validateTimeSiteResponse = validateTimeSiteResponse;
