const deviceProvisioning = require('./deviceProvisioning');
const machine = require('./machine');

const get = (provisioningType) => {
  switch (provisioningType) {
    case 'awsIot':
      return machine;
    case 'rabbitMQ':
      return deviceProvisioning;
    default:
      throw new Error(`Unknown provisioning type or endpoint: ${provisioningType}`);
  }
};

module.exports = {
  get,
};
