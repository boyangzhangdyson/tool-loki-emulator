const debug = require('debug')('lib:provisioning');
const request = require('../../../helpers/request');

const { validateTimeSiteResponse } = require('./timeSite');
const { env } = require('../../../env');

async function provision(serial, { cert, key, ca }) {

  // We need to call the time site first, to simulate the machine gathering
  // the correct time. TLS calls cannot function without the correct time
  // and the provisioning service is behind TLS.
  await validateTimeSiteResponse();

  // If we get here, we've validated the response from the time site was valid
  // and that if we were a machine, we would have sync'd our clocks
  const options = {
    method: 'POST',
    url: `${env.cloud.publicBaseUrl.product}/v1/provisioningservice/deviceprovisioning/${serial}`,
    agentOptions: {
      key,
      cert,
      ca: ca.link
    },
  };

  debug(`Performing provisioning request to '${options.url}'`);
  return request(options);
}

module.exports = {
  provision
};
