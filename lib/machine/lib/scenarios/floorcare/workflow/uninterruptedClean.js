const uuidv1 = require('uuid/v1');
const {
  initialiseClean,
  fullCleanInitiated,
  firstMapGlobal,
  firstFullCleanRunning,
  secondFullCleanRunning,
  thirdFullCleanRunning,
  secondMapGlobal,
  firstMapGrid,
  firstMapData,
  secondMapGrid,
  thirdMapGlobal,
  secondMapData,
  fullCleanCharging,
  fullCleanFinished,
  firstInactiveCharging,
  secondInactiveCharging
} = require('../messages/uninterruptedCleanMqttMessages');
const { msleep } = require('../../../../../helpers/sleep');

const cleanId = uuidv1();

module.exports = {
  workflow:
      machine => machine.publish(initialiseClean(cleanId))
        .then(() => machine.publish(initialiseClean(cleanId)))
        .then(() => msleep(30))
        .then(() => machine.publish(fullCleanInitiated(cleanId)))
        .then(() => machine.publish(firstMapGlobal(cleanId)))
        .then(() => msleep(30))
        .then(() => machine.publish(firstFullCleanRunning(cleanId)))
        .then(() => msleep(30))
        .then(() => machine.publish(secondFullCleanRunning(cleanId)))
        .then(() => msleep(30))
        .then(() => machine.publish(thirdFullCleanRunning(cleanId)))
        .then(() => msleep(30))
        .then(() => machine.publish(secondMapGlobal(cleanId)))
        .then(() => msleep(30))
        .then(() => machine.publish(firstMapGrid(cleanId)))
        .then(() => msleep(30))
        .then(() => machine.publish(firstMapData(cleanId)))
        .then(() => msleep(3000))
        .then(() => machine.publish(secondMapGrid(cleanId)))
        .then(() => msleep(30))
        .then(() => machine.publish(thirdMapGlobal(cleanId)))
        .then(() => msleep(30))
        .then(() => machine.publish(secondMapData(cleanId)))
        .then(() => msleep(30))
        .then(() => machine.publish(fullCleanCharging(cleanId)))
        .then(() => msleep(30))
        .then(() => machine.publish(fullCleanFinished(cleanId)))
        .then(() => msleep(30))
        .then(() => machine.publish(firstInactiveCharging(cleanId)))
        .then(() => msleep(30))
        .then(() => machine.publish(secondInactiveCharging(cleanId))),
  mapAttributes: {
    cleanId,
    area: 6.91,
    charges: 0,
    imageMD5Hash: '489d2f6f65303e8a006ac16f30096495'
  }
};
