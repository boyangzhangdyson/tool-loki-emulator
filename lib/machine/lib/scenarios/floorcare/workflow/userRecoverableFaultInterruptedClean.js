const uuidv1 = require('uuid/v1');
const {
  initialiseClean,
  fullCleanInitiated,
  firstMapGlobal,
  firstFullCleanRunning,
  secondFullCleanRunning,
  thirdFullCleanRunning,
  secondMapGlobal,
  firstMapData,
  firstMapGrid,
  fourthFullCleanRunning,
  fifthFullCleanRunning,
  sixthFullCleanRunning,
  secondMapGrid,
  thirdMapGlobal,
  secondMapData,
  forthMapGlobal,
  fifthMapGlobal,
  sixthMapGlobal,
  eighthMapGlobal,
  ninthMapGlobal,
  tenthMapGlobal,
  twelfthMapGlobal,
  userRecoverableFault,
  thirdMapGrid,
  thirdMapData,
} = require('../messages/userRecoverableFaultInterruptedCleanMqttMessages');
const { msleep } = require('../../../../../helpers/sleep');

const cleanId = uuidv1();

module.exports = {
  workflow:
      machine => machine.publish(initialiseClean(cleanId))
        .then(() => machine.publish(initialiseClean(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(fullCleanInitiated(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(firstMapGlobal(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(firstFullCleanRunning(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(secondFullCleanRunning(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(thirdFullCleanRunning(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(secondMapGlobal(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(firstMapData(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(firstMapGrid(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(fourthFullCleanRunning(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(fifthFullCleanRunning(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(sixthFullCleanRunning(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(secondMapGrid(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(thirdMapGlobal(cleanId)))
        .then(() => msleep(1000))
        .then(() => machine.publish(secondMapData(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(forthMapGlobal(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(fifthMapGlobal(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(sixthMapGlobal(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(eighthMapGlobal(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(ninthMapGlobal(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(tenthMapGlobal(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(twelfthMapGlobal(cleanId)))
        .then(() => msleep(1000))
        .then(() => machine.publish(userRecoverableFault(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(thirdMapGrid(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(thirdMapData(cleanId)))
        .then(() => msleep(10)),
  mapAttributes: {
    cleanId,
    area: 11.95,
    charges: 0,
    imageMD5Hash: 'c811853a466a9f98be42f543bcff7c24'
  }
};
