const uuid = require('uuid/v4');
const debug = require('debug')('machine:lib:scenarios:floorcare:workflow');
const request = require('../../../../../helpers/request');
const { env } = require('../../../../../env');

const cleanBody = {
  sequenceNumber: 1,
  persistentMap: {
    id: '0498538a-bbf8-47c6-bdb2-f9bc125bf723',
    cleanMapPosition: {
      x: 232,
      y: 221,
      angle: 0
    }
  },
  cleanedFootprint: {
    data: 'VBORw0KGgoAAAANSUhEUgAAAHgAAAB4CAAAAAAcD2kOAAACCUlEQVRoge2ZPU4DQQyFnyeAaGg4AEnBIZC2Jck5uAQNUja7ErfgIkFBolkp54CeioqfMQWEECWMC/R20MauNnGkTx4//+xESuSxkInrYAc72MEOdrCDHexgBzvYwQ52sIMd7GAH/9GaNsGTFfZ6ATTfcGHf7DUFAEzG94fP2ANwBaCZ1SxwKcsn1TuciwJ4GAAnT2dFg3lEtUfhTgDFJ1tkCEAA9CMAUYiGweM7J8djIAT5Nn0VEZEQECABgpt3cCKerX8MqwcBBBhFyVTHB6RyqsX4wRvpqKHPaf8trYEcpd3DbL1aSWArx6Dl2PSzjlpeku4QaDneT3rPIy3H6SzPJVOOMcyl6kuauAw/K2LbOge21MWqY1PVMxK4EgOdTVys6WTWE69XW0YDm7LuHNjqmaTpZBdytoi7p+pc4CoX2Hzf79xRm0tXrkWgexGbxgLHXODdO+psYOPyhQZujjOBcWj9gLSBFAU0LWyeuKzFmgY2jAVurDHRtYibxa4t9EVhVBMvYmvNpIFLYyTzVG288/OOOl/nismK4oGnAk2giRFPoybQzJZZj/rDV/1F3NReXVzgtC/byayriCW6gEJjb9ND/+MawHRb425jLOq22dwGuNom7SyLgDIvUX9ahRhLXRlU2ar+MhWUKlBZvrFr3Q64mshbT9ZmdDtg1BvfdG3LdPA/An8ALAZ6to3oj7oAAAAASUVORK5CYII=',
    resolution: 40,
  },
  robotPath: [{
    x: 321,
    y: 123,
    angle: 232,
    timeStamp: '1000',
  }],
  cleanTimeline: [{
    eventName: 'CLEAN_STARTED',
    time: `${new Date().toISOString().split('.')[0]}Z`,
    zone: '1'
  }],
  occupancyProbability: {
    data: 'iVBORw0KGgoAAAANSUhEUgAAAQAAAACICAAAAAAtjWq7AAAFxUlEQVR4nO2cvW7jRhRGz1AiCJDchh0BrVOqSJVScGMgcR7Ez6DOgM014M7P4AeJ14EbwUWeQGVkAexYUUS4EjkpvF57ZSmmHA6vgJ1TCaTM+82n+bmcO7BK+LFxpAVIYw2QFiCNNUBagDTWAGkB0lgDpAVIYw2QFiCNNUBagDTWAGkB0lgDpAVIYw2QFiCNNUBagDTWAGkB0lgDpAVI0+802mQ6nKZHHHYa9L/p0ICrRZAfADc/pAGTuxhgNqTqKGJDOpoDLqdxNqIEHn7tJmJTujHg6iilnAZLuOnfdBKxMZ0YcDG+j+dxFu3jktPFHHB5djEYMvdTYN+mgC56wOXppc8UHXnmY+2OeQMujy7jcg7BbFQD2njA3TBuwCS+87J4ME9H7udjfkADbolhPie+O+LWdLB3YNqAySCbg9/jIb5RPcPB3oNpA+6zaFBGVH5/5vrLrxc/GQ66C6YNKMt55s3KQblSq/zp4nliOOoOmDbA8/yCsJqh+lo9XUySxHDY5hg24Iqo7OWFdpd6+bwAOInZqLtgPhOMGd6z+O5SbTxocwwbEJF6xUOf/mrf1v8nDBuQQh6yoN+PZ2YjvRfjQ6AKCxSV3tP2mzYgBqIMFvs6AjqYBDNy9br5n4Kx8ciNMG3AvFfpsKy0Zs2FfMsfdE3vyOjjf/mr/wW9hPDL+q3Jn2ZDN8R0JlgsgqAKgyBX7tqdJdeGYzfC9BDwgV7JyqmXr+6lkyknhuO/iekeMCYq8FZ6U/K3upkNDYd/G/NbYvOBl29eBDWOfI3IuAFjf56H2276V6bDv0kHu8J1uG3JU6V8MmDegMjduuT392Cj3LgBV9tLIY4n3wHMGxA9+GrLLX8P2m88D7jQahFsHgPuPrTfdA+49tE6X08CAdD/mA3dENNDIAfF6ySwi9DNMDMEkueP2zcClOanmVbHsidmjPwMn5IGX1LA3xr9h2w2aKIHXJw3K/0okC+WqqSd57z3MVq6TiYxE+mNH4Voy4Dj4+bffZkYKelCaVtzwJSDbESzE2DPRUK2JYnd0eIQiG53PwGnxcdAWwYMmc2alvzkf/YXtDUE3n388Sr9eQiIZUNtGeDgF/hlnK5UvV4CWEc/dQKtVJ1/UPMBcga0NQTOfHx/tJpVH3X41uKmUGjQWoEDup49ZFlLOnamtUkwoig/Bwd+qoqNL38vUTxa8A0tVydqzYB5OSCIWFS+9po8VT9Phq6DW7SlY1daM+DMy+Ioe/jYKz/mSrvwRj9QH74tBsvaPfLb0rErLeYBozQjSPHmjgoqqL42UIHaFCV/ugt4t6P2dOxGewaM704Lorgq0X7RcwClcEE7T6vCpjxB4wLF2X1rOnakrbdBgIsBWelR9GAVFJogB7fSqgbn8Zz0xhRIaTgQqxG2+TY4GA6LOKKOqfoAEY6z1LoHYR0qx32dLCngQLsHyNUI29wQOZncDTKKMPNKqMMyVb2lQlVOrziGQ35Xeq0LaGAWRmmSiG0LtbofcBgTlb1yVEAEeEBf9+sB/pRDSF4lSE4IYZmdInd0tN0NkRNSz4vvicHNY4Kl8pwqfIhHJ4+/8PocUHPgLLwx799R+t+0vCd4wlU5vPWHKZWTlV5I7sJvWxN9VTAQPiLR+qbomOse9146mBOnXuF60XD7i47WI+kTAgZ2hU/gW9l/MORwsr2NSfvRd8VMYeS56jfZ3H6nBtxTI8F3w3RxdL39SUKSwJnhsM0xflL0RfuvIYS96PjPdFwX2IuK+Hd0aMD1EPZsR5Rue8AU4LzDgE3ozoDrk8cpYM+6QJdDYMaYvesC3RnwtQPsGx3+ExXxc9Eb2Y+DOoJYA6QFSGMNkBYgjTVAWoA01gBpAdJYA6QFSGMNkBYgjTVAWoA01gBpAdJYA6QFSGMNkBYgjTVAWoA01gBpAdJYA6QFSGMNkBYgjTVAWoA01gBpAdJYA6QFSGMNkBYgjTVAWoA0/wJqf3prjManSAAAAABJRU5ErkJggg==',
    resolution: 43,
  },
  zones: {
    data: 'VBORw0KGgoAAAANSUhEUgAAAHgAAAB4CAAAAAAcD2kOAAACCUlEQVRoge2ZPU4DQQyFnyeAaGg4AEnBIZC2Jck5uAQNUja7ErfgIkFBolkp54CeioqfMQWEECWMC/R20MauNnGkTx4//+xESuSxkInrYAc72MEOdrCDHexgBzvYwQ52sIMd7GAH/9GaNsGTFfZ6ATTfcGHf7DUFAEzG94fP2ANwBaCZ1SxwKcsn1TuciwJ4GAAnT2dFg3lEtUfhTgDFJ1tkCEAA9CMAUYiGweM7J8djIAT5Nn0VEZEQECABgpt3cCKerX8MqwcBBBhFyVTHB6RyqsX4wRvpqKHPaf8trYEcpd3DbL1aSWArx6Dl2PSzjlpeku4QaDneT3rPIy3H6SzPJVOOMcyl6kuauAw/K2LbOge21MWqY1PVMxK4EgOdTVys6WTWE69XW0YDm7LuHNjqmaTpZBdytoi7p+pc4CoX2Hzf79xRm0tXrkWgexGbxgLHXODdO+psYOPyhQZujjOBcWj9gLSBFAU0LWyeuKzFmgY2jAVurDHRtYibxa4t9EVhVBMvYmvNpIFLYyTzVG288/OOOl/nismK4oGnAk2giRFPoybQzJZZj/rDV/1F3NReXVzgtC/byayriCW6gEJjb9ND/+MawHRb425jLOq22dwGuNom7SyLgDIvUX9ahRhLXRlU2ar+MhWUKlBZvrFr3Q64mshbT9ZmdDtg1BvfdG3LdPA/An8ALAZ6to3oj7oAAAAASUVORK5CYII=',
    resolution: 40,
  },
};

const createZonedClean = () => {
  const cleanId = uuid();
  const performClean = (machine) => {
    const machineCerts = machine.machineIdentity.certificates;
    const options = {
      method: 'PUT',
      url: `${env.cloud.publicBaseUrl.product}/v1/${machine.serial}/clean-maps/${cleanId}`,
      agentOptions: {
        key: machineCerts.key,
        cert: machineCerts.cert,
        ca: machineCerts.ca.awsIot
      },
      headers: [
        { name: 'Content-Type', value: 'application/json' }
      ],
    };
    options.body = cleanBody;

    debug(`Sending Clean Map Over HTTP to ${options.url}`);
    return request(options);
  };
  return {
    workflow: performClean,
    clean: {
      cleanId,
      ...cleanBody,
    }
  };
};

module.exports = createZonedClean;
