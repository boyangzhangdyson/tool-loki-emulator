const uuidv1 = require('uuid/v1');
const {
  initialiseClean,
  firstMapGlobal,
  fullCleanInitiated,
  firstFullCleanRunning,
  secondFullCleanRunning,
  thirdFullCleanRunning,
  fourthFullCleanRunning,
  fifthFullCleanRunning,
  sixthFullCleanRunning,
  firstMapGrid,
  secondMapGlobal,
  firstMapData,
  seventhFullCleanRunning,
  secondMapData,
  thirdMapGlobal,
  secondMapGrid,
  thirdMapData,
  fourthMapGlobal,
  eigthFullCleanRunning,
  fourthMapData,
  thirdMapGrid,
  fifthMapGlobal,
  ninthFullCleanRunning,
  fourthMapGrid,
  cleanNeedsRecharge,
  fifthMapData,
  tenthFullCleanRunning,
  fifthhMapGrid,
  eleventhFullCleanRunning,
  sixthMapData,
  firstFullcleanCharging,
  secondFullcleanCharging,
  thirdFullcleanCharging,
  fourthFullcleanCharging,
  fifthFullcleanCharging,
  sixthFullcleanCharging,
  seventhFullcleanCharging,
  twelfthFullCleanRunning,
  eigthFullcleanCharging,
  fourteenthFullCleanRunning,
  fifteenthFullCleanRunning,
  sixthMapGrid,
  sixthMapGlobal,
  seventhMapData,
  sixteenthFullCleanRunning,
  eightMapData,
  seventhMapGrid,
  seventhMapGlobal,
  seventeenthFullCleanRunning,
  eightMapGrid,
  ninthMapData,
  eightMapGlobal,
  eighteenthFullCleanRunning,
  tenthMapData,
  ninthMapGrid,
  ninthMapGlobal,
  tenthMapGrid,
  eleventhMapData,
  tenthMapGlobal,
  twelfthMapData,
  eleventhMapGrid,
  ninteenthFullCleanRunning,
  twelfthhMapGrid,
  fullCleanFinished,
  thirteenthMapData,
  nonthFullcleanCharging,
  firstInactiveCharging,
  secondInactiveCharging,
  thirdInactiveCharging,
  fourthInactiveCharging,
  fifthInactiveCharging,
  sixthInactiveCharging,
  seventhInactiveCharging,
} = require('../messages/batteryRechargeMidCleanMqttMessages');
const { msleep } = require('../../../../../helpers/sleep');

const cleanId = uuidv1();

module.exports = {
  workflow:
      machine => machine.publish(initialiseClean(cleanId))
        .then(() => machine.publish(firstMapGlobal(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(fullCleanInitiated(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(firstFullCleanRunning(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(secondFullCleanRunning(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(thirdFullCleanRunning(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(fourthFullCleanRunning(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(fifthFullCleanRunning(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(sixthFullCleanRunning(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(firstMapGrid(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(secondMapGlobal(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(firstMapData(cleanId)))
        .then(() => msleep(1000))
        .then(() => machine.publish(seventhFullCleanRunning(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(secondMapData(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(thirdMapGlobal(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(secondMapGrid(cleanId)))
        .then(() => msleep(1000))
        .then(() => machine.publish(thirdMapData(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(fourthMapGlobal(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(eigthFullCleanRunning(cleanId)))
        .then(() => msleep(1000))
        .then(() => machine.publish(fourthMapData(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(thirdMapGrid(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(fifthMapGlobal(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(ninthFullCleanRunning(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(fourthMapGrid(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(cleanNeedsRecharge(cleanId)))
        .then(() => msleep(1000))
        .then(() => machine.publish(fifthMapData(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(tenthFullCleanRunning(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(fifthhMapGrid(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(eleventhFullCleanRunning(cleanId)))
        .then(() => msleep(1000))
        .then(() => machine.publish(sixthMapData(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(firstFullcleanCharging(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(secondFullcleanCharging(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(thirdFullcleanCharging(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(fourthFullcleanCharging(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(fifthFullcleanCharging(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(sixthFullcleanCharging(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(seventhFullcleanCharging(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(twelfthFullCleanRunning(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(eigthFullcleanCharging(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(fourteenthFullCleanRunning(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(fifteenthFullCleanRunning(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(sixthMapGrid(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(sixthMapGlobal(cleanId)))
        .then(() => msleep(1000))
        .then(() => machine.publish(seventhMapData(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(sixteenthFullCleanRunning(cleanId)))
        .then(() => msleep(1000))
        .then(() => machine.publish(eightMapData(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(seventhMapGrid(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(seventhMapGlobal(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(seventeenthFullCleanRunning(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(eightMapGrid(cleanId)))
        .then(() => msleep(1000))
        .then(() => machine.publish(ninthMapData(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(eightMapGlobal(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(eighteenthFullCleanRunning(cleanId)))
        .then(() => msleep(1000))
        .then(() => machine.publish(tenthMapData(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(ninthMapGrid(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(ninthMapGlobal(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(tenthMapGrid(cleanId)))
        .then(() => msleep(1000))
        .then(() => machine.publish(eleventhMapData(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(tenthMapGlobal(cleanId)))
        .then(() => msleep(1000))
        .then(() => machine.publish(twelfthMapData(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(eleventhMapGrid(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(ninteenthFullCleanRunning(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(twelfthhMapGrid(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(fullCleanFinished(cleanId)))
        .then(() => msleep(1000))
        .then(() => machine.publish(thirteenthMapData(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(nonthFullcleanCharging(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(firstInactiveCharging(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(secondInactiveCharging(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(thirdInactiveCharging(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(fourthInactiveCharging(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(fifthInactiveCharging(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(sixthInactiveCharging(cleanId)))
        .then(() => msleep(10))
        .then(() => machine.publish(seventhInactiveCharging(cleanId)))
        .then(() => msleep(10)),

  mapAttributes: {
    cleanId,
    area: 70.5,
    charges: 1,
    imageMD5Hash: '90c6f88350b709de02dab3800ad6a07c'
  }
};
