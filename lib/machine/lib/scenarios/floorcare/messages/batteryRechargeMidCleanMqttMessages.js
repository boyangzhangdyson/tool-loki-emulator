function initialiseClean(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 100,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: 'scheduled',
        globalPosition: [
          0,
          0
        ],
        msg: 'STATE-CHANGE',
        newstate: 'FULL_CLEAN_INITIATED',
        oldstate: 'INACTIVE_CHARGED',
        time: `${new Date().toISOString().split('.')[0]}Z` // remove milliseconds
      }
    };
  };
}

function firstMapGlobal(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        angle: -180,
        cleanId,
        gridID: '1',
        msg: 'MAP-GLOBAL',
        time: `${new Date().toISOString().split('.')[0]}Z`,
        x: 0,
        y: 0,
      }
    };
  };
}


function fullCleanInitiated(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 100,
        cleanDuration: 2,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: 'scheduled',
        globalPosition: [
          0,
          0
        ],
        msg: 'STATE-CHANGE',
        newstate: 'FULL_CLEAN_RUNNING',
        oldstate: 'FULL_CLEAN_INITIATED',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function firstFullCleanRunning(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 95,
        cleanDuration: 3,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: 'scheduled',
        globalPosition: [
          -491,
          192,
        ],
        msg: 'CURRENT-STATE',
        state: 'FULL_CLEAN_RUNNING',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function secondFullCleanRunning(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 85,
        cleanDuration: 4,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: 'scheduled',
        globalPosition: [
          -601,
          -1260,
        ],
        msg: 'CURRENT-STATE',
        state: 'FULL_CLEAN_RUNNING',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function thirdFullCleanRunning(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 75,
        cleanDuration: 5,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: 'scheduled',
        globalPosition: [
          -3464,
          1213,
        ],
        msg: 'CURRENT-STATE',
        state: 'FULL_CLEAN_RUNNING',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function fourthFullCleanRunning(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 65,
        cleanDuration: 6,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: 'scheduled',
        globalPosition: [
          -4513,
          -1037,
        ],
        msg: 'CURRENT-STATE',
        state: 'FULL_CLEAN_RUNNING',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function fifthFullCleanRunning(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 55,
        cleanDuration: 7,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: 'scheduled',
        globalPosition: [
          196,
          1883,
        ],
        msg: 'CURRENT-STATE',
        state: 'FULL_CLEAN_RUNNING',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function sixthFullCleanRunning(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 45,
        cleanDuration: 8,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: 'scheduled',
        globalPosition: [
          -3289,
          695,
        ],
        msg: 'CURRENT-STATE',
        state: 'FULL_CLEAN_RUNNING',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function firstMapGrid(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        anchor: [
          16,
          72
        ],
        cleanId,
        gridID: '1',
        height: 144,
        msg: 'MAP-GRID',
        resolution: 43,
        time: `${new Date().toISOString().split('.')[0]}Z`,
        width: 144
      }
    };
  };
}

function secondMapGlobal(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        angle: -180,
        cleanId,
        gridID: '2',
        msg: 'MAP-GLOBAL',
        time: `${new Date().toISOString().split('.')[0]}Z`,
        x: -4937,
        y: 1964,
      }
    };
  };
}

function firstMapData(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        cleanId,
        data: {
          content: 'eJztfcuSAzmS3H2+Yq3PdUi8gfmVtj6sdsekNVtbySStLjL9u+ARHkkWmS+SyVcVLXt62gskM+AIBCICr//7x7/8+z/++T/+8a9//P3PP4evz7Pw/PX1tw9Fy8+HotXnQ9Hq86Fo9flQtPpcS5GT5+niP+K5jiI3Ps8R+6Hv3kKRW3yeRZC++wEyrFO0TNDjKbpFiqvqskbR2o++CkVb5Fivy+Sv/B6KthA0+TvLFF3J+1tTdPZLt1L0aIKupWg7QRdRdKViPomkvQi6A0WPJukaOS6j6OSX5ii6ifffQNFNP/kbKLr0B1+Douto/VUUzUtxY30+FH0o+lB0D4pOvv9zKLqG1F+lRdeQ+mMpmpbvQ9EG+fYk6c0puk6KD0Ufij4U7UTStdSu/M4+FL2GHt2FoA9Fv4qi62i9kqJXt0YvoEXXkPTqFN1hkuinUXRDbT4UXU3R+5F0Ha0/lqJ7R/s/gqIpCfei6OS770vRpRLcgaJXnpG9xH08lPwyLdoqxfeSq+ry0yj6LsuldbgrRY8laMuqxWvpuRtFr6VFtz4fij4UfSh6AEkfivai6Can/a0JuoCiV10Q+pYUfbTopUi6Lzk/gKJHEPTWFD2GoLeh6FF0PJCifUl6JkEvT9FzyXkDip5Nzl0p+gnUvDBFz6bkQRRdT9KzCVmpy4eimyi64Wc/FH0o+lC0laLH+UXPJuQGih6XMXo2JTdQdEk1biPptQi7E0X7kbRXJY/RlQS9CkWn395VC2Z+9Ryfy/EyFN1OyF5NNfk8n6I9ibkDQVsH/W2VuU6EF9WdbRSdvv5D0c2ifyh6eYruRNLzlz3sSdFdSHp+GLsvRXcgaU+KXoOkH0jRR4s+FN0u4IsT9FiKpj/x6yh6/vOh6EPRh6KfRtF7kvRgit6RpIdT9I4kfSdsnsDDXxbJ3X49wU8gbIq08/IrKPo5OrWVxA9Fl5H0oehD0W+h6NHvvYmix+3COBX3jSi6v7CvoL83UnRfkV+ji78kRc/X3hen6LJ3vshpD4+jaK/37yXtyXefTdF1756SYNun1zXx7JvPo+gWcvZ9VuS57TrL96dnw3PrdZY/nJ5rKfpVBD2SoqdX9fUpeluSHmmLnl7ZD0UvRNGno92JoqdX9dUpenpFPxS9PUVPr+arU/T0Sn4o+lD07Of+FD29ih+K3p6ip1fwQ9GLUrSdpKdX71kU/SqCPhTdhaJfZYeuoejXEXQpRb9sLPtQ9KHoORT9utHsGoq2kfT0aj2XosNzTsgPJOg2in7J86Fo9flQtPp8KFp9PhStPh+KVp8PRavPX399/dPf/vjv/+V//eN//p9//Osff//zQ9jy89Gp1edD0epz+yrs5wj+wDfvMx/yWHLOz598OkXToeuxkI8k6eF5hu0UHeiYEvFxJF2bkLma1P2Ox3g2Qcsy3CDv76Boax0mf+ndKHoCSXsd1fMogpYl2o+gnSl6DWN9mSW6gKQ9zsR6FD0vR9GFP/QSJE1Jcx09u1L0aJIuk+Z6glYpuurHfhRF429MU3T1zz2doAdQdN2pCT+RooWOdhPjT6fockp/OUW3ELQzRa/hOj6Ioht/7ukkXUrph6L9Kbr5555O0d1HtHen6FI6N/3Wz6JoT5J2puhxBF0e699M0s/SogdQ9A4kvS1FjyLomqTarhTdzPgLUnRznX4WRXtmHcdf+FD0Aym6tKvdXJ8PRdO/sph1fHWSLpXh5vrslS96DWu0J0WLMyDvm7++9PNXU3TdD78CRfuS9EsoulaDfglFt9CzaWXI61K0NfP4oWhVnl0I+skU3fZ8KPpQ9KHoQ9GHog9FH4o+FL0HRfcnaRNFr7wk9EPRk+m5G0V7k3T6m7dGXT+Womc9L0/Rc+n5UPTeFD2bmMnavA5Fz6bkhSl6NhUvT9GzifhQ9P4U6beeTcbLU/Sqz4tQ9MokvQhFz6bh4RT9LJI+FO1D0e+2Ri9E0auStIGim3727Ql6IVv0oeiJFF3//pNP/NyOdtn7Fz7xEym6RooPRR+K7kjQh6IPRfel6H0H/Q9FjyZpX4peJdb/UPQ4eu5B0aUi7k3QW1A0J/aWv/1iih75PJCiZ1f1DSh6V5I+FG2m6JSuqfIDXiD351F02/Oh6BqiPhStkvShaIWgvefRfs6zmaLfTNKGffofkjZT9Hup+lD0cyh69rsvuh/NvvZgEZ9I0sUUDV9u/PejqHk7ih4j7ut08xsouo/Qz2uU3Sl6fEr+iQRdQ9Gj6bn0rXtJPH7/uRRdrr/XPd/fefrmFZmeR9Gj6Ln5uZSi99OgB1P0i4h5FkVPr+79KbqVpqdX9xEUfbTorhQ9vbIfil6Col9I0IeiF6Lo6RX9UPT2FD29mh+K3p6ip1fyQ9HbU/T0Kj6Wokdtenip594UPb2CH4pejqJfSNC9lz08vXofil6Kol9L0D0penrVPhR9KHqd50PRh6LHUXT58r2nV+3xFF2qSU+v2jMoWn5+KEF7UvRjnw9Fq8+HotXnQ9Hq86Fo9fnrr69/+tsf//1f/uU//8e//eNf//j7nx/Clp+PTq0+H4pWnw9Fq8+HotXn/Sh6eKizhaLXir9ekKJXC1FfkqJnkTEtzYei1eflKFpvtUdni16QojWB3UPFfrmOtm0P6yMFnn/f2mbkq9+5dprRI6u/7Znfsr70nbtQtMXGPCNrPf3GdUl3p2h75V+jq91RhmmKLpvVeOx49hLm+lIxXoGih2vRpWLfj5Lzd72EFl0u9uMEvo6imyTch6Jf7Dq+otjXuI43PT+Fopc318+naN9vfHt+CkV3lOEdKbq0q90o37sN+kvm+k5W6h216FIpXsQWPd8zupsM76hFH4qufNuSFC9grl+bohsNwc+h6G4j6z5z+s8f9t395PgpFL14GPsK6ZA7Oh7nFF3zqldxHuf//mRz/Q4U3fAszaPtIfY9nstDkJue5anG7UK/wrB/J+dxHy16VYp2kWsfLXr8sL9l4D820zeQ9ZMoutPCh3elaE7iO5jyd6Xoge97x0H/wW97V4peRIs+HU2eD0Wrz4ei1edD0erznhQ99H3Li4pfUuRHv+8dKXrwUub9KNpb6CkinnJGyWtTdEzU045v2Yuix1ujhz0filaf1+poL3kW0itp0YseGbXfZqsXqtS+zytp0Ys+z6boDYh9LkVvQNCHog3Pp6OtPh8tWn32HPTfosKXP8+n6EUdxsOzdpTBduGvr6A7o+mlyNpyZsjW9r2+WqdveSOKHqFFL0TG9LMfRa8y7J922/Vn6lt3oehVtOgaw//dDp6Urs3pv68W7faLa8eqvCNF+/7ejSfPnH7++VW6w1i4ZwDyGhTt+WvyfChaffaj6BVs0V1czp+lRXeJ836eFu1O0r4UPTtnNOP83fa8AkX7VenFbdHdRHz2+/elyMTcHiju2zFenKLzSk+TdXlgc40UD6Ho9irMffeenfHhWvTy6a4JmXf/zbWO9m4kfbRo9flQtPo8gaJjmt6dsHP5Nw0oW8/ed1NffsnnMA/njvAcCRsU4NKjDF6fom11cCf4e/mFWvQbn28kfShafd6DoqeOra9M0X2juc3P5eb6MeKeZgmeRA+ea7TofjSdZgKeSMzhubaj3SON8WLU2HO9Ldp/FvUlCXoFiu5HzU6/+OwR7XZyjkOI+ee7aTjOis5ROf71elu0DzmXLqxYf5betO0XTn7nWVr0WKszr09auijRYylair1f9nlsR3vRMWv5uY6i6wl6eoUvf67zrq8j6S0JuoaiKYO3/Ztv+FyrRZc/b9rNHmmL3pSgx2rR0yt73fMov+htCXpUR3tbO4TnMVr0xgQ9hqK3JugxHe3XUfSr7BCe+3e0Nyfo/n7R2+vQpRT9uk6G574d7RdTtK3iP4Kg+2rRjyDovrbol1K0vdo/pJvdcyHfr6Vo6/NjCLonRU+v2l7Ph6LV59nLHt7g+WjR6vOhaPX5ULT6fChafT7mevX5ULT6fChafT4UrT4filafD0Wrz19/ff3T3/74z//4j3/+P//2X//5v/z7P/74+58LnKUv1wf08OWi/Bt/eG4F2pfLX959+fjlk/zb4S/tYfwtqNizZZsWqUKYMHyF8BWi/HvAX/rfHyTYPGWvIN3Zk6QVu1D+K5SvOHxFh3/3/+5/8VL6kE4wR9tI2iXyPWLJcbcOXeGDiBL9V6xfKePfUQQNUuoeQNwcdScNu0XCY/MXH2EE++976QZo194HemMPA/4PUkov8SLE00g8b+RVGU/MYnqAcRybOqJ39EaGhG5w8v9oc4cidJZ41C2WOshN3WeGysm2XpTy3GDGR5jNk0Zf53PVbTjpWNf3qilqF1p/VuJJq+oeYvsv1IMNXsZ5j7uhu00xvKwQU6ZgxvjqP/ceJByYu0AhNvklk33xlo54TvT4igXdOOVt1kDb/+4/mFym0FucmUmf7Nauecr3iQnwq4qib1yq7aLJeRLlW32gSXfu9s76nfUzzs+FX7Qmcx36IbRfZFa2e01zbXlz1z2mfjvxK3oxSf79rcwm7kfRLxheLxspLlKoA/+r7J/WwW8wOCPxDwm7li3Ot+a/hM4H0D9lA7dYbR3SV0ScrP593MbVfngs/WQkdIn12Y//BQ9lgcUjG7LUAR424q4P/Bsa4CIHbifrv8VfmZduWsL+yR0VZQv3a4bihKzFjMqcm7lTDkuJX5V4cuT8ZjvOveDlWHx/q3PZqLs9/7KSOrzRwV90qNad2uNAb2snfjjpo5u5VdfvlGmcN/LHRK/Fbm0pf/cSruWMWz+nTQu/eautnHFrTmLk5dzEXf2uuxI+q7cjHffS8JUM0ELK7blsX+LiXeAJ3idbcBa0zuQ15zPITyd7c+58u73bYLuvG/6/J2cuS9I/m+jVOZ/FbMAUfVt/71a1PmJwTZmercxLw9glMf8Rd/PZ6h1N9U6VfB0newwrtyRg4iWB6vUeyG7VnM09Pojm8/BxY5Zl7udeWZuf6E6fq+n1Uc5dLfPNNL+cF72Ht71LcP6tArdX8rWCw+2DyHQ++nYzeEzzpYsuLsv2PD3zcQHZGycubpliu2YNxnt5Hitj4rGLd0e7aIyvLceY1fX3In120mOby7eLK6KUb1iTsaTv60mIZ85frs7czAbxU0mQ3eYQVpMsK+uPLl+39TTSZz4zpehTMwe7Ub62GOMkkz3F+zXrEXfk/CL7smamj636ZApqJ/uyRepNc0RPT5ncoPIz9nB+Kmq/AXVR77d7p09nfzX7atKfmJA5u3g82u0f5C+b+YvN9f3Wcmx4FvOxp7lCZXVZQ07YWDA5l/tpiy7NpWr7dKW/fZb+m5zTqdk9M+KzTvzls78vT/5lS+OWUrZ7pcongtbr+XuqzVl+/XYvU5V5cXzdT61OUjQ3ae/FO3V2Zv/2NYnftHnKs7wb97dajus2ne31XDTcridjzmOpl+V94fuPUPnb0zfH7XO0Cebc+u9m5y+blZnyLF+V8/mpu7lszGGH15mLeVMG4TewPZH/WttCedi2uOt2rnc2KO7b1Ndl0q9tBz5swt1zs+J7sr2wm/9iv2t5Rzt3FO+1Cfcd6V7c9Q/iLw00NpzFsNOu8veje+1cAB+/lV5Qk++26axu10r8HIqvn7ybm+w6WV8djkqntlTefayeofjGsHyV4dum6o667fL0GT3iYSoP+CI03xJ/3+glLj9n9nh5Ayk5r2cMzwWMz2D7loj7nnnCGXM8H3MfcT+5m/H0f09V8RtGtHuR7pbOb1k4IOC0Bc55no7SH3IM0360756VPXa05o91URvyPXuxpO0n9ufWAPxxjE+euLYn42dn/iyc7zLB4Tc+zxqIA+ujT7m7kOzHzTJPnv6zYbv0NxNx7B+eWKODx/jgAw8vYPyBc8vz5xnOjZcLo+GB1eMw6DgSeuzBnJsp3zZ87jO7s7jNfdInXBwJR1KPI/3jYP/+xyZew/gWJd9nSmf9RIfzgXN2CLQNgIc0yIHeB568v6P13itJsskinfmGk0nqcfRziwmQ1+H3yBWZ6mt7ELy5K52a3kedUvkIfueGjz1yUBsJnhvVXuTE6ev49Wsu0U4pqC02YsIo7HG+o7v4mfrWdRQf1iovOfo3DpbbfmRuhLt1FvB8euD2f84H2olB97K2GBVtNcbdK6iZPSxgxcE7Dh2vi8Mnp4Bu/+fc05zyOtsVzbI14bNb00wt9N0S75y04DWJqbkpo9v/WfAAzizo6S7QrYeTrVC9VwOdT/tskOm4mS5+4/ybJ33W6/45T0ks+MFnTbTt+L7Vmu+UQJg/I3leovPyi9tnYY3+7f+cp+kWB4yZSZHZNfUP7kQrPsH88HOThVs+JfVe/5tOaa+OK4uTiFuGhXv3sjX3e8Kfu9Z3WFG7e/5vyWossb1tqv11u+Bp3DN8H3Muc8hXdjvOe1v7/G9pJN7Q+zaYqlfsf6fZ76uX+a2t4P9W4av/mbGR3xb5Lw5zVxqJ12im40jlutWBy+KeKPp1PuKUT3MyTzDrJm5onQ2D4rYfu0frjNH9FWsJt2z2/qbdV0RYkwHB8d0ZS0HW6sHtj0xOXNgo9o0N7bBuzma28F/6z3QA/f3amPmUxIb9yTek7WZYPDzG5/jccbHq1hM/jlevX5Sum0s22aIJTkfOJO22zfUtj6kXXcNyUcLxTq2yqjuTe/zd4jOXiZ2+DuxxEzGnejbMdOnF/OJ6I1y+cGbTcHnd4rPjnn6mGzcsMJscDrdeCXJd4vAea483upH7L7fcc23futN+PpJcmBA8l+rWtZCr/fTVmJ8Onla9vckZr2/u1Vqi7xbn6krm77LU9fLRdvlG2/U09LaUweaU3XncevdOcEX67OKWWHdBVyXYMMW4FpmfttnWzx1HvY/oHMuqObHwevZF8w2yGtyf6t35gLxm16aIu7yhtjXS7bbkAre6HrNx7IkeETTzZ7dUeE32ad5jPWqrpbP8z86+OG+uRYu19vGZRNvl7bP9joOTWGzqJlvyv1A0pmdmL8Ld3qPO3Zn52P20ot/yYd9Py/iWypwb3Wc/NJ1iu7hdNoa0w1mV09QfjemFIuY0pz4zmYpa1Jszk7uQ4pqeLz8/T+OkzRYznxOfmXz1xb1lWy7/vL7nf/RHRC8U6YTAwhW7m32zb73FT6wdP1XYyc0U54duHEu1nPI8/8xUR53h/ZY2mVCNKZ2CdGVD0WBw9pa5KxYvacOk/N2yTBv4sz1F3/57spsv5D7PPzMx1n1vi6279dfaZtqcTljik7X2Z2VT+jzxAzOT9lt9gon9ykte0pk/cvjvyRFyISd6/plJzk/a5YJDLBYPDV3wS6YdnLERp4vHZj5txMXk3NYFZ+cNNBd3LMQb7uj/J/xKN5ky/VY4nPxaOnN7zucgFoenLUPuXFjwjfszL3WifKKNvzfu+Qqfg51aX3eW8nft+Nb/51Vgzs6cttqWv580XD5zf84n7JZPix7H3vlgeqrC34z/6YKhb0TMt9b4/SXn2MfTdpn0vI7t6NF/TJjSbWP31ue8fxyPZSfzppOLlGbPkZ5x/cfvn9T2mwM5f0b2iYE7iSgmIpUjXT6/QuXc2zr2f8f/OPdwd3Fsv8lx3hWOBTpbXbBkCb7OQtIZb/9kJvzb30e/ceac+AVepk6/XegQ3wg4d6WmDprbNer7ZpfqlPp/76Oz2c85C/A9heNmNO94PDqu+Xd/caIFR1089/nDxEEDy6PGCR0nlmF5gehe6ZEzdfCn3eB0MJv2HZfzS5zZP/cXJ2t+MjN+npyZbM2x1c79/njWmqdh5tLFWt99zi1u9JpaXtlHvnfNqUz2XMusu/TzTvncJpnZ+GpuEVma+u9jv3/5FMD1u8/cRctbr8zGnx+fd+5cTIZRq4no+bh4kfjZTTJzaYnv7E7Y/8lVQmuN7+qWy+kmPZXJZrpmV8qxcZnvOMdvXG6Uc2N7nlDakhGa2yQzl807sVHnftL5qrpVGfykUzZJ44n7ctZMVzTPeUJntd8sN087O4nnm1O1vCfphPf5LTLTSfCT4f08oDj1uyezuRO5tW3XO7Ypz+mW89RnDpW5pM+c9Zdv1R69prMfnfvnlPgNs+Bzk0nffeOZuPw8fl2XzMbPrZdyHgcea3c8rzgMczHGsKUjndq1U1f1+9B7/S6lqeZYcxuX7PG0rp07OHNpom3yWr/ZftVqWsvPb/XQZ2fwzs3vpAPxzSidObnfpsDut39pK9Nzrbv3/67qqvNbN7/10pUgeCFfNuuZnkd3p2P8yRLkcOfdhHOmcJMLtaYUS113Zki5YKA8O79r8nyjDeZ2LTF96uZPNOJxmmDisC831/2v+2fa6Zg7kW9ppFwfL8/fe4m7eTaW3XQ08dYt+rMNdJwycJMHhO24+X3Sc59v+1O389yNnDyUeCKzv61hVlYxXtKFVmfttzSJOz5B6lQH9joyYireXWrqYaox5wO200D8guzGmn5f3F1W9x+dRFcTjbE0WdUm5gIv/ud8Wni5lc8yR9MZ7sm2Ppo93pTKWN6N6y5ujg1bkM6nQzY0xNgFh9vOIPo2+bvcuueLCs+S27NtfLC2+/SJ7XZqtCGrm9Uup/+b3mx4Njfe6RqVkzTYxJ0U335+p6FgW5hzNtSf25zjhrqC3Nue7w1w2gwrl05c6O1suAptNri4ojkeReHNz1bXfnE9z4buPhecXdoWL/lsJvHWQ3u37u7adY/Iq/G4cBLXFl380Zp4/QgzmWudJXHTZvq31cNtLM7lrC7o0/M/cdqbH3QS/+N5XD6jbNsQs+zoTLTEC5w6vD+Ta5O9G3rljjmIl302ELm8h3FCn0775fIY8wNG6m1ULsbs30ebeSbWfZ43H7G3cbmgllv9n/W9bz9AJ6+2lPFsqfTs4L11V9R7a+QDdPKnu5FbiVwzlGdLV07IWItp3t6P3IHI4z49u7B6W4D9zn7kVia3ZMhtz/A5FduGmkfdWvhkHlfv/KFlvPD4qh/gi28ncWraetEkrlD4k0aXbQyeLc+Y3P69nb8fM6ZspW92B/vcSLDkHB18wzceQ7YzN73HfHKf36rS/YRR4xLm5g+i+fC25dnzwNk3fS6lbP75UHYNaesDwXv7b3h2JWz+cKUf4a3h2ZOv+SOPfoJzhmdPti525N7s2ZerC1y3t3v2Zmqjq/Z2z19/ff3T3/74H//8v//bH3//88/e9sV/OZf++vrTFfx3iG0AqABxSAGgCYgld+AHAbUVAAeQC5as/+k9QGnNAwSA6ksFiAKicwBJQJKfBghfNbcIINLUGgSIOLVViONFnKaCehGn+QRxgojTUoA4QcRpueKng4jTqmsAIo4bcvZEvUldjuVQ5ofc+LVe5nNwRL39ffP2k10twpAiEdZVh2CodGUZVBIti3lwlKuj5PTtg6IYI2sDRA6ropoN9d/MTuRkWakhkR8YOeeMLazxVVKAsJNXWVEmXc3VPtk7fVVegLqW15aIcv1yzceBqL+9Jal7f3sv88Ngv9LLOmUmGZBTBnuNBKkm9doKIp9OUSleWQLyI5+5W6eovADBRNVsCDYrlYG/0lFh2w6KWIcmqLqhUjKgUK1+QKxRVqQ60TVRUB2CaixQG2pVZRaUoyEMOE3VOUDq4Jypfa9RcOwrAZyF3oCJqHVUEss688GxI4kOhk4M31BwKkbQNgpo2+BjMgT3QDULqL/dl2idDkucB2+f7G/H64n623thUFQxYlIHO4K3Tz2LglJ05EUQ+2tWNPZRQa1QW4Hy2EsFBalfbyNBYz8VlKW2vW0FsTVhNb56e/mqfROoheSIOi8ttkDUY5fBNTEEXerUvSVlEGUd+cFTzwRpr8LbOwrDQAMiyCX2RkG+sR8JSqrXWVFNA3kBas046yi6QK4FedFyr2/vcFCTKSg3WlNB2qe7oUWNOtWZqJdlJy3NspyzoR6EdLsZibpNLto3nTAfS8j2m92LLElq2+06UIuFJl+QamQfDAS1AVK7BJQG5dOJFqRuRAqR/0pe9QUodOSrodhR9Pa91FEZGlH3X70yCFQ6as1QDw6S9kaW5Tb+Sv9eieMn+28WtSF8X+VQpbJUtcmUs2r/Yx1qG8qhfk3tdUe9d6SmPQ6oy9K0x3UHQFAeElGXrBWxkVrWbZu9QZBaYf3NHLQH6Pty6faUXMMUUrIMOXNhOxTUIVdKVlC/3jm0Haqi4DlKdya6XVc5K1jKNYvlQ7sDlcghvMK4d7NPretyNqf9tqMuZ0uj9vQhaaAtGBRp20J3gdQKQ6+B2B+CIm1N9IeOnAvWV4Ci4zAviBalKKILUBXRoqgsXtkNKovnwK+y+GTjraA89n6g3mRA+vbQI3UgfXsIwkTQtwf6Bfr2SL+gBxB9qIaaE/VhPHFU6wjOAEe1jmCIs6v6m0Al2/sEKfNdFqBKuxQVqU3uUgsaxz9B2iq9toJowQZBbXC0WYJGL0FQN3bKrqDoacEgdSuuEGEYGJ2vBsPPUTQJ8slzDGjoShwbO+okBo79HUFFtTfiVzrKHMeKouAdJQNSXiA1UIkcHwSRJSeoqx29LkEHloBiNAaByJJKVtSXAvMdda4jWwXIyRiOFgNSvQ5NkdqsHm4LUpvV42+gppadZU05iyJ1t5eikVrWDYr0d6AuzqAaCdRFHZTdKO3X/QdhAgjOrXpdQP2VTn0UIPxBrX6UHtCctkOUHtDopEevSC17DIrUu4jSG1vSVolJUcxHZUW1J0Z5X1HtAUJTaH8HQlOQF9Hd1hXUEUU428FQr3vV8Q+o80L3P0qLtZq0RvhY/4d8JtkpywggJibXtPv3Lyok21GSla3l0rTCnFHt9kjpIA7iikWbbXU6EMZxBjar89kMt8AOrtjCgmLYS/VDNkyDkAzTJETDdHVNPk/HxuTpI07UDkCsDSgYdUhUbWD8Rva1GcY7sjongiFDtybluLzSJDrDqoLBE9PZF/kE0zBGw0nljfz9RlOZDKsyCh+C1SURvjp2DBgER2BvJpNY3RThX7C2j2D5vpqZOBgu0mWlPQW3EKy9gRmEiD4I9qauxNpeMRnO4nrFzPczNIkmr1M3P1Z+3mt7RZPfZzGmY3lQF2Isj2rqBSPmZHgjGKF31PaN1DfHgEfwgNBc3VQmm3sMKfYLsALGYKUFkD1VrFuHOjQDQrSkYzMgJMsu2E9JMMzeKiMbYmNnEGLlbNVukCqrwxpl+OycsU9Whd3Gy4ebQsZNaSBsInMS89WbRxsoeUJtnxTgoiAoD40QpU7Ztw8zXuoQUnmGQYAOUEcTQMT3QcPvJANrhzqeACLCZygEmAF1DEky9sA0SysAQipGSl0qNAp8XUJpsx7CeoolbexjsSqKDvioDZHYB31yYSxPwNoUgiFM0mDhUN5tqb1PshODmPIUDQdp95QMlyJVzfr+HvnIkJGK4RDTcbkrykVW+YMjkbR5fTjPYzlyZJ5U6iL14FWlAStgd8sIC6D6MElGZ3QXpTKLjoagI08Sb6TDHkMRQrBAIsWP6VD1GxBiBdKaRRsCg7EkDl+HqtBJVTY0db2TqmxvXzGbqRGqS50HQvVSsqpsdOqYdIgXRae6klULIwOhrFrYVUFGyKxa2APNYBC5xaCjYlYt7KFmsl8WqL0OEPNwQQfGLC5Jh+olZ/GWumHRTgiIE06i+iGAkDmqo5y5q6UHoNJ1MsedHoK6Zhhy9uJ2XF60m2aOW/h4VMGJlU/B+P3uk8n72CciA60cDWsI07GIx1griwfQjR4pFfegGz0d5LL4mh1Wram4ex1qu2fxUrvjpL0niwvbIRlWFUIoOhB6QFKqKpQYyGVVocRILqsKJYZygJAqJOm2gJAqVHFzM/zYDpMOZFlyCL0COi5lCec6jJIvwncBe+hnpRCS8R1KBWqwaaV9hE1HP9XIVSXU/pUlNkTqSRytLMFhh2q4ASWzSBWqUqPsvURMWTtF9qFVhWAje9ZXh4TMYCvrgNH9EmelOJmU4VZu0go9BIgGIVXQwCKL492hDhhZh7Ec1KfIOsjlqC4EPixQPQIr7UF0OiplaJWb/lSPrfDdMhCqlS/ag3q4IYazqNHPDKFKECFLH9sTYQRUNoqEaR1qhwL0gMoGoANs2Uo7dT2EFTaKDmM9ho0GK6D6XIAFMNt70WSFYVeRpJLrYVC1UkgVQuZ7oRslqNYVyZt1WJq9CFIFHXqLZP+6ldc+XSQ16EpW10WS+B32KNEpVwpJ3UCYrFEUVu10VWEdTPkV0iRmQg1HAXv1e4upqSiE1VO9kfyujqqSCbOMZYAZsEX2UMmGe+pG0dJAo6WZ88opjNwINd9SBsKq7avJ+j60KFeekOQEQrVXJRJqD+0yKNQBs1DIrJ5HYRWampHC6jOCKUpO5YxFkWxch5rEKJIwcj3WjAY9YLAXofUb5zeKWpU2UHMkcdmhuuaAXarmtFsVNV89/hTaixq3xjipqOlrnCspiVC98JJEvZvToAYQQvpBrAoghGTIVCT3gdkFSd8Wtc+tB0yUGZ2u9XipEkJIzq6UojCppwGID2f1B4o6uK1Esd6lEerkUVET1HmV0joQaitUR6itYKUMa6pE6n5g1FElVPcIMj2hA1R/q0qw3qG6sFWchw41IqySdOuw4dRkgZhB4eQHYAYMkb/cm6zDWK00AmarQoVUXsN7QEjVuWEVKqTqbpt9F1IF1dgq/bdDTcMAQqpQrUYFUkX1cKp0jQ6DsVEgFWMYQEjFEAYQUkUdj6rMzPXqqSkAhFScYClNYDeLamQqoTJZCqGOkugaApXJIvOEHrFWPCr1mmayUq8VHEtrrUelQYcJK6W7b+/l/EaR2bHu7Q4DpeqmA1NZ2n9ltqxD9tDCqS1WkHNi9MWRCcaH6YojFSywSQoPueCKiTB1qpAMFqgVlGwwoKYGJAEMqOYLsEvVvU4ZcTqEzJ7TG4AO0Dd+F9WHllXCAKj+FWAEVL8aMAG2Zj/VlaH7U9JhqyTCO1R3q0omvEP1YAEhFec5qow4HVarEVTUR3U/qoxW3f54qxHUG+4GhYTy+6QDbpWpFmSBlasoHafHHJ4yo1v1jjIYhFRZzWaVZFeHajarZLs61CGmSrqrQ/Z9yXd1qGazSsILzqWnkA1SlZCtFFJxnqVKzqvDYrRLaaWhSAob+6/4sb3Fssy81EyoY0qVuKlHrxp0VSTPANnpChzm3rxs7iLutYfj2vTLxGxSK2coYV+H8wpYCTX7WKu+i1MttaokRVMitaqcnfhImACptJJl6JAmS9zCDqm04jR2zdJBtopL2aHG81Uczg6D53fRaj1ksxehTQOnYKq4sojQPYWEPsRBx9wqbnCHNFniJHeo06eAWLLHcAPQA6oLXWVCE/GDjH1V5s461AxcFVcHqQBPmaH/kaFKlan7Dsm7eCAdaqgCCKkYqgBCKq8hKiCk6qEKhUSfjQxVqprK7kIVCon+HhmqyLwOYBzYgrAVPa40nmGUelxpPMuMe9CcICCk6vGt/LLOpPdAslIMgS3I/FIthNTDTFh0sEuEZFKn09Og2asaCNUtrJzcH8ikI9TBHaYS6+s5zyRTaoDKpMy3deg0rIYRFqjEds4VqlHqnCtU09E5V0hrHxRyugkLWwSqdwqIFXgxmHMuMNG8R4WFbmEi1FEVEMfHVh1VIZXA4ExIrFjjVA+qIJBDUhWYB1ahEWocB3IEUhmcQkdl8IR0dQLhaEgVqsOJJhNIZVCpMiOXWggPyiCQlpNS9RgQYjRK5ZsEqo1SBXU4G6UKyiRgZ6MHxJJIbcpzTjqZ1rQVclK70VRVctZsQtNFIpnzTU1XkOTuUXrCAqieQNNRtQeMMiUK2ADVV8fEGKQqg30XvSxz1qnJYqIO1Yw0WSTUoTZZkzUZiIezQYhR1W7I9B+gprZkHg9QtV2m5wA1ddt0oMxV5+iaDpRdxmSlEIOzUU2Cvg69OBVNQkKMV9KgTQLGDlXbm4STHarmNAk2O1TNaRKKdovjRzEaoA7BTcLYDlVzZPIKUMfcJiEw5vizlXapCqe3moTPHarmNAmuO9QhBjADauQiU0qAOgQDQio/GM8YnvoI7CmzDGa9owzZMORiFCgYggVdPjKWM/BrkThF8eBaIs7aTwXj+4zfWjZM/SqG1fVqmd8vbC37PU5otUR5KzlPWp0eevF1UlmGhy0pFU1Nd1MnobQsNrOpR9FDHJndbOqNlKadt6mvUhkttkQYqr3XAyZPsdCalcGjzFoCqtvTJOKT9VQGK6B6QU3WaaDbDwYzoGb8mwSeSAx5g1hixSk1mUQGVBPaJKTtUMPDJgFvh+rzNwmHO1RXpEmw3EdEzWQ3CaU7VP+5SaCN8VJ5ljC8Q01JNskVyJowSoXBt0Z1RQAhVR9Qs7YxOn6NatibBgiVC/uaBgg1qivSNEDorS9Z66YBQk3quTUNEGqPYflTMFY1aTYPEFKlMn5XlqrpANp0XKtZ3TxASMWVgYCQinODTW11N0ZKO5arABZHIROkylTSDiFV1nWbgJCqeONZIGcBe/sqpKpEwrG3ArZhtPMKfRiOSn01k5J6fVug6e4QS+wYlgL2+rbeKI4v8oBhfJHAGPNRadJMO4QE5JQVqiCQnbBX0AGyD1aFnMHC4a34cOGiHWyFEcwlWcBYYM7FeIKxcZozT4KxNpBTT9xK06VWCyIY2x44+SS4Auu4IrgB6+APnHHVTFA1EIztFJyPEoxLgTkDBXOTIV932kYM+aJ6pYIhX+QSLGDIF7X1BUM+zkAJhnxJl+4IhnxcDgiMVYwDgzGYQ8U1NMOQl0sEx/KcdPnU4HVNJANfTD8pDuQzEnNp1BB1RaXXTiMY6y298RV1NWYwfqLU13GWSXAENr7EBew45RF7YC5YA3bAxpf4TP3XdAAGRnv1GKAEw5CPk02CZT2o8SU9puM61g/64qLxE0WfHCNh8AF96x4b9VE0v+PE+geoL4JBXbgGDPlSS4ZjkxWow2BYVqQG6mfHkK9woZ2VVy61GzxxZn0dcSmmv4o1JNSNUAEJQXYuhcG6nsLRLCvkaJwINSjunRrQD67QmYGgyGoZxLpXPzpRMcmC2UA/IWLFLGfTALGcNo7+iZQmOiSRkEYsEbbIQUpg9jYcyIsyfddKyOGv8sPNDK/AMpTh6LtcxmiljJftRY3uaBaZw0CLp1u0wzB6GtieFxiKWilD0bE02TgrW9PA7FFp7uwcfTdri9p7C0fSQsgmU5kD1y2yRpiuyQc2AtcqQhkEcqxs0mShHkyj4KYOt6hWxiJk61qe2LpWIE5pVFUcCu7M9ERitfOYKVdsXSsr7r6jdg3Vxh500xRXYo2N3aC6HIOmRgT3rtSHnGjl6GoxDTStTbpiTJrtdtgxIjhoV3YylHTMruV0KIlcQygYx2unpqbD6VDSPY00/h7qn2kqnQ4lmA0bfx/y9mBoLIe8WfsTMExXLFxX6nQoiUU7mGDIV2K298M0Rq5jFAz5Cle+AkM+rmwUDPmqO3wf8nF1o2DIxzDXOR1aepjv9Pc8MdvLSfQWEoNIwf33U1DnX3ACDmq6nYSGSHToCmknkWPHOpEt2APT1DmJO0OK/vD+AViDBcgH/rp7xvZywm+PFcJYDvliI9/i7vROxZXIwJAv6fp0wZAvxbF+aP+U8uH7kC9Ze3nRn5StvbzoW+IsHTD0MWE+1DDky9ZeXvQ9ZWsf7U+JqyoFQ75i7aPlmfNtvRsq9jrBKLgA61SIc5G4UT7tbzmY/mdi46sojgPbS81/D9b5/iz1yTmpK4KdjoNsEmB7i+PWG5tDO7AHduS7CJ/949V+H3z3GL04wwnY9E1cW2TC0oghX+F6dOAq2xLC+H3Ix3WMwNCXzJ1MgmUbA+2REx895Gr6LC58yFzOKBjyNc0vCYZ8ja6Rk/ig41zHcsjH+UHBXb4ycH+Ak+CjY8/vV8XJ+ltTzBDC+YGYfHlHrIsrBOP3q6YABONOPU6UCc7AdDW8upY9mlX98+p6Fi4wlPdhU2sPJZphB6zjrmBsdOUCQmDwXRlN4PfQHpULBgUXYO5s8LKJvWMN/gUn4FKtvtCH6nRyUnAADsnkgT5Vzt4JdsDUX+ABmPbZD6KvlRN4giGfD8Hkg773eLYaH+hPPaB1Vh/0v8p4SDDkCzqY9gBbcaTr79Vt6fGi2i8fiemqAuP3LFQBxvuKtY/2124N2D7qYNVKVxAY9ans/x1Lfau1T1I+qrVPUr6arkcQHGTjjh8x5GvJjRjyNWufpO3VaI+Bu3w9etTxFLgC054AN2BrH8lXdmztI0nWjrkvxEsONnR1TCY/+l/vfs3kgX42ziIKTsCxjp/PwAw9gCEfd2wJhnze1bEc8nnuzcD2MmAL7YDx+aiJPue1v3Z62D8qMUMtr/21ZWvPpt/P6sU63ZDX4wJrj6ryVd2zJhjyV2uPqvWr1l+q1r/Sn/FV+anWHlX5a9YeVflt1h5V+e9+sskj7dOsPaq2H2c8BWNH+2DtoUcBDBb6ASdga48K/UGg60z+hHMXhpLHcmxRHaz/SCQfB+4AE4xrZBzHS9+IGbp78ecwT8T+wEMcHP0bYMjrB9ZH/NeOdT+dYMjLxY+CIW8PJMbfh7xc/igY8vbQdCyHvBaaAkO+EL3xKziZvS3E1p+zYgvtfCIOLVh/Fmz9ORBbf+bNn5n+lee1O92cjPZNsPlblK8k3T/lKF9hf3Zo7F6lgfbLFWK2h5U7G+9Z3mPVcFzOmUvB/RUu6DJl+bzgOI6fcsx0sPG3KmYsBnkDQh3bQ4Zy4GL+CMub6ctAHMfxTLGGL+BLselHIK6j/QTGeg+zJ4pNPzKx2YNCXMb+KdjZ+NyIdc7MhYFYY77Oi56pwaDP2XlqTmcOnN0Z6skPMM7dCJrDl88Lpj4BQ0WxupVYrsXk9J9gXJsVNXEvGP90h38sD8C0R0HGr4iJu8j3K9ZIWjA2ZSaO50H8z47ZfkH814hJNJNXzg3p7mGz7wNzYaVgyJ9pn4KkQiImlgbDkD+z/YOkUiImU7xhyF90Olsw5C1MRQVJWEZve/ZC0IPTbNceMOStugpHMOSznXvAkM926wWZjcOm1PF9GfJVnTMUDPka46kg05MdB35eUlnR2769IKkuTAuTX0mFYZsr+ZBUWce0N0FSaTFwElYwTNaQRj4FcxcdcOnyBi7dHMvjoX3l95JmOwTjfZwhFQx5kq7pESybazleB5kU7vjQHqhvyMOov+AjWPxLvrANLR3xifCvHPGNACAftUeweJjtFWxHJtszFI4XbO9QDu0FfQiFOyqpL8HiX+oTHMYRQ76q8+Wmj6EydRkkfuuYe2qpv92BccYX9Ds0ay+Jv2Lgzj7BkLcd+r9i+hdB4jusAndjf46yYbmN5eiSA+Mb9uc4xMPn4ZJyf6DZg97cwTDsRbR4nfYkcu7Z7FF0Zm/UnkYulBUsW6bN/sh5UxEGcMSQz5v9cV9SBX+wP4IDxysr7+G7O/4+J3SdnhATuzdOf6IR2/hbiS0eKsTmb2fi4szfU6yzDo7nQkXOzcr4IJipdIwfgjl+ecrXHTqLRxSTX0f5msWPKl+Cg2Pjn2Cmhp0cERSTo/3F54HH+BvJ1Y6j5YuQ1we2fEP9EpcmBW/vgwuSkuUPiuJMPq18jG+bYu7PQ30UUz4cOyOY+uA9sc5ggi/Fh3gFuL8umj+v2OKVTMwd474Qm3+j8mVn8WMj9r5QHxTb+OOID+OP4jz2P8H+MN7AJcucNxScgQ/2Hi5Zjgf7ISFZpL+J3wdOw6jP8nlO04k8gq2/DooL86Goj2D2V1+JOd6DD8HUJ/CF9xW2lx4AFHPV2Went8t1bPE+5a1pjEcVs//qMUIdV2/6K9jif+iD4EM+QnHy5s8ptnydylcGxiNO5SvckDeWW/wMDJPYNTAZhstg8bKedNQHB8sHJWLGy8ByMEJN9bg8H/I9itl/IS8wVoIbxveryV8FV4tfUX/gMf4ciC3+dMSWL/DE7J/gX7DFn1GxZz7QJ2LzXzOx+a+F2OIbyucP8Y1iG78oXzB7SfkC/dlA+UIc/RHBiMiIESJUmwoERkrdpv6AkXLn+SCCkQLkalr8vuBq/p4jZn4RO1qBx/izEdO/8JU4Vqu/Yos/M7Hlb+Qwte4cWv4mEls8GoitPTxxHu23YssHqHxd/ZkPbcSH+Ecx8wO4oS8BW75W5Wve7GshPuRHFdO/s+8Hi5eK4uii6SdStM2mEvXa647zQR64HDZ1CAyXJB/6r+B6yIcptvyKIzY+veJm+h6I6T+DX8GxWr5GsfGZiS2ex77L7jINQ7R8gmLT90Ycs9lHxWZfHXEZ4xEHF8wNo/8qOLgxHnBIeUXzTzwx+QFOHSdP/0/aH9n+av0FKejBTjABRgoiWXw0EDPeBkZKIpv/LP2vY8vXYKk5cBrjQY/65B7QW3+G/Jn9xYt9hfc95peQMhuK9Res9gLm/IaOnx1bvgaLM4AP/SNAnnrInwXIU61/YLYS2PqH+D8dW3vKcXwd87wVYMjTO9RYDnnG+Fv8xY7Nfok/2bHZL/E3O7Z4W/IVyXHdr2AcUTNYfmYgDmN+GfcAuEEXjQiGiz4wfgKOwOWQ7xBs44nke5Kz+NxJPig5i89h7ATbeFPhr3ccx/wDTr907tBfI6YMnc0/oPId+6P5BsF+HP8S6uMt/yH5144t/yH5147Nf4zE5p/J0oLk7KwaJ/FNcnY+jZP4B2dEHOafgOvR/BPqY/Gszn9h8awb5yerHBHEpQaNmOMNMOo7zv9K/jC5cf5XFjElnL+g87EZ8V7HbJ9B1vJ2bEs9ErEtjZF8bvLD0dIPwU77yxCIGT8NEn92zPYZPPHRUhksHRpsKYjMf3RcUzKMFKSzpR1esbelGzKfgmwH55NlqUwPXugPATvgPC4NUlzHpUEFU4IWXw+yvQbWR1eTNULt/oCoHWefAVEZBtNNdrwlrLm176IqmdP4TQ448pkrs6pCThQ3WefeIWf1ZRV8h1yIIWvkk2cU3JrC5hKlwgFMgTFcq4RcPiar71NgBNdkbX6HmmBssnIfe269vbdXoXsfwT6MCb84VlBg4rKFJodJYXLYYAMc69sw+ZNYXxyp0qGtKW5YHSlhE5mUxWicZm2ymSwFm0UdBmI3LtDC+rPQ3cr6rfywoEvKbW1wI+RyX9kPmzApMtinUS2LKYFRr2bLsfS68R7zZ7KvMBsLumDNQkpeH5ficNBJ/QNX1OqRI/IHzqoPwf7AYWiQNbgpco0tupniMq6o0i9Y3GkfiPSzgJHaT/SDBjkQpmPOYw+ydSYh0KyGMZVkcaiVZzf2e8HYwEg7odjqKFsHEwb+aBhTUQPzWEMlzuz3VT8fOK7Z97nkc/x+MjvF8mxLzPT9yeIeypdsnpPy5yEnW6ciiy6DLSGUYxNSjyOz2UW5Uj7aEsHKE7qjqRSbOY+RDps58wwPmGr+IY9rEfiHVuwP8pJ2WFwhQtjknfMqpE3eOa+VaIfFFCClDMO4uAHl3RepJoFgm2yzcgtuXFBs6xydklRsXaNTJSjRBtNEXMbFBoKTDX4st2DDyi3YcNpIhZv1xvIacjwur5w8drIJK1ULDpw2UrXJKidHX3Scx/fp2k5buOh0bXj/Qx0lQG+Dd1htvkQ/kZnwcNX+oHucxSOXP9jRfn6wP9gc3sAfLaXZpLP+oZoXSN1og02LqQlp5jZ7OT6iYwsT5XSJhJMwbVoEzDRn0xwst8MArdz7Zm6bYHOjvbZ04xYzuI2Ce0vZNK9iq6O2RCuHaV3B9TAtK+8f3cik8jeGBT5p/cZpm0QCRj9SV+ljj6cP4x+wGMjpKZASmcgfQjiEJvqH6sfYU/4Q3TiZKA7GYLMn8gd8ItlyhMbXFov/tRv3P+QxQdmwr6bwQEYvJxx0q3gIgGSltx0G6GXw6DgfPo/yZhNOLDeHKch5ZZ13S+hKS2ZnC+L03LWO65gQl88Hm+AJWh5pM+zzY8JfbAgOoBgTzKiPGxP8aoSwuSKk8Q8OfwhHf8CyrcGPcwT8Aw/M5LFa2eOMFvlD5B+wW3j8BJae2LqtEPmHdJhY4B8OMwkQtI9w48yY4jxmphXbTICMytlbppkNhU0txw0Bl8kiWcWcCfAygnRskUgmtpUfSXG3PqaxDYsbxsx1JGbm0MqbZQJZbplk7XI5DP7QhQRbZlY/H4ZDJlbKXRo7iGJGtlbu00E+4HBYySG/Fw4rNaTcMtGsb7CVWOQj2EosXxXXeOgYgjl4UZGDrawKg77PVlaN5YeZBuA4ZnJV8ZHKt0hdcDrqKFgskbmSzMr76BSP2jeOmddKbJnXQmz8Z8WV8qkJ69gy5ZHY2iMQ5zFSFnmqHjhDk40NdINFvop9sEykYtoiV4mTt5FH8dFgKNhWXopJzclWYrlITBPtArHNvHtiy+Qqvz105+Ct/CHUVg+pEoc0emj4vmMmTz2kjnWROT3G7u5w8B0CcR6dWMUcagdt78TzEcRrFsyl5Y1QJz5aJeROlELILT6ZcFymDVXrHrVMSrdEyB0/kTAM3K+mcNxEqFATes0pTDpf2+Rcig4ZKAyE3CPbFOZxE7BA7mLBrm6B3PeZCbnrNxFq4l43sufEdWE1EKoXUj1h0I3LlIqHZldKxTVhhVJ1Z0G2zKpU2c54EMcZWzcTD2ZQyEMdEqGO80VtSre4ukdWx57M89/0wJ4OeQKEKlh2umdUD/vBPHDicS8KVR30kKEuPw+kUmXLXls/Z8LDeVQCVRn0tKoOtfWzDhSZB25nT6iRgZ68lTMXgPHkrY6L7IdKzXCVBbapEtvhcsUwT5dTDzJn7t1JdFMyfV45/Q1z7FmPhOfpcB2rFvD0uI5VDZKT0+UQE0hV00BcdOQUDKIKDyZthtWMyAmF4KakMJ6YCEwfWk5UBGbCXk5cFKztLycyCs563GcwrAl7OdFRsKqAnPiINW70weVESME8gL8Z1mElVMOqBnICJY6mpwcuJ1YCM2EvJ1oK5hHi0bCqgrgJgnmovzeslkFO2AS2Q8cHw+oGiH8nmEew4yxErJEPRQJdfF4wPDT+npMzonUCDu+TNfFcwCUYayyKepRjedUF1pBXMU8gjYbVjIZE3PToevChOIiZBF+Ko7QH+FRcJAEGvhVXmdBCe8ga/mFQfXGGg56Q6Q0rnzEY1sA3RsPauSLlq0zYx2xY+YvFsB4FHKvhJAs4BWNNSvcN03F5Un7H8sTDdQtx72HJcJQTtGUYkvcLLnrIppywCcwTQoGxxpMJcpFfsPIn9cOaUMYZUn/BQfx14UcwTwh1hlmfwXDRY5EbMRPS0j6Ctb7SfoI14RmyYZ4YmwxrDB1MPiakg8nHfUOiX1iz6vVQb9FPwbogXvRXcEqm74qzXo1QDbfCg791jXrgFRaVuJuDahhrYHO0yxt0zXoZ9PeBkWPl7iOZH0J5TdK+Mj8E3MjXYJhXSjjBZcCKVNZPMfkKhuN4Aq5i3SsI/hTrAhDwq5h8FWIegh6qYfLVDCtfcTBcA493Jm6jPRTsla9o8mEFH/VLsbqNY3lUt0swctBJJ4AEYztmov564j4yDoax5osbigTjcKGifIt8gnniLXDCefCal5L6ADfy2wzrhg3hQ7Ce8Sl84fca+c2GaR+TYfIbFeOIZ9NXxbpAIlB+x5MzpL0FK7+iD4Kpj43fdwd9VKz8+mI4HfQN2Kub7VkfrCcJhnG8VNQNBoKxfRxxgf0eznJJvDoEWE7CL6EdlzOMlO8L1jBR5AWu7O+D4TieMC3vg4EzPgSTz2BYN3wIn/h+I5/JsB4JIe0hmPpr8jHbHEy+pnwGyucH6utgWPmUcpzSHzQMAJbbfMJ4qD4xxytgXPATh1QN41iOGMf3Kc52YL+sWeyumx+OP5/VnoZiWP0v1E9wob4mw+Q3Go6qP8Ew9dUbJr/OcBtP7BZ5Ki9FaIaVX2/y1TTaN8V6Yqk3+arqrzf5GvXV5MOJekfl2Kw6GHbAOn4JxpyS50U2mTgc2Ve5KSHo+9j+IfDKGrZ/iDq+m3536213Non+YwfdqL+47ikp39Z/EGclw7j4hieay+8LruN4IJhHE1h/DYX9136vsr/a5xkGy++Bjz5gjPLgeqchHOTp/ESGpRxfOub4M5aTr8bv84C78ffSkbz4fEnH41PH3WU7jG8dt9E+4NqtWNU/4XjZ8cG+4L6u7v574xdXjEQuQBKME9twWOFROVYwWP0VU34rd+rPjd+Pao+9+ke9kPqo/lPHrYzy4EaMzEunqvivHVdn+iG4sD7qfyMlf/g+zhcM2j/oDxesb7D+IZ8v6o959e9x2Eqwzw8455MLToLGLx0zHtAT4zumPdAT8bvy6/jFE/U71vxp0FR5xzEdndCP3lzNPxfsaE80nutYFxwESaR3X5vmQ5IwHbqjCy465L0cjTAG2nI5s4R7H7zMChaYInoyDYJ7Kpbe91G4cMXLfGVBv+YwhntKCg+W9jLX2eHYiApHnwwXnqDPsAtVSBXGHlUhVRxvFauQKo7joRwBGKnecjhxh9TugqneDkdlqJAKy43UlshJk0kzVl6Ob+kwKDlyfAvW1WcrxV8S+60c34IDk6PBKLetqBbJ8S0dek8jqFBjUhmfAZOaHLgfgHpKn+7g6rBpB5ANXvD2s0FIhcOICCEGowPdTVYKgwMv50p0wL4mx0pgtkldAzlVouD+Mk8IMSoNrZwpgRtjTCrACj9NK6iQF8YVhW40QgqdKYNCb3exKaTH5Qg5YHmFXo+PslK660G2X0LGZt/Feb0xVHaehEaM2ihB9l6W7omPL0JpbhYpCOTKewgpMJgSKuStaYWQVioTHsgB7HUopE4gh6RAqD3ae0IGCJSqcryiVE27t5ft9aXB3STEQgjHwVqZbPTlde9qh+xlsrW1Q3Yr2fmKONvuW8pyQw/HbdlXXxoP2/ayrb5DdisrZbeyUu1HiI9wEpLtyWhaGrQVXNVSrhjRDcUdqj1FbCeQE1JZvxu1yVzSUsyjH32YsYKV2lkDLC3Ks/1Uc+OLUAX68U7W8WBbr5DjZJkPpjbGn3KA3u4kwxn8A31u3YPdobKhW7QxKauJfdnB3a0E8+CiKjgPPFgp7uDyqrG6PRxnT3uDkIqHbetiqw7JpKzFqoOdBVAItbPLsr4vbERObAWF7N0DobqpsqYPUKmTJX2AVO9AqHN+uge3DlyaIuv5AINd4KSQ6k2psl5UpvsbOxyHL4GFo5Uj1NR0kHNd4HkEdnZc7eGG8YZFgZ5WtBGq5viqMNBOFkJN+/lMOFpghTSbkVBzfrKgG5Bm0yuM9EgpVRyJhcw9vlJFaoTUyUo4XqqmsNr9awJ5yLdLhEqsrJMFVMvgopaWaF1DIc8MslLOVrFGldsWWV8eAQeIg82qGlUXFdqeENnCjTu4gv2yXMnFzi4n01TvvfUU3PLkeXyaK4TcjlsJqaKN321mcwSGA7ECSazX74Y4NorAbHGGQmpsIqRBplTcFeEp1SFGUBjtrk6FapDDQKgBMJQQkPGDldK9D7KcU1ar0/vDfVihu9zlqNRpLmksjRaK4SotnFPE4Ukgr/bBxiFcr8ijuLFvCDAO5pYqDBZFKIyyStkXQo0xdY9uh3VUfsDEiCcS8ua2QEgX0etPJbrjlCrRJ6RUvOJHN+d2qL607s2tvcWC6YbA6Gg2Fepo5ShVVp/QUSo7Ai3qi2zbP7b0d9g494cTHgA5psiaXmwJ4J42LPntkKNGVOi1fV3SDweuu8iEnDUvhNHGMoVKrG7e7ZBe0EBInp3CSJ49IYmlkFxLpPt2sSvP3DyFxcyXQt5ySKkSY3hKlegVUKo0ZkQVJgtQpPqcHwHEydY1W/pGLjS1i08HrK6u2A5QD6U9ljO/TqAbU58KgznbCrN51wqLXdgnEAthWX2B4zWxCqnenlCXKuietYrT4RppF6i0o1Fw12ngOoVKOLoBCrm/PRFyBVckrNztTEh99gqj8uwcIRWYpYl2Egekdch5FECcpp857jtCjvs4/A/QdvFgUXyHlWtoCG3Ji8LGgT4Sqtl0iVDbyGVCjkeFkONRJeR4JJvTcFJWYe9WyAZ1hHRcPSEThYGQfqxKlRmm6ra5mjlroqv+cXBkZJJHIe1VJWQ/ki2BNTOkxQ4Egc4CcvmwxXT8bhrTEwj6soVpTaFFXjjyFrBYOIw7QnMJFuHiAtFswVRV2MbEqELWV/aF1cJ1/7qtrcNgHpRCVjAQ6pyA7mlDQjQbsTjQ1IIpR8j6DoR+bDKBml51lbCMzS2Qu0OzQj96BQqpz5GQRwsFQhpkSuV5piKlCtR2p1Uo1Docow5YspX2rlHguhI6QA2XnGzx6pBuD+7u6JAREEurLaj3hLb/mHD0ghTSyU+E7L9ZIa6/IzkCvfnPCln9RpjNzVPIYIpSWfREqXDMAdvXAUa7xluhHpfoEyGDqUw43n4uMIz5N4W630e34yD3WplXkfdyd7pnFXIcc5FYgs74CBCH1udqAS9u9a08WQ2lgBhzCHEHC6dNdBtbR4w0i3y42aRFVtijJyYlFDJFEwkZSwZC3XGLDVsC2X+dQh74jYWiApl9lN1LHWqKBktRBSa7iFghrSiF7MTSEgoM6qq5pB9mROAiYbL7leXDkYuzklaf970C4kBiJAD4y4BVuQLEDQ88aRsQ51hX+u2FkCOO7C/rkPs5kK4E5EmoLG0pxbEUTRLY2QXymGqwAchjqsGVQNo6R8i0klcYgtIeCJmlj4S8TzcpjHoIIJpboHqqnlLx0lcrzbr6yErzaOqx8KrHg5l6hdQgb3pFXhDokPrF1TZDO/6m485o/i4WAI/zYoAtjtMIuM12DP+wXAz3K5qCCtRA2ktzdsh4Txobmx1aHLW54QZX+zDuiWC0AIhcp003iMXBtUXjsCfX6B5GBQiZOCrgogpATqXJIIE9K96sBqTi+j+fFNo8Bc5UB+SogGO3AUmkjL64LskzISUJ2UznEmfxAtbMF2HtYTd0FgxjqSluGqMqYCVqd9ssh4jbjpGt58CG645dyZZDxH3HrlTLIeLCY1d1U4QXrx0df3yv3CNMuyAef8Pl7hQSlx7j0keDkAoHdxFCKptVkRi1w1DpTeIS325SPKsQIRVXFmq03yH1SgLp7uEOlbnahPuVhjFTkBzgeM+0XOTJ49z0tDGsRTC9SriuxY9zVQqjWcmEM8+5dVxziA2XtVhKsQCOEWyCVDyYWvOtHQZH5c8QEudsEkLIkGyOQ+555GJBL0f+wWZW2m4sIPPRWQ/E8jJvkaSk5zocuyuWpnlerqSnhXVIFa2EeniIl0tAujUes8BYIecTVVTuE+lw7N1YXdcHzMx5GCzF82mcXcBCPZ/GWRms6vN5jHDkDiyGjkGumoLZbwbx3jzOsGM1oc/MIMslVti/PF7gjuoXxvMOkw0dcrUT3BRA1UlANEpRnYS7L7DZ1DYWRHrOnAW5iLN5TpyFQKg+fdArrgOnxUIi5KxzJtRFZ+hEgNzEr1fCY8WK1leurWnIMghshLyDeyAc108o5J3cQaFXV4yXa+MIY4GJUFN7MROqXqETCVTTp1f29vFUTV+U+SHcnCb1TQOh6lVyhOoQ6224HapepaCQkXOKhKpIKRH68aJVgbyktRCqC5gqoepVolSx2b2iAtPhSlNIwrnDTK44dZi1BUPREC6zBQtvBtQbzAOc3L9kyaZCHY+yXFjQzZM6H3p3Z8N53lzuCTULPAQPV2oK1KFar/JsOFNQSiuhxuR6w2bDCX8CG6HqVVFtD4332TlCHemKl64RB1Ukvb0Rp91Lx9HbGzvUlYwlEqoiAWIZ7qDmC1fyYRkucwF6jWDDJV1SWgh5MY3s7exQQ6nSFHpt/aJ9P/KIAL39Dfuxx5vjsHSYcbXe/tYhr45yhGr5AVEjxtWAqALj6uoJmy0yxhJeKA7EqHLSVIfaZHqjGaJqu2lLLhUt2oJ6G1qH6o1UmaXrUIeJKjMTvUk0IVVlZqJDXQKldxg1aKhUUE5A7LCMtyNByDpedyXX8jL4x5U3Anllm9yA06G2YJP5oQ718M8ms0c4KVHP3HaEOuI0mUtraRgvtVHI5ekya9WSnXwuxwp3qJZBr1FoiakBvUahJV5JrNcoYDC2C1LQ35Mfb6KBNUhe+2+TyYaWOOvcJAmO01qiQeyU9DxFXIfyFHhfjOS1O1RfBRBScdZZz1THRSbNIKQKej8jIKSKerARIKTirLMeV94hb6KRZGVLnHVuWTyZhGNrCCEkZ50BISQzFE1yedh5JloHCCE5r9ySQuYgWiTkyfZBYdXWb56Qze31l7nUszmFjW0kJ121bGe5y0FXuMxMDFSTnFqHeo5Nk9xWwzauSIjtC46t4BV6HWEhlUAdYZv6k9iJk1gjgbxQJitM3ECexRfNWZMsTY5/wI2aieTIthvOs1opr/fC1TNYwTCMmqOQmhMIW6M+C+TB7s0RUnMk1dUha+QUBjaKE++6cEYTEJtneAlXk1RIh9numZKtM5zClCvkOuQUplwwB8gG9QrLeKGAQjZZVFh1DJWb/r5woWikEipsRqxswRlG5RfovB2wLxtyMMSyVCCF1CgGiW3uGJHS1AZqncDMA/Y9oToJcvcoIGUeCGkKBv1lu9+KpQykrdSurJLwHme/WqPIpqDG/itH2yA0LuRZ9rs6HY7l9ucvnAtbqZMKg+2BUTjeEKSQTZYVcsLPSnFb+FEpp+WaRouNJ5A1jSW7QtvNRAg8G88Hb3K8DBb/VoPYj1HHNpLSOnYcgb3RaBgV0m7IyTZONohyaw5xMOtOzDutJP7v2O5MsnIXRrKl3G4+wkoi4DCaD9kbUsYOo8U8Was5fr2diMOgq8lBN8A69sq184LZp6S9XJduvKdH9x4z8AKWrceYwSaWzaoD20y0EFjHWGBZKzVQpXFLJnD/Qh67FjAtq+RggbPdYqZ7cD1NjxykJst67bYxyUC4pDOscpUVcB7vKtPNtFlnUWEURT4GKWokgXkDkITZwAerKfIVN35f5CvswBJaA3PMkdgaWGOTJsG1nHPrbGiU91fdSNMkngYeb0CTvS2OC/n0ehLg8Q402RvTA4VI46O4sYNJjA3Ma9AkyAamhZGhGZjtJSM3cDNrLXt54BRQPtnrg7uIaftkbxCuDDafQjbiMmJSpwKYd3GJzwHcrD66N8mNY6XsXfJ+vKVP9jZ5Pw6PxDRc4h0Bq5OqzlPHjDNwlId8nlN0tRpW6zyWc0v5WF7oi1p5JZ8DcVMncZSHnvwobxtHDqlPoPOuF1sBc6y08qh5HPs+7hg6/v2QeHcb3x/MOW5a32DecSW2m2eLYTrE2bBmSqosGAOmDxyJzQkOhrW9qzes7a23RgLTLXaGx4usidVz0CspgRnbiKPcMefFSjGs5k2vvwRmPJMMK396l6bDHVd2Wz2xDgN6EycwYxq588pJjpTb/4g1PM6N2OmMZ5Z7TYE1N5irYR3scyHmtFeWPE3HQYe/LIkanCOl9iBXYp7ElRtxGSNBYvLjiLn8r3jDmjcowfAY7ynmZbklGSYfGSEPOluVySW9lxoZC50iKZIr6jjqeFEkWQSs9kfv3gZWfdNLsLFtwo8hpOwdTGpPiiyxBFZ7UmQFZsdZ9QurM5Ng1SfgLJh8RMM5MLwtIn9mqCw3SQOTr0BM17TIjZnAh+BZ6oOl6conMfXNI14GznrntzOsU7HAUj/6usBSP57kXAbDmqrRyB5YXUcN/OUcsWSJAKkfT77StAEw9VFW2TpZ42VY6tdG/SNWxwZY6suNTMhZAGdOzGnCA1iTzJoPAdbxUrMnHfMmpewNqy+EMFaxjpd5IOYUW2qGuZG0GtYoKxXioP5JyobVPUrJsOpbkvsmgdUep2BY7V/yxDz5CjGqYtXHNBjWM4qiLIAGzrbRj1j7oybPgFVfNbfmcP6ObuyLhnUKJgbDOr4hOFU8bpTS92fVx2jyZdXHaPLl47Qg8LiRkpgL04th1T/J4wMXnbQNxlcZN6YSa/8P1r48qTpY+/Kk6mDtyzO6grVvHVc0ETvbSECsGxG9tS9vbvLWvtzo5a19a7UZAsWN2WhPfeTJ1ZhGVcxJkcEw546bYa4FqYrtblpHebDzifN6Wp6CrShV3Pkc/vrr//1/YV0Dqg==',
          'content-encoding': 'gzip',
          'content-type': 'application/json'
        },
        gridID: '1',
        msg: 'MAP-DATA',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function seventhFullCleanRunning(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 35,
        cleanDuration: 12,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: 'scheduled',
        globalPosition: [
          -5222,
          124,
        ],
        msg: 'CURRENT-STATE',
        state: 'FULL_CLEAN_RUNNING',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function secondMapData(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        cleanId,
        data: {
          content: 'eJztncty4zYWhvf9FF1ec0HiRqBfxeVFJ+maSVUqSc1Mspmadx+AOOBNpH/Jlk2W8NfndvuIsgV+PLgQBKX/Pv3824/vv//45enb83PbkFd4ab5Q0etQEYSKIFQEoSIIFUGoCEJFkGsUdcLhhT0GrKibcXhxjwAp6lYcXuDPJylaa5jroCIqureiw4tLRaeEFY2KqIiKzsCtiiqUREVU9PmKDi/w+RVVKImKqIiKqOgMUBEVUREVnYHbFVUniYreqWhfXUXcrujwIp9JEXOIij5G0eEFpqITQkV3V1ShpNsVFU3VCHuboqqy6v2KHl4SFVERFVHRGaAiKqIiKjoDVHRXRfNfrEbQ2xXxNLbOKnUPRVVKoiIqoiIqOgNUREVUREVngKPrOyuqUhIVUdFnKzq8uMcq2lv8WbmgvXeeoaAZfBcsCBVBqAhCRRAqglARhIogVAShIggVQagIQkUQKoJQEYSKIFQEoSIIFUHmiqYJ2MqnYpegN8QgVIShIggVQagIQkUQKoJQEYSKIFQEoSIIFUFeW4J1eOHOwdZkCAUt4HwRhIogVAShIggVQagIQkUQKoJQEYSKIFQEoSIIFUGoCEJFECqCUBGEiiBUBKEiCBVBqAhCRRAqglARhIogVAShIggVQagIQkUQKoJQEYSKIFQEoSIIFUFeXpqvX57++OnfP/71949fnr49U9jrMKcgVAShIggVQagIUm6a2X9K9bfRzO8ruoS3Y7Vv+8y9wwtNRSeDij5EUWWSqIiKqOikig4v9PkVVSaJiqiIiqjoDLz1o+MPLzgVnQhWNCo6SlFVkqiIiqiIis4AFVHRUYoOL/b5FVWliYo+TNHhBaeiE8Hm+oMUVaWJWfSBiqpR9faKRkVURUWfqujwnaAiKjo7VERF51D04JKYRVRERVR0BqiIiqjohIqmX+3k38NzexYdXmQqOh1UREWfr+jwAp9fUYWSmEV3V1ShJGYRFVERFZ0BKqIiKvokRcuHqOhqRfnntbLDC/z5bCvaeioVkT2oCEJFECqCUBGEiiBUBKEiCBVBqAhCRRAqglARZFK0nO6o9Kx+CzTreHgBj+e6idnDi3l+RVVLYhbdSVHVkm65SHR4YanopFARFX2uokolUREVUREVnQH2aHdUdHhRj1Y0f5BqFnBiFkJFECqCUBGEiiBUBKEiCBVBqAhCRRAqglARhIogVAShIggVQagIQkUQKoJQEYSKIFQEoSIIFUGoCEJFECqCUBGEiiBUBKEiCBVBqAhCRRAqglARhIogLy/N1y9Pf/z8819//vrjl6dvzxT2OswpCBVBqAhCRRAqglARhIogVAShIggVQW5VVOENfcwiyC2KKsygBLMIQkUQKoJQEeQ2RVU22MwiCBVBqAjCExAIswhCRRAqgrAtgjCLIFQEoSII2yIIswhCRRAqgrAtgjCLIFQEoSII2yIIswhCRZD3KKqk0r29LarmHbLfcqmxfKeiK+iaCqrb+zt9KoJQEYSKIFQEYY8GeXBBVHQF71X08NWMiq6AFQ1y6zlahZ+ARUWQty8qruIUNnF7Fh1e5M+GS9MhzCIIFUFY0SDMIgizCMIsgjCLIFQEoSIIFUGoCEJFkH1FebJjOeVBRTMN21NnVDRjWiZDRVzIh6AiCBVBqAhCRRAqglARhIogVAShIggVQagIQkUQKoJQEYSKIFQEoSIIFUGoCEJFECqCUBGEiiBUBKEiCBVBqAhCRRAqglARhIogVAShIggVQagIQkUQKoJQEYSKIFQEoSIIFUGoCEJFECqCUBGEiiBUBKEiCBVBqAhCRRAqglARhIogVAShIggVQagIQkUQKoJQEYSKIFQEoSIIFUGoCEJFkJeX5uuXp79+//3737/+4/tPv/14+vZMZ6/DtIJQEYSKIPxodMj1imzTRUO66czwPT1weOk/j2s9haZzjeoaZRplh+9deiQcXf7P4zpT0ZNPhnTbaN1oM3xv0yPx8WpsXeMqZ1Q0pRrdN6ZtTJe+x5/jI8mXq6UmYlvzrIquVGN8Y136bgZ7KcO61HZVAfJlLzIrVr2osG3Tf0naUDnV0OAfvjdnMNYN+bPKruSra7vh/5RsXWrOUsU0dQwooLQhhWIirTOMibZnLOVOqpgmZdNGirE929S2l2iSYewxL4xttWdTdimOx5aseszKxxfXjMhQnayq1b+iQqKWv7bBxZ1yjI3+QthWs19vO4aFje0+27HrhLEdu1kZ+8oFVwl7ffjKRmwFE2wGG7EPUMYcW8BG7DaYYDfCRuwDlDHHFrARuw0m2I2wEfsAZdfm2LSa58HF3acZWy7refy1UPdJtNUSnwpWRt2nTVut9zFVLJS6Q8qt6m4yWMOiqRsmsrcuYOZwcpkvCtQxkHvLRZOFqSkudXj+r4KR3Vuv0811LR7a+vf4o713XSHe/pkid1Tu5ON8cdrswUlsxzo9Oly2ittr1YY+ZsNk2lx9vwKvLc/HM7vnvZWOZ+Apmx4e3xxfV595WyvAl8PkzU6G7Z6wfw/Ltjr2uwt38/ukZl97fQnTrohbJd3ya3Nszeau2WnsLr5Qja20o91bMnj5VVt7974zDfyPFsXh/onvtf+okRo/XuN+JV5NdT36cObdydhKl3Jxy2Adc9LvzELJsNk58TTT//CXQu6Te/l0eP5d5hQe+gLc3dxNFyzn58aPfNn3PlW23LO0Xm3wwOsM7plzMih5+LUZ7/a2NQp5wLHctrOuWSKuCrfl2tE79GnSTLP9pctbQy2/Kp5D3lO4mu6cvszGZOjuXHIVc3p7Ci9nPaeRxtacqMyMvr5y9EGHwdsGN6c+ZZy7MzG6MT1aZzuYDe5Nf47Xu/e20qAYRFOf69UXu8+i0dEonrq7brqvxgnofat4ev7WCVRavWwbl7N820vaaHPb5rqjnk36Xaxl22lU674HaXtgOV4mni8sms2mXnbtVUxL7+nbWMmhlx7b5XzqauRZwxr9fXuLU+fZ4pjF8qLxh8sznwpuDXlFnszbjO9SvXoX5vGHy5Pux78ZaV/b8qHVW3yPP1zO9VQyt4qVZbrx+3w+sZLxxxJ+LAGEiiBUBFkq2mtrqmyDCkXRzkdXZDd1f67FUtb22KlcfJ5Lqv1TLpbetsfqZbnD3FL1H3mxFLd5ipjPAFea+PEXK3ObcxN56mHliW/Js1S3PTs2vwd+vlCu+rfnWcrbnahd3wY/KuVF15XC9bQ2uhG+XYpdyK22Kk/XuxaaNu4yXuTjxlqeyqt1ychltfXLKzKLNnF1e1nN114mhZcrrDfehObyzUOYgOP1q9US68v3PtLrt63hgKZctFqeT2ychZitOx+rHkYvL1iZ3WsCqTG7vOO25lO3IelWV6Q2V/rvXQ6sV97Q1q0uhV5e/rxcvcgzX3G3vAp/eeV9Y9Eiz31F3s67Gq1urNv84hnw/NRtbyXda3eA1nxr9+tGb1k5h1fWMUU3F3FuLVncSt+LNWHVt5rLhcYbLeJlY7peD67q7rTnIjb6ZL+WNR8UVfkxlJO6hYeLoeBqALkYOtY7xl4s5Zy/I9flOrqL51R+NXN2Tjye/m4upLMXp8hVXz2fLd8UCZtL6VZvt1/RSG9T2ErA/Bbcwwt4PFwCBaEiCBVBqAhCRRAqglARhIogVAShIggVQagIQkUQKoJQEYSKIFQEoSIIFUGoCEJFECqCUBGEiiBUBKEiCBVBqAhCRRAqgry8NF+/PP35/T//fPr2/NzlN5UwvVcvzRC5Rjvd6RSpHPm8LUZ9o33blsjHSOVtulEhRkYNv2eG1XK+b41EXYy8cSnK95GFTvUS6RiZtkTxycHmZ7ocxZeXKG4OXgWJ4leIG1PUp8h0OpelT880KqhOItOY+IJGIt0Y0/nyeypGOv/NPpXTGNe1ErUx6q38lbh/xrZaXiHuu7FdryXqYxSRKJbF2mAlimWxvS7bYllc25VtsSyus71EsSzO6BLFsjjryzNjWVzfSam7WJa+baVkXSxL35kSxbL0yjuJYll6OSoximXp+2IiHph42E3ZFsvSh+KlS0tslRv+ik+RbUMpWYo67dppmzI+jH/FmtFSfAVrxn2Pr27tuLdpKbO1fRhLbZ1kls+R7JFP+2dDG0pZQoxsNuGTFxuCHqKQnLnWtvJ7Keq0sRKpxsWUdKNrZ9rOjcfBxYQJ8kwTIynZcOues1KWkCPxmbd53UvJ4nF33o7b+hiJM5/yxQVxFnJk896GlFku9KGUMzS9lpL5lIO9Fmc+5WevpWQh5W5v2vJXYl73RkoWUs73Jh/bqGiIgjfTNqucnX7Pem2nbU5ZMZiiftyHWKt63xYvscb13rbl91IUjUrUxygUEzHyatojH6Npj0KMRtembbxuWzkqKbI255LLUch1zA6RiymZ25chkuOgc2RzHVM5ioktUdcEJTVneLuO2ErkUsdIx0iVts6YGFknrVuKtBg0wzPjgS5lSZHuJa+HyJX9GyIxGIbIjQZT1LdOcj69QtxYorQCNLYTUh1zqEIxM7wNQa+kCubQlxpp+nRHhuxkkNA6yQzjU9gPB061OVTSqspW7btS/BTattTSHEoaOwnlqFsJpSMxEgZlxGAKXeukY8mhNiVMd0HE4yuH0KabIbwvR9SmO0eCJE0MVQpVPqZmCLtOuSBhl0LZIyuhdBQuh6orbXwOx4TPoeRckND1Uo1ymHMwNi5DqHMSKiVhzgqlJcyJoKRUOudh2WpyU6Xy/nYutuI5TDY6FyuphD6FPrQSxgMaD6AuW10KdadmW31XXjeFaXeTjdQVDWHewdRPDaHsUZ9DrYauuWzV2ob51txsqZxmyuVeT+VUUSH6kDCkMLcyKotV0rGnUKXQlydbncJ8jOILpa26Fe35yVqJq/yntA7DiCGFaaG0ld3vJTTjHoUU9kPzpnICx/Qd2iLZIy3dquxRHO3k/c3VyrRK22lrHGPMbZg4WtCTKyP9mcpVI/Yhyon2IZS/bIdSxWFA3iMjYW6HUqq0KVTlcOfQDKMZSbM4FGhnSWicH3q5FEZXMY3y67YSjm198hyHA63U7iH0U/KnBeZeh1I10v2EPnZ2UnGSDe8Hz6laxTD29LmVNRJONXQIVWl1cyjNvGyN3aiXMA0N+pyiKfQplFLFMKTQap1D16ZQiiFbvcs7KFtDO75Q2hqcKYVMHxveShNkJYxDX9nBITReRis57MsQKIehqEsvlFovafRzOLbAQ2jaMhBx6TYH0xkZsziVQmmQY5jeMi++bNma3r7ZyvA4hummLzuOc4fQ6SAN8vDkXvo6J6F0PjY/ufTfJod+lJND1ckxGn7X29I1pq19m5ugFMZDFg+RLmEasXXjWYJLQ7ZOBsA6h7HHl+ProrpYA530CzmUcaeVULpWl0OjejkKORwzNofO97OtNjcUZatrSy/Zx6PQ9305KRi2ejmCLm8Nne9nrxviqDqXKm/tXTsrZPClx0lb43mQliTMoRlqd7SRQxn2dhL6PD5oc9jlmhIkijv48vK//wNUfEyD',
          'content-encoding': 'gzip',
          'content-type': 'application/json'
        },
        gridID: '2',
        msg: 'MAP-DATA',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function thirdMapGlobal(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        angle: 90,
        cleanId,
        gridID: '3',
        msg: 'MAP-GLOBAL',
        time: `${new Date().toISOString().split('.')[0]}Z`,
        x: -5023,
        y: 5023,
      }
    };
  };
}

function secondMapGrid(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        anchor: [
          11,
          26,
        ],
        cleanId,
        gridID: '2',
        height: 144,
        msg: 'MAP-GRID',
        resolution: 43,
        time: `${new Date().toISOString().split('.')[0]}Z`,
        width: 144
      }
    };
  };
}

function thirdMapData(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        cleanId,
        data: {
          content: 'eJztndlu40YCRd/7Kxp+5gNr49K/YvihkzRmAgRJMDPJy2D+fWqjLHHRkbzJtC+O3NEVF1UdVhWLEgP99+7n3358//3HL3ff7u9Ns0Urmvah+SJF55EiRIoQKUKkCJEiRIqQ6xV9OnmsaHpGIuevrW138+q+hqLtZU9Zb5eSzit6DW5e5aco2l4sRVJ0qaLX7VhS9NEFvY4iniDsipdX1ErReSnTjj+QpJdSNOfTKLrstP8pFU2VPF/ZpynanaSiiK/fX1LRziSdn12/lqJdSZIiKZKiD6vo5tWWonem6OW2eKc8V9H129y8yq+laPkNx3XsVtA1ito9t4TncI2iT4oUIVKESBEiRYgUIVKESBEiRYgUIVKESBEiRYgUIVKESBEiRYgUIVKESBEiRYgUIVKESBEiRYgUIVKESBEiRYgUIVKESBEiRYgUIVKESBEiRYgUIVKESBEiRYgUIVKESBEiRYgUIVKESBEiRYgUIVKESBEiRYgUIVKESBEiRYgUIVKESBEiRYgUIVKESBEiRYgUIVKESBEiRYgUIVKESBEiRYgUIVKESBEiRYgUIVKESBEiRYgUIVKESBEiRYgUIVKESBEiRYgUIVKESBEiRYgUIVKESBEiRYgUIVKESBEiRYgUIVKESBEiRYgUIVKESBEiRYgUIVKEPDw0X7/c/fHTv3/86+8fv9x9u783zSXcvOA3ExbblBS9iKKbF/S9K7p5MaXoXSNFL6RoLmyublvnB9D8FEVPp92jqrdVtMv29PaKdteSbtGKpOijSZKid6ro5tWWop0runmVpWj3im5e4fevaIeSXl5Ri8t3xssoKju7TNHuJN1C0c4kvYSi5W4/lKRbKdqRpHVFl1XyeYp2I6koas8U+WlV/UCS+OtrtaJXUfShRiMpegFFXOGXE/sueXtFN6/y+1G0vv3NK/x2im5e8Lfj2nvWPpmehBQhuvMRkSJEihApQqQIkSJEihApQqQIkSJEihApQqQIkSJEihApQqQIkSJEihApQqQIkSJEihApQqQIkSJEihApQqQIkSJEihApQqQIkSJEihApQqQIkSJEihApQqQIkSJEihApQqQIkSJEihApQqQIkSJEihApQqQIkSJEihApQqQIkSJEihApQqQIkSJEihApQqQIkSJEihApQqQIkSJEihApQqQIkSJEihApQqQIkSJEihApQqQIkSJEihApQqQIkSJEihApQqQIkSJEihApQqQIkSJEihApQqQIkSJEihApQqQIkSJEihApQqQIkSJEihApQqQIeXhovn65++Pnn//689cfv9x9u5ew86hNIVKESBEiRYgUIVKESBEiRYgUIVKESBEiRcjbKzKRm1f7Gt5a0e4EvbWiHQp6CUXLSm9pMJ9XkZmlbUU3r+5TeImOZmbPd6pii9dRtC1ph/pIkbmyYucV7XI0eulWRCJ2KOlyRddV7FxX25mkyzratWuQhF1Jem5HW6/sJYp2I+n5HW399d0IYC5TdH5kufTVnXKponNnqPXXb161l+J1WtHWROCYm1f9Up7birbHoulSZJpMfgpFU8XmF65bW6xz8yq/nqLrKm0W29+8qm+t6OYFfzuunTp+KjmF6xV9Okn6kgiRIkSKEClCpAiRIkSKEClCpAiRIkSKEClCpAiRIkSKEClCpAiRIkSKEClCpAiRIkSKEClCpAiRIkSKEClCpAiRIkSKEClCpAiRIkSKEClCpAiRIkSKEClCpAiRIkSKEClCpAiRIkSKEClCpAiRIkSKEClCpAiRIkSKEClCpAiRIkSKEClCpAiRIkSKEClCpAiRIkSKEClCpAiRIkSKEClCpAiRIkSKEClCpAiRIkSKEClCpAiRIkSKEClCpAiRIkSKEClCpAiRIkSKEClCpAiRIkSKEClCpAiRIkSKEClCpAiRIkSKEClCpAiRIkSKEClCpAh5eGi+frn76/ffv//96z++//Tbj7tv93J2HjUrRIoQKUKkCJEi5O0VmcjNq30Nb6koNCYaco3x+eFSDLcWcClvJ2psTNdY01jf2JAfPsX44nhrCZfxfFXLTrTWraKoIflxbeNc43x+uBTji3HRLnQ9R9ZxhzrqTRsvz6TZxvWNbxtv8qNNMb6Y1HV76JZPFzfrYI+9K2lbX7LqzjZ+aEKXHvGJzy+mxmfSjt49T/W37HVHXS6521g0bggck/c2/8WnSWTe2ubGe3NLT7doFv8+kvvYvBMe9cAqcG1R7pwnW59aNPWvzc3SpKOQtvLvf2IyV2maxVC2fHI8uJnc91x70jkfe+bB9XLR1Glnm69I3XETNXObs6Fu+eRk8DO5wrHaSeHBTfVxOiCeLjtytbKDFbk7HEgfvc6kzgbD5ZPj4dGkPaRNfOq5UeSjoCObp2Pmam/vNvZyOjjYPZ7yjyfGJ2pPh9Llk+O6judUH/tEe+10yls7ZItOsK9p6Yne03PRyRi6fHJwUyt8ruuvql5p9sXf+hC0GDzsflxn08ct9/T0dFLPtWfVzlHrnp3IZmLP2F5t3JsnxL0qT8Jn7ffE5konXnu+OoGo09v1k+RZ67OTJY3fe1SfxR9LWWmZ8169aO5rE4vT2fBqe187aa5Plc/OUTblv/PzZ1K/9DIfOZeNf7VHnNjaGCCO5yuzaeG1uzrjfgdzxEWjX54oN0+K880Og8P5c2lxM13rbM5tDta3L8h3e+WzGOPtfCI4r+6hpW8ox6limie1F6w0/8CyX+t5e7yKn09kkufTy5zZ52Z8IbNyLl31SetUz4cPOw+PWQl22MqXn7U9XrwvPuiF65Nq6qImfH6lqvroE9Ktx+4G8+XHw4ePoo4/Gj3nxx5dmJq1Q3NVEy8SV/rT9t/8onQHs5fFVxrTSDhuf9wxTTKPL08PTe6CMXptOrmYiS4H7a2/PQ3mxx8bni7AT06W0/aTI3FmHD+/17X/flztcy7QPj8Ey0NC59rtK87LZe/2bPpk90/zxO63xu9N1/s8qb6K9wvOe6vXUccWTy9Zl373+M35me8rNz6jXR851qystr2tb+SqvMV36CdS93dLxxm/a99OHh5rU5FjH2tNbfM75OUFz5rOnd1ZtG129dv0leu7kw/L/dHjpIFt39+wdavR6rXAOx8HyOnG3VVn7noJR4/TZrVxv83WXW4rc/wdcP52msU9fedvv6p3SC7b0sqtXet3Ve7I3GUGT+sO9/2ZzRtxzSzt0NOlvqYq7v5+0udz+b1uu75H+flce1PgrjvQ09D/d4FIESJFiBQhUoRIESJFiBQhUoRIESJFiBQhUoRIESJFiBQhUoRIESJFiBQhUoRIESJFiBQhUoRIESJFiBQhUoRIESJFiBQhUoRIESJFiBQhUoRIESJFiBQhUoRIESJFiBQhUoRIESJFiBQhUoRIESJFiBQhUoRIESJFiBQhUoRIESJFiBQhUoRIESJFiBQhUoRIESJFiBQhUoRIESJFiBQhUoRIESJFiBQhUoRIESJFiBQhUoRIESJFiBQhUoRIESJFiBQhUoRIESJFiBQhUoRIESJFiBQhUoRIESJFiBQhUoRIEfLw0Hz9cvfn9//88+7b/X36TVjX2Nb19qG5Lz8Sa81oh5psY20bcgol2bZNqSvJD1OK24XWuJrSr/P2pqspNLZr22lZF5Nx07I+JtsPNQ0xhTA8Luvjuo/LBtOGx2VD140p9Sm51oW8bCipH/I7jDmZWur8M+DOlFLbtqTOpXdIPyI8Ns75cLTMt2ZalpLPpU7L0m+uj2NfU9e4YA7LQkyxpDX5mLwZa3IxhdHWZGMa/PQOtvFtn2trbU6m9XlNV5LJNbK+JGdDTek3t4fjZTbWtybXeNebviYfUy21T+X0cTdjTV1M5Tik1McUC1rTEFOfDaY0xlRLHQ9z2/hg7JRiWUI5YinFsoTga1liNXzoBldTLEsYu67ULy0byvGLG+XU+bZ4yakvJcu/+xysn5zZEFOtUUxdE5y303Z9E4IbQ01DE3prfH2HmMauHeq7j01nbDkqNidrD+8XU+jHx+26rpbTlFRLZtP7dWMYxrrPvunb0NdjlJKpBkNJQzHflTSWFtnnZKvBIad4ONuauqYPLnQ1hZg6NyUf01ja2ZCc9V3ptynZmEoPSMnEVHvAkI5f38X6lhSPbd/107vH4953Y+63Nveqvm+9rymWJVWipliWvrbIIbWzPr19qVFsg33fB1tTLEs/DEOpe0pjtZSXDcYf3i8mW/p7PAg5WZ+OtGtLCrnuLveqwXduqMk3Qyi1dSanfsijm7M5jSG3OudKKsfB5d4xtrZL7+5CSaW1phRiGlzeSx7PRtuOpqa4ZnC2q8nFVOrucv1ik8h90+X6jbHj5DoMJYU+l3MsqbQQ35ZU2oTPI4NJJUsV9LbGcqy9K9HZNu3W+xpdrpWvP/od+tyEfPkZ8LZrcz/weQyI0QRXY5eidzmGEoehxK7EMa6QYl9jOSR+qNHlI+uHsudxzIOpz03CGDOaaeVYKlNPJT6riNHlXu3HEn0ZJkNbYigjcTA1ljIflg65T05LuzLgppjeaChlDm0pxtDlBhfyUB0L1eY+G/I4HmMob5QH+RhHN71RKmdbul+KqZxtqVEwJdbTTIx5dI/nmbptjrZ0kGmp7X3dNg/+rne1GDn6Mh5FG3ll30/qSixjXjwKOdaTTzxGJdrBliOYYxdyp00x7bkbO19XTnFoyxt1TT7rDGUmkGJqWkMZ+tK2qWmlTlBjVOfq6SqtnGJU62upchymxpCjbfOsItUoNcs4qk31zdGHeshKLANpMHXb0oCDrbF06OBKdLVtuLKtq23Dl+hLj0sxrRxK9adtuzKHSDF13r4r2+YB3LihjK/BljjW45tPBHGkKv08mBJNqWCqUVzZ2zKupfqm6PzhCOZYxh3fl+iNnbTnOOSm4kOJfTnlpJjGm8HH2cXD//4PLWX2Kw==',
          'content-encoding': 'gzip',
          'content-type': 'application/json'
        },
        gridID: '3',
        msg: 'MAP-DATA',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function fourthMapGlobal(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        angle: 0,
        cleanId,
        gridID: '4',
        msg: 'MAP-GLOBAL',
        time: `${new Date().toISOString().split('.')[0]}Z`,
        x: -4980,
        y: 2838,
      }
    };
  };
}

function eigthFullCleanRunning(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 25,
        cleanDuration: 19,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: 'scheduled',
        globalPosition: [
          -3720,
          4413,
        ],
        msg: 'CURRENT-STATE',
        state: 'FULL_CLEAN_RUNNING',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function fourthMapData(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        cleanId,
        data: {
          content: 'eJztnduy47pxhu/3U7jmWhcEcSC4X2VqLhx7V+Iql5NK4tyk8u7pH92gJJ6aOpNrdfXYe/6hJAIfgEajAVH/++Mvf//jz//4468/fv/5szk1Jzex+X+dt2bFbn/Hs21rLRZK9ev028/5D3oOonug7hTRa3rRff3OEBmiL4voNb7IEG2oriFSqmu96LshGn/U10H0oE99LaI9xNa3QFIQzb38lo+d//d99KJtdVl457N80S3//glEd/eh64H2WGw0f9PpJ70bzsO27Iuuq/hM+3il70X0ChhfApIhMkTPQfROOIcDZIgMkSEyRPswQ2SIDJEh2oMZIkNkiAzRHswQbUJ0/U8GaReIDgVpjOg9gA4FyRAZIkP0AUTvg/Txqt+PyKb9DYheB+mQm9bvHWgfr+69iF4J5fCA3ono41XdO6KPV9QQGSJDtGszRIboKIg+Xk1DZIgM0c5tiogvGKJZRNPLBugK0dxlQ3SBaP6yIWqWso7PhPTxKr4a0aOQPl5BQ7QLRI9+I+3wtgXRI5A+XsF3Ibof0scr+D5E96L6eAUN0RdA9AUg3YroG0J6B6KDQ7od0beDdA+idUgawo9X+T2ImhUkei/7eKXfg+jypNCtiA6G6X5EY1i3IToQpEcR3eO6xzA/DuFViB6Bc6g+9bi7/vKonj/pfzlI+0O0O0h7RLQzSHvzRTuEtJcZ7Usi+jZO+7HQ8RsA+mx0bYhO9z708ONQnotIf9SuIbqAcK23QDwAoOcg2g7xgIDehegS1MerfKu9E9FBzRCpZohUM0SqGSLVDJFqhkg1Q6SaIVLNEKlmiFQzRKoZItUMkWqGSDVDpJohUs0QqWaIVDNEqhki1QyRaoZINUOkmiFSzRCpZohUM0SqGSLVDJFqhkg1Q6SaIVLNEKlmiFQzRKoZItUMkWqGSDVDpJohUs0QqWaIVDNEqhki1QyRaoZINUOkmiFSzRCpZohUM0SqGSLVDJFqv36d/vTbj3//l//64z//54+//vj959N+fuf6Rnv5dv7D9XneLxTdVjBD9AV60fAJhuiNiPY61B6uy9dH9PATlT410AzRHUj3hKg5vcgX1Yrfg3RPiEbv51j7mZD2BujhZ3K976fjP4fowWdyfQ9E2yHtCNEeh9rCu78+ood/PuHrD7SHS/TVET2hPF99oBmir4zoPYCWnXWzvTRfHdE8pMurhmgV0Cb7DoiuMd383u+C6AEzRIbIEBmiPZghMkSGyBDtwb76Sv9JiPivhkhF9G5MH6/6/hEdBpQhugHR+/EYIkP0DRF9AtTHq38bIutDK4g+AecwgN6LCDc8GJ5PIDqgGSJDdCREH6+qITJEXx/RxytqiA6P6OPV3D+iQ0MyRHcgetWa/+NVfQ6i8wUDNIvo8oIhmkE0vWiIrhDNXzZAA6LlF3xzPFsQGaRNz6H51oAM0ZMQPYrp45V8D6InPOTmqGaInojoEVAfr+Y7EX1DSIboBYie8FypY9l9jzC8F9IhUd37lMdvBOn+B2E+BulAmD6H6DCgPjPQ5kHtFNon3PXB+tY7J/2DQnpX6HhgTO9ZoxmirwzIBtpLEG1/Otk3RtS8CdFOIO0Z0cfh7BvRx8EYIkNkiA6BaAnS+N/vw/lxLM9BdFn56y9RLVV1Cef46sehPBPRtzBDpJohUs0QqWaIVDNEqhki1QyRaoZINUOkmiFSzRCpZohUM0SqGSLVDJFqhkg1Q6SaIVLNEKlmiFQzRKoZItUMkWqGSDVDpJohUs0QqWaIVDNEqhki1QyRaoZINUOkmiFSzRCpZohUM0SqGSLVDJFqhkg1Q6SaIVLNEKlmiFQzRKoZItUMkWqGSDVDpJohUs0QqWaIVDNEqv36dfrTbz/+/S9/+ed//O2Pv/74/acBWzfrU6oZItUMkWqGSDVDpJohUs0QqWaIVDNEqhki1V6JaHdPt7rPHkfkJv9/vrL8W34fr/h2e20vMkR32qEAGaIN9n5EO3xI4bp9AtHHK32b2UBTzRCpZohU+wyiQ0EyRKoZItUMkWqGSLXP5IsMkWqGSDVDpJohUs0QqXaoOc0QqWaIVPscosNAsq1G1QyRaoZINUOkmiFS7X2I6k9/fbzKt9q7EI1/Yu7jFd9u70RU/3uwvvQ+RB+v6r32HkSH6zmX9u6BdkCzgaba+wbax6t6rxki1d6B6NCADNEGew+ij1fzETNEqhki1V6P6OCADNEGW0P0jJno8IC0XmSIGh3RY1X8AoB0X/QYpG+B6DFI3wTR/RU9+MKj2rZJ/76qfitE90H6EoBuCR3djZU+dL760m5B9GUqfZvdugC5HdHhsd6+Rru1wofve/cguq3C3xDRvZAOi+m+ZMi9kA6J6t580e1VPSyk+1Nq91T1kJjei+j8vgOBet9Am/+MA4B6j7te/6Sdn157x6S/5fN23J9eHzru4ZMfstvXaLusxittr+eud9QQ9/mi1/emHfXWvSLaUT96JC76JpD2iGhn0/8jC5DXFGlngJ6P6FkLk49CubZnTvpzEbIb/Xfr53wczNlegcjN/Mv5ytw6/7wBtcOvZD2C6LKy7qqa1wjOV7fax7E8C9Flb7hery9VdNqjpn3u40iei+hbmCFSzRCpZohUM0SqGSLVDJFqhkg1Q6SaIVLNEKlmiFQzRKoZItUMkWqGSDVDpJohUs0QqWaIVDNEqhki1QyRaoZINUOkmiFSzRCpZohUM0SqGSLVDJFqhkg1Q6SaIVLNEKlmiFQzRKoZItUMkWqGSDVDpJohUs0QqWaIVDNEqhki1QyRaoZINUOkmiFSzRCpZohUM0SqGSLVfv06/em3H//8xz/+/D9/+9c//8vf//jx+8+foT2FfIoJ/09/993JN6fWnZz2cfSScGrjyYdTcPgA19A/Nk7+15TPdLhML6KX3vDqzR/5ZEI3V0n/vFELUCclzn1fPo3+A+jNyftTS5/m9QKWF9LL6U3nz1kq3cKLJzfe9HnpFHdLO6K3UnW5l+Evzbm0YH714bc3YxkQNCxocPBwWWzA7Q00fNxM2W4Zhp+gPvBOKGJbXoo/oZQ4nfoz+QVYmzrUlDuVbdZTLVZp0kYbX7ZhLL4be3+F25eS0st96W4gQe/LjL6AXwWmFHa2F8814OorL275Maf+KPceUK94d6WurvwpXdO35/5cyC9CU/vVSmlnSrpxfHxyQr1lfumv3lr+CUVvL5Cv9+kCX8f2YM+/mGOkhOuv/iT+6w6yBObaecy/d+C+HEagbz/Y/0eNPhuahoupJm8aLPMA3tICQ4WWytjOOJHrd87Eb1ol0Arz/MZ32dBrxh/B842/mnjOg3Sp4ed9wKej+lLMlQlybQC5qzosjIW1jqxMxMrUytOP+MXahS7vtTgalpzRq0OgDZPxWjHW5pJJJWZmBqWDrlRX60Dntr1u5Jlxe+Na95ULgTUX4yYlGY3Pftn3LrTFOEjSuumKI1C70bmFx229XNirNlkIWN4wQuZ878jzz5ViMnEuj+/L5rhcL1wOjKeND3ca9/1pe2/yRh+cw5fzKNpwXUwc6E3CjTJ6+a19cDUDdNsgGVVwzWl8bkW3UgoOedbXI2utwe2x3pE3VHhtybZ1Fhmtw9dGxoezGsuFWImLx71t0hBXQ2Om+26o6+zi8jqgWomspiuStcHwhkljxUEtRUTjnriWDpusDeuEwSDuWulN8yqjdcXSAmOSeFnvfp9YZw+9ZHHppedFViI/SWjfHT4NDTDN3obJX9xCO9WUwbojfkfvXxrLo/Qc9+Db0oGTdXKdA+6NlC7KPNqw8JO/LDRRSbBra6F3rB4WxvBMOnrLAmqFHkN3k65+Y1K7mrt4kHozDJL5LSRt/f/yPbIB9KgjTDdbTmrGYAXVtF9Lk2xMGd3KXkmMvd5pVBs1+2Tz8KSmwxbxXLnpqxaYzqfPQfvRXPOkNDN3mHXVUyc9A1JissmWy7Mp7o3jkk1d78TpzlCU12149beAOHGsU5c6QSgvu3rxXFf/LgSvXejEeS7x4yimmZ10vyPEWt7V6xdDeAi/V45fPKdIN504+jA/xc4TyTCFrB6jeA7AbXuwhwF4Fcx45TjEU0zdQ315/P1UgFcxOdg9cFBnq60uwN6xVHw6w2GhMITY6wcTXo/xfYuWJ2O82ltY2tp4upN6MBOzLzvvDYw3u/RU+NtgHmaMyzRzHtPbMtpvAXmoUV4DnoushZKYftQuFgPattBxQp8ad4+yy0s55kdtsihVdnaO0xdnd0heEvbM5kfC6v7MQRwjxtT8ecDnd4iB4nW+brxZ+srg/2Uc2S++Z89w+VjrZSc8Q3xxdu7JJOP4XPDdpyN0W/sqzvUoONrYrjSnAF/aNdfzO/OH6o4z6Zyhupk63XeoSjPlaMv05scLiKZYJzifPPz0jdgPHf14CdXJrH6eI543y6pn4d4QYLyF5zTSfO40q6wZh2742oj3DSBn1j0V4XM2HrYda1lafe086KwUz+dkwpjfM3bA1g/1Xw7h5fM6u7a6i3h9EOa5u2Dbzv1cHOh41fbbSyliOtz6leL7MN721a/0ml3gV1Pc+P3ge0HekZLceZAzZTjOMcxuiVWMd3WPr5Uhn6c4TS9M98QuUN7nrbYf3DvGlDyDcSm1MBly7WNhpH404MAd8vJrftMQZBHo3RGdFvccaTtxkebiVzQXNhsfWmnc8A2RIywF55Euf2dpYTN8zs1tHfhb+uihh/w4gTEZ4NcnNaaZmsWvfC3abd9jOuDAn0u1zZ0gmvme+dIXsDWkj3w76Rg2TgqPJ/TZ5yjMvJQRqEuWrx00jZHOdbyZJ4QsrmE2raW/ePB0yXThm8ltnNsIm/eo21b0Nz2u6ZCddOYM63mymW7SLn1BfNvaftOXnBcfgHUYmzlWLUBG5weWkW48j3TD1/GOlNGcR7owZrcxXZioprfa9ESEmcY9VIqpIl2aWTYwXZqsxx3shs2gaY8/Wvqzcl0s9wSsct5mLfxXn3Iwm+k6ZnZ+ALtwceC6+nA47Ykmp7nzNMMW5XKI1s1sEhyl064/Jlx96OEmrAtPJFg5BzsEFaNHWzxtV/DDXBce5qmtrqZoJ+c6l7meU2BjdztsuoYJ4V1Oa9qj7adQlGXWUpx1cfJ4/Zlwl0nHkdM9O4hZzDud4fRfD7hks77qUuKuyVMl1iAvZ3TPrmKEeq9z3ZYfaOCvD6wvw7akn+cO3M7mc+c79eA2Zg7mvuX78y9EDFPj3Znl6fjo3NxnjI8Br1OeIz157tbOcg3vJDx3/n4V87qXPjuP2cPR+/HK2xhvJLxlm2o0fy6SVvv28LK5s9L78ssbvfENrlhPtw5T6ALqua/yrjuQcdJnZ555C+QNwcCNGznDJDolPXxx50Z3LccuNz6wZ3eI1WNo6jp5I+WhC65466nX5szH9jTorvAqCa/JFD/nJNzCuuuS8cWjz5acx+xDAG56Dtq+0N6e87raQ5x/pF0lfv7v6HUrjmNyLG/PO5n3Od9RjeYTlMPPDi3+/tHokUjjlS86/IoDGTnrvZ9XvCOWWH1w8yhmm/ew0ycghfHfr1IO4eLPdbNdHd2duOztYeSOaGsZ4rnZ3YfFcGHmcUl+5tFzZ/bT4/mXX2efi3Wmueu9fHFr65JP8SUzuKfLulEAcPml69Hfrx7lN/lGyehr7yuZvJnp4wjdey6lvP5Vl/kcxeWmxigGu/r7RfA78/2jyXfjl08V7uu7X9t/NPVyjbx84OYMRvmdhPEB65iuxbAUDHNPihh9g/4qzJu5125ySbf8Ru1wHGX2cb6jrxMu5X4GmJPd7GZWj0+8zyTqrl+yfE52Zycxbvt54C2PGVhe0S3vkqwn+Md5PH3XZfr65dB1PweNbmuL1bOT2yBuaZy59z77f7uaaO9pCz2XsoZwqSlGM8BkebtlrGwZb1dT/k5m4DtbYTlpOJseuPin+X+9DHmGmXbV2c09lW2Sg5kLenYwD98HfeEpPpfExqHKwmOurqLIyefMfuAo8Bltmu0oIXsP2YUnTbmrHMiVnA3D2+vF0Ozj2qYfOArh555usA+q93CdexLa5VJxJOfWkqMl/cznzH7gaB0697iDXTC9j+rkKX2X6Y6RnMuHnFNTk3TU+M/kNz+WfghkL/u39zOd+z2OafbOz/3LOK86esXsZ3Fqtpn5x91wfIQlmxt+i+P8t+u/z7/++l3XVz+O5LmIvoUZItUMkWqGSDVDpJohUs0QqWaIVDNEqhki1QyRaoZINUOkmiFSzRCpZohUM0SqGSLVDJFqhkg1Q6SaIVLNEKlmiFQzRKoZItUMkWqGSDVDpJohUs0QqWaIVDNEqhki1QyRaoZINUOkmiFSzRCpZohUM0SqGSLVDJFqhkg1Q6SaIVLNEKn269fpT7/9+I8///e//fj950/Xnlx3cqFvid1P54pKLnVQfK1rXLnmWbnYQgVWwXuoyCp2QVQi1bnyKYlVjlFUd2qblOq1nlQfRbX4ZnzrG1H4tqTvnKiWVHT1mifV9b0ofIuy57KUb3m3beuCKLzV17u3dPc2Nq2oTKrrsigqS9vXUuO7rr6JmWtUVNs0oqhk3vteFJXMh1xfSSXzqZWy4KkdvktSB08l831Tr6HgLldFJQu+lVJ7KlkIqSoqWUiN1CFQWUIXkig8YK1ppdQBXyR3lW7AN01DEEoB3ziNuSq6X8yVbqD7JRelLIHul9peKEW6XwqVbqT7pc5VRffrmkbuF6nudEnKGenuHeEVRXfvYhTWEU3RNaV+3SlSWXqfS43yKVJZ+p5LTaon+Il7XV+Uk7btT6khFQtP6jpFSWvyNaqEO1/zzJoUPiUGH0RlUp2rqiPFvbUtz17xiWsERc2b2nIHqEAqlP4C5UnFJPcjLj7l0kOgHKk+N6Lw/XTXtqyIte/a+j5qB9/FUK/hu+2plpraz3d9SqLo7rkNVdHds89SMjyoIMdWSoaHx+Tc1Gt098x0SVHP8r2rdade53secS1/ib6PlbVHN0zcQ4CPVG56UVSWXsZfz88XGdrI4xkHMv7QeKSST6JQnI57SI/xF5reybUWP10k46/HuA3O9XK/Ft0+FJ5QkRS3NBSeNNOleo3K4vqmfgoewtAE6Ustnt8g4w9uiFRIUhb6E6hR5FMcnhiRs9QBA8S7JP2ThnzwHfusjC8hh9D2UneoGKOwLkr6hGMlfaItKjnurZ5V4H4dWEmfCOV+mduhLV8/D5nJt8Unkyvn1gx4dEDomTwU1YiavWNVOl7ycg0k+i7Lp1BxYtNwqeG+SLHn4+/D03jz9Vokxe0ABVfBvg6KhnXTN1Jqas3oXK53p4Hs2NfxIwWpl3G/Dugh5MAaKRn1nuiYPD+5JrZNm0VRWVrv6vuoLC37Oii4ppjrK6ksLbcRFJWlZc8HRWXx7PlKRyLFng+K7u55VoOiu3ue1dDJ6O4+V7o0NiMNgXoNzs+F+j66exCvEcsjQIJ4jYjRH0NX6UY4lVzpkteIoa90yaPEKD0EtybVpnqN/kTpIRFeKsZUvCkUXG9upSzk3ag/pqrwOBLxIRFeMSbPrCM8ZiS3K6VOeHgTz9pQeIKc+BBSVJauyfWVVJaO53Ao3LIv4526B1TmWAOKStZL7+lY8dyIa/QPTcMEO1bSX/Ip0R/HMx4UufpWfHlfrrXSDvzK4MpcjGv0KVHq0ON+NFU19VokJS3dow7Uzq6+0pOSlsbNTl0rM0IPLl0rbVvmnK7lOa4tcxU59uKzoDIpadse7dDRRCZ3oDbqvLRmj/brfKglo7btvLQtuiopmRF69InOS2tiSJ06uExRVBZyaHIH6mddiPV+1Acpfqi1pf7ZBZkfevTdLkjb9ujXHQ1jJ4rKQkOuvo/KEmW2gLskJX6p/OochYM8jsojszpqaseKRlyXxEtljMYuiZfC5EeKPJMoKksSL4VJmhTP71BUlsTRWlseH9N1bVMV3b0behb5l45u4VhRUWnGSVXR3TvxUh18Vpddll5H3ZB8opdPoc7cZfFSCNVIiZdCSEkq53qN7k4dud6B7t57ptvBC3e9eKkOHrrrOSKDorv34pcQhp9y0zgZD0UNngjKcWQFFU8otJNXkmrFT3SsaMZlZlA+Vp40r2Q/jIeipBfwtSDzCr8vptrnoRLPzL48BDknZu3Lk0xyRz0GqmXFXHxZW/RUoSwqk+KRA9WT4pGDcLohxV4RypFiPwjVnihgKdFTCbxJcfRUgnJSPHJ8ec5K73jk+PIUYootcr2WSLFXhKKStTxyoKhkLXtFKCpZy17Rl6d29S0zg6KStdnJK6k4aGipEc1VvW99vUZl8TxWoKgsnscKFJXF81iBorJQECYkaKz0FKR4VjSO+sBjxZfndfYh1VfS+OsDx1JQdPfAowPhGN09Sot5jGkK64pfgqK7Rx/qK+nuNAU5UUQicmQFRWWJPHIwEVNZIvtrKCpL4pFTJmlSvo2iqCyJ1zJQVJbEIweKypJ4LQNFZUk820NRWTpey0BRWToeR1BUFnJoco38bt/xOIKisnQ820NRWXLjpNTky/vMa1goKkvm2R6KypJ5jFFz0fyAwSilprmjp4JJOWle6XNfvBScEh6TxqMRiu5Orq4XRXfvOQKEwvNpGumE5SLNY9IryjtdQyuKMHwsSWnDiHnONUEaMaAIJH2IIjtIGYal8CSl5aJIaboyKZOUtiszNvX9hj+qTOckebb1Za4nyY7Pl0CApLRfiRIQpGeREYVMHKH5El+QlEYrwQdJ9n4+8tMgkzRbCVtISrtF9BNHPsBJmSNKJdmHEupBStNFdD+S0nYISiGl8UqQRZKjAl8iMNdk56VGAaXKQ/sFlCqHij2gVDlW7AGlyjypQ6JUua/YA0rVs/PlJ16S5HmdH+RMMtYm8yhVL52hRKUkxSWVkJVkX0cN+QjnGleHDRWMZFtHLb3eoa/UF3vIVEcOHDGVsg4deFuUQ0YuXDguy+CBf3eSg4HMkNJFsbqHTHX8YLpxrqsDCHORkzyMPCLT0ZwspcK6xkkmBhKlanmqh0SpWp5jIFGqVtyZx/RAUsZCW36y0vnzpINSSSYF1YFM0nPkqmQzeIYiyekMnqJQoVpIlhzs+SByGOlFdtJzkkgZ62XmpdoLdv49WPKyMZyvElaejjqRMqyySJkqe5ZBBk6Zm0nySAmNSB4pwbGMvISpVyP7qno1cSEh8cmSiwhMg2aw0q/w3gzJsx1kD8k1QtapgeSOFPjhm23uo7wYDUqUuVRNae5WkgklYwXJ+bvAz+Vr+xTrJ6MYkkGAJD6+4Z4DmSE5YoTsIbnnhPKYOpKcU4B0kNxzIKlU3vFqlp/cSpKXs8GJ5PUsP2OXJLvNUKZpkuw28V489s1xsMjPGKQ1LDvkklSB5E4YSmIFK9wkZQZMRPjCCuR9y/MeP0KeZK7Y4VUo7mylCvA53rvaCvBInnR9MUrleakbSsqIZFdbAb7Oew4q8WhElCq4JBJ+0gceGpAoVeChAYlSIWMhEqUK7FQhUaqQk5CE9/ah77NIFCOyU4VEMSI7VUgUI7IXJYlJBDm38lFtmWK8pEYgUQzJjUCiGIk8hUgUIwVulLZMbRTT5noVcFIukQgkSpXYqUKiVJ3j/uzLhOs7DvwgUYyO10yQKEYnY8GXqdx3eXgv7ps5+IPEfbOMBZK4b+ZlMNZJSLn7ATvLUDs/y86JK2ApHimLFI/UsZTkAlZLRQZXI5MiIzuZIDLVyI/lEFgXKRkGL6WKbYrsVDusCWIMsvZjKQvRzDJxBrde7X1d7HbwYLLSx0cVyeOIZCbsUVb3dN8iW3HXbXkxtYGTaYKlVD+w9MNUzlKm8iRSoq9OpCwrskipPheSemgrg51lW7s3S15MBClVGLpZKXOQbuZF9jzoAsvIkzUNXpY993Ys3UgmcYxytRPnJjXqeFqExEzR8TwYuH1jFhcUS6+j4creLJY+GTNjh0TuMKeycgoci8Yszo1jUZpgmnoVpeo5oA8ci8ae2yhwLBop/u64gpCpyZ0/X02STJH3Ji8jNJVPTpK0QOYBMnDPgcSMHDjthCxFkYndSC5lTpKcQEYDMgq6vlSQxid70b5UPyXONkFivu6EFQ8rCjbLqjlwj02SMgjcGZJkCUiiUVLHATMkypx5KodEIbN4fpIoVWbPTw42o1Sy5MdVZGkb9qJytWu4P0NiJ81xdom8MUvuseSNi2x57UveOGMzzTO6etUzHEh8cmA4kNi/C7zmhMTmnizwIREmxSZXiSAq8oQbubd3kXPHkBmS+2Rkv0Ghd30vOHeRuygkSiXZAkiUKjHYkr0qG4UlL4zUFq52PEtG9rHU2bnMcrVnz4CsWJGCLpQbZVp5lBdHkTzjIL1WJKffIKmQGcMdsmMpCYB6tRWS7FSzZ2cOmSE5sInsZJBGkatAlwOHLpBIdMiSHRKxa+DRDekhhWQqTZZD30qZ0aBZFtxUyCJBFjKzpIVtKWQvkuGkRiSPweRE8goIEtsBTeIXu1KM3nH1IVtI9leQyM443zciERY6HqGJHWPveB6EzJDs3BL3jb7lTpi45/SyhkVWtci+7AIi5QopC8/Ero8ixD7KVUjZjE7sVPuOd1ogUUhZAEKiCrIAhEQFZQsaEtWXBSAkwaEFfOGckOmiQLfhva2UWMqCKHUipUaZJa1xokjsrHgOIPFiSJpxvUisa2QfFxKJrsBRH6QjGXntDIl4W7ZyU5mtWqy7ndyoXOW4LvUsZXc19fxRmWcrcr8seXqSq1jxtSKRYXPcRVNxXyR5/NarUcBmkQI280cluS9XoZW4PSWR8slRZGzi0GQkOVCnzsBS7tuylLidOyHJtnYz1LeVuD01IjltFXuRPNPFLJK3BGMnsm/qKINsOfsRo0jHoyyI5FRW9CLFt7cieR6MUgxaETUs0WTUb9o64xTJ+4Yhi+QlXuhE8ugOSaTE7VKqwNmPIOhqKC7oAnvCUHx7S2POS9CLBm3PsTdf7Xgl4vhqGuIc9FiKJp27uNoPKy+WvNTy3De8k+wd9w1aAfH6l7s3LR84cyJXa7SZy0d52YmqL5bNJ88jhQJ1ziMnvirbSD7x1dzW9A5LHyUmLLIfEmWoke+HdCfLVFM0kMHlmoLDeyl+biSthPuGIDmZJLIvw1nKHCJPE557e6D5yV+8WIKx+lESX0khQz+kOiFjM+Q6USoKiRtJ7bKUvBm3EcXANftR3ivxlefmjrIN5LkzIHsnmRMUMtYacX+OcUj+oNdRpFqTPx2ePS/xFa8XSA7Zng4nbVJT86UspVHKdNxG2dfxUWQYUqaoguzscF6UZDesYlBmCdV8iRhJSgt2iCdJcuRGMqHMHfVg7lcJZe54sPuy59dShMw51bLpR1IyRWXXj6RvZRnOknMykCgkLXAlkZJ8OTATqkQhs6zZW6xhWxq9dXkYUaoa9ZXkNUlZpIdy3iXKVg8kTik1PLUFz9Lxmi6UFDlJDgJDyZ+TlNVxSa7jlEtdpeI8Djk+Xh17PqzTyrLF80meVpYtXo755FZuVM4AeckFla0AkrJsKfsESM/5+l6Uyg/LYZw0Sn5YDgeUSmJRSJQqDMthHFlKQVJD5cdoSEYnn4zDTilwNyvHWiD7el8ck0qRJ9xQ8qIkuZuFkhclKaunkhclKaunkhcl2VWwZRtIQtPAP26QEnezUBKhJGWBUBKhJFNlVbYHE3ezUlkcimoqZ5xAS52rnMvZiI4W/CLxeolyQ0mEkuxq2qEt56tyL/0KR+JoccGc23JejtqvBCehJEJJSjcr5/VIyiRSknskh1yQwwks2YDjTCCtq4e8SpGytRVKXpTk0KAshwbFjWhFwJ0wiByWlkUmoZFEhrriYykDp+NPlm0eZKuKlBrl8mJa4PJH5VLfLBsokDgm1Mi6OxdWWeLYwD8ump3kc3LhTEFfrhJt6lK9bzmj4jjqC+U3N0j2tRho30wTcpUolWzcQOKPRLmQKFUrHTiXfoXVhLy4bDB6mVJzOTxIs0SUMpeUu+cjbaFscpNMQT4Z3TvLllAo2+NtlnC6HByDlA5cNtZJekbXiZTcZtmEbylC7KSQJcsq+xqhL7KXLQMcUMPhrEZSFn35KFoClCAhNiJ5UyCWDUwKGXiElmNwkLJ45BHaewnGWpYSEkPibFCQgKr8IgxJ7iqQxKqPHEFBYnM28liARJljiPLJaIU+cueHRCElYC6HFknKHkHxmZCytCy/xUMyBFlZlxNLskcQy54zSV5NxLIhTbKvq2P0ul72CCKPX1qndLL+LcfculjXv+jPfcfYI49fWqeEzKzKmdTMIzTyoOszh0yQ6FpuWJWz5Ngslh0TpMz5k8t+ChLKjN3z70+1nNCIxQX5Ro5HxuKgSPJCLPJPhjU+1PuWn5HxwyK9xVFZ5ElF4ryFlyYLLAPHwLEcAyPJgz2Wc2AkOc6J/NuyNFVzC5aTYCRjXe9jN5yWS51E5mXrimKmIBJvj0NyAOcTGznGiVObkEkSKbGcZWxkbwISJ0YaIRlYOoEjV1texJUjnyfsGDR56M/eUWQehg7snQTb0fFVOfMXG35v4gCynPvEgVWZYbNI2Xwp49cjCyMLBJzUdJ04qCSST06HcibYw3vJeoGlTLjlVBtat8YbOB7qcl/O8YSyiekdBdfizHGwFItnyTGyHFKOOITaygnAkETyyqtedYPnZ8m7WpBUbFp75PpeHNvxsoopR5Cpyw1ZRJZ9zV4CF0W9SXKbgNnKkbdQ9n9J5pqfLL5CjquVk3kkO45yy7E9SIFTDs+RjEP0hTJ3Q/iBA8A0x9R4A6eDyZs7mSVxdJi8al154Vxxm5M/x4Q06YtDLvsa2Lbp3fkq0Yn1Rjg4K6lsjhhJyqqN70uzRErnUnnP/UrK7P05JsToqKnsEiKSlARsCRFJnmNClCoMMSHA0jItSWoX2H2UKTWURvFRptRQWpCmrra+GKWiOL52JJQqylqS+5WveW/udTgnX9sXpUpxeDFKlaQVokgJArl7+66pGWaMBd+1Lp2Hhu9kSi1H6UlKG3Ui+dhMOV8NKTFhFsmHkAMfevZZvGhThrOXjLqMbi9J8sg/l0pLy2Euw69eungxD6I5a061/Aq0nAeMnqUcARQXhKxhe3ZBQdZl4r5oleLT2SMFWT2J6wvd4PlZ+uFGcJVdqnNKOTwgqWxxqiGHrvo6ODScuvn16//+H3UaEV8=',
          'content-encoding': 'gzip',
          'content-type': 'application/json'
        },
        gridID: '4',
        msg: 'MAP-DATA',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function thirdMapGrid(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        anchor: [
          12,
          18,
        ],
        cleanId,
        gridID: '4',
        height: 144,
        msg: 'MAP-GRID',
        resolution: 43,
        time: `${new Date().toISOString().split('.')[0]}Z`,
        width: 144
      }
    };
  };
}

function fifthMapGlobal(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        angle: 0,
        cleanId,
        gridID: '5',
        msg: 'MAP-GLOBAL',
        time: `${new Date().toISOString().split('.')[0]}Z`,
        x: 180,
        y: 1892,
      }
    };
  };
}

function ninthFullCleanRunning(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 15,
        cleanDuration: 23,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: 'scheduled',
        globalPosition: [
          864,
          1525,
        ],
        msg: 'CURRENT-STATE',
        state: 'FULL_CLEAN_RUNNING',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function fourthMapGrid(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        anchor: [
          12,
          116,
        ],
        cleanId,
        gridID: '5',
        height: 144,
        msg: 'MAP-GRID',
        resolution: 43,
        time: `${new Date().toISOString().split('.')[0]}Z`,
        width: 144
      }
    };
  };
}

function cleanNeedsRecharge(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 55,
        cleanDuration: 25,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: 'scheduled',
        globalPosition: [
          1761,
          -1032,
        ],
        msg: 'STATE-CHANGE',
        newstate: 'FULL_CLEAN_NEEDS_CHARGE',
        oldstate: 'FULL_CLEAN_RUNNING',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function fifthMapData(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        cleanId,
        data: {
          content: 'eJztnd3S47aRhs99Fa451gHxRwK+lak5cBLXbqpSSWp3k5OtvfftRjcgiSLZoD5pKJJd74zt15Qo4CHQaACk9L/f/vy3P37/+x9/+fbb9+/dRbWgH5dfFNGyFJEoRSRKEYlSRKIUkShFJEoRiVJEohSRKEUkShGJQkTmQtq8MJ+pK6LNi/KpKog2L8jnihBtXoxPliISpYhEKSJRikiUIhKliEQpIlGKSJQiEqWIRCkiUYpIlCISpYhEKSJRikiUIhKliEQpIlGKSJRuNYpSRKJeh8hMavMKfl3PIJqGMafNq/jzEK0DcyBIrYieB7R7SG2IvgboAIjkKigisRJfRbRrTK33F50YUhuiVwDaLaQWRF9Booi0FSmiCUTTVVFEi5V5XRs6GKL7VqWIZiv0WkAHRPR6bV7dT0BUTqyIFNF7EG1e2c9C9Ahp86p+HqJu5v/uTu9EdBC9FtHm1fl8RIeE9PrUcfMq/VxEz01BNq/Uz0Sk7UgRrUeE/0sRiYi6O6eIJhFdpYDegGjzCn06os2ro4g+ENHp7kiTEU1VVxEt7oCshbR5dX42IoX0FkQHhPR6RIeD1BKLFFEDolN3tVZEJw7Z7YjaMW1eqU9HtHmVPh/R4VCtQ3RKSGsRnRCSIno5ojWAToloHaATIjoloHci2rxqPwORTkHehmjzSikiRfRpUkQvRNQK6LSI2gEporMBUkQvQ3TibqaIFNHPQ6SxSFvRVxGtAaSIFNFUddcBOiGitYBOh+gZbV4lRaSIPk+KSBEpos0R6aCviBSRIlJEHyJFtAGiw0FSRF9GpF3tDauOp0RUpIiaKlv+vyJqqKwiUkQtiKQ3K6KGt58G0VdOoIhEHR7QK34f7eCAXvMTcoeFQ9IfIhSliEQpIlGKSJQiEqWIRCkiUT9+XH795ds//vTff/zXv//4y7ffviuwZWmbEqWIRCkiUYpIlCISpYhESV/Kq7pD1CmkKT0u9m9epE+TIhKliEQpIlGKSJQiEjVGtHmBPk+KSNR96qia0Pin41UPUkSiFJEojUWiFJEoRSRKEYlSRKIUkShFJEoRiVJEojS7FqWIRCkiUQXR5gX5XGm4FqWIRGksEqWxSNTrET0+r7bzC9CKaKria7R5Rb+K6N2Adg2pBdErACmicyCar8RrEO0WUkG0VInXIdolppbbHl6FaKeQZESvA7TT7qaIViN6rIgimv2SlXdB2rzKr0P03G+CnAzROyBtXuFXI3q9Nq+wIjoAoh1CUkQbIMLTKiJF9C51iqi1je1M70I0/c19imgR0E7b0M/raJtX9PMQlQ/YNZyfg+gAel9H27xqikgRfY4UkSJSRIroE6R50QaINq/Sz0SEL1BEb0B0OEzv+LU0RXQ2SO9CdCBIikgR/QxEpw/Zrb/ceOLfb1z3g3KnxKSINkZ0CEyK6KWIngF0AEjvb0WKSBGdHpAi+gBEm1fw3Yjuq7gOjCIS33MQQGsQnax7vRvRoSBpK3oZotO2IUWkiBTRByFaB0gRKSJFpIjegWgtoJMhekabV0oRKaJP0ztuBz0YpHc91XggKSJFpIgU0SdIESkiRaSIPkGKSBEpIkX0CVJEGyE6FCRF9CJEuhvbUGFtRQ2VXXtT3+ZV+1mI5t6oiBRRG6L5t50qGrV89/5zkDav2taIZFSbV+3diFpPoIiehrR5xT4L0SOmzav1DkTd01U8LJqi8U/IPVPVQwNq/a3GUwsRbV6Iz5YiEqWIRP34cfn1l2//+POf//XPv/7xl2+/fVdgy9I2JUoRiVJEohSRKEUkShGJUkSiFJEoRSRKEYlSRKIUkShFJEoRiVJEohSRKEUkShGJUkSiFJEoRSRKEYlSRKIUkShFJEoRiVJEohSRKEUkShGJUkSiFJEoRSRKEYlSRKIUkShFJOr1iA73lFEroqUKH/wJtTWITvAk2pRaEN3CODyQR7UhOiWaotZWdGJI7YhOK0UkSkZ04kBNake0VofB2obomVNPv2uH0FoH/fXaIYxpvQ/RYdQ+Adm8qFtpCdGpZmLzkhCN/+uEWu5oJwZz1WsRHbJTvh7RM+/7aLXFoladDtEzVT0UHJKGa1ESoq9BOgTi9yJ6x5l+ul6JaLcQliXN0dZ93+Pm1XmHflZH27EUkShFJEoRiVJEohSRqNchOixMbUWitBWJ0gmIqNdNQBTReaWIROl916IUkShFJEoRiVJEotoRHXAruk3rWpEiErXcjg7aytbGohPuiGi4FrW+ox2kbbTrlbHooFJEotYgOmU307yoQWtb0atetSO9HtHh2to6RK971Y6kiEStm+lvXtwttAbR5oXdRuu+7eGUUkSiFJEofapRlCIS1YboxN1METWoFdHmBd1O+rUqonR5X5QiEqWIRCkiUYpIlCISpYhEKSJRikiUIhKliEQpIlGKSJQiEqWIRCkiUYpIlCISpYhEtf88wa3fvNg/U2u2Gk/61eDr75g95NdbLunZWKSIRCkiUYpI0O3vE21ehXfr+VZ0EkBfTx1PAOnriA4PSScgor6OSFuRShGJUkSiFJEoRSRKEYn68ePy6y/f/vX3v//+77/+x+9/+tsf3377rsyWpc1KlCISpYhEKSJRikiUIhKliEQVRIdf43heiMgAIXcxPv+zu4Sty/SJQk7WXKy/2JD/aS6mv6Sti/V5QlCuuzh3cT7/s0NiJiqrsTKp4eK7izf4T/hvZxXWlBCVtxcfL6HHf8J/X2n1GrFulVlBC0oQyzv815VXl8PW5iX8JCEtaFOIynTmFlmOYTYPipuX8nP0wIuYYbc0GPGxQ3pNuK5iYNwdlZckSkhzL7wPYdofp0S4aqO6Gx413D9IW9cq3eDS6NWi2+6oDUxWaWAMSsPXsq647jqiToWmNeqN126oc+xH3cR67YOCRqxqo9L+96BRgqp5w4Imm5WmDFOabVbaBR80ZvXK0G4mtHmFXwWrrR8uVb38/3C5brIt/Nn3/tsVXUO4XyDCFGa59ZfrTtzCn51v0rU3xLSIpGBYhnfdslv4s/fdvNZAmLCGS1AKCAFf3dxb+NPtffrWkgtTCw1Yz1koBELEV3YBF/4cYPrCuwbzNbltosMsFXp/C7+6wif/PUB2mvdIZztfeGitcYkPrVut5Wka/+5/1pTvc5gbPUxu2KNWvExIaqGtZOfPeZAWfjdS3aVCJtcLDk407lskzyNsDCRHi9nXoDJq7Qa7LsYUj515sY13zUAnI48wChwi37hWalydBc73XK+7M03YxAzuIUPe7S2VNwndZMesw+N0U77bnRAwPuZ80tSjTmd2fTPm7URkesAZjYuTlOv7lkk+TFZ4ynKbi0/dg3g3rdnj1O9+PWI6mfLTA+Ek5m6W5cQ820/kNeP7F7uJ67DD6Hy7iTvRmutiw8LYN3NvQS8sD03nMRMb748XYo/ZBg9944yJumftp8uUJ4PNxApm00BaL9bEJdhjBj1xn/E1QN700Olxb80NDALeUeudetEep4WT9yZX2PE+XRiF46713u+bNeM5wKPLdKDW+1jRa52mU96lhjdd+Yf15nl+5UIdKv5OT9HMzH8vLVMIvXhuHXqOZE3bDpFSTC1M3M3dXrSI9rjcdz9FmeT4eGn2miHXpnQXHuZWhaZavUh4cpFvIpEZIUyHmOdxmxplFfNLnbdBW47HSyt5d+8fz+G70e7qPtcpuPuNgl1pZRPTuToLE0ahxvW6x8kOzQHdaLKyOaxn4GLXG0W42YnybQwQVxoFuDMj5N0a0M63+Mtj16M8an6uNWaxNOA8gff2el1TjT0OaVe+9zMBebvj7o6A+ZY1F3FrOJia2oxSi13mZLdkGYW8eTc1fj3WN90nrvZhLlFniOPHWpuC+cfrFuooHZ1dyhHm/6PTTC1BUmOcnBo35x8frvsvSanNbGmJTOD62OanJr1+akI80Tn2uIgzRbYhXZLJLgbW69A3tZRwyCbbsJDVhFbMB27f/bh2cLgo+7h+9YZW+zgzGKXSR0sJZharXhhop5vgaIlgVIB9p7ALS1MTe8LP5AWz7e9uZWB00/Dep10Li1GLE9t1UKe/0cdcHu593/Magbz+NLdw8kz3l4cfs6ccagJn601i63nObZXvN1N6KdBVC9rC/u3oHoYddu2XIF1Yv3qije427dyO6GIjPXDX36KV7n9G/0Kma78xZuluR0UqbYwIOel5hvxnEtJ5nlOZ+ZNZ6W4lT5im9pZGjwLOzhrFadPhcqeFCf3y7W63t/9MrmmIN6TseGb0yHFmuWlh9BgegM81tMfHqeu2/hFm7VMw29ZEb1iO4sBcBJy8SDePpB1gSWka580az9yzMnXrfWqcmn6YWlq7O8jK5xxSVEumuLzkPG6n8jrzQZbpF7E2zGlmbp6YzigbtkUOs680D7Z1523h9omHtaSmMx5oP3SebvuW8cNT/HPhQGy24v7pnieo60PtxKM/841uOdJK+6fHabULD9De3+LXvHe6nBgcaAt/CWvbkvzs87RTzW0uf338fpqj5gVNTGcfnp3Kt27mGnMN9tAJbBvRtp5/bWlNd0scbpLVBHTqW3ykLZDmu3omb2fdmVbwvCbvs4nQ1NjSmgfvPoNajVSaty5sJp9rH+o5og9p60KXP9lm6XNAG+ZQTUDn37ZbPQF0MhFduHuk4SaeveefX6C5YnzP5zzNXt74p2LFkWPF0E46y07eq0gu3B12jCfL12Bs6dzP5Iz7f358DUXxa9nGFFuzmn0/Jb4Gofyla+M7xNclMrtPwgV8C8GwwloZDQ+tFVtFbfwOMM97ll9L81uVHh5ea2Pfuozw8JoZfaeXDOcWEM8X8yZy6Pn7CzXYLYGb2/U76i7qa6hNzbAOu0P6KmTjydSBnl5+F7CbidP8PZDHmdJ/HRdp/u7bE4JaRnWYB4lfoWVQJHO2jOFeLYhOLkUkShGJao1Fd9+RunWhf65WjWs3X/p/2JXo53FN/vzjIX4m5fXEFlLym6z8yFtv66ktz/xub9qc2fk9Abn1iwzX+97NzN0H52h1TYta4y/444WtinH2dweOvxjRupo688VWNzTHu5qTXxp+WLUv5z/ui5iF/ze5Cnv4kXjd3tLEDt3U4Zkb4U8xSK/b6rzd3wzCj9GO7kA+R/S8xdn0rcDXL5ye+gHlx5u+zrONcPfI+/1XTs8/ZfX4YPXUvcULEfSo+1hXmrNfND2J8v7h/8f7rRcj6IG7+g1P6Xv7b2Hefj3FcpidePuBu3vB2XKz4d0PtUWeeM816ccR6RQdvvBcftZ6fMsI5Tkz3/B/1jsnKkrxsajFb6o5/b0UxLHxeaCJwWNm4WPuSdeDt8cvPFU2Wnc7z9f4jRkKzz0tDbplvfckD/rM8lt+1GlpsneWJ3vm0K34avexzvAkzxy2L9T+6I/tzCN7uu5n2hj98ePy6y/f/vn7//znt9++fzf2YjAERW8BJH77K9q+S74vNlx608WIFvDA3964vloHNpiANrDtgynWgh1SPnPPNrlYLLzYO+eLhTP7GEOx8LnBdQPagW2gYgxUyDDYVOxw6fvO9sVGsDbZYhNY74q1HdiB6ovWgI2hHoVC9in5YqGQgx38zdEU6IMiHU19n23KduiIFXwIWddjIbEdQnMcjE+uWDhqexuKNWBjqi/uLoMzGR1aOPvgnPV85mx974qNYKH+xQ5gB1+P9pfBE5xsA1i4SMVCqTxxzhZK5Zlzypds8IPrioVChs5UC4WEi2/YdlCqYFO1UKoAF4nPnG1KpVQdFBJA91xBtEPwub6WbR/zex3bRPX1ZKPpQrFwZriAQ7HpErtuSGyhkLGzprwXqhA7F3yxFmzvyqmg+rEbhnIqgIOfE4sNYA2V2Wew0bjcnrGQgD0aT5/r8kWJZui7YqFUcD0HtoAg2s6Xo4AgWueqhVJZP9QXQ6ls3/lioVSWekq2UCpHlwxLhUc9XQUcLNACm3yqni1cFbQD20hVGOhUIVEVItneUhXQQn3xAt8cjdzaI50K6ueLhTIn5hypRokbf6T6JsYeiUZKpmcLrFLXUSFjJpk6k6odwDKcmK9C6nprig1gh1pmuIKp424V8/VNxl1fbMFyP4q5bSQz+FJmaDnJxFpmaFfJdjmMZAulstyPYm6Tybqu0OigVDZUOB2Uysa+FKODUjnqR4gdreer0JMNUGu+gtnyVfBsgzela6DlnmLZck8xZCF8+2Kh+n3sSt9HC02nvzkah6G+GOBE4FEswEmOPrfMv3oqpGXL2C3PMGoTzdZ4zx2H7OC4gmRjz9XP1lY4ZG3iC0rWU8fhB4NsGLBUjve/LDRwtIYtxRzHpXImv9dZnkq6mKuUPVYrWO+Kx7/B5Vpkj7hD31ePVyPE3Kezx4sVkq8er2Vvc4dxFNTA04CQfW4nwdTyYPng9D17TEq6nsbP7HOc7IZSXpPPSaEt+xwpqc9ln0NlX+uLQzIGt3ocyzfQFc4eyxcpvmWfW7mrfCioU4RDT1GdLnr2WD7omrb4XCcTq8fyJWerz8G67wrP3NgT9b/ssXwpVv7YVeCi2Xo891FX+ds8QtGI4wx7y9e7K97nbCX7HAEo3NXjgSJaPR6GHErr+UJy9vbzegpqkJ0blwdb5okeB24Yp0PxmTHzQp9HicEPt8eTo/J58rajnuBC8dQVXF88pFDFYxkDt4+eymMDAGCP5bWBy9tT/WygKJx9HgGSqT7/pTicPQ4nPQViR2MJeG4Pga6v7eEFxedrCKG7eCzfQINe9li+gdtHoPZlB24fgdqfHWjcyx7LB1lCOZ/JdeD2EKh920ghOXssX/TBFY/lixSUHaWn4CkqZ5+v8ZW3yW3ChFIe7J+QVabqsXyJg0+g/g3NyVafmcdQyofxAaJRKuXB+OE6Si6zz22I22eg+OM6GqyzN+hpnMgeJy3QQbi8GN6coYECLk+KaH3HrSENaEPgi59ye4m2HM3NK5WiZus4Enm23PAdWd8N3A/Iuhp0s/UURTq2PY0biS21MQznaENX8hGyPNRzIYOrA0W2POZ4tjXDIstDPRcyUtaPFlnDtS4vRtSJ0KNF0jCqcs6IFsDlhoyfixaOd2xxUdIEVwqZLY0KWIVsY0kEsrWcJSW21nFIIkvTKUSXbR0QyPaB4wNZvgqerKu9nazLVXBcDM99daAqBO5qfDRQ4oO2Q8uBNpLl6RQcjQkth9nIlgZztBEtjd4ukR2oD/mOLV0Fb9jG3rDFUyVqovzi0PF4S6cKhsMvfVCwPNoNbH0JRmQ5lgW2lFECq2xd13MkJkvtCjiT5Thm2FIzc1wqR1HMcql85zmFJOssX32yvue2QbY3hi30wdBTwEALLTYMgVoO2IA2ltZOlpNxsNCA4RJR6uqzhdBsuutRmJdTug2nwi/OL3PNgS2MPFzmbLkPJrLWlA5LluEYtj5ymyTLMd6xpU6HFovBU0/HhfQQC9jii0ONp2RppoLWoh0MByiyqQxWERdy+lDGoghNFDKjoZwKbeJu5dnWeIV26GqeRJbTJMOWR8WOLbNKbOvUJFtT4xVZVzJ3sr5esmz7esmyrfEqW0vZkuVSWZ6awKwbusZQZnWGbRx4mp6tp5k32oiWY105SiTLUZgC8rLEMKClUQMtNMKhTPJweg82UkTCYmTbew6qZCkjxeWMbIkz1DfbxFPtni2NC3agD4IpIM+uyPIUMLFNJTdDG3kKCNeILORCdAXJchN1bH00bHGJ1HJEwuUMsDC2cRMd8O51N1BShEswaGm9B8+MtqcVHfxctEPNCMm6gUMBWe6DkS2zGthS+EIaaCOFa2SVrbWFZLbcf7lUEaYKfBWwzHGgSZ5hy/23I5uo/+aFIbQEJy8MofW0rjawpeBmqJAw1c7RzFAhk0v1xWg9xedy1If6YoM2Wl45yzbQXBNth5YmU7g8l9CGskTXR7Q0VcK1PGiTiWdKeDRbalfl6ODLmhzZYeAKZhupqeBKX7acQnRsebXAsK3TVrK1S2abeFXGszWRAzJZZ/n6kqXZi+Uapbp4QDYmnqeChdbLqWrHllYLXC4VWI6Tlm2ggOzYDiW4keXmHehUhpPWni1kcmzxxc72nCT0PdjAM9LAtg8crrPtOaOAzw1oeUi1ZAdaDEPr0XKC4chCJ/Q3RyNnFIZs4sy8Y+vz1Uc42VJ9EV22VF8Ei6v4PAuCfkSWQ19gS70bL1m2NesjGyggW7aUkFvDNtWmghYmCYEbUra89BvZ0gBkuFQwMvAqco97pJaWYbDFovW8ag7WouVVc7AG7ZCKxd3FQOk0rsUnsD0NuHDmbHmSBMUgS3MkKCRZXhrs2PLSr2HLyzCWLMyPeoJDlpd+PVtepAlseQDiUkEMMnRRsuWpkuVSJb4oXKpEV8Fxqco8iUuVhjIc5+onXrNx2UILdjwJJstzJCqktRRG4MUBWjuMmZZjexjQcgLpcJMWLK+feLbJuZujZRYDNqClcR9fjDbUPCfbvitLIfm9Pc9UDVtOpzu2HBkSW74okexAoQBJZmtKEkiWQgFehWzr4i5Z2vewXIUhlsE622g87yGQpemq5VLxhMhwqSJdBcOlitQ1DJcqUvaFFsDCzLSL16POuLJXQ7YGc2QFIScXoxz1PB7xeyP3ssiW5rxcKjhvGK5ldjBBH641gllbyVXyByVe7LTZ4qyNM6jg0dIAhKyydWVJMuAtEV2gQOHZ0riP1qKNhhObbC2nLj1bThEHtjVFDLj96HjTILGt2TVZTqcNW55rWDqVq+l0tp4X1LhUvq6XkOVWx6XyvFrCpfKcike2scytsg3U6mDmRZZWnjwXkrfXPBeyp3U1tPhV+JHWp3E3P6GlrAD397Pto+MPwi3rjlPxSNZwpxvYcires/VlgQRPBWUsoYBsXV7L1nKKaNnWaUs+lfVlbY8sdWfLpbJ1WyBbx3M6LpWj3m25VLiEwY0hWw65nm1d6s4Wmt3AjTBb7t1cqutuW7aBtiItoQu8kQV9IdvEqTgfTZxP5vt4oCNYwxOEbB3v8oEd0PJ2BNgeLccNsNBTel8DhfdoaR0Rz4w2UA8tRwda5YYaZVu2fRxbisBAg2ydlpKlDWEgSXagiDSQTZwSR7Y8fUhsuW1wqXgHyXGpUl3LJsvTByrVUKYPgS23jZ5tX9Y3yPKybGTLq5Yps4IZX23e2brSNchSH/RUKkhccgjyli0Ni55L5bgfebYpRwa0EIIG3gWC02bL+zzlaIj0uR7vPLIDb+zgmdFGQkcPWYClbUx6/s8OPCOgHw4DyzEnseX65nuaLMytaGFwYMtr8j1Zw2tBfS5VNHVFlSxUgjssWt7JwYuSLc/3LduaFZDlrKBjy9vFiayzZQWDLI0a2JCy7cuyIdm6kJKt59U8wh69LTk/2brzTJbCFzf+yJOachRmMf7mKHYVtvhBkVcD+GiZTdjcVHCuzBt3ZGvcwB6aeAqAhcw2RE4DyHL/7XPvTqZukZHl7Cuy5R3gmCNDguyTxyOydceMLGM3bGNZG8GYk0zdQMu2bKg4ttfcrENbl6vJUoaMsd2g5cXrgayzpdORpfzZJba02MXjEUxLSy/D0SrBeOyptZOlkd07tnSNvGdL4asc5RmQp3E/8ZQHrggmCTDxzKt5aOEaQaKahwlPyRjMS3x5MXLGDXG0PdlEkd8PbCmdphsGwVJP8TnPgTDhc86AFu/zMnQ3BFqPNuaxGy3e+WVoxcbnsRsCPaHzmRVYn+/+8JEs58A+D20wsNHKmM9jCliKzz43BodLX+7Hj//7f4VFOVg=',
          'content-encoding': 'gzip',
          'content-type': 'application/json'
        },
        gridID: '5',
        msg: 'MAP-DATA',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function tenthFullCleanRunning(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 5,
        cleanDuration: 27,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: 'scheduled',
        globalPosition: [
          11,
          -8,
        ],
        msg: 'STATE-CHANGE',
        newstate: 'FULL_CLEAN_RUNNING',
        oldstate: 'FULL_CLEAN_NEEDS_CHARGE',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function fifthhMapGrid(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        anchor: [
          16,
          72,
        ],
        cleanId,
        gridID: '1',
        height: 144,
        msg: 'MAP-GRID',
        resolution: 43,
        time: `${new Date().toISOString().split('.')[0]}Z`,
        width: 144
      }
    };
  };
}

function eleventhFullCleanRunning(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 5,
        cleanDuration: 29,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: 'scheduled',
        globalPosition: [
          11,
          -5,
        ],
        msg: 'STATE-CHANGE',
        newstate: 'FULL_CLEAN_CHARGING',
        oldstate: 'FULL_CLEAN_RUNNING',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function sixthMapData(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        cleanId,
        data: {
          content: 'eJztfcuyLLlu3fx+haLHZ5B8k/dXOnogSzdsRShkh2154vC/mwtYYL3yVbXrtfepyL7SWZW1K5GLJAiAIPh///iXf//HP//HP/71j7//+ef067td7pnXX7/+9qHoQ9GHog9FH4p+LkX65x+Kdoj40wnqz9tD0eaP/O4U7fiRH0vQLop2/ciPpWj6WRTdn7xpD0VX/NAbUDR/b+97LP7qVyl6NkHrFH3lTW6iaPePvAVJDyLoHhQ9m6Rb5PgyRXMPuOGnfiBFY6BdPuQmtn8Pir7wQy+n6DZafyuKlqX44pt9KNp8tw9FH4o+FO2R4Ytv9lMouoXUne/2/ShadlY/FG288qNI+oYU3SbFh6IPRR+K7kTSrdQ+haL36EePIOhD0e9F0W207rh+DkUPs4x+F4q+QNI8Rd8zNPsQgpZ00YeiDYpu+9FXk3QbrTdRdOuPPpOi53n703ftRfMSfijafOlrv/9lit55RfYa8/Fw50aSfpIumpPi9M5NJP00ir72Dg+l6LkE7cnAvAs9cxTtF+K1JN2NgGsp+ooQH4o+FH03ip5I0oeiL1B0nRA/lqB1it41IfRbUvTb9KKvCfIDyTmn6Kvi/DyC+Eyj6OsC/TSCvgFFz6Vj5Z3uN9DuS9KryTl6p/up63tR9GpS3p6iV1PyUIp+FjULFF2+6LMoejURV1H0FVf25xG0uQLyoejOi0Qfij4UfShafKOl5Jln6qI3J2kpS+164W8n6J1JWqHoFkPyayS9J2GrFN0Sg3wnik5/57bfnN6HovO/vg9B6/Jv4Yt40Ssp+johj5BjfP56iu5JzFcIWrzW1fXpqz+j7b5G0UOuvanpH4peKOKHoidS9CCS7kfRrSLck6KHkPSzetFDSLonRe9B0g+k6L4EfSj6/hTdIuCbE/Rciua/8dtR9PrrQ9GHog9FP42id4ohvyVF+oVXv/AbU3T4yqtf+auELRN4+GSV3PW0h/Of+u6EzZG29fYbq7GX16tf8BnXh6JrSfpQdBNFa/r91cK/BUXrU+CrRH4rit6DpPM1vJfs/riVoscL+w7994sUPVbk9xjiX9BFjxP5dY1yJUV7CXpNtPDw7e3fu5s0StF+Yh5B0S1Pv7Y4zxfkuf2sxlfSMyfBvm9v98SLv3wdRV8h577XhjxfO87y+9Oz4/rqcZY/nJ5bKfqtCHomRS9/1fen6NuS9Exd9PKX/VD0RhR9BtqDKHr5q747RS9/0Q9F356il7/mu1P08pf8UPSh6NXX4yl6+St+KPr2FL38BT8UvSlF71w19E0o+q0I+lD0EIp+Kz10C0W/HUHXUvSbzWUfij4UvYai3242u4WifSS9/LVeS9HhuiTkBxL0NYp+k+tD0eb1oWjz+lC0eX0o2rw+FG1eH4o2r7/++vVPf/vjv/+X//WP//l//vGvf/z9z29G2DUu0V2u79enPhT9HIoOP/GhaJeYvz1F5zRcEvRMkt6YogMdl9czSXpDivb+1A8l6EPRD6ToDQfauxH0TSn60craDrf8mkDP60Fvpov2/sQzCXoJSV+n6NkkvZiiW+t1/HiKLl/32p95N4LuSuUcRdf/zDtRdPc+d59CmO9D0W297jem6A4E3Yui9zEdPxRtvva1lH4o+lC0LcmHog0p7vFmP4yiNx5ozyPoul50F5J+Vi96a4rewzJ6a4retxd9maSfT9GX3+1nUTQnx4eiD0XXkvSh6DtT9Jn034iitZf+UHQTRQ+yi2754Xeg6CEkLddQ/xkUfYmcn0/RHej5rhTtjTx+KNqU52EU3dpN342iu10fij4UfSh6AUW3i/Gh6EPRh6KdFH1FjA9Fb0XRU0lapuidU0I/FL0PPfel6N4knf/mHb2u2yn6amt9677y3Sh6OS1LFL3PQHs5Lcfv9F696NWEzL7V+1D0airemKJXU/D2FL2agCsp0tc8vOyHogt1fUrUMyhyNz3nySQtL1g/i6L3vhZ70bMo+hYkfXrR5jvdk6KfSNKHoq9S9NFGb0LRe5O0StGNP/ijCPpQ9LtTdJ/nfyg6er7++1yeH0rR9c9flutD0Z7r51F0Z4I+FH0o+lD0FJJ+IkV3Jum+FL2Lr/+h6Hn0PIKiWwy3346iJbH3fPYbU/TM60PRh6IPRU+k6JyuufsHvELuz6Poa9eHop3Xh6Id1wZF56P197t29KIPSbsp+p1Jclu66EPSFRT9vlR9KNq8duqi11P06mdfQdFh8e3JIr6QpKspmn4tLes+jppvR9FzxH2fYf4Fih4j9Osa5WqK1gl6hMjPft5VUi0XxHgXeq596r0kHn9//Ymfz6fn69fpM8+fvCHT6yh6Fj1fvq6l6Pv1oCdT9BsR8yqKXv66j6foqzS9/HWfQdGnFz2Uope/7Ieit6DoNyToQ9EbUfTyF/1Q9O0pevlrfij69hS9/CU/FH17il7+is+l6Dl799/sejRFL3/BD0VvR9FvSNA1FP2WqvpD0V0p+m0JeiRFL3+1D0Ufit7n+lD0oeh5FF2fvvfyV3s+Re9bi/+NKFq/fihB96Tox14fijavD0Wb14eizetD0eb111+//ulvf/z3f/mX//wf//aPf/3j739+CFu/Pn1q8/pQtHl9KNq8PhRtXt+Poqe7Onsoei//6w0pejcX9S0pehUZ89J8KNq83o6i7VZ7drToDSnaEtg9Vey3G2j7NoU+U+Dl521tRr75mWsUvdtctkbFuqwPomiPjnlF1Hr+ifu20d+Vov0v/x5D7YEyzFN03arGc+ezt1DX14rxDhQ9vRddK/bjKLl81lv0ouvFfp7At1H0JQnvQ9FvbDq+o9i3mI5fun4KRW+vrl9P0X3/4uT6KRQ9UIbvSNG1Q+2L8n23SX9NXT9IS33HXnStFG+ii15vGT1Mhu/Yiz4U3fi0NSneQF2/N0VfVAQ/h6KHzaz3WdN//bTvHifHT6Hozd3YdwiHPNDwuKTolke9i/G4/PmL1fV3oOgL1/o62tfFfsR1vQvypes+FL3HtP8g4/FnU3QXub7jQNsb+DhW018g6ydR9KDEh+9K0ZLED1Dl35WiJz5vjaLrhH6ayM9+2nel6E160WegyfWhaPP6ULR5fSjavL4nRU993npS8VuK/OznfUeKnpzKfD+K7i30HBEvqVHy3hQdE/Wy8i33ouj52uhp14eizeu9Btpb1kJ6p170piWj7rfZ6o1e6r7XO/WiN71eTdE3IPa1FH0Dgj4U7bg+A23z+vSizeuek/63eOHrr9dT9KYG4+HaUxBjn/C3v6C7oOmtyNqq9nBN+97+WudP+TYUXTcEbh9oLydh/Xp9L3pMtPKaa+6vHkDR7cr2URTdRuuMPFtr+t+3F93tF7fKqnxHiu77e1+sPHP+/de/0gPmwns6IO9B0T1/Ta4PRZvX/Sh6B130EJPzZ/Wih/h5P68X3Z2k+1L06pjRgvH3tesdKLrfK725LnqYiK9+/n0pMjH3O4r3HRhvTtHlS8+Tdb1jc4sUT6Ho66rvFYPu6b3o7cNdMzLf/Tfvr4tee3160eb1oWjzegFFxzR9d8Iu5V96oyti14evu7k/fsvrsA7njvASCTs6wLWlDN6fon3v4M7w6f0re9HveJ2Q9KFo8/oeFL10bn1nih7rze2+rlfXzxH3PErwInpw3dKLHkfTeSTghcQcrlsH2iPCGG9GjV2366L7r6K+JUHvQNHjqLnTL756Rvs6OccuxPJ1qhqOo6JLVI5Pb9dF9yHn2sSK7WvtSft+4ex3XtWLnqt1lvuT3l2V6LkUrfneb3s9d6C96Zy1ft1G0e0EvfyFr79us65vI+lbEnQLRXMKb/9ffsPr1l50/fVNh9kzddE3Jei5vejlL3vb9Sy76NsS9KyB9m31EK7n9KJvTNBzKPrWBD1noP12FP1WegjX4wfaNyfo8XbRt+9D11L02w0yXI8daL8xRfte/EcQ9Nhe9CMIeqwu+k0p2v/aP2SYPTKR77elaO/1Ywh6JEUvf7V7XR+KNq9Xpz18g+vTizavD0Wb14eizetD0eb1Udeb14eizetD0eb1oWjz+lC0eX0o2rz++uvXP/3tj//8j//45//zb//1n//Lv//jj7//ucJZ+uX6hB5+uSj/Fx+8SnQHIXz6FeKv6H6l3KXpH06O/5vwUb8R5EuQ9yFStF8u//Lul5fH+LjW5c6/i3/3T9pvTGCnpIoM068Q8KgQlhmc+TL+Denqi1h8PYfaqboE/lcov+KEx8VFzTcoPPk2/t0/gYh5ZkQ/Pj/51TQe96xOi/8VqzyyzvOYTjk/fB3/BpeTDO2zPxmKMz5Dffbf9zJC0Mh9eLQjTs/4PLT5fdo5XXTJ8fzWwOjMPnxh7Jj+8QdCq4x4L6wdtdmxNk1P0Knjxda66WkPOGv3k0Z3J/93L/OXVJ0IIkN/tn9d0SNshF1q3PgMvXv2ijNEX3aL885w6AknFsvZP/aPw3n2Trr1XAfc1WXOeJ9V0m6Xqv4y86t9ZFbmyw5y1DtOrJ2zf+weriuzw3RQ07P9cq0nzSuXBd2u/61p+LvQvz4PzveamZ5y1E1OjKWzf1wzkhf7v/CDBpjrn6Mrr2idc0IX5wL73/Kc8IxhMNt1ztTxSVc5NbXO/3HVoF5sBP0XWmG2l57ok1mFOmMQbJHwFMtsuyVOutBcTzturDNr7fQfVw3wVaXEf8tUfNlZoBRPFczZqFjVS0sTyVMaZNNQPxE9rc7hKuh8Tz7617UjfqnHnBpI52SpItxukw1lNdsuj9dYu/ynIfr2eHLLffrs31f2uR1W1BFfx+pxtlnOX87vUF+jRZ4wn28ZVOejZXna3x4uN7bJPuNqWEDHanKhQWZtxLbdS2cpeYyVuzloj6Vf1PCb08oeF+uqUSKPs0Fy/r/RpRdaZEkJbSnMJwWy9s30q42ybXbtCy1s9pjTeM9hQrk01GcH51rAYOV5t3anG9vjqql+ubuuuiI7o2fn11oA7jA83IXI85/NzNEniucynnXhSS7E057WFudG8NLoOPbaj1zzhY69N9Kwoq+Oja2zKXhGP20b4ccRqaWXfKGHfuaZr40K62UjPLX17U27cWMqPzWulOp5z9pvuqVt2Rt5E2t3tMGFGp33kLfMoN2admMwHKwndyLSiVTrgZmr3v/5LuAK+4vdeqjYLw2AhXlzWLGHXh3XIqsrYcnXUr/lTmzoniUZFzm7TpUut+F5eHZlOWE5HP9y6ldDyecB2X16cd3Q3+8CrWmv85WJ65fMXs38SuhrR2Rjds7d+Xu7DIilRjxbiLt6/ffVtK9OifsiFxd0Lgf2rzcaFnyy8wVmt2tt9TqyX2ThD+d3Tzhp1qD8QvdZ6+aP6VwvsuOXF3e2jJ1dsaKdhsyGRn9AD3uh2X7ZdW/3sB6jyO/O99tZ6vew6K8JGqwq8IsX+frLvpdXun+mmV8qvEFF7unea+lKN7/0+8RjrmB956LNl4IyB+Yvk9nubZa/2mTZmEOPbcR76sytTj+b4fYAf/TV9C8vh++yGm80YtYU/GVU/ZpAzJ5QyCtXdnctIO6OV94QjVnv+Nsxn63csOsz615G/2I241xmzuVSyNXkb6v85SXXmcyHJXPqltTSO/J/lfrZ0ufH6n8lP+RK9bMWmNkh/r6B9/IwzRcGwoK+XF5eu20OXp4GlmLGbO4r4kMvb4bN8LDRu5y6f/KSx/PiPQIKa4NhVxLbFWTesNXgftdquPg8bqkkr3eYszlyRSltJ/BuTA2b6WJXNcTLx8TX0xZO5JwPG38pgL8RvN8Q6me1xjUxfTfXQ8ftG+P6i2k90hqbxs53UlLrj99vxGpvX52ob+5mK5vyZFFx1fy/oT12L7A+pj3unBQ6Z75+qTXWuisXeVd6yfWP3lx2fWhzXDWL78ibvnDobm+LtUncsh3Wu9ENg+Nu+Uk3jY2vx5rO0qVHXu7FRHKv3R5siHX9enXY4m3bYceWtDNn9pCLe2HiXhPvWFXbzPxZHZ0/pw0uJZiJb16GOO3/rkRCN9yKzdSrNYPv/ZWRO1kBvM6O3tq/ftgRvroOsLXNfSXGseH9vDP9K1lL6VpvZj3zjPver14EuxgBF2VQ1uLTt7hhz2J/tb7FoeDB/hbYkYJ27fLvibRzhXyu8FneiPytshc+nty94k1ONdnFu32tezDFedljfyzjty9xLi0FHqdr65ahcXdui+5Dp/75PiGU793dfC/Gv7aueTTG19ca49g4dxn8fBLtix1DeF/JzLghgnCNbX31eubFaF3fl8w2qBeML7m3D2F/aZFYtxKtutu36Ofr7LrrXnVBoy/7JUftcdHl5/732IZY28o+2xbHPfaNmsKt1TZaqWpx3i6X7M8HGu6ffLDBjCimhVCu5VXcYLjcOVR9bOYtFztSJXQal1kbGmcK7MowwiMaYlU/3WbQ3LUlzmaFOBdrX+H2hOeLhuMM/tCiYatzppum00a4HJ03mTj3VU4zjsvKooybf5Vje/VMpx0s2EfWzdvi5EItne3Pv83mudtgmJ0VpqWl+Y3598D1se927L49sFDkhhW5MFkf68rbvLT7rJ2t1VWYt1dX595B9XEI4ziK8bgyqHsGxEZK4C0j4j5rZtvVUC+n6sVJ96zc5Ulk51FVfXexP+M2r0Ur9vi+9wkP7VJrF8brbHh/zLdu5z7Eh/Fu2md9aW7TIpobr/dgfbf3fa7Un1WT9ibSR2dfWYlbp2xprtrP+bLFt5PzpSn0hSXsd/TzxVjiEuN+y07bpHxn4cgd2mVGndyyKDVD25XX2V9t9vP5oPl6tutGcePdmmHN1d/6kaVp9JbF2OPrcs3l6/9dzuZxezhcriUtZ4OPnrjpyN/LFVstC7liah77wbcFG2ZX2b7+36XNO2hfLs502lmW3epTJ3TR+j4aDndrp/Xa88vO2Vlz3hKhW1qU+/p/K0bFcmPt0CuXo2rBYz0m/V5Ndbl0shLOmGuwq5+4/ORZK/m2/y7jK6f/fa2tlvmY5WB598qVxLXFMunLvWdBZ1/TUssLPHf47zIoeXb3tqba7rrzw+qq7N5193DfmD+fqL6k/taLGT/qf/MR/vnXu7Epbxh1mwW+79SU8wbhrebGF7r0V/+3pd2WdMpjG3IHIXdsyUuP6lbTfmPb7RyZ9/jfFsdr0/jVDXmtzfK8ERnPQvw3pmtuTwgnr3zDf+c+zDm1axP7CybELxgv+5rsOGJ0S4Lnci87HW3nQYWd/x0tdRxabt0PPY7JX91Ssz83axTcb5v/7oYaKwQ3pILuEfdk+rzKVTvN/zu2Mi9tzaO1q6ua5yIg9Ojox5UtY3+xozG2Vd3xgDnelrD7v4MGnj3k6NJ944OubJKUjxIabokVLnB5uIzVcT0w0Xhv7ZrjfQo7o4NH0az5o78ugx+cOm9okYvJ+HL2vSJHIF8R7Xxc62z2oTZXes0tXiOqd3b27/jHZeiQE+OtDbIyt+4aIEc78q8Kbe5qkOsTkPaq7Ctd6uNo6/HBkJf9j9Pf7kjSyoR6/u57jwe6KXS52Bhfycnbbd/cNAwPR6IeK2b59AujYeHx+2JC/nQG2x+MvC2D5IvK6W5T1Nn1Ffbn3bFNQ3F2We7EKFsNL34xyncj+w/JPr6N/PUzvdfdv3kXZcFt2R0jXNwQ/qDBcEOY7qrWSDuLeGzKsGP981zBLv1v7/ens+/eSXXstcAXOudM6vvig2aa5Gg199ojGS9n6C0XYI6769tqXzt9XalcYYfXYzbOzCQjaOFjt3aTzTS3kLt/Mju2bI/PUl85SeOiDMtli61qrn3K7eua5ooTRuIpD2euhZEz43Uc3xpRnrnvNGuw5eXcHab4kXspv37kkKwcqRdPK7Wc1M9Zmu7XLYEv74HYl9h74nGbAzXz4ayHfnqLkdK576hBNVO473hxastxPU2zP/fe57fTX1RuOWu2ZTN46dbMo68eNftWDC7f9/JDPx/NOr91fFTw6XfYu9Yq+c3XgTqzFS4z78977+y2lcuaLsfiLTmQK77lxaBdaICvNM5MP5rrXCd9duXWSPJcPFdyR2m/+an8eCpO+VQtzWv/iw1eJ/+eHf+zAZeVNLPzifC0XfYWdNhqp6FWzl/6/NOz7QkX9+b69swPmJmz2Vbb5tPMauSaRXVhuxz+PTuVzkYsV1I3L/k/a6MrSqCs6piLf7rzj2ZswLOOfXy7tYUGXY32rTbglvV1blNf3lvxVdzR/58xSN1ZhMmdfXL6Q+nCTjqb2reWn9dtuzmjd869mDNrZ+7PtPVpI1/mIh101/rkdaETTvrLiXZY7hRLWui86fZcZ02bL6yms4XDs9n6sq2Wkvouh9TFCvGYGi6z246IWG638fcLlhuV1VE11NN9emc/yWW9uaXSeZW7b76/rmXywpx3lhk6m1m1ZK4vuQ7Hxu/ZwvDBQl0uLH+m/M48koXYDnv1eUHUkx+bjX/MLJZemtN3sZAPV5sbIMfG4kUS6Jqm+HXh4y64DWeLvyefD9t04dCFFV7mikGfDZNVLTbTkOfrpQtFEe7hUZ4orzo3Mk5t3cVAxJKaOI0SOesAl25DnHvzU7N0pkFH17x0M8JMQYnLSWZ9AW12lB0WINdSYe8Vjzm334/Pfz/TOVeFamfTEC7tztn4zdka8WU0aLZ5Lw/ePStltlZ50urZbuwXmgmo7k+E+lp8c3YYxTlNfNFblkOuG8HiS2M98oDTWYdt0dNbyoxLc/8+9jrWy1XaplNt0rVNXueJDBum30ZH3tNcl7UeT/rfZfWTOb0w24eWXfLV1ljcUrQUQTmlfCbP5jy3ZlePcJX74odaWfIRzszpNG8RrbT5ddt5jhXT8hCbs3eWhta5or4Mdq2n/40oz2x0cSkseabfLm2w8yS1HTJ4Gn8LOubC+j+l/tJgmjUdr2y0y7DT5hBbb7R2UdTpxIxb3+J11hrL+4zm4/xnFsSlO3Nu9c8GomeigYfiRFs6Zpb+Nme1feXYgoVKRNcMsYvhdcLHsNgufnTpv/MW2bEYthTZuQiAzwUOLh3sbclsYtbmnGNkn6lx7BttHSK/YaEsuUHTnpF3rh7PzefTuf72LWFzzbRlua6p9fk+eGlRnf/NdfLaeJov73Y5aBedgLS1MnFj/fsz5bQ4XM+aeGagjkTssRL4uK1jextgqdHv/b+Lkb3SvQ/fnPXHl/bWngzoDR9+JSa4ElU4NXH8hVVxlt0dHrzLc0mb7jLatjrK2ihfmJXGJDw7DZ1Z7yehr3ZaLm62QtYONb0Vkz83C2Za9DjkMVNbzi3ph9v+m7dulqpFnk2mM11gbf6dbwuzdudNvtMh2C6Hz2V7XW0nbWXcrQy/02DqchW6OxYwmHUilvvBua17abvOVumeWdbg2Tcry/rn+zS29gPuH1mbmb17WscdVxs77w5zKWS3/Dfnmq+1+jTXrstu5HnM4CQ6M7+uP7/cu7X18aoxtLkd7MzPm2mWteW7M+fztv8ul9fX2/siBjbjRy21+hHlXPvct3LvtneBXNcuOzaFXa4J7WiRMSqnr5WiOlk5X2/my1TN2d1Ps419oJqHZS+sy+8dHPtV11Arm9sHr6f/pN/suHY33nlyz+y+posGHD99yGZflXnPbvFtj+nCELhUPscNdQO5X7tOG+C8GTaOcNnH5Goi6V6f5IbmeBaFX7720bg87PcO963iTdf4h2937Sbxq3Wj9+6hu+sOnHfjcaXW2p6++KN74u0zzGyUd5HEXUUOvm0/3MfiUqjrijG9/BPno/kR50m8BY9no3k2Z3WrR64bOjMt8eyi1U9hcmtVeseovGNY4m2vHUSu7w+d6U/n43J9jvkBM/U+Kld99tPZZpmJbZvnm8/Y+7hc6ZZ77Z9926e/eZ+8WVPGmRqpC5P3nmIK379HPqFP/nQzci+RW4ryIpvmcnl9szTyt7Yj70Dk8ZheXL6+snDxN7Qj9zK5J0Juu7Bn1rd2TTUPOsDz3XjcPF+KmvHK4mE/wBbfT+LcovaqStyg8CfNLvsYvEjkmN1Mv5+/HzOn7KVvsQzA0kywZhwdbMNvPIfsZ25+c/7sxsfNTvcTZo1rmFuu8PPhbc91z0LA3/S6lrLl60PZLaRtTwTf237DdVfClqtV/QhrDdc9+VouIPUTjDNc92TrakPum1335eoK0+3bXfdmaqep9u2uv/769U9/++N//PP//m9//P1PVEMp/pdz6a9ff7qCf4fYJoAKEKcUAJqAWHIHfhJQWwFwALkguf1P7wFKax4gAFRfKkAUEJ0DSAKS/DRA+FVziwAiTa1BgIhTW4U4XsRpKqgXcZrHOS/YYA+QcDDCn0HEabnip4OI06prACKOm3L2RL1JXY7lcM9PufHP+j2fgyPq7e+bt59E7dopRSKkWodgqPTOMqkkei/myVGujpLTp0+KYox8GyByWBXVbKj/ZnYiJ++VGhL5gZJzzthCsq+SAoQtx8qKMulqrvbNPuir8gLUe3ltiSjXX675OBH1p7ck796f3u/5abJf6fc6ZSYZkFMG+xsJ0p7U31YQ+XSKSvHKEpAffOaunaLyAgQVVbMh6KxUJv5KR4VtOyniOzRB1U2VkgGFau8HxDfKirRP9J4oqE5BeyxQm2rVziwoR0OYcJp25wCpg3PW7fsbBcexEsBZ6A2YiFpHJfFeZz44DiTpg6ETwycU1AIJ2kYBbRt8TIZgHmjPAupP9yXaoEOK8+Ttm/3peDxRf3q/GRRVzJjsgx3B2mc/i4JSdORFEMdrVjTGqKBW2FuB8hilgoK8X28jQWOcCsrytr1tBbE1oTV+9fbyVccmUAvJEXVeWmyBqPsuk2uiCLrUqVtLyiDudeQnz34mSEcVnt5RmCYqEEEucTQK8o3jSFDSfp0V1TSRF6DWjLOOogvkWpCXXu716R1OqjIF5UZtKkjHdFe0eKNOdSbq97KTlua9nLOh7oR0vRmJuk4uOjadMB9LyPab3YosSd6263WgFgtVviDtkX0yENQmSO0SUJqUTye9IHUlUoj8r+S1vwCFjnw1FDuK3v4udVSmRtTtV68MApWOWjPUnYOko5H3chu/0v+uxPHN/ptFdQifVzlVqSxVdTLlrDr++A61TeXwfk31dUd9dKSmIw6oy9J0xHUDQFCeElGXrBXRkXqv6zZ7giDVwvqbOegI0Ofl0vUpuYYqpGQZcubCdih4h1wpWcH79cGh7VAVBc9ZujPR9brKWcFSrlk0H9odqERO4RXKvat99rouZ3M6bjvqcrY0ek+fkibqgkmRti36LpBqYfRrII6HoEhbE+OhI+eCjRWg6DjNC6JGKYpoAlRF1Cgqi1d2g8riOfGrLD7ZfCsoj9EP1JsMSJ8e3CRInx6CMBH06YF2gT490i7oDkSfqtHNifo0njirdQRjgLNaR1DE2VX9TaCS7XmClPkuC1ClXoqKVCd3qQWN+U+Qtkp/W0HUYJOgNjnqLEHDShDUlZ2yKyh6ajBI3YorRJgGhvHVoPg5iyZBPnnOAQ1DiXNjR53EwLm/I3RRHY34lY4y57GiKHhHyYCUF0gNVCLnB0FkyQnq3Y5Wl6ADS0AxGoNAZEklK2pLgfmOOteRrQLkZA5HiwFpvw5Nkeqs7m4LUp3V/W+gppqd95pyFkXqri+lR+q9rlBkvAN1cSbtkUBd1EnZjdJ+3X4QJoBg3KrVBdQf6dRGAcIHqvWjjIDmtB2ijIBGIz16RarZY1Ck1kWU0diStkpMimI+ule098Qozyvae4DQFDregdAU5EX6busd1BFFGNvBUH/3qvMfUOeF5n+UFms16Rvha/0/8plkTy09gJgYXNPh3/9QIdmOEqxsLZemL8wV1a6PlA7iIKZYtNVWpxNhHCuwWY3PZrgFDnDF5hYUw15eP2TDVAjJMFVCNExT1+TzNGxMnj7jRB0AxNqAgvEOiV0bGL+RfW2G8YysxolgyNC1STm+X6kSnWHtgsET09gX+QRTMUbDSeWN/P1GVZkMa2cUPgSrSSJ8dezoMAiOwN5UJrGaKcK/YG0fwfL3qmbiZLjIkJX2FNxCsPYGphMi/UGwt+5KrO0Vk+EsplfMfD5dk2jyOjXzY+X3vbZXNPl9FmU67gc1Icb9qKpeMHxOujeC4XpHbd/I/ubo8Aie4Jqrmcpgc/chRX8BVsAY7G4B5EgV7dahTs2AEC3p3AwIybIL9lPiDHO0yswG39gZhFg522s3SJXVYI0yfXbOOCarwq7j5ctNIf2mNBE2kTmJ+urNow2UPKG2TwowUeCUh0aIu07Zty/TX+oQUnm6QYAOUGcTQPj3Qd3vJBNrhzqfAMLDpysEmAF1Dkky90A1SysAQip6Sl0qNApsXUJps+7CeoolbexjsVeUPuCjNkTiGPTJhXE/AWtTCIYwSZ2Fw/2uS+15Ep2YRJWnaDhIu6dkuBR51azP756PTBmpGA4xHd93RbnIKn9wJJI6r0/nedxHjMyTSk1SD167NGAF7GYZYQFUGybJ7IzholRm6aMh6MyTxBrpsPtQhBAskEixYzrU/g0IsQJpzdIbAp2xJAZfh9qhk3bZ0NT0Ttple/uK2kyNUE3qPBGqlZK1y0anhkmHeFB02ley9sJIRyhrL+xdQWbIrL2wO5rBIGKLQWfFrL2wu5rJflmgjjpArMMFnRizmCQdqpWcxVrqikUHISAKo0S1QwAhc1RDOXNXS3dAZehkzjvdBXXNMOTst9vx/aLDNHPewtejCk6sfArG73ebTJ7HMRHpaOVoWF2YjkU8+lpZLICu9EipmAdd6ekkl8XW7LDqm4q516G2exYrtRtOOnqymLAdkmHtQnBFJ0IPSEq1CyU6clm7UKInl7ULJbpygJAqJBm2gJAqVDFzM+zYDpNOZFliCP0FdF7K4s51GCVehL8F7K6f3YWQ9O9wV6A6m3a3z7Dp6KcauaqEOr6y+IYIPYmhlcU57FAVN6BEFtmFqrxR9l48pqyDIvvQqkKwkT3fV6eETGcr64TR7RJnd1F+le5WbtIK3QWIBiFVUMcii+HdoU4YWaexHNSmyDrJ5agmBL4sUC0Cu9ud6HR0l65VbvpT3bfC35aJULV80RHU3Q1RnEWVfqYLVYIIWfrcnggjoLJRxE3rUAcUoAdUNgAdYMt2t1PXXVhho+g01n3YaLACqs0FWACzPRdNVuh2FQkque4GVbsLqULIfC76Rgna64rEzToszR4EqYJOvUWif13L65guEhp0JavpIkH8DruX6JQrhaRuIkzWKAqrDrqqsE7W+RVSJWZCdUcB++v3FlNVUQirZ/dG8Ls6dpVMmGUuA8yALXKESjTcs28UvRuotDRyXrmEkRuhxlvKRFi1fTVY36cW5coTkpxAqPqqREIdoV0GhTphFgqZ1fIofIWmaqTw9enBFCWncsWiSDSuQw1iFAkYue5rRoMeMNiD0PqN6xtFtUqb2HMkcNmhmuaAXarmdFgVVV/d/xTaiyq3Rj+pqOprXCspiVCt8JKkezenTg0ghPSTaBVACEmXqUjsA6sLEr4tqp9bd5goMwZd6/5SJYSQXF0pRWFSSwMQX85qDxQ1cFuJor1LI9TFo6IqqPMqd+tEqK1QHaG2gt2lW1PFU/cTvY4qrrqHk+kJHaDaW1Wc9Q7VhK1iPHSoHmGVoFuHDaWhBWIFhYsfgBkwRP5yb7IOY7W7ETDbK1RI5dW9B4RUnRu+QoVU3Wyzv4VUQXtslfHboYZhACFVqPZGBVJFtXCqDI0Og7FRIBV9GEBIRRcGEFJFnY+qrMz111NVAAipuMBSmsCuFlXJVEJlshRCnSUxNAQqk0XWCT18rXh012uYye56fcFxt9Z6dDfoNGF3ae7bc7m+UWR1rFu700SpuurAUpaOX1kt65AjtHBpiy/INTHa4ogE48s0xREKFtgkhIdYcMVCmBpVCAYL1BeUaDCghgYkAAyo6guwS9WtTplxOoTMnssbgA7QN/4tXh+9rBIGQLWvACOg2tWACbA1+6neGbo9JQO2SiC8QzW3qkTCO1QLFhBScZ2jyozTYbU3Qhf1Uc2PKrNV1z/e3gjdG+YGhUTn90kn3CpLLYgCK1dRBk73OTxlxrDqA2UyCKmyqs0qwa4OVW1WiXZ1qFNMlXBXhxz7Eu/qUNVmlYAXjEtPIRukKiHbXUjFdZYqMa8Oi9EudysVRVLYOH7Fju0tlmXlpWZCnVOq+E3de1WnqyJ4BshBV2Aw9+Zlcxcxrz0M16Z/TMwmtft0JezPYbwCVkKNPtaqz+JSS60qSdGQSK0qZyc+EiZAdlqJMnRIlSVmYYfstGI09p6lk2wVk7JD9eerGJwdBs+/Rat1l80ehDYNXIKpYsrCQ/cUEv0hTjrnVjGDO6TKEiO5Q10+BUTKHt0NQA+oJnSVBU34DzL3VVk761AjcFVMHYQCPGVG/490Vaos3XdI3sUC6VBdFUBIRVcFEFJ5dVEBIVV3VSgkxmykq1JVVXYTqlBIjPdIV0XWdQDjxBaEruh+pfEMpdT9SuNZVtyDxgQBIVX3b+WXdSW9O5KVYghsQdaXaiFkP8yERSe7REgmdTk9TRq9qoFQzcLKxf2JTDpCndyhKpFfz3UmWVIDVCZlva1Dp241lLBAJbZzrlCVUudcoaqOzrlCavugkMtNSGwRqNYpIDLwYjDjXGCieo8KC83CRKizKiAq0VadVSGVwOBMSGSscakHryCQU1IVmCe+QiNUPw7kCGRncAodO4MnpKkTCIciVagGJ5pMIDuDSpXpudRCeOgMAqk5KVX3ASFGo1S+iaPaKFVQg7NRqqBMAnY2ukMsgdSmPOeki2lNWyEn1RtNu0rOGk1omiSSud7UNIMkd4vSExZAtQSazqrdYZQlUcAGqLY6FsYgVZnsbzHKMledmiQTdahqpEmSUIfaZE1yMuAPZ4MQo6rekOU/QA1tyToeoPZ2WZ4D1NBt04kyV12jazpRdhmT3YUYXI1q4vR16MWoaOISYr6SBm3iMHaovb2JO9mh9pwmzmaH2nOauKJd4/ghRgPUKbiJG9uh9hxZvALUObeJC4w1/mx3u1SFy1tN3OcOtec0ca471CkGMAOq5yJLSoA6BQNCKj8Zz5ie+gzsKbNMZn2gTNkw5KIXKBiCBU0fGffp+LVInKJYcC0RZx2ngvH39N9aNsz+VQyr6dUy/76wtez3uKDVEuWt5Dzp63TXi4+Tl6V72JJS0VR1NzUSSsuiM5taFN3FkdXNptZIaTp4m9oqld5iS4Sh2nM9YPIUC61Z6TzKqiWgmj1NPD7JpzJYAdUKapKngWE/GcyAGvFv4ngiMOQNIsWKS2qyiAyoKrSJS9uhuodNHN4O1eZv4g53qKZIE2e5z4gayW7iSneo9nMTRxvzpfIsbniHGpJsEiuQnDBKhcm3RjVFACFVn1CztjEGfo2q2Js6CJWJfU0dhBrVFGnqIPTWl6h1UwehJrXcmjoItfuw/Ckoq5o0mgcIqVIZfyupajqBNp3XalYzDxBSMTMQEFJxbbCpru7KSGlHugpgcRQyQarMTtohpMqatwkIqYo3ngVyFbC3r0J2lUg4Ritgm4aeV+jDdHTXV1Mpqb9vC1TdHSLFjm4pYH/f1hvF8UEeMIwHCYwxH91NGmmHkIBcssIrCOQg7C/oADkGq0KuYKGKK75cmLSDrTCCmZIFjARzJuMJxsZprjwJRm4gl564laZLrRpEMLY9cPFJcAXWeUVwA9bJHzjjoJyg3UAwtlNwPUowjljmChTUTYZ83WgbGPJFtUoFQ77IFCxgyBe19QVDPq5ACYZ8SVN3BEM+pgMCI4txojMGdai4hmYY8jJFcNzPSdOnJq85kXR8sfykOJDPSMzUqClqRqXXQSMY+Zbe+IqajRmMnyjv67jKJDgCG19iAnac8sAemAlrwA7Y+BKbqf+aTsDAaK/uA5RgGPJxsUmw5IMaXzJiOq7j/dBfXDR+ovQnR08YfKC/dYuN/VF6fseJ7x/QfeEMauIaMORLLRmOTTJQp8mwZKQG9s+OIV9hop3dr0y1mzxx5vs64lKs/ypWl1A3QgUEBDm4FAYbegqHWlbI2TgRqlPcBzWgn1yhMQNBEdUyiLxXP4yomCRhNtBOiMiY5WoaINJp47BP5G6iQRIJqcQSYYucpARmb9OBPCjTdq2EnP4qv9xM8QosU5mO/pZpjHaX/rI9qNEczSJzmKjxdIt2mIalge15ga6o3aUrOu4mm2dlaxqYPbqbOztHf5u1Re25hTNpIWSTqcyBeYt8IyzX5AMbgbmK6AwCOVc2abJQD6pRcFODW7pWRhKyDS1PbEMrEKc0uirqhDtTPZFY9TxWyhXb0MqKu+2oQ0N7Y3e6qYorsfrGbtK+HIOGRgT3odSnnGj3MdRimqhamwzFmDTa7bBjRHDQoexkKumYQ8vpVBKZQygYdbZTU9XhdCrplkYav4f3z1SVTqcSrIaN34e83Rka9yFv1vEEDNUVC/NKnU4lsegAEwz5Ssz2fKjGyDxGwZCvMPMVGPIxs1Ew5Kvu8PeQj9mNgiEf3VzndGrpbr7T3/PEbC8n3ltIdCIF999PQY1/wQk4qOp24hoi0KEZ0k48x451IVuwB6aqc+J3hhT94fkTsDoLkA/8dfOM7eWE3+4rhHEf8sVGvsXc6YOKmcjAkC9pfrpgyJfieD+0f0r58PeQL1l7eek/KVt7eelviat0wOiPCeuhhiFftvby0t9TtvbR8ZSYVSkY8hVrH72fud7Wh6FirwuMgguwLoU4F4kb5dPxloP1/0xsfBXFcWJ7qfrvzjqfn+V9ck5qimCn4ySbBNjeYrj1xubUDuyBHfkuwmf/erXfB9/dRy/OcAK2/iamLSJhaWDIV5iPDlxlW0IYfw/5mMcIjP6SuZNJsGxjoD5yYqOHXK0/iwkfMtMZBUO+pvElwZCv0TRy4h90nOu4D/m4Pii4y1cm7g9w4nx07Pn3VXGy8dYU04VwfiImX94Ra3KFYPx+1RCAYBz+x4UywRmYpoZX07J7s9r/vJqehQmG8jxsau2uRDOMA53pPAjGRlcmEAKD70pvAr+H9qhMGBRcgLmzwcsm9o7V+RecgEu190V/qE4XJwUH4JBMHvSnytU7wQ6Y/Rd4AqZ+9pP018oFPMGQz4dg8qG/d3+2Gh8YT92hdfY+GH+V/pBgyBd0Mu0OtuJI09+r2dL9RdVfPhLTVAXG75mrAoznFWsfHa9dG7B91MCqlaYgMN6ncvx3LO9brX2S8lGtfZLy1TQfQXCQjTt+YMjXkhsY8jVrn6Tt1aiPgbt83XvU+RS4AlOfADdgax+JV3Zs7SNB1o65L8RLDDb07phMfoy/PvyayYP+2biKKDgBxzq+n4HpegBDPu7YEgz5vKvjPuTz3JuB7WXA5toB4/tRA33O63jt9HB8VGK6Wl7Ha8vWnk3/PqsV63RDXvcLrD2qyld1z5pgyF+tPaq+X7XxUvX9K+0ZX5Wfau1Rlb9m7VGV32btUZX/biebPNI+zdqjavtxxVMwdrRP1h5aCmAy1w84AVt7VPQfOLrO5E+ouzCVPO5ji+pk40c8+ThxB5hgnCzjOF/6RkzX3Ys9h3UijgcWcXC0b4Ahr5/4PmK/dqz76QRDXiY/Coa83ZEYvw95mf4oGPJ213Tch7zmmgJDvhC98Ss4mb4txDaes2Jz7XwiDi3YeBZs4zkQ23jmYaSZ9pXnSTxdnQz9JtjsLcpXku6fcpSvcDw7NHZ/pYn6yxVitofddzbf8373VcPxfa5cCu6PcEHTlOX7guOYP6XMdLD5tyqmLwZ5A1wd20OG+8DF7BHeb9ZfJuI45jPF6r6AL8XWPwJxHfoTGPkepk8UW//IxKYPCnEZ41Ows/m5EeuamQsTsfp8nRetqUGnz1k9NacrB85OK/XkBxh1N4LG8OX7gtmfgNFFkd1KLAdvcvlPMA7Vihq4F4z/usE/7gdg6qMg81fEwl3k8xWrJy0YmzIT5/Mg9mfHbL8g9mvEIprJK3VDunnY7O+BmVgpGPJn6qcgoZCIhaXJMOTPbP8goZSIxRRvGPIXXc4WDHkLQ1FBApbR2569ELRwmu3aA4a8VbNwBEM+27kHDPlst16Q1ThsSh3Py5Cv6pqhYMjX6E8FWZ7sOPD7EsqK3vbtBQl1YVmY/EooDNtcyYeEyjqmvgkSSouBi7CCobKmNPgUzF10wKXLG5i6Oe7HQ/vK7yWNdgjG87hCKhjyJM3pESybazlfB1kU7vjQHnjfkKfRf8FHMP+XfGEbWjriE+5fOeIbDkA+ao9g/jDbK9iOTLZnKJwv2N6hHNoL/SEU7qhkfwnm/7I/wWAcGPJVXS+3/hgqQ5dB/LeOuaeW/bcbMM74Qv8OzdpL/K8YuLNPMORth/GvmPZFEP8OWeBujOcoG5bbuI8hOdG/4XiOUzx8HyYp9weaPujNHQxDX0Tz16lPIteeTR9FZ/pG9Wlkoqxg2TJt+kfqTUUowIEhnzf9437JK/iD/hEcOF/Z/e6+u+O/54Ku0woxsVvjtCcasc2/ldj8oUJs9nYmLs7sPcW66uBYFypybVbmB8EMpWP+EMz5y1O+btCZP6KY/DrK18x/VPkSDByb/wQzNOykRFBMjvoX3wce/jeCqx1Hixchrg9s8Yb6S0yaFLw9DyZIShY/KIoz+bT7w79tirk/D++jmPKh7Ixg9gfviXUFE3wpPvgrwP1x0ex5xeavZGLuGPeF2OwblS878x8bsfeF/UGxzT+O+DD/KM5j/An2h/kGJlnmuqHgDHzQ9zDJcjzoD3HJIu1N/D5wmkZ/lu9zmU7kEWzjdVJcGA/F+wjmePWVmPM9+BDM/gS+8LzC9tICQDFXXX12erpcx+bvU96ahj+qmONXywh1XL31X8Hm/6M/CD7EIxQnb/acYovXqXxloj/iVL7CDXnjvvnPwFCJvQcmwzAZzF/WSkd9crB4UCKmvwwshRFqqsf38yHeo5jjF/ICIxPcMP6+mvxVcDX/Fe8PPPzPidj8T0ds8QJPzPEJ/gWb/xkVe8YDfSI2+zUTm/1aiM2/oXz+4N8otvmL8gXTl5Qv0J4NlC/EYY8IhkdGDBeh2lIgMELqtvQHjJA764MIRgiQ2bT4fcHV7D1HzPgidrQCD/+zEdO+8JU4Vnt/xeZ/ZmKL30gxtW4cWvwmEps/GoitPTxxHvpbscUDVL7e/RkPbcQH/0cx4wM4oS8BW7xW5Wve9GshPsRHFdO+s78P5i8VxdFF658I0TZbStTTsjvOB3lgctjSITBMknwYv4LrIR6m2OIrjtj49Iqb9fdATPsZ/AqO1eI1io3PTGz+PPZddpNpmqLFExRbf2/EMZt+VGz61RGX4Y84mGBuGvar4OCGP+AQ8opmn3hi8gOcOk6e9p+0P6L91cYLQtCTVTABRggimX80EdPfBkZIIpv9LOOvY4vXINUcOA1/0ON9cnfobTxD/szx4kW/wvoe8SWEzKZi4wXZXsBc39D5s2OL1yA5A/gwPgLkqYf4WYA81cYHViuBbXyI/dOxtaeU4+uY9VaAIU8fUOM+5Bn+t9iLHZv+EnuyY9NfYm92bP62xCuSY96vYJSomSw+MxGHEV/GOQBu0qQRwTDRJ/pPwBG4HOIdgm0+kXhPcuafO4kHJWf+OZSdYJtvKuz1juOIP6D6pXOH8RqxZOhs/QEv37E/Wm8Q7Mf8l/A+3uIfEn/t2OIfEn/t2OzHSGz2maQWJGe1apz4N8lZfRon/g9qRBzWn4Dr0foT3sf8WV3/QvKsG+uTVUoEMdWgEXO+Acb7jvVfiR8mN9Z/JYkpof6Crsdm+Hsds30myeXt2FI9ErGlxkg8N/npKPVDsNPxMgVi+k+T+J8ds30mT3yUKoPUoclSQWT9o+OakmGEIJ2ldnjF3lI3ZD0F0Q6uJ0uqTHdeaA8BO+A8UoMU15EaVLAkaP71JNtroH00m6wR6vAHxNtx9RkQL0NnusmOt4ScW/tbvErmMn6TAkc+MzOrKuRCcZM89w65qi9Z8B0yEUNy5JOnF9yawuYSpUIBpkAfrlVCpo9J9n0K9OCa5OZ3qAHGJpn72HPr7bn9Fbr1EezLWPCL4wUFJqYtNCkmhcVhgw1wvG/D4k/i+6KkSoeWU9yQHSluE5mUZDQuszbZTJaCraJOE7EbCVrIPwvdrKwn9w8JXXLfcoMbIdN9ZT9swqLIZN/Ga5lPCYz3apaOpeeOd58/k32F2VjQhDVzKXl8XIrToU/qB8yo1ZIj8gFX1adgH3AamiQHN0Xm2GKYKS4jo0r/wPxO+0KknQWM0H6iHTRJQZiOuY49ydaZBEezGsZSkvmhdj+7Me4FYwMj9YRie0fZOpgw8UfDWIqaGMeaKnHmuK/6/cB5zf6eKZ/j75PpKd7PlmKmz0/m91C+ZOuclD9POVmeiiRdBkshlLIJqfuR2fSinC0fLUWwskJ3tC7FZs7D02EzZ9bwgKrmB3nkIvCDVuwDeUg7JFeIELZ457wKaYt3zutLtEMyBUgp0zSSG3C/2yLVJBBsi21235wbFxRbnqNTkorlNTrtBCXaZJqIy0g2EJxs8uN9czbsvjkbThupcLPeuF9Djsf3KxePnWzCStWcA6eNVG2xyknpi47zeJ7mdlriotPc8P5BHRJgtME6rLZeot/IDHi4ah/oHmexyOUDK+3nJ/vA1vAm/mgpzRad9YNqViD7RptsWUxVSDOz2Uv5iI7NTZTqEgmVMG1ZBMw0Z8scvG/FAO2+983MNsFmRntt6cYtZjAbBfeWsmVexfaO2hKtHJZ1BdfDsqw8f5iRSeVvdAt80vcbyzaJBAw7UrP0scfTh/EBkoGcVoEUz0Q+COHgmugH1Q/fUz6IbiwmioEx2eqJfIBvJEtHaHxsMf9fh3H/II8AZcO+msKCjF4qHHSteHCAJNPbigF6mTw6zofv436zBSfeN4MpSL2yzrsFdKUls7OEOK271nEdAXH5frAFnqD3I3WGfX8E/EWHoADFCDDjfdwI8KsSwuaKkMYHDh+Eow+QtjX5sUbAD1gwk2W1skeNFvkg8gPsFh7fQOqJ5W2FyA/SYWGBHxxWEiBon+HGypjiPCLTim0lQGbl7C3SzIbCppbjhoDJZJ6sYq4EeJlBOjZPJBNb5kdS3LWP9diG5IYRuY7EjBza/WaRQN63SLIOuRwmfxhCgi0yq98P0yESK/ddGgNEMT1bu+/TQT7gcMjkkN8Lh0wNuW+RaL5vsEws8hEsE8tXxTUeBoZgTl7syMEyq8Kkz7PMqnH/sNIAHEckVzs+QvnmqQtORwMFyRKZmWR2v89O8ah944i8VmKLvBZi4z8rrpRPVVjHFimPxNYegTgPT1nkqVpwhiobG+gm83wV+2CRSMXURa4SJ28zj+KjyVCwZV6KSs3JMrFcJKaKdoHYVt49sUVyld/uunPyVv7gaquFVIlDGhYa/t4xkqcWUseaZE6LsZs7nHynQJyHEauYU+2k7Z1YH0GsZsFMLW+EuvDRKiF3ohRCbvHJhCNNG12tW9SyKN0SIXf8RMIwcb+awrGJUKEG9JpTmHS9tkldig7pKEyE3CPbFOaxCVggd7FgV7dA7vvMhNz1mwg1cK8b2XNiXlgNhGqFVE8YdOMypWLR7EqpmBNWKFU3FmTLrEqVrcaDGM7YuplYmEEhizokQp3ni+qUrnF1j6zOPZn137RgT4esAKEdLDvdM6rFfrAOnFjuRaF2By0y1OVnQSrtbNlr6+dMeKhHJVA7g1ar6lBbP+tEkVlwO3tC9Qy08lbOTABj5a2Oi+yHSs1wlQTbVImtuFwxzOpyakHmzL07iWZKps0r1d+wxp61JDyrw3WsvYDV4zrWbpCcVJeDTyCvmibiojOnYBBVWJi0GVY1IhUKwU1JYVRMBKYNLRUVgRmwl4qLgrX9pSKj4KzlPoNhDdhLRUfB2gWk4iNy3GiDS0VIwSzA3wzrtBKqYe0GUoESpelpgUvFSmAG7KWipWCWEI+GtSuImSCYRf29YdUMUmET2IqOT4bVDBD7TjBLsKMWInLkQxFHF98XDAuNv+ekRrQuwOF5khPPBC7ByLEoalGO+1UTrCGvYlYgjYZVjYZE3LR0PfhQHERNgi/FUdoDfCouEgAD34qrLGihPSSHf5q0vzjDQStkesPKZwyG1fGN0bAOrkj5KgP2MRtW/mIxrKWAYzWcJIFTMHJSum2Yju8n5XfcTyyuW4j7CEuGo1TQlmlIni+4aJFNqbAJzAqhwMjxZIBc5Bes/Mn7ISeUfoa8v+Ag9rrwI5gVQp1hvs9kuGhZ5EbMgLS0j2B9X2k/wRrwDNkwK8Ymw+pDB5OPAelg8nHfkPQv5Kx6Leot/VOwJsRL/xWckvV3xVmPRqiGW2Hhb81RDzzCohJ3dVANIwc2Rzu8QXPWy6S/D4wYK3cfyfoQ7tck7SvrQ8CNfE2GeaSEE1wmZKTy/RSTr2A4jgq4inWvIPhTrAkg4Fcx+SrELIIeqmHy1QwrX3EyXAPLOxO3oQ8Fe+UrmnzI4GP/Uqxm47gf1ewSjBh00gUgwdiOmdh/PXGfGSfDyPnihiLBKC5UlG+RTzAr3gIn1IPXuJS8D3Ajv82wbtgQPgRrjU/hC7/XyG82TP2YDJPfqBglnq2/KtYEiUD5HStnSHsLVn6lPwhmf2z8e3foj4qVX18Mp0N/A/ZqZnu+D/JJgmGUl4q6wUAwto/DL7DfQy2XxKNDgKUSfgnt+D7dSPl7weomirzAleN9MhxHhWl5HhSc8SGYfAbDuuFD+MTfN/KZDGtJCGkPwey/Jh+jzcHka8pnoHx+Yn+dDCufch9V+oO6AcBymk8YRfWJOV8B44CfOKVqGGU5YhzPU5ytYL/kLHbTzU/H38+qT0MxrPYX3k9wYX9NhslvNBy1/wTD7K/eMPl1htuo2C3yVB6K0Awrv97kq2noN8VasdSbfFX7rzf5GvuryYeKekf3sVl1MuyAdf4SjDUlz4NsMnE40q9yUkLQ57H9Q+CRNWz/EHV+t/7dtbed2ST9HzvoRv/FcU9J+bbxAz8rGcbBN6xoLr8vuI75QDBLE9h4DYXj136vcrza9+kGy++Bjz5hDHlwvNMUDvJ0fiLdUs4vHXP+GffJV+Pfs8Dd+L10JC++X9Lx/NRxN9kO81vHbegHHLsVq9onnC87PugXnNfVzX9v/OKIkcgEJMGo2IZihUf3kcFg76+Y8tt9p/bc+Puo+tirfdRvsj+q/dRxK0MenIiReehUFfu14+qsfwgufB+1vxGSP/w96gsGHR+0hwvyG2x8yPeL2mNe7XsUWwn2/Ql1PplwEtR/6Zj+gFaM75j6QCvi986v8xcr6nes8dOgofKOYzqq0I/RXM0+F+yoT9Sf61gTDoIE0rutTfUhQZgO3dEBFx3yXI5GGAN1udQs4d4HL6uCBaqIlkyD4J4dS8/7KExc8bJeWTCuOY3hnJLCwtJe1jo7HI2ocNhkOPAEY4ZDqEKqMEZUhVRxnCpWIVUc86GUAIzs3lKcuEP27oKl3g5HZ6iQCulGqkuk0mTSiJWX8i0dBiVHyrcgrz7bXXySOG6lfAsKJkeDUU5b0V4k5Vs69J5KUKH6pDI/AyZVOTA/ALVKn+7g6rDpAJANXrD2s0FIhWJEhBCD3oHuJiuFzoGXuhIdcKxJWQmsNqlpIFUlCs4v84QQo1LRSk0JnBhjUgFW2Gn6ggp5YFxR6IYSUuisMyj0dhabQlpcjpATllfotXyU3aW5HmT7JWRs9reo1xtD5eBJaMSojRJk72Xplvh4EO7mZp6CQGbeQ0iBwTqhQp6aVgippTLhgRzA/g6F1AnklBQIdUR7T0gHgVJVzleUqunw9rK9vjSYm4RIhHCcrJXJRlte9652yFEmW1s75LCSna/ws+28pSwn9HDeln31pbHYtpdt9R1yWNldDiu7q+MI/hEqIdmejKZ3g7aCq3qXGSO6obhD1afw7QRyQSrr30ZtMpf0LtbRj75MX8HuWq0B3i3Ks/1Uc+NBeAXa8U7yeLCtV8hxkuaDpY3xUw7Q25lkqME/0ebWPdgdKhu6RRuLshrYlx3cXUswDi5dBfXAg93FGVxee6xuD0ftaW8QUrHYtiZbdUgmJRerTlYLoBDqYJe0vl/YiJzYCgo5uidCNVMlpw9QqZOUPkB270Coa366B7dOTE2RfD7AYAc4KWT3plRZDyrT/Y0djulLYOFs5Qg1NB2krgssj8DBjqM93DROWBToqUUbofYcXxUG6slCqGE/nwmHBlZItRkJNeYnCd2AVJteYaRFSqniIBYyd/9KO1IjZJ+shONQNYXVzl8TyCLfLhEqsZInC6iawUW9W6INDYWsGWR3uVrFN6rctsj3ZQk4QBQ2q6pUXVRoe0JkCzfO4Ar2y3IkFwe7VKap3nsbKTjlybN8miuE3I5bCdlFG/+2mc4RGA7ECiSxXv82xNEoArP5GQrZYxMhFTKl4q4IT6kOPoLCaGd1KlSFHCZCdYDRCQHpP9hdmvdB0jklW53WH87DCt3kLkd3ncaSxt1orhiO0kKdIk5PAnm0DzYO4XhFluLGviHAOJlZqjCYF6EwSpayL4TqY+oe3Q7r6PyAiR5PJOTJbYGQJqLXn0o0xylVok1IqXjEj27O7VBtad2bW3uLBesbAqOj2lSos5WjVFltQkeprARa1AfZtn9s6e+wce0PFR4AOadITi+2BHBPG1J+O+SsERV6bV+X9MuBeReZkKvmhTDaXKZQidXNux3SCpoIybNTGMmzJySxFJK5RLpvF7vyzMxTWEx9KeQph5Qq0YenVIlWAaVKIyKqMJmDIq/P9RFAVLau2cI3cqCpHXw6Ibu6YjtAPdztvpzZdQLdCH0qDGZsK8xmXSssdmCfQCTC8vUFjmNiFbJ7e0JNVdA9axXV4RppF6i0o1Fw1mlgnkIlHGaAQu5vT4TM4IqElbudCdmfvcKoPDtHyA7Mu4l6EgXSOuQ6CiCq6WfO+46Q8z6K/wHaLh4kxXdYmUNDaCkvChsn+kioatMlQm0jlwk5HxVCzkeVkPORbE5DpazC0a2QDeoIabh6QgYKAyHtWJUq003VbXM1c9VEs/5RODIyyKOQ+qoSchzJlsCa6dJiB4JAZw65fNl8Ov5tGuEJOH3Z3LSm0DwvlLwFLOYO44zQXIJ5uDhANJszVRW2ERhVyPeVfWG1MO9ft7V1GMyCUsgXDIS6JqB72hAQzUYsCpqaM+UI+b4ToR9NJlDDq64SltHcArk7NCv0wypQyP4cCVlaKBBSIVMqz5qKlCqwtzt9hcJehzLqgCXb3T40CkxXQgeo7pKTLV4d0uzB2R0d0gPi3WoJ9Z7Q9h8TDitIIY38RMjxmxXi+DuSI9Cb/ayQr98Is5l5CulMUSrznigVyhywfR1gtGO8FWq5RJ8I6UxlwnH6ucAw4m8Kdb+PbsdB7LUyriLP5e50z1fIccQikYJO/wgQRetzNYcXp/pWVlbDXUDMOYQ4g4XLJrqNrSN6mkW+3GzRIivs3hODEgoZoomE9CUDoe64xYYtgRy/TiELfiNRVCCjj7J7qUMN0SAVVWCyg4gVUotSyE4sNaHAoKaaS/plegQuEiY7X1m+HJmclfT1ed4rIAoSIwDAXwasyhUgTnhgpW1A1LGutNsLIWcc2V/WIfdzIFwJyEqovNtSiuMumiRwsAtkmWqwAcgy1eBKIHWdI2RYySsMQWkPhIzSR0Kep5sURi0CiOYWqJaqp1Q89NXuZs0+srt5qHokXnV/MLNfITTIk14RFwQ6hH5xtM3Ujv/ScWc0fxcJwGNdDLDFsYyA02yH+4d0MZyvaB1UoDrSXpqzQ/p70tjY7NDi6M0NJ7jal3FOBL0FQMQ6bblBNA6OLRrTnhyje5gVIGTirICDKgC5lCaTBPaseNMakIr5fz4ptHUK1FQH5KyAstuAJFJmXxyX5BmQkoBspnGJWryANfNByD3sis6cYaSa4qQxdgVkonazzWKIOO0Y0XpObDju2JVsMUScd+xKtRgiDjx2VTdFeLHaMfDHc+UcYeoFsfgbDnenkDj0GIc+GoRUKNxFCKlsVUV81A5DpTWJQ3y7SvF8hQipmFmo3n6H7FfiSHcLd6qM1SacrzSNSEFygOOcaTnIk+XctNoYchGsXyUc1+LHWpXCaFoyoeY5t45rDLHhsBYLKRbA4cEmSMXC1Bpv7TA4dv4MIVFnkxBChmRrHHLOI5MFvZT8g86s1N1IIPPR2QhEepk3T1LCcx2O4YrUNM/DlbRaWIfsopVQi4d4OQSka+MRBUaGnE/sonKeSIdjdCO7rk+YmeswSMXzaawuIFHPp7Eqg6w+n4eHI2dg0XUMctQU1H4ziOfmscKObEKfGUGWQ6ywf3kc4I7XL/TnHRYbOmS2E8wUQO2TgGiUon0S5r7AZkvbSIj0XDkLchBn81w4C4FQbfqgR1wHLouFRMhV50yoSWcYRIDcxK9HwiNjRd9Xjq1piDIIbIQ8g3siHPkTCnkmd1Do1RTj4dooYSwwEWpoL2ZC7VcYRAJV9emRvX0+VdUXZX0IJ6fJ+6aJUPtVcoRqEOtpuB1qv0pBIT3nFAm1I6VE6MdBqwJ5SGshVBMwVULtV4lSxWbnigpMhyNNIQnXDjO54tJh1hYMRV24zBYsPBlQTzAPMHL/kpRNhTofZTmwoKsnNT707M6Get5M90Q3CyyChyM1BepUrUd5NtQUlLuVUH1yPWGzocKfwEao/apobw+N59k5Qp3pipehESftSHp6I6rdy8DR0xs71EzGEgm1IwEiDXdS9YUj+ZCGy1iAHiPYcEiX3C2EPJhG9nZ2qK5UaQq9tn7RsR9ZIkBPf8N+7HFyHFKH6Vfr6W8d8ugoR6iaHxBvRL8aEK9Av7p6wmZJxkjhRceBGFUqTXWoTaYnmsGrtpO25FDRoi2op6F1qNZIlVW6DnWaqLIy0ZtEA1JVViY61BQoPcOooYfKC0oFxA7LOB0JQtZx3JUcy0vnH0feCOSRbXICTofagk3WhzrU4p9NVo9QKVFrbjtCnXGarKW1NI1DbRQyPV1WrVqyyudSVrhD1Qx6jEJLDA3oMQot8UhiPUYBk7EdkILxnvw4iQbaIHkdv00WG1riqnOTIDiqtUSD2CnpWUVcp/IUeF6MxLU7VFsFEFJx1VlrquMgk2YQUgU9nxEQUkUtbAQIqbjqrOXKO+RJNBKsbImrzi2LJZNQtoYQQnLVGRBCMkLRJJaHnWfS6wAhJNeVW1LIGESLhKxsHxRWbf3mCdncXn+ZqZ7NKWxsI6l01bLVcpdCVzjMTBRUk5hah1rHpklsq2EbVyTE9gXHVvAKvc6wkEqgzrBN7UnsxEl8I4E8UCYrTNxAnsUWzVmDLE3KP+BEzURyZNsN11ntLo/3wtEzyGCYRs9RyJ4TCFtjfxbIwu7NEbLnSKirQ76RUxjYKE6s68IVTUBsnuEhXE1CIR1mO2dKts5wCVOOkOuQS5hywBwgG9QrLONAAYVssqiw6hwqJ/39woGikZ1QYTNiZQvONDq/QOetwL5syMEUy7sCKaR6MQhsc8eI3E1tYq8TmFlg3xOqkSBnjwJS5omQqmDSX7bzrXiXjrTdtSOrxL1H7VdrFNkU1Dh+pbQNXONCnmW/q9PpWE5//oW6sJV9UmGwPTAKxwlBCtlkWSEX/OwuTgs/ustluabeYmMFsqa+ZO/QdjIRHM/G+uBNyssg+bcaxH6MOtpI7tYxcAT2RqNiVEi9IZVtnGwQ5dYc4mDanZhnWon/37GdmWT3XRhky307+QiZRMBhqA/ZG1LGgNHbrKzVHP+8nYlDp6tJoRtgnXvl2HnBHFPSXq5LN87p0b3HdLyAZesxVrCJZbPqxDaTXgiscyyw5EpN7NI4JRO4/0EeQwuYmlVisMDZTjHTPbieqkcKqUlar502JhEIl3SFVY6yAs7jrDLdTJt1FRVKUeSjk6JKEpgnAImbDXzQmiJfcePvRb7CASyuNTDnHPGtgdU3aeJcS51bZ1OjPL/qRpom/jTwOAFN9rY4JvLp8STA4ww02RvTHYVI5aO4cYCJjw3MY9DEyQamhpGpGZjtJTM3cDNtLXt5YBRQPtnrg7OIqftkbxCODDabQjbi0mNSowKYZ3GJzQHc7H10b5Ibc6XsXfJ+nNIne5u8H9MjMRWXWEfAaqSq8dQx/QyU8pDvc4muVsOqncd9bikf9wttUbtfyedE3NRIHPLQkh/ytjFzyPsEGu96sBUw50q7HzWOY3+PM4aOfz8knt3G5wczjpu+bzDruBLbybPFMA3ibFgjJVUSxoBpA0diM4KDYW3v6g1re+upkcA0i53hcZA1sVoOeiQlMH0bMZQ75rpYKYZVvenxl8D0Z5Jh5U/P0nQ448pOqyfWaUBP4gSmTyNnXjmJkXL7H7G6x7kRO13xzHKuKbDGBnM1rJN9LsRc9soSp+k46PSXJVCDOlKqD3IlZiWu3IjL8ASJyY8jZvpf8YY1blCC4eHvKeZhuSUZJh8ZLg8GW5XFJT2XGhELXSIpEivqOOp8USRYBKz6R8/eBtb+podgY9uEHy6k7B1Mqk+KpFgCqz4pkoHZcdb+hezMJFj7E3AWTD6i4Rzo3haRP9NVlpOkgclXIKZpWuTETOCD8yzvg9R05ZOY/c3DXwbOeua3M6xLscDyfrR1geX9WMm5TIY1VKOePbCajur4Sx2xZIEAeT9WvtKwATD7o2TZOsnxMizv10b/I1bDBljelxuZELMAzlyY04AHsAaZNR4CrPOlRk865klK2RtWWwhurGKdL/NEzCW21AxzI2k1rF5WKsRB7ZOUDat5lJJh7W9JzpsEVn2cgmHVf8kTs/IVfFTF2h/TZFhrFEVJgAbOttGPWMejBs+Atb9qbM2h/o5u7IuGdQkmBsM6v8E5VTw2Sunzs/bHaPJl7Y/R5MvHYUHgsZGSmInpxbD2P4njAxddtA3GVxkbU4l1/AdrX1aqDta+rFQdrH1ZoytY+9aR0UTsbCMBsW5E9Na+PLnJW/tyo5e39q3VVggUN0ajPfsjK1djGVUxF0Umw1w7boaZC1IV29m0jvJg5xPX9fR+CpZRqrjzOR3dRwZbOvq9atl+HH/IGOH6s2JuHJI1ccGMzE+8Xw7vo5gJRd4wU7WC4ZrID3Gz1HHFPGnIU/5ax5IvcbKNGsRc+mmGWw1H7VvbiKwTj43ExPWk/1TWxwhsvzYxnJ4Mq/1q/ROBsOmo/7ZJx7P178blQ+v/zU22kZVY43s2fhrdIxtfWMtLf1mk3EmhyHw0XmFeHI9nLBUdj3dZlfnLouXAuuho+qFxY5Tpj8ZcSY2fA+t41wB6xzj28i+LoAPreNcQOnCIps8U63jXIDqwnj+rUXRgHe8aRsdiq87vGkcH1vGugXRgnW+SycdK0tnkY/2SbPJxsTNTfzWudmbjk+ubNj80rmhma38uadp8gthQO5pvGjdmaUgd2yh1fNr81bhwafNdK3qefW6G1f+y+bJVDQBiflXszb6T+bVxuRLzuezN5wJlCYa1/2owvWP6y4iXK6b9E3W+b03nJwTQFet6ArDwQ/+5ZMM6P5Ws9kVrw16qsnef/rSG3J3492J/VMPavzXoDswI9mSY9vsk9hACABpa9oY1NRAYK/1wyGnPE+uSO+LlSbCGF7BpJwrW/l7VfoND6BgFV+w1yaCqvQcHrtH/IFb9ih1DTjDDFZPYk+JgMV6R5X3DFBkfIFb7Sc9hBw52xnmW90Ntx7/E/yROFvLN8n6HmiuKmZEr9VgEH85ZVsx4q9r3MNjVn1Z/AAb54ZBdeUHm5WoBKfnAavo5cVHExtaigN4+iOPg1CwkpMNJsknemqcSYy8aP7DijnLAFaIkVt0x2wec6CZZmcYHzIyaqn1gRUdls4xEWniAb7MPrOZjE29cpldNEJv4QXGHgyKFn6LZAVLZVT/QCUQ+EMbsbDE9qxAfWDHpYB80/oasLfQPuCYr5Wf1A6tKpGEHTAosS5TsAzuBMNsH3IWhRwQ6qOZRuYgfpHEooNR1mFo7rrrrZDRN9kGRD7w7quPrpL/JN5Acn+SDw8lvUrzBWR6z5stjo7jVRpZUdXzAVFAvh2kA22lWcpoGPrDj3yTbGx/ompDTpIyOvR0Ap3El5/yhtJ0Udeh3eASY5CrjAytul+0D7lzwssYBMWjLIDdYRA9O0xY1WRgfMMPKS9ouPkjjIDGtRcJCNXJyhojOVWIpXS2iR8sNlKA+PrDDSSSOjw/KqKEm6UrOTp7W4w76BymN+tYIa5aGOCsl1Q+c5flKQhfOLraqe1LyHB+wj3mpiY4PmPyr2XT4II1TPfgBU9c0o04ORGYKcJZwaOnTPw+GyxIPLchns2aQbLmR0abJR/iAafGafoQPrPqfJCDhA55mpClI/YOoQRmnSUj4IMTxgUhqm5A08wgfcI+HJgLjA+unGgktLblmxRelTHlL1i0lzxof8NQcJHyLHMnqK8pKV/8gH84lDCJHPincjQ9oT3tnHxwdpSWC5UORySAUstYBi3njg3o4LVEkLdM4LjKKpCWwXWQ/CD6wjqvDtGBN53gg9w+Yn+7lyGFXRr4gx37/wA4Rla1N+IA9WXcV4YMYhv7QD6jo8YG8i5Xf0+1g+ID+i55cig1CLnj7QN6F6yui6fSDfDj8VF5unP4i+U6uC25aWzKe8IFGqVnf20n5naGC9QPT2hqYxQkVTP/19gE9Ka35jQ8sudjxA2dqXIOV2KDHYmoarcQWPdbzlS2o+KCOArn6gbdK1pJD5WRnXuA8xw84gCYNEWJ33qGYt5cP7PRlOR0KHzT+hhz/i2dyAE3OPmDB+0kOKMUHVldusg/yqI+c5W0D10Sb4XG8eZaXj6r3WzXsrIJ1Fi7iOPCceByfnYUJ7lFr2XCxBVtiRpJlf7LDzkJbeSBmakAUSwy7is2SKsJSirZwRUxLyoulh92GtnJEXG0hqwhlXGGrUkUcWMccLEHF0SLTRfjhEhwsR8UaqZLN5IKrRaoVc40OlqnwwxIbsFzl/UsMtGyrvC896eoNV4tUV3l/etLVEXdPWn5f8tGAvUWqq7wvS2wUqTgOnC1SXeX9uCwIS18wz3CCZyDvxzON4EnI+zQ9o7Gopw+TxyLVxG14Mng/x4XEMhG7ETmWhVhHTzZXw8HSk4i1B+dsOEvkMyfD2l/pyWFrpkb21PNDnEH6l6ZZAWvkNDvD2j/zZJieaiPm/r1UDWukKamnXS0JN2XDGllKybBGllI0rP0nBcPMRvPETL2VfFDBzEdTz7da8i0994rsW0mbq4bV6ojFsO7mitmw9p+YiJmBG6Nhtb9jMKxbMaI3rFo6mryVJW9M3qo6GpEPxaMEDnGKLFmjfDITl5EVbBfVyEwmbieRmYrs26PIDTaQagm6YJiRmcD+Rk9W8yaBx7Z9wahgxz1p0n89K30CF8FqXftG7CarWyHjw3Nl12tkHEcUWckWGW/+UEJFxqOnp+mbjlfPrZe+6XhGHi83Tsj4t8xdyKO42p5Q0R++e57MRxX94ulpBvWssXfVinKIvrIE3TCpfvNxlOgj1jNDfFP96G17Z1P9ObJym+rbPmvZvg/Rz952eOrKC7ajeG4xEX3v6TR6zheeZ1RpDjBws+IWMh/5PCKFMoF5+oc+65Tn6R7qkcfAjBQmnUU9y77rkckdc+VbC1EAa2VarUQBPPaTyFzf6Y+2W13ks5I2uhLacbO9DWJfWGIuNruLfGCMWOSrap5jb7zIZyVtZK88MEt/yB72jrkSrjvAgb1tA40iTwu2IZGYe0vU3OuYVoQc6QJMs7waZhEE2a7cR6ud6VXFBkVSkG0hIT7swXaCo217glVbkVzMIgMwezsutkUQdjF2HFlZBcVcOXfq8NXAsqe6YxaY5ns0HG07OWz1ainHTjMZsL0r2fZceR87w1qtfxQQp20v7kG1LGTdOglshrz4eTXYDl7ZiIjMrcOOMZE32Bk8vB99PXzfa2hHn6/YcT+aZHJ4OLiRGyzhSXbMytiS3wZcbesQTiWDdztxZz5OJYNzq35XwvsD0w2VFD9g2u7ZML1ScZ+BablL/iAwt7HJEcRYFuMZfLLbG5jxGdlnDUy7HucVC2YwRvoPMGMx0r86TrThZZc+cLSViCjy9eHcRv8F5tYM6e/AzVZSosiXx6atKPJlblpzhnV+0PEFrJknwCIvS4x6CfMAc8OGjNeOCzc6yXgG9radMom8zEzQ8Q/MnVCiH4B1/Gm5FmA9rgBY3qc02z2T5H3qKMFDzM1FElLzUsGeu0CyvA8zFVS/AY/9RVnehyWitGgPcLatLlnkZZ6ul5V3382xyTbK5CKYe0PkyFtg1jOQI3GB9UxfKZ8omBtAUG4Q2PbmesNceUP5MMGcfxyxV/cUOAnmXg27HzVvzYs975F46Kfj+6mNv5f7PFPMS1a2h/lntWcU25lLTTEWYrkSp7h3SI4Hxdx9ifJkgmWl96//9/8B8VZZsg==',
          'content-encoding': 'gzip',
          'content-type': 'application/json'
        },
        gridID: '1',
        msg: 'MAP-DATA',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function firstFullcleanCharging(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 15,
        cleanDuration: 31,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: 'scheduled',
        globalPosition: [
          11,
          -5,
        ],
        msg: 'CURRENT-STATE',
        state: 'FULL_CLEAN_CHARGING',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function secondFullcleanCharging(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 20,
        cleanDuration: 32,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: 'scheduled',
        globalPosition: [
          11,
          -5,
        ],
        msg: 'CURRENT-STATE',
        state: 'FULL_CLEAN_CHARGING',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function thirdFullcleanCharging(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 30,
        cleanDuration: 33,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: 'scheduled',
        globalPosition: [
          11,
          -5,
        ],
        msg: 'CURRENT-STATE',
        state: 'FULL_CLEAN_CHARGING',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function fourthFullcleanCharging(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 40,
        cleanDuration: 34,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: 'scheduled',
        globalPosition: [
          11,
          -5,
        ],
        msg: 'CURRENT-STATE',
        state: 'FULL_CLEAN_CHARGING',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function fifthFullcleanCharging(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 50,
        cleanDuration: 35,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: 'scheduled',
        globalPosition: [
          11,
          -5,
        ],
        msg: 'CURRENT-STATE',
        state: 'FULL_CLEAN_CHARGING',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function sixthFullcleanCharging(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 60,
        cleanDuration: 36,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: 'scheduled',
        globalPosition: [
          11,
          -5,
        ],
        msg: 'CURRENT-STATE',
        state: 'FULL_CLEAN_CHARGING',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function seventhFullcleanCharging(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 70,
        cleanDuration: 37,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: 'scheduled',
        globalPosition: [
          11,
          -5,
        ],
        msg: 'CURRENT-STATE',
        state: 'FULL_CLEAN_CHARGING',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function twelfthFullCleanRunning(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 80,
        cleanDuration: 38,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: 'scheduled',
        globalPosition: [
          11,
          -5,
        ],
        msg: 'STATE-CHANGE',
        newstate: 'FULL_CLEAN_RUNNING',
        oldstate: 'FULL_CLEAN_CHARGING',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function eigthFullcleanCharging(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 70,
        cleanDuration: 37,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: 'scheduled',
        globalPosition: [
          11,
          -5,
        ],
        msg: 'CURRENT-STATE',
        state: 'FULL_CLEAN_CHARGING',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function fourteenthFullCleanRunning(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 65,
        cleanDuration: 40,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: 'scheduled',
        globalPosition: [
          1898,
          -812,
        ],
        msg: 'CURRENT-STATE',
        state: 'FULL_CLEAN_CHARGING',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function fifteenthFullCleanRunning(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 55,
        cleanDuration: 41,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: 'scheduled',
        globalPosition: [
          2613,
          1054,
        ],
        msg: 'CURRENT-STATE',
        state: 'FULL_CLEAN_CHARGING',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}


// Resume Clean after recharge
function sixthMapGrid(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        anchor: [
          12,
          116,
        ],
        cleanId,
        gridID: '5',
        height: 144,
        msg: 'MAP-GRID',
        resolution: 43,
        time: `${new Date().toISOString().split('.')[0]}Z`,
        width: 144
      }
    };
  };
}

function sixthMapGlobal(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        angle: 90,
        cleanId,
        gridID: '6',
        msg: 'MAP-GLOBAL',
        time: `${new Date().toISOString().split('.')[0]}Z`,
        x: 1542,
        y: 2537,
      }
    };
  };
}

function seventhMapData(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        cleanId,
        data: {
          content: 'eJztfcuOLDly5b6/Qqj1XTjfZP9KoxZ6NGYECJIwM9JmMP8+NDvHGC9/RGRGpkfkdbCquw490sN4SBrNjEbG//3jn//t7//473//lz/++re/Tb+OslL+/PWXg6L1clC0WQ6KNstB0WY5KNosB0Wb5aBosxwUbZaDos1yULRZDoo2y0HRZhGK3C+U3YV5zXKiaHdRXrUYRbsL8roFFO0uxiuXg6LNclC0WQ6KNstB0WY5KNosB0Wb5aBosxwUbZaDos1yULRZDoo2y0HRZjko2iwHRZvloGizvCpFLxQJ3Z8id3f5TSm6n6DdSNqXoscI2omk76bosqFvQ9F3E/SZ8sMpekuCnknRVqM+T9AbUfSxZv0GFH22YW9K0P0UPaNxb0nQvRQ9p//fkqB77aK9KdqRoHszQz5L0XMo3pmi1ybpDSj6jQm6j6LPNO7tCfpqin4AQa9O0e70zFE0L9pB0apwX9PsH0DR8zyrzxC/Oz3rFD1nPVpu8o+g6PnloOisUQdF30LR7uR8P0XXX/4mJH0VRbcE3H75b0/RtFB7P0G/DUXzX/z4234IRfMkfJSgH0nRmrp+nKAfSpFbIeFHUvQVPtqjn9+9fK8D8jFSD4reiyKp2puUN6BoukBf2/y3pehUvn6EHBRdvPcYRSuNPij6IEX3vXH38myK1iJFH6HoBUha3wH5WHOWGvnjKPqI+zFPxlLtD6DoYyStNfK67g0I+g439vILl0n7jSm6p5lvS9G68B+jaL7hb03Rc6fa5+h9WYqelS/7pgTdR9Gp/IZj6Ospet5f/1CKnknwm1D03STtTs9HKPrsgd+Dojve8eMpeoyg+Xe9FUHfseh/lPDdqfkIRZ8h6DHKd6flVSh6gxG0RdHn7ZqPvu32Uz+MonsyHT9L8NtT9Pwg/44kfdVE+ywdvwFFX5ER8PIUfaxJzyVpF5rupeijzXk2RTuQ9JUUfVU5KHpPivYm5aDooOitCXpVXbT2jS9K0esQtANJ91D0vQTdT+NB0auQtE3R3lTsTtL3HpP5gRS9djkoOig6KPopBL0qRS+VJvqKFEG0FyDnHoq+Jmp42+Q5Cl6EoH3sonuFewmC9jIdP0LSLvS8PkUvUQ6KDooOin4sRW9F0l7xot0b/kyK9rWNXqB8Z5bab0DRiaavSA192fIoRddkHRQdFH3kgMNzSNq94d9D0W9C0md/2eqg6KDoGb+PtkXEQdGv6aLJ6wS8IUHf/VuNS1cZvHT5boresBwUbZaDos1yULRZDoo2y0HRZvnzz1//8Jc//uOf/vff/9d///1f/vjr3w7C1ssxpjbLQdFmOSjaLAdFm+WgaLMcFG2W9Ut5jzJdUjQdJM2V22D/7iK9Wjko2iwHRZvloGizHBRtloOizXJN0e4CvV45KNosl6bjUWbK9U/HH+WmHBRtloOizXLoos1yULRZDoo2y0HRZjko2iwHRZvloGizHBRtloOizXJY15vloGizvB5FS4ntu8loFL0mJS9F0fcL8Ag1vw1FHyHlBUj6Ll30DHp2puirv+igaIOMZ1G0A0H3U/SsUfC2FL0HQS9M0d7EHBS9NkHnFC2LsDc1L0LRmhB7U/MSFK2LsTc1L0/R3sTsSM77ULQrQcs3iBwUbVL0SiS9LEVb1+P+BuRsU7Q/SbvTs03R1zX9oOig6PuofIFyUHRQdFB0UPQK5aBoN4rees/jeyj6MQQdFL09RS9B0kHRQdFB0UHR3vQcFP0Iil6ApIOit6dod4IOig6Kvpqi/TcZX4Kkg6JPUfQa5aDooOjHU7S/NtqdoIOig6KDooOi1yDp/p9IPf/vg6KDoscoMmo+Q9Fb/6raXr8i+0Mp+n6SXuIG7temaE6Gl6bo2Q0/KDoo+t7yshRdirYnRTuS9D4UvcyRvWWx9iZoN6rekaJvpumg6GkU7U3JQdFB0UHRQdHrEXRQ9EoEvSdF3xxFekeKLmV7CYr2puLFKXrlclB0UHRQdFD0CuWg6KDooOjnkHRQdFD0cynadnu+hZ59KVoT62Xo2ZOidbFegJhXp+ilykHRQdFB0UHRK5SDooOi76Lo+SH+3Rv+XIqmg6L7GvPcpL7dG/48ipb+8KDooOg+ipb/7LMEvRVJ99y9/zUk7d70r6boN6Jq+0cuvpai3Qn4eore+rzi91H0UZp2b/xjFE0fFn6dgh9D0XWDH3vJHCVbn3ujct9vNT6vvClFuwvx2uWgaLMcFG2WP//89Q9/+eM//vmf/+s///Xv//LHX/92ELZejjG1WQ6KNstB0WY5KNosB0Wb5aBosxwUbZaDos1yULRZDoo2y0HRZjko2iwHRZvloGizHBRtloOizXJQtFkOijbLQdFmOSjaLAdFm+WgaLMcFG2Wg6LNclC0WQ6KNstB0WY5KNosB0Wb5aBosxwUbZbXo+jlMm73p2ibEvvv35CiOUrWTzPtIud3UvTZSfPjKfr8ZPnxFH22/CB1/VUr0a5X9Tza8HtI+U0p2vcippeeaPcuyl9bXpyiV/jtu5eeaHuPH5Nip2++n6J9y44yfA9Fb3q6EWWbouco6o//9e7E3k/RXs17E4r2bNobUPRRIZfOXH/+Hd9cvo6iz77xZdT7/Q7Is77yXsflLSj6Gk/Mnf3vj6DoueKaK/xjKLp3et07Ls4b/5tRdN34Z424H0vRR/5u7V27l/t00aPNmvu7j7xtd3qkPHcUzV2X+fG3/VCK7v/rrVH1FhPt82J+9q9fgqSvpej8Td/zN19QnkXR7WJ/mbiw9Kk1It6CoscMwqW6j+/hvwRJXz/RPm5M/jYUfbwcFG2Wg6LNclC0+f6Dol/32N8vQNLzHJCP+HNvEZx97ija2vW432x8GYKeSdGalex+Pfq2rXd+Y3nmKNp2JQ6KFhv10ZyAH0fR88sPo+gr0rT27iItrz6KXp6ivYf6QdFmeQGCvp+i2zjkd377h8q9FD0j3/ES3bMp+XYU7bsfslu5n6JnbBl9dHd313JPruO9Il9Ppdu6R8ubULRGw1eXt6TovsYsbxh95q07lccoekRlP2uDaSdiTuVxip7z6Xvf9MMpWn/Pj6To+eH2FyBguzw6ip779T+OoudOjpdQxfeUZ1P00Sj1ve/doTxG0X2f+ootwoOiB9777WTdT9Hj247PsrFv//Zbafo6iuYa8qymvSRFHxniX9eQl6Xo8YY8IyAy/+YvJ+ZUvoOi55cXpegbhXotaQ6KNst9FL2Ns/AV5TUpWvq+XbrqXoq+XbDXkeNVb57Z2mn5Rpne50a+y3JQtFkOijbKt3r870nRMYo2yzGK7igHRXeU35qi9dxbm2TfNo5elaK1p1+1y7JQ9qXIRsNt6H5bIf/4UeQ2yjRL3DevZftR9Ggz5wj8xrIPRY/RssvYOZVXpeiFyl4TbfeG318OijbLQdFmOSjaLAdFm+X+nyc4x5/92h9HkTXKbJTPf+2PpAgNe5aF+2Mpel4jD4q++K+/uRwUbZaPUXQerNi9CV9dPj6KfhOCPm86/gYkfZ6iH0/SK4b3X6x8nqJjFB3loGizHBRtloOizXJQtFkOijbLn3/++oe//PFf//7v//jf//o//vGf/u3vf/z1bwdn6+UYVpvloGizHBRtloOizXJQtFkOijbLQdFmMYp+fIzj40Uocp2h8MtF/d/pV9pbplcswpN3v3z85ZP+r/vl8q+2t1ivV4SoMP0K4VeI+r+TMObqwdV1UabKrzj9ik7+t/938AdZc0Woiv5XrL9Slv/t/31iKx8a67woV30Eta7LJ/m/E1+Tqq3dJXylImz1MSVUucmdU6Y6zOuiuLuUr1Nu+AJnMi2daHyZkPEwuE6FhHE6HnxtFRikOgsvVdgxH+cK6BqD6mJ5PNT9TTlG10PljK5De91TzqfjMcC2iw0wEnWor/VyoutiIh6u0Hy5mo2nafgCPvb6yd/dyHoJj/HERPp1Ck7e+c8+Mcwr9sYw+84ZuURW/nWKXN75z04Bzitr9nuMjJnRtszYKbR55z97RUBnB+SXmR93DL1F5kYI9M5/pr0WvMXR+RVStPtG4RJ5Fiy9858dNf41q88U43phbTIN7xqLy/wNX+jxf3d0BRZNoUUx1gySu5dnsO3vGqd3Ees++O9+TuuJ+TvkWCGUds0i7Xes8uyIB8b2x+mef8f+MYT7J8KqBh4W0jr72+aC9cfTRv800wUXKmhpqr1Ov6wtA5sq3AyujT64x+qol4Gvewb7qvI/dcDt2ns+JV+nP9YdGsyQdQUPa22T+fvslettpzn+bwm+IvtE+Y2cFKy+SsCTm0fL1tn5bFjW7DPNXfjkA8bNKYS+MLpnvvWS8PnhofO3/0d/+kIbjbqrvuh8pJuJsKrQZ4foJvUPmTqLo/2W8Cvy51QoPbjbTtndpl10sZ1Olat5sU7kV9mec3bo/Ay4Jf6mE64ND/3nDTrnMp7kVJ7+cGauXKv3j9icn/XPZqfCHPMX9vmNlb7UM6+ky65mj1uX+n5L8/41eSMicTOIrnh2Z27qicNhvb9oB5z08vWSstIDl4wvWpT3LsFPDrVdmeaqd19LIZ3oujWpxhq+NFouV9c1atfX2Dnv7+OxuFvTY8mAfYH43vwQuFqqZ6kff7dO7/ICehqUtuzO22iz4i2N/HQt++jKVVWzU9R6WfHF+dV4tgOmVTPldo28Gp1nWbyLi8ulgKd3znXEuZ6/asCNh7JfHsZ5nsOMMCNwtLLqLnijs+bI+eo4N0AvUlOuVMWleHPr7+U+1q3Wv1m0PqjavqAPFs2LYe2td8CsfrgJlaslsmHhTpTiYhG/9tdmLTSUW71/6+RcOhu7pYnMZO+flPCZEzS/7K6nBZ2HwsPZf6xZUGc5DieZrjLml80zfO/atJqdqzsehZk9EXDW8IshN2s6LJy4uIqQxwsXdYn/8/eNQOCtmbjd9dszbE7Tzmiwb+qCWx5u1qkrU36jLVzCbiPi4dp7nVkZT/rdm96bN4m2vn7DYVwzuL+5E2a7Ya6h94eDZnzIWYP8UtHdGv8X7NyEc9dc8BkfdrNH4A3suSScd8jq3F1u+p10zCrqGb3n5vrJU51tKZl19/au/sAisfNhyYuV+loJzwbp5ibRUk/M6utF5XeaEsP63AqL3JrJt+vG/Z2BabFblujZ8dAr22lZLZyvKovG6rpptKT7LtJCh7N9u98xO2cX/N/H+mLHgwZnh5qvDuzebN9fCl9vBvNpLG7Nh3nVd9apFzuXl2Gk24V8Kf/5rfqAZuWlobMQtLh2gGZ9oPXV4VazXJuxF7HAOjP16rVCmxPinbpA3K3LZJdNhXxa3ubcoCV7aSEGO2dAX7ht899+ted9PR+h55c8ihUVtlMvXDi+d+ykXayFt17QnAsxy8GMkrpWeLOs3Qh3G0zUAbHdAy9xNuj87p07NprnFuLL4TObSeMvl4WZ2PKtqrl08G5966tOOVdF227cfvHTJfKvlMdi5G5Vgy7F8K8DcrcL/WwQ70Lhzwz6GfWzMupfaQW4vHJqKI21kOk68SsRnHM7Z/npvHpaWIPn4h82lhc3oF504G9aDZv0P/D3y8+X+F82wmYD1CcBl7ZgX7Ibztfgjf36D82Da3t/OwQx0zPX62288TeuXcR3MEatE26StD+hjW6WgUuK/IXDNxfuv14sZ2IZt57GpUHzRuQvZGJ/kP2rlXzusqfbra75QX4Z0OPfXmUd3yTdvAvvs+6S+TcfIf6sI/1NeMPPZU/cmEgXUaGRwltWPa7HI3E7OVx0epcsxJTXV71F1hf83ouAp7vh/moAr2/zLOj3+7YHdnezlPhVC3Fe+97F/mLg5yZCw0647aWbqbKYvnQbCn8LV0s74N5k0Md7YCn8eassFnvqZpVYN6DunQKvo/Mf6oJpo9VX/N+ZJHGWo7SwjXxrS96vV97D9X14JjzUhPtShUY63LJ/dE/obWFp/bE66YGeuHdGnG/ULycLz37xvGdxvV/8DkHQPRaHFe209tHZgb/uhqkMK2nJL2Wcfp1iWmVg7iVbn73uuSWXYzC6kpX/nlNhUTWMTrjOjNq2ga9X6tmErwXr6mo79ToBZVUnvtKa8BRHYS7VfGNVmB2JswbrrYNxTvtSFzzmJe9pH636yJvsL6eZLzoKN0MQ21gLu8nXycYL//1+O/MbUaGZJfHCPLwKJ1yNvgU3eWbRhGN1dRTg8jKdkScx+99XjsG7UL8QCN1aMa9yR27H3nJexFVyoj9Lvb7tqYt5sgwulfi7aH35j3t2AC6ovwyOLZp/SxeNhLllczZp4tZwv6Jx0Z6MOEyy3AcvZPngP8+vztvugjlv6VaTp9G7C7HppRTr2TF76u9lX+3tu0HK3Vb72pp82aqFa0Zmd8XGjvvQ8PPhuOso9Ydc4pfTRtOquO6aiNvqJR27eMHI/PbwDaGLgemrPZqfsCCzE7YEnlcQq83Kq7lel4TehiGuPjM3is/Tet/fLL13RlwnuW1GvbbThmYcheWo9fUWwtUx/vd1ys46YCuaddLYbpnV8xDD+vbh7AS4JHs+oraybXxvZOJV+2AzmLV2EnY2WrG6Is7tJbilj5yqFnXTbXTu0T7Y7YzZ2TzYsufcgvs2uzDPRjxOquQmT+WamZlj0StnSR46BjszXhhW+faZ8JEeWKXiMnV/Ju5xtUO/NqQvAkEPHv1ng+7bKDhfRs4uaXjtXrhvdzwvxkBuT6/NafaV4zzXwdsFvT5U69JJ59ucpvV7RV6hCx5JabgIhCxe93GxyM+FkR640eByWV1LqLnyKdaCva9C/wdSGXQQPnwnwUklXM2flfswFhzepYjh9WTYCvq+Rh9cBam3jIs7TJJFVXL2w6sz9C3YV3dfUDw7t9Nl+uSsdfVyvfDA0YitvJ5l02YhBXh+6ZgnbGjCzWGwHQZ+uU54ICFk62K1JfMm2vbNbK7KtZW1/JvFy/rwWoJtI+uVemE7yfN8RC6ZJrcK6Url3Sqfa4ty4UjtdVk82TA/IfZwnz/QC49mC65sUM8QfKZ61pTe1s8lORVh7nDDhVG2ZH/vwP0dHuU9LsGl1Ev703PkLofprjX0vBN1fiWYfeLqiINf6993I39bZ845Z9sh8fNY9taKy2+5+hWM07SYt6w2TwN+J/X36Jy7PYFlXtYSbOeW2a01dtbxvjxTv9n3b0b80l7xdTFFsBonvRjci2lwFy9diTiNs/Rzn3kzzu82M6/L0kI7H3nY+LXB9ZetZNxtrKpf7uw+TvjdFuV1WR+TD0Ue795oX/7UzUb+dzm2Dy+lDxiP12VV7Z4vfItvuNPfWzv0sRRN+CYf9sERfkcIc60smxf3RdLvH9nLIcGLZnyzr/og24/bKddlzo6+b/7ef3JiObH31I4Pzc+nkr0xeJZGxEMazz3UnM3tjqXtyeU94R3JXXbMdxJyZTNjbeNx7SbAfahdsgy+LYWO99GyrGZzLmwk3rkF+a20zq3/W9cdPKOcq+TbHwm9vQX6xhVc+7mEV6H2xmNe9bQ+a1GeJy6f32S+9OOpC3n4p58levw+si81i+f4PWv1Ruzg88zO/gL40m8vr5wwOTP5lvaoP2PsPJlflLWI2Ge+9HwSn+uf87F5+8/y0anLPfl7cjC+0fJdI/gzRuoKuVuHmM49rvN/bu3Ws+3NK598LdXo+/XwOskoj1moi9218KslKc84WHP/rmxdtdlfH13K49sjFncPz88o2z9PMudhzf27tE+5zv5t9vArj+ynMH7HL1p+9N+1d8xNksvn+wQsvof3jTDIEkGf/fe+TtsjKnevIr+8CfSJtN/qgvV/tkf3emde5LR9W7D5IaPk7AfZzr23xwyVVcrH3vi85TdrBa6urbO9eJHB+V1Zg4/Z12cm9oU396DRvRldHavX2j+rP8g+Y6jcjOHv+r2qh33EpWPQj46I+3m+9cfnHfOrf25vTJzLmvqmVPzHQx3nUi/Gy55O9HVcaTa6dPXP3NB4qs/7CZa3YnVX3sJs8OHOQf0wzzfBUTcffZpVcc9zej/B72yI+frAzcVqMrdd8dCy/TjLVwJPc+HTOL9kf/dxqXt3R2ZzaRfd5Fkr9A6m7/9FlHkf7nyA31qeOxK7nZV7bYMu1c26VvepvAeumNvxZ38+Qu/9qed3e8oLR+nWOb7jyO2bEnxHZv1ZjttqUO2cjcfcqFWv5H3pHRbDWljh5HTNWflXRDweCPiR1N4kws+HZk88bV9mtqiCV6NbP5LdzXMeF9T6jZzgVQ18h2pYurbve4/3PJHde36l5SxJ/uTV3XEKbuNQ4Q259ySo7M7dvdyuLNKnYXd+/KDShF8a7beL2yPjbuEc7bce0HwWtesW5nW6GqypudNKz8jIuvXDv/fo8dM4/dD93LOHYD6bitU+G+ncnc7HfPnzmbcQ1vncWv6pGP0rk7k1tq4CkU9atl/cdJrjcMNyWVtgx4Gj91+aP8XfunGytob+hKX3M9Rttn95oXz31fUztH2i9e+8gn6Osg+3/V3XyA/R9eevf/jLH//5j//nf/7x17/9zflfTlRQjb4TKT86IDBPLWaD6Vd2U60COz393+xCHjB0mFwSmAhzcgZ9h6XpmzNhC9Vg/3AMIRrsb461JoP9e1OYisBCmCBGgZCp+Gaw/Mp58tlg7dA3b7B1GINBP3VY0F6BrsOaxtMuZG4tGuxCFl/i2dOW8EUVT1vOCpvCMoGr/iWAIYuQMg77cCwutmCwP/XZJ4Ouw9rGh6dfJTilTmB/ewnBR75ZYczBYO2wt99g6bDE8TT/KhHkKEwd9k4y2KWK4FlhlyqS56ZdVmIJk8EuZJrcgF3I3vmOcOpSJd8G7FKl3kl8s8LWTKqpC9mJzmygwJKittcT5qp/Gwgb2hsBq5uSwf7m3oHFYPtVp6k0wi5knbyzv+1NqFNI0aDvMAd7VW9+nUqxV3Vy5HuqwdShg8xRia0u6HgWITvt1UV8b9BOqa7kyWCXqvdnIewUVD9Fe9opqD6EAbtUPpbx4S6Vz1M02KXymCkKu1QBXSZSydOIXpDFQmDnRl+VCXuvCCyEFU0oeFVqaEIFzB5NENjbKx189rRytFe8qrcvGuwyN/Jc0aLGwV/R3kbaK9hozWXCzlWbJghZlck2uTZg6ZDkVO2FNmXvDKYOy5C592CbOK2q9m9z4fRh3yHnUdWx0VyJJrNcs+HqkLmPq+YnVSMKu1Se86jqmGw+TMbG1KXyaZAzdal8zSbG1KUKmEdCu8DIXsiAqbeaPaiQvRAJU3Q2NQRypnhCzhQH2NV3NNibn+tkc19gHzr57GktZXy4k1M7HwY7OS3ge83/yhDSE5J2Tw9jDFGFLkZOHMAS2EDAmtl8hX6QA+gbOxQwYuLwpKJPRaQK3HrzfYALdITQOYFSBad/GzxdyVC1SYqlWcnHYFj+TUFboVjoTjkPLL2Rqs5pxdJZqcWBpS+z1wkToNQ6xoKgWMdJckMeka+/PhOLUTJlrJ+KVU9OxeR1+k6oNsWqKTHnFKuqzKO9siSLchvPRb6CHlYs8lXoN8U6ysPgB0odGk4wtDo6XbHI16emN6xtcnVgka8FP7Aq6zwZnzrYG+afYpGv1cG/TJXeaX481zkaBv9eVyisOMERe/b3ZDiqtaJYNQDU3XieoNHG81RUlY73pRb8+fdlKLVunbugiy35FCwLd1+nk2HlmHwJ1lWixHL+vAXIF4H9hJkQkmFMhZANdxPKsMiYOD4y5PGpE0As8vpEeTPa5xO0sGJdAZobWP+FHlYsy0mGIg5YSzrmeEjoX5/7BwxrH3bVbVjkK1j0FIt8heMjYXz5wvGRMP58wbqnWOTrVoK9z2kbOB4SxrevUMmKRb4aUzAs8lUo5QDztGNoZcXaxye+nY4Jl0wemZ/dqmwDi3yNyidhfvfh5AdWzmsy+UQ/dG3UTB7RH2GCcalYxxDHZ4L+CRMWa8VOMNYJxeK09AlCeUW9BYeFondPkzTAPvg5GloRmBI7v+l4qd6e6vBqJqrCQE0UCTnwA2CcCucBYBhKV2GEFpkIM9aNRogxJupcYJrMHgHkUk8hUxgLhUKuOZFwWFiAXOopZIXVL1C47n1tHxaqG6gXKEz3VZU2o8BOnA5k+V6B/flEKEFJl4IJqRCrgjRBYTVDQKGnldQIfaBKAoQ7JdQpHAsCYE7UD4DshQgYxmwHDNqEQDEi52pBExKnGp8mGD4CJ4FUtBWQ7lR/WptAqtlKiMVcoNzAkrF6hwZYMIfiRIheiI6wZkcor2oYovxwmrje4lXJUf3ii5LnalcIoykjQOqyRAiLsnOlMEyZmhgQ46rzDEg95ggxzAKlCtBinlLFKdKEBAyevQ8YM8cGYHaOsM/BlKEwBPYRm0rCyOkwCaw22gFpjHfYB3DvIpiuUWFXzW46Pe1+Oczt/io5tmu+ZiHsKw9lVsg52AC9swkLSHIcYawck4DU8YEQk06giEHXM1DI2HUBoXw4DX0KCE9FoBdYHBUUYLPFqkogJydbi2ofot0yKvYqgY3TKhIOfSWwTMNOAqSZ5Ai5Kk6E5KoRDtdEoRv6CjCY5Q4YR5cpzKPLFA59pdDDWvKUytM16V53nxrFvDpHWAvddIURnrfAKpC6zp6CSXvaXUCGJUoRiFVDYB+ExZw8ce87rNBIIobCHKlUAWGRSjhDIXju7VXY6GpnQqwLvuCLugtI7wqQLmAjbGabCax0AXsfAXZbCD0IyCEaCGN1hBIi9dRIEs7osK9tHKJF8v9DgVEkIRiBiPfImwVmRHTkewWWYREChkJVAMg5WAnJVSGE+hI2BFaoa+FKoffGpELOX0pVu6vAXhCZa4GT5wg5fyfAhvmrgSGBIEcDQwIj4mqFEMrNQcjuaqs2cxCyhTY+LDBCP9vTmMaHncDqGTlTmOBrCpwEwpmS8FwTmCxEl6tAuEoSy+tjstFTkqcKMa7saYkWkwMshQ1UWDFUJNKnkCbERMhogSMcbivgmJIKG6MykdBVKmTA4Nm/gPBePFvURvAAsDb6qR320UtTdSJEtCCoVB1ST3rCBIUcCIspN0AO74RXORqtmbBbcoTy4eAzjYScO0z0SBNhTlTXCjMtiv69SSCXVA9YEAwTGAXSwAiAfRLGs6eVFoUDbLTMJ8KovS/kKER7hTqFaK8QK1F8ekF9HgFS9SVCzG7pMoXD6gNMUMieEAa5d4RtDBWB3UlIHEgKGfqthFiAHKXqKwOjyFn2SD3CMDJiBUZGzTv0Ahk171Bu9+urhEHZXUwwpyUW3zrMWHD7mxXSSepiAMJH6kICMjQ4ETL06wgZhvGA3T/KIAeQod9IyCBNIuQCRKm6DnLoFIV0lTylauwUStXQC4FSmZ9EqVqx5Vib3xizCQr7CA50ggHpI0FI76FG+odTH+19zfTU7akIpAEZZJO2Q8ZPImEL4eypeTEdJoFY9+XDAtOwcxTmyUIh+reZnqojpDk9EVIzNEJ2SgUsUAXCpEJnRiAgVIH0gsIR3AXEvodnE0q1xVphdZF7CIBwVz2lokPkKFVFLzhKVTE1HKWqsL4EdmK7ZzrV09Pggu3VAA5lLlx1laNi2NPI9Yh/WznLKiF8XkrV35vKSebQHfRyalH32sxW0S9qDHZ6heK10YJKcqXjhAVIuFIYLCSZJCViSlAUkRDrvkAvsDoaNgo9TZdMSBOxEA4TMcn2Y+CmQSMc1jUgzWlHSF/D41VhmNMKIwNqlCqOeAkgRx2lioyWUKpIU7wSVvOtFCaMuu55ASLyFCkkt9cihcyIqwnsU7IbWyqz7OY3gbAKZH9fYa6BXyRb1hNN8QroOOkKIU3xTBgtQCKv6jKaKgAc4TWFniaiJxxui77KR4vtAWI6e0rlx7aAwkCfjlIFzG5PqSSEwcGgkCo3Eo5Qt8I+7AoHoULObkp12m1TmLAV6UFd4kZWnwsKG01xPm20JzWPp08E7+ggKAzc5euwCOR2RIdZIPVGh32m5DgURYwCEUeUNwtMmKH2tCDK3Vuk0LZ9AiE0cGcDcLilgNgQ7kwCFmikAthoEldCug+NkGODUnEHKVCqNmLZgHQfIFUx9yERcmxkwmzxDUCGZSsho5ZNueoe3xjeCoNNDUDMwQipuuGiKih6QiyLkVIFzqNI2FQzCOwqqHAXqL9WIfd57Gmq+N4omUe+cGNH3iywgjqc3+gQ25g4vOgLPQLcUNMhdU4jZHs1p8l33wqBwULImHwGdIwFZZWquhFRBeyN4IQVyJ0c6RSF9Pc94bAKAGkVTITcLm6AwVsEAxCrhgwkhdnChoAjkKIwMpoH2mv0ZvMDjp1nQKgvDv5Kp8aedi8mnj2VqUIoX1QZDeBT8ya8DhXxlblxBzj0hszQRhdAhFSYKs0AQM7frLO7ubFFBkjrqxJyB1h/cKD/dwxcjwDHjhkgaXeE1WIjonOaGxtoCm1DJRCebLNJ4AhXA8JCFt3uBDJ4XQCDt0kHCPs5NEIEu7gedbfUZpmsVq2vxxGjHRArewyE6KMYCaG+7Ck9oIh1v9Hl6T0iRkJ3PDWaJ7D3UTdUdZmIMMa6XxLtw8KzbIgLzIANmj8WQpjTSBjsEDMlqp3T1URUm0Gg5Hk5ZEMIlJ8t67Z4IJTML4eITdS1uyt6UBeVqw6jZn/ECkgbOOrS1hc2RMairikdQj9HHQxBQl/8oijpnAVpJgJFyBIzpYoiZKWRUAm7ekN7o8hMjyDqHOwQe1WizbrM3U2DRtIpGRx3OqDcOmTzAyE1oQd0mL/REbL5ui4EyckIQ10H1y13U+ZZIAZD0JkSZMvI1FcV2BI3UBSGaWizJpDORSQM2gSM9iB7FeZrOIFY2mC5BRdHyFF60MVoUSbpX4mhM+Asve/SyXQRmVMezoXIzG0OgSJkQiIJnKngui7n/M0iVUYPChSpckn2VKTKJ69cpCqu2IdFqnIKSohUdD0EilR0PYLGVYKr1O0a0OjQJWrvLFJVqvpJYkEdpsKlvIhUdaj6IlJVhuA0nNXNZTqAk4TvOsTIEShSNUwcxAmDxFmo3AR2t3xi8LZIlil3TAR2ISXCyjWlNIEhcaemTgJHTkp1Ak8xcy+weTpxtQvp48hJqVFgsDwaQMaBi0TyO2QcuEjYv0MGnYpsCnRIW1S3DEIfvpHhjipCJu4vSKaLQGas6KZPh7RFJeNEYK104ZsImafAUEkTqTLdf93jC57bOthc63B4mk2kygzCRNlMDGLJ2QaZSFVcGilXIlZhkkbQncqORxhGdjI7TrbdJjudHddkuVKTiFa42nvibmdkwyJrZQgbmRVB1vTxXKTtCzNXXtmZDbIUDyzyVu6lON3ZDbJG0paWnV/d/aRvrFjyc6JhLxg6x1XDweKksrPcMRMBkcvYMZMoE/F5FqUcSOjqQr8vGGbiJHIwO2bmpCf2U0knnHKoUNz8g9Qtekx8foNUYKRQBKmAJcf0y17hJ19PjZAKDBC2UioQpyMNUsFI3TQqOExApFTU4o1prbBcJyS1SAVUhSbGoYLuB/L3pIKjJY0KbBt6bLxLBUxnzRLRikifpYwKX7NVFK2II8eMFcn8dBlTUpGbpYOwoppellHYK5LzZwk/UuGzZYBMSnLCYhziqOhOD9YUGflSUb0lWUxKYYbOCmVUwLwJdVSENiqU05w8LXirwPiLOHglFU1XmMibSqI5COMDsmtgFfqK/geTVei3tpLHn6jk3cIZFdK2NA3XCHSkifaDMdh12ekdRSsQGosT+ylNiJOHxr5Obhpt0+GRJM/BKpxWxGR/ooMwOQbIGwdychiEWqGSSlTRKlRSz1SOxvkjuxXjEyqpZMNYhUpquWVNs1WkAoNQKjRe1VU1lqqm+S5SEZ2JrjG6FDDmAhKGpYJDrGlKjVRgQdMKlTRCVWmFSkr/RStUsD7oolWoYL23rUJDkikOG8mpkSFuzPiECta/17pS46MpYQDpJ7QiZ1i74xMF2UXR2TsK9vuiHxVYUWMYFdi5idEqKmJlWqGCtdqiVYjouZvGhRXauMytE62oWpGat4qiFRgOWpG1ArFrrRBOs8NGdIxkPfd329dqv2Q3XHX0XHbYNtQKpxUYDlqhknqoIKnQ8ZG9H63VEdTntctWoZLSRYqRozDTSYqmxbOn6R85kjP9phg51nOg+R85GzrnFjjAfOmLhR+CqaTdnTI5dM6Joh8VKmkMgw+dt6rprEIljXV0g859VSBWoZKmqY0/UUmTD3SfWMF8uJitoiLeGcuooAdiurErU7h+dVRU3UuJphszd4+i6cbCLDmtkD8pDnpeK4JWDP8KoveR7cYnklYMlwvNL54kNxJUPPR8bKSw+Gq+FEjuJlK2Cu2G7j5BF5piK+a6mWLrE8pnq1BJQ84mmA6HEhB6j6bYCneeoim2Yg5gHRWwPWLlKCx9WXNWoaJHTvXKkVx6Z9s7dKyXhGTvWDkbuieM9aVyvpREZVA5owqTDWPlnCuS72IVKnpCfDdWzts+OpL1rc7srk+iSapzv3CvTCtU0gxTWypUf5TMRatSw5QyjXeoDionp5cVTPtL06jACEpuVCQ1x5IfFVAGWiHf0t2hOComrcBwSJ6SVtl+tIqqFZiEybO11WM4JE8+qkemWfJkrHpMwuTJaXe74nipsF4Del9E136pATnIybHnakBEIDn2bQ2IMyfH3q99jI0KlZTOrVaopBGJ5FqhkjIHUCtU0gjNrxUqacSmiVaopH2NLlahktIrTmZJ1gSvKpklWekZJ88pVukbJ89JWOkdJ89pWukfJ8+JXLl1l8ykqxkaJnkqg0qnOXmqi1oQak+eCkUCId4qVNICuzl5Wlu1YI1KAeZYN76CNiUCN49AYkrEfY3TlmXDmE1JUzsFYzKlQpygOFPh5zNiUalQgL7+O28VTisQb0ymWRv9v2S6t2UYUamQiFYQ3NaKpBWcKIVktgK3P5nR3ej3JTPLWyno9sIua4Uzp7BTG/cHtUIlrbCqtEIlpfOXzG9p9P60QiXlZmbCIZBUJp5TS80qPAZonkYFlE92VhHg4WccjOoVCZxmDtBeAdMkc4D2iqDLRoaXKRXgNMMNlQosThl+qlTAr840kEu3jzU4n2lCy16FcprphPSKqL5xxhEPqYCtknGMQyrKaMukkmZYqhlHOZKcI1CSM68V6BUgOevBCcGgVLDKWWCpCFYxmasoWKWssFKz5gB33KDMsjcMxZQDsJwQURwNI7MqJ2KHBKic8P6uvPD9Cd/ftVu0z1fFWAgyJlDR3HFgNFCSu71VCAWSbT0qvFZAV2SaJ5Lbp7oiJzLvApyXnNg3jnt/ObH3HHf/Mh3VXoGgf6Yr2ysw73LiGHE8YpcTR5GLmHc5cZw5pk1mmnxFMlHsa1FRYFrkbBUV9n3OHM29aZC0jAq4kLmOCqwl2aaIa1gpxif8hIzbXPktnofnMu2VXoHweaZFo3ZUs4pJK9hvtIrEh0DH0W4qnlmSmX56kVyTYBXCeh8KeEdlv0gaSLZ3qKQeSj3TwlPzzQTTzpadi/EnKqkNh8YB023VzE/oiPLcURWscgboeMEqJhM2BauUgd3YMKIlPYENbSpjDJNhFZGbsIJVwgjVmStxxsZULoYrOjATF2eDk9jrXlyOxHVC72n2uGzws/PkkNwv0UswtrIc5VPMjtEMasGcQIGY+6KCi2LsksvnFQcEebs8xFD0ORtG8lUuxJEaqBqGlu98EEMjlckwNFBxhmHEF0+cnEbRi8mXwJ9g6Z9QYYAIlv4JPMFWNJO5cz9hQMj7FDs4kfJ9ij1mpcgDTD3dDGPbLVfD5CMbpk5OxAwe5GgYsYNs8jB0kE2eAC8mmzyhBq4kwBHtTyZP9JlrInCi7VjR/pjhOMjngWn1NfAVC85xyOcVV0ztVAxjfKTCv280vbJhzLjE9ieH9ie2P3m0N2lyeMcBK0rKxJWGGv8+02lLhdizvfz+TJfN2pclA+isfZnel7Wve8mT8QfMFcsRR65QHG8ShAvsD2BsAGWP+ZF5+Er6rymm9gyYb9371Zimzb9MN8TmX/dL0vn868atrZj4voYwqowfla9x0U9sXzvNN+Gn20BYUcln4Y6czD+vmONLTjIr5viqhusYz5PiZoobmJl5ZTJc1eQqzjBOGBVPHGABlGAY/JZoGPx2XIQPSXSMxFUxQgOChQ8xLAtxVox1tWiqesfcsCrJcE35/HmjPgmGmyaVFw9ceSaoOMOIu5TJcLKVjRgOX66GEYPr/AI76utsGCGXnAwjGSgHw1yfNau944hdwj4eSlBM/awJ5R2L0cv3AVM/ZcM0I4th6qdqGFs0uRHTLi+TYWRkFGc46vgr3nDS8V0C5asYT8Xk7dPf+ks+37hFItgpRhBCsIyHxhy8jnNTTP0dZQMySY5L4N9nGQ+NB3NKIE6YP8Ub5vhwhmGJl4k4w9vJzTD5q4a5XhfDSTMYcjZcM+cjMXY5s8lnJrjJ14JuWAjOimmgaTK2YOojzcbuypuXTWRnGH581s1cweAvT4axY5QascOW0nhutuwke8Md9+6czp+bnWrPzSq15xkurX1/N7ygf7xhelzBcLH1jriZ/gLm3Qs5Gw4w0Iph2ifVMOdDM8z1nPI5HrcpJp9HMmUx+fxpvADHwvGr7e12uupzwV6w7AcQO8W0PzSNueOM9aZosnXHBfO5eOJK/eYMI+xSJmK5yQDtIfY2P4nJbzFMey4bLoWOBjH5jcB+omkeIW+33WFmU35v9hzl990wpz3Sl1TBHA+aet0x9/mypiYL5vzwhpGKnzVZN4mD4qjfgLnrlaPhgPGdDGOLK2fD1IfFMOdHJS6IfOVmONj8JsZ8Kc4w1u9i8hXMl2Ly8QxO0XTiVAP3uormEwtG3KhoAkiSXBlb35KMj8DtI3m/YtrHxRluNVI+xdz3yc1wNDeImOtNMYwAdM78fu755EScuN5Ew0PfEHO98YazRTeAC0K+2eQt0L9ZM1vlHpFgEQFJTOqY9o3mmXXMwzTZAXdrD/rQE3vaI8Ew7f9omPZJMkx7JBtuupHY+QAOWK9zNYz1OjfD4K9MhnOjfiAGf8Xki+CvmHxdQK5fwDwtI7goRsSzaAZfx5XjJRuGPSw4KcZGRjdmgFsC5nM5KG1/HxVTHyVi2rslGmb7gmH6P94wdoWKI6Y9XCbDPhlfwBwf1TDnYzFcHe0LYuxMZJMvIpsqm3xmTwe2j5lp2Rumfe0MF4vYASesX6kZhr2dTD5uQCSTj/Z3MvkS1p9k8nGzQbC+jzsJ43lxOj+TZrAJpj+jCW6CGSZOeltlTTxsI3hSjPiBJHw1xUil6MoEz23XgZ/PcmkK5QWmf1QMIyKXquFWub4DO+qPyTBOgmVnmPPBG2Z8IRiuZl8C+xFfIGaEz+TjbkU2eTz1h8njMR6KydPHG/UvsQ+0z4iDrb/Ewx4kLrAnTR6e7ikmT4S+KsYXbykoJl9M+H6Tj+d/qsmXMB4kF0z6K9PfECzzPfOgjmCZ77nCXhQs8zs36CtJHhPc/Snl154XD/9HUs+AsQsnmWnAbG81TPugEAecpJC0N2DYB5IkB8z2BsPUTyZPwPgvJk+cLL5CjP2YbPIwxS2bPHKiBPOfmPZXBh8ljggscJpsfScGn4L171Ot9fx5jqY/FFf6B/b+6uhvF2I/7CNi+AsiPzDbOxGHKbWz9lbzH5zhaPEk4uzrGZ+V9wCUaDhf8F8j9XvHok9qgj4Yz80fYf/JhOd6APkKLicq1r6KTfvxnP5CMX4a/W3y13gaqgRix/ngMX4lA47+m47v5mCfCx/AI7ir86Hx9E2u0G/dHIX9WAwzspuhD5vHoeycDI/xoPpT8oMD9YvigLzHzPWqWTjeG2ZMl+tBs2D8hPW00X9JjZih+FSxHkveKHeNiGG/jucZue3jOQPkKRM3xsdobzReOZCSYegrwfg81q8Ee6BNE/dIYS90zPVnPG96Fqgbk8D0f1Lg57u/wl1H4ED5NTU/yfkB+HeBuHD32P6+MN5n318ni78RIy2A7es42+YecOOWLvjqwwnzh3w1x2xC8tUcjxzZ+xzjv/Z9jgfvU4S8jvHdpOnVgpGymQJxcp58iP3ZHDPtEuzXjqFfE+zxPnxhv4zneWyN43lBNoq8THEj3xOwZ/xXcFQcLI0E2GE8RfgbTU7mampGIWb8czxn/FOw/H2QAXH2vu4+qb9t3x+Y55Rgf8vVUNhKhf0tV2kV7pcrP92/CNY+4GZb38pvN9/C+fgJvKwqOeJCe2cirpS/Gc4TkzGA7bgDx0NXh5a7Q8zTTRw/0XJq7HlAMsN4HpOOf8Eib7Tkj0xcKE9B+2Q3lu9XPhLjlTj30DHtkfF5D/s1Zn6eqRKiTBQzUyLCX+oY/iKOkXTM48IxGGb6mzfMhBeOp8QEh8j+TLQncOJSMPQnzmcKDpEX9gB3g4/3+UA+xjODyV+q3YCC9lUmMEbDzE6M/HxXwHa0FBjXvga2RzIGprPnnf7YLp4zh5Dt7faeXRZAzLsE7HnA+hI437r9FnizBjHPWzuM/xxPhyCSYsQzcSgiiTa1kw0Sb+i4jO9TzJQHHAUXHO3yJIlXtFycXV8g8YyWmVKOcxSCEY+QEwyTYp4paxrfadlSxpvGfzpOdq+VhkgkT8GuOBBcJp94UEFDLmUK4zgFMPQxblfo2MEexOULgqGfcDeD4GSXkwH36Wz3PhTFWE9CMczTsnofRceB460ZDpHjkzjbKRtgpjsIboqZZuk0HiuZHZgvTuO1HVc7JKwhru79Jx7iAS5MstMrTTqu2K+JwTD8kxgNj7xG/fs6BcvI1O+TAKR9flJcGrMcVd7KeGdEfLhVxjvHc8eMtfE8ZMNFcbQzScByHwZxVtxMPuA48lLx+Qj/LibDTEJlf1XGT2MxzDTFahj6PjbDFclfE3EeSYv4fh4PHp9v7eJ9XVrLjNPPN8Zn5fuB2d5M7HBlluCkeJyF0/ErR/Gtffqc++Pj8wFZ2uP9suF4/v2J+tqed4ctnvHRcrL1RPtHJuzF8wL7djzvHX7iM/fVG+sh2i/4xJdi2gvgr+PM9LvJMO0/Z3jYD8CMH6ZgmPH1KO3vuAazd4hjo31EzIw4jWd3bPZHNcz91WaY8e4J2E24aQzxeMGM3zs+dyM7Cc8d8xfsuUf8aPw9E77t+xwvKzV5XB72IDAvH03ZcLH9Xm2fY3wwZfDj2mTxDOXfNcTbk95bIxjtEdxFkFtgM+1HCeF3THtILx8SXM3eBna0f3W/S3Ax+Yhpb+ZfGqIXd7GeP/eI55+eYz0QHBUz3pIMI/6YdL+uY7l+kVja2/98on1bpL1yQQHHC3AOljJKPOyxInx4nj1Nk2H417EZpv5p/PuK+Gof78S4xCAWwzx5USifZVIXyt9gv8dsmIn31n7e2RONn8bjn+Sv2692Glaf995KPEoL7GDfR/IXLKXantsxVvZfsPzpSMx4LvR/x3HYazo+QiY/HA+B++vjfZ3PeP59vEhH3qfylGj6X9vfzZNo358Vj5MJyqek95g80h+hjQMCpSlGfkvU/exuzE3YHxMs8kU31jc8Z3wtBsPMyfeGQ+MZaOJx5IeY9kTj+0OwMzBV2h+ZgRv00j/BvCFR80M6TrxiTfMjsmgXsw+AeTo0aj6EGKfIJ4ia/9DHlkc8Lmr+Q5brVexKgtoUJ7TXS76DYNrHetelWJu8pCAYjmoPRb2hU63RifwCp8mOObSgmOPbntP+i4nPyxifLSrmeqs3jwpm/1XJ3xLnY7L5pZ+X3zYImI/6vkx7q89X4jF/9ftzxGW18nnFZt80w9Gy7oH7Asv1kriav6W48H6TWMGP2Gu0D4AjMnzt/YVphOPvM09gF8M8jJINj/lJPI6iEHP9ZH+UMsYvcW7u/Lns2LJ/FTOdcDxvwz5TXBn/sO+X2/U5X7R9lfl0Nj6q2RPeMP1Njq/KW0z6eCVm+yeMx0r7Q8Y7MC/TbRi/EjDntRjEsEdCxXjv0pxdgyq4mb8GzP1dXKIquNllzcDcvw2Z8rVxganixvw6ueFSPt8YX7G/tzvk7fvE4Xdn8rUwDrQRF/gn4zn0U6iYz3JVvD1PinHUxPRDs6sQqT9aSXbWj7iYf6D6phvI3vQDMO8k8dBPjfaN6DvgcKEfe2/aVQzEPMihV21muRXO4gHE1N/VMI5Gy3ooLtzE/VNZ7xTnYvYiPi9J9efvp30Tk+Fg572Ix+E2Yp5t84Z5msUR1+H/Ewc7+UfMi6FMfrtc3eSzy9VNvjZuf0J77DL1ZJiXi1NeuZ3eLslsiqtdRg/snF0VIOuZc+50hB8YScc4Wi8Y+tJXYg996Yth2E9y/SUw7HG5/1LfJ9ctnD/nwbbTc+gXHw2P3z0gzpU3swBzf9ab/DzThjtR5cRfsJ8jwfOE/nfNcAu87g+Y+7dynaVixsOcta/C/3GU3y5rF5wVY71xsKe6MnF2DRox4vuu8PMpKz/2fs/4l6uGsT6bfJ77jyaf5/6i4/gO3M9zHP+B8TDH8RUY7xrPGY8yPgLz6Rz7O/CGB+Ovq2OXz/gOvMUBF9ZKMhBPZHvD/CESzo/QGF/h/On2kV2PI/NdrCP72Qtijge9LVgwL+rRfNGOmU+HG5AFY3/ea/6jYN7/VgzzV2M0X1RwLnYrQ1aMfFBfDUO/e+hPudi7Mf5DPG72Fn0sV3sX3oEo+vp0t/ek+l0v92Z8i7g0xreattdzvcD6qdd98wqRpu2jfRWC4WDXKDZtX+D92NHwuEytaXu5/xf0MnXBtAcTcRxXaDVtX2T8SI8gCeYFa3pESXC2e6MkgV4qoLADjhxlvYN8VGiT7BbyOiqKXd4iefu9gldWhDYqaIVOowLbaNGNCsalvFXYjeTBvqVgJOvBVK1gqI8HU6WCtk62ijYWD6sY0Qi+ozXbDkCFXWbOg6lSkSxgDoLS1NI4Q6oVDPnFBkqTQ4gVP9uov7XlGELRLkg0SQVLF8k9z/78OQ8oxWZ4HI3F+3jte5qI5R9gCpTpwwwJmbTIw7VSkWzXhBUM26XJeOD1gMmxQkIZ47QcKrztZOEdmT+clcKoYKwljgrcjJ/SqGC2erYKXjSYbDhmxvtSHRW4/iWRjWwbnpPhcnYkSjAWCDnSJGxm2wANhrlBHg2fjiQVxbhPIWfD3AAtxLbhWQ0HS2gkHodadIJmboB275cYC35xhtG73fsFjkyQDoYxlQTr++NImAZmQLKY/DxCVBLb15igkMgPDfqSSHCZuONsnVQcQs7FulGOFDFnziqYJOVHBbIQi7eX8lhOcaOiBObRqRiF+6DFQcwSYbcUh2Z0vyhPZzSViDFeHPRioZ1fPHGBHWw0djM3TWc0yrYq85qJfWDeFzHzDrNh0lYMQ++Xapg0spvlUnXmpRBjWAqW9shlnIG4Ko56a2TlMK4Ow6TapJZL+rJVCMfVI9JfbcpWj63Iakq28h6Yips/pAKHnSpu/sh6qzqzW1gRR/qLVcDfqt7eQYeihlEBPVCDfS2vW6+mueVKdeGqmuauvNak2oiqFVHsGtnaxjTN8YnGiz5qGBUwZMe3NGZqVj8q4FpWW1JaQOxjtEV+G6ucN79rIz3/rRVZK3BTTMXv3WW9W90bhaiAoqy4GkUqsvWsnJXLckt6YMqSVeDYcKmjYgwuq2DWULaKgm2Qku1bKvKqT5+g4yEVTinkTrZWKGONszSNCs5S/u6j3GBeq/2JVvCCjVKsgrdlFBzilQrmT1erCDjUWJpV0MKv06iARqpuVGCJrX5UIEgmfcsKWHE1WkViGtb4BOO0Z5/I2Yal/E6ln7hTXvkLmXrZeb74RMOVNdWzwvEOiur5J3YxeXVWwcvHK47Qy2kmmBx1GhW45qU0q+CJpVKtgheJl2ovZQ5kKVaRsWaVYoIVZsUb63bP92BdB6pVeK2AQq3TqOAEwh1QedzoXd2ogG9QcXI/j0u+dc41reAEwg0kUgGbrCar8NDDNdmfMMBZs1VEeNy12Lf0JVS/pVpFxZ5krRTM82K6WtkWz1NA4xOBx35qsQrGGasN7cCN55pGBXa+Kn8Uta8H0zRdfKJiL2G8wy6nrjY9ureSL742Mr1tCNb9CeiP8YlKSfOowI5aTVbRcOVEjaMCZ7Z04KKCJPtRgSjk6MrIYyijs9M08hhBcppOWooVzOSso4Kp92VU4PozUznyQ2bNm0LRClpvJY6Kkd5oFeCj+FHBhEc3KpBxUIakHlucPK4sFX5iDqtVIAidy6jALnnOo+J00BwVdpA4siuT2XVhVPCknx8VI7XNKhBbyKbpUmCycRsVNN1t3tq906mMCuigxJ/XFe0RecDSKjCSUxwVxbaooC5SwrVZyY8KJim7UYGL/pJpqXTatraKMm6GQUX3s8vFJ4o/XYxTtAKaP+HnbXtF5d4sriTJ46ZsXloiFVD0yY+KerpeRfulcn8zWEWDA5+C/Yll8NknMo3PhF9lzaJPnLcKITlHXomBX2bN4yrthKumpALKMeEyKqkYeW9yzUvW67azVVSt4P0WuPJKKkZqnFwmIxXce8S1WVLBw7C4WEtyr7HboBUqaaYblkYF8lW0QkWXS0L+/PP//X//MeoL',
          'content-encoding': 'gzip',
          'content-type': 'application/json'
        },
        gridID: '5',
        msg: 'MAP-DATA',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function sixteenthFullCleanRunning(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 45,
        cleanDuration: 45,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: 'scheduled',
        globalPosition: [
          1249,
          3427,
        ],
        msg: 'CURRENT-STATE',
        state: 'FULL_CLEAN_CHARGING',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function eightMapData(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        cleanId,
        data: {
          content: 'eJztndmu48iRhu/7KRp1rQvmntmvUqiLdndhxoBhGzNj3wzm3Se2pBYuoVN1KIpS4K+2+29KYubHXCIySdb/fvnjb99///v3P7/89vXrcDKt6NvpF0O0LkOkyhCpMkSqDJEqQ6TKEKkyRKoMkSpDpMoQqTJEqgyRKkOkyhCpMkSqDJEqQ6TKEKkyRKoMkSpDpMoQqTJEqgyRKkOkyhCpMkSqDJEqQ6TKEKkyRKoMkSpDpMoQqTJEqgyRKkOkyhCpMkSqDJEqQ6TKEKkyRKoMkSpDpMoQqTJEqrZH5C60e3V/RFsgcivavcJ7I1qDc1hIn4noHkBvjeg+QIbo9QAZIkP0XIhedD4zRA9FZB3tkxAdEJIhugvR5xTbEBkiQ/QDiK6rtFzRe9G8HKLrSs1X9aNwXgrRj1X+hRE9EpAhMkSG6CGIDgfJEBmibRDZaHQHIouMVETWinZFtHuVDdELIDocJEP0g4isq92BaEtIu1f6sxBtB2n3ShsiQ/RMiLaCtHulfxzRtCqGaOjL+0tVMUQqIoNkiH4e0TaADJEhMkSGyBAZIkNkiJ5RhsgQGSJD9AwyRIbIEBmiZ5AhMkSGaHdEtnZ9B6ItIO1e6c9GdKmPQXgJQB9F9NEHzQ8M5scRLT+89wIwPgvR28kQqVpGtHvRnkXziHYv1jNpDtHuhXouWStSZYhUGSJVNhapMkSqpoh2L9KzyRCpMkSqDJGqW0S7F+j5dI1o9+I8o+zF8qpsMUTV2pKaYSIZIlWGSJUhUmWIVBkiVYZIlSFSZYhUGSJVhkjVt2+nX3/58o+//Pf3//r39z+//PbVktp1Wd6vyhCpMkSqDJEqQ6TKEKkyRKoMkSpDpMoQqTJEqgyRKkOkyhCpMkSqDJEqQ6TKEKkyRKoMkSpDpMoQqTJEqgyRKkOkyhCpMkSqDJEqQ6TKEKkyRKoMkSpDpMoQqTJEqgyRKkOkyhCpMkSqDJEqQ6TKEKkyRKoMkSpDpMoQqTJEqgyRqu0RHf6pyS0QLT9W+taI1rAcHNLnILof0Nsislb0aXgMkSH6HEy7V9kQPS2ij/69MwfSoxHtXuHnR3RASPweGkNkiLZFdG+l3hDRWiWvK/uxv8Bw9yp/DqL7K/xx7V7ln0e05V9q+TKIttXuVTZEhsgQPaHmZjSDNEF0+x8NkSEyRIbIED2d5pfUDJEhMkSG6BkQ2XKIITJEhsgQPZn2iK4PBmn9bwUxRIbIEBkiQ/QkMkSGyBAZomeQITJEhsgQPYMM0ZMiOhQkQ2SIDJEhegYZIkNkiAzRM8gQGSJDdFhE849s7V7ZfRENF//2YtIQ7V7A/WWIVBkiVYZI1TwiPmiASGuITCRDpMoQqTJEqgyRqqVJf/eCPY+sFamaItq9SM+muVa0e6GeS9bRVNlwrWotjd29cM8hQ6RqfTFk9+I9g/S1692LuLfuXd7fvaDPj+iNMRkiQ/RIRLsX1RA9rQzRpyHavaDPj+iNIX37dvr1ly//+OOPf/3zr9///PLbV/vb0tZlf6GcKkOkyhCpMkSqDJEqQ6TKEKkyRKoMkSpDpMoQqTJEqgyRKkOkyhCpMkSqDJEqQ6TKEKkyRKoMkSpDpMoQqTJEqgyRKkOkyhCpMkSqDJEqQ6TKEKkyRKoMkSpDpMoQqTJEqgyRKkOkyhCpMkSqDJEqQ6TKEKkyRKoMkSpDpMoQqTJEqrZHdPgnJbdAtPwI6VsjWnuy9uDt6LFj0Vsjuq/yb43ovnHmzRHdo7dHpFffEH3SZ55MjxyLDtmGDNEdejSiA0J6LCIbiz7tU08lHdG9lXpDROe08/y/7urfbz/9VohuYWjvyrq/4i+B6LqVLC1j3B59U0Tnvzvv8074Ioi2jF5eBtF2VXkhRFvpJRBt29VeCNFWJzxglvZ4RLtX+aMyRKoMkSobi1Qt5WhbnfCFEFlXG/XoVvRCiLYLHV8E0baj0e6V/pgMkSpDpOrx9zoaIlWGSNXh5jRDpMoQqTJEqgyRKkOkyhCpMkSqDJEqQ6TKEKkyRKoMkao9Hvw0RKoMkaqDdTVDpMoQqTJEqgyRKkOkyhCpMkSqDJEqQ6Rqn5fzvDmi62eQlh7X2r3ij0G0/oza9Km23Su7PSLtwb2DdaAtEL2pDJEqQ6TKEKkyRKoMkSpDpMoQqTJEqgyRKkOkyhCpMkSqDJEqQ6TKEKkyRKoMkSpDpMoQqTJEqgyRKkOkyhCpMkSqDJEqQ6TKEKkyRKq+fTv9+suXf/3977//+6//8ftf/vb9y29fjdm6rFmpMkSqDJEqQ6TKEKkyRKoMkSpDpMoQqTJEqgyRKkOkyhCpMkSqDJEqQ6TKEKkyRKoMkSpDpMoQqTJEqgyRKkOkyhCpMkSqDJEqQ6TKEKkyRKoMkSpDpMoQqTJEqgyRKkOkyhCpMkSqDJEqQ6TKEKnaHtFhXxPatQWijiOdHJhwcnHuT8Cjae/670NrhU8+eXfy8eTT3J+IR+EzbW8Sj6U2JbWAKQynEE4hzv0JeBQ+4+qR8P0svDbPah5UOcXhFN3cnwGPBk8A83H67c/ha9hY5mnNovKnWE8pz/yB/w5HEeFAXXh3MI8ASE0F8fk5XguwGkAfhsV/4DCypAtyqLb4Uy/mpkYTqA/OclOgucGt/DNQAz1g6/wppAGHRGhE2CzrOsA1eGeI0+/ctFYKenaHti1Yh7MzdvmIfRza1Y8TvK/ZOjwVDgTxGAH4o9rs1YipjxPvClVpr8PiVD47g70Kyx8t6FIDvZxlZqLKSTj1Sih/PNWdzvoX3fc8s0wSnMto9bVg6inwclVuglF/zeWayeV5OHcar8IL0VxJiq8XJ6T9Xi50jf+v9vhLMv2HVuaxYyOdz5QvWV4m472JzSyNXf/7/AS1FF6qc9qBo9LZVBr/XC9ynIfA3sBnVtKuV9WWGM/nRq/MeK74uSO6XAkZJ21u8tP1pDA3Oy1BHvt7T+nvyCWOO1IsTDvC9zIc6lGmxJ3TVadhLqBSKXdWK5SPPx5Pyn/F9zp+l2mI06XpEsvlfzmnAIMSSVzAet1Zb0r5mu9s1nlO+ReS0pv/PKaxCF+NNDrzuYWz10B+OUaXBdZzSyxz/770GXd7Ba6orxK8CbfvG4ueXNO2vtCV13j+6D+z/WYxxBh3Lu6eB55cM+wXZqs5YPeuNw53fHfQ9nNWZ9T3YO9mjs2vSF5NvMszwTjQzW5BviHx2eH5+v9uY5WbWHIuwjm36Zl98jelfF3HevW/YxQ4Rt2XSdFsrH7Zhm/XsO4nfJBtz/sJX/Tyc04zcr3I4q9S+9mUc/kuD5Xv4TZBJ7HgzDrcTKx2uzgyrkTdLE9N70NaW35dxXvIBZHJktPSGvBN7a6YyqrnzYrq9L64lW0CdbnpeMPv7XLpatUu++U8qcu/xt2t1v81W6myxLbBFPKag6myHvxojodtj8rexRat5EUHSX2b7XMbyYtipKpdLwduXbePbY7uDel+jOPa3iNGrNeE6B7azV60S1/G0I9qI6/YHGeeDdDukPl5jitD8VE5zj56svFCzupYfLwlI8oKZ59BubwBTuaYh4SOh8xlCOPCoyjTfdXHBI+HTGYQ5F33WS8stNncPYt0fWduedfNmK4x/dhOp2WNdyBdRLe2d2YrGXMwpzPR1dbk5z9m+pIUZyKjy93HT3/U+SUhTqP0mfuEP+9Z+5dkOH9Ptbu+n/3TXvLwkgiX34qh7WEZvK/nrb5tivuS0LY+iUEzaM8I7QBrX495n9c9T+geaKXrQdBW16oPt6r1GGgrC9NHHNUe1NJeayowaAbtqNB6XtEzDPfgPfungOZOw0Il3I1uXtEXZp5iP+/VvwXBm/t9p+9RWH8/5vQlhzf3Y09ffvjKTEdYN3eoL71GYf09mtN3IN48THDPTv6rwL1+ReTV8xXLL1BYe9Pm9A2JN8/CLLxk8gXpTt8gWRYecbn/VZyT1yfePM21tPH/cnhn3y/pF57RuvtVnXM3SkyfTbzz5WvHvanpGna6TYJHvJPHDu+87+T+Wyrmb7HY8faBB9O/GJ9vF25u1m8uliQ+dsfK/RhnnyybuyKPuOngoRdiMpRPb3C6rf0irPt53w4i1yBnLvTSazM2ulXhgZegzb9OeGZaO1f8/tdkLrTWdPNimPOcUm+pPuq+hochv2zxkxcPz4USXO87ptaZu0DmHqm+DYEW3nT0+Xc9PAjxNIBZaK6TaazcESHO3SYy+2D1bSA/99quLe6LeBBl5SVAc5PY9I1RK4nO3J0ksw9Y36ajk3fQbXPvxKMwq++3uvW3D02vZuuzN5vMPmp9u6QSplDduLx3EMntIkt3Vl+02uVb5FdWmhZuRLlet+KiuJsV0d3R/ATQlc3Qy0ntKmg6z+G3S6CX6qx2r+dG5JZfTXw5fN5GPoeYmLfkdv+m6MGimW2xfey53QOFJxtjW/37Aqap+EGCjY2hPfKVBS8hJTa5ikRsHrgH2gq44+QEG3JT47Ypuut3Me1ejd3QrQduM/Qm7wh7y647s1ozE4fc4Ju+v+49R76b1cT1vxrqYuX26u2KbztzjC+vUdZX5pa/FzLX3eu0C7+5ZcDJaut05+XTXzZwOE3e+zMuV18s9a9sDB5u028bfJMtxOt7Flaa4rH2rreid7ETfnNDzVwztGY3vYHj6j6vuZtmjrg9vCW1M7mwfCPX8bZ3t0bGWrqv8M3j4eV7hmfvcX3vzEu7RX162/mbpvf3PQBx4A27n9e3b6dff/nyz9//5z+//Pb1q+PQqrbBfzt9dZ4iB9dqFFdPzvuc0IVTK+DiUMRB7upz7Q7aoK+BfiWya7mKwzbphoYusfNpEIfPSEQ+O/RtDy6F7nAQiFyWTN/LhX8z0xlyG7I4KEtxXJZM5SwhBHFQhxLH70H9SuGzS/JdoPbdAo469PO7AYpTgxstTn8xxW6hQDW10C0O5CWkbuGfNrjRQpmaS6VbKFTzjQpVyHrnC/1UFZsiVa6JZdJ+EFtD7bacfCgtdwvDZmhh/DCOnPDxbvEGg5BatxgTpmHoJ0Kb42hhKo+lum5xNG5+tDh3DdmzBa4+hZjEYZQQq1QArodPJfdjmD01bkUNr6PPQ2zioHhZrmPD6++zXLmGbcPnPPRPQmFySVKYBmUpQy94hbIUuObioCyFLyJUuUJZqoBv5Jrj71V20Qe+KOTgR/mKkatRLnbNp+BabwkV48ihN4QKM7v3/MnEDn5UHD5gIbwzuSgE+XvR8/kKu8D1q+xiTVIHdMwTpkR0aaDG4j07JugDOyYI8yc5Jui5ZJmbLzqoQ0mlioP6lZqdOIjq6jB0V8G5GMU1cNHLMbgOoSaqETrI1ioTRAfxdK3UQjz1+9Cc7w7K0nicQQdlaTzOoIOywFAiJYO2FFr13UFZWuv1w1Rn4JEFM50Bn7eAk3SLEWoKo8WVyUr9kyyuKPEVxYhswOW5ocPBXhSdD+NRzESTS93iX+2VqW/TT0Hu4KRDooVSuUYtHK3DBYOhjBbzDumQiW3k4clntknabhFbuBhVbC10osa2yGVqfKLCIzlaLEaFMUa+Sxb6U7f48EQcRgs1qomLUbm+Nbd+IqRRoRN1C6waD++eBxEYJajl0nfRhjb+FGBvchUqX5RWuYKVL1mDw2xx9W7wvbpwrdMQilQA2kEa5BJUbCMwFCUpPrSfNMgFqNi20iADYsV2l5yMhxXbZHIhS+GgvcIvDt1BDIXTDzto58nx4IcOyuKHLOWE/pG8H+QM0HcSXFGpEvSr5LOXskCfS156bsX+n3wbvwdlCUPqDsoSAjMGB2UJ0o8LuZS4CWV2tbcgdFkaQWTnmGdgF32UkQKd9FXHrgxBRhh0rcqIja5w03FcliIxAZelSEyQTwUolZRk7CPH7QSiAHISEwRy1dMVgzgDXQPa4uopD3xV0BVwjUflQM5JTAAun7KX+SKSSzKaJ3Y8MmHJ0HFtodTkCk/slZ3EPI3OkF2Q+YKc72MtucR9mUuWcx9ryUmviOxkZErkCtcWrhi50ORqkht7BDnpEFyW0ujaBi5L5UEpcFkqjNnouCyVCQYuS+OJnI/BlaUZiY8Vz4MIOjhlcMQl0HWAI9RXArUQiGeonaGDU4ZCPQcdfDhyO0MHPxRdPx+08hJ97ceg+jFRX0EH1Y9MHh2UJfKsig7KEvk6oIOyJB6hwEFvLInbGTooS+IACR2UJfGcgA7Kkpg8OmwWPDihw4bAgxE6bAi5SDkbNoTaS9YQvrD2NDCV4kPqFvGHkrtFxgLf05BXCo8/ZLE1tA4VR89Sh04Vx1boI37oFspUeQwiC4Wq44XEUbtU+LhYGOILxMBy0dFWl/ma8ExT/cClSmI9lyqL5Qk/8ExT4VrTd3mmqVFwF/5w5AkALRSjxsYf5mAWmp4buoVRIXkaAAMHwjCVhNFirJEp1A0cRNck1DnEhqGMpuHAAXjNgp3D85qlyXPwXjPPAYEi+wqdRSrUcCBuoTsM54Q5ZQu18BwQKK+oMONKZaD11MIxb6B8pBYee9DB6eHKST2hRdYaovCsOCnEJp+Edl3hZ6RWFQ9zrhIoWgQTpdTQV2oLXsoC/ai22MFW7AQcr6GDszdOVQKN+m0YgpyvYAjsOtSCk1fw8kno73i6fgwLLr0fHIZsleZUdBBCOc5R0EFS53znWbDTpSqlLg5ccRfHPMd5cixwKAHNjpz01MiOx0gce9C1In0MXZSe6thxLIcjHzpuQzgqooORUBx28upkLiYnEUQhlySM43KmMYojx30Yx2t0EsMFdtyWcJzHgUNCNsfOcyQwsJO5isuSJb7nsmTurY7LkmUe47IUjiAcl6V4mn0dl6VwtO+4LCVTpOy4LDC40FzFZWmSkRIz6BNh6Ja2T3j8Bpspo3NeJtlMsRnHaXAWspHhQBEy9vLEtURLQaBMu4ltz6LlaM+T5afa0GfsTLGor7LAwJbnA6zPgJYDD6wsWcmz5KiTYIOaCFiIdkYWYHngR4tDoOehBS0OgQFierE4BAYeLdDimNezWTmaOYvCYpDlbokWfyrHIvVlm/uCBp2356KJSwUV6oEHWUd9GlsBVgFm3CRNBCtYuHeCJThwdQM3JyQJ0U3P3tHCBeSw3LH1EhF7sdJo+Sr4MBTJjchC90oXRyPHBP1olkRDjmbJLOSXe3LIVxBz6F4MshJIDmybNLMmNsV6Pgph7ZDO3w2Bh0355RC4rfsglgdOLCTaWJukXWhhhmoS+CJJqF+PitlKYlXE8rjuGXv0cqLGtsfXXOYYZODlMsNcyDOYEyuDJpc5pjFKQlaQo/HgG8RydB6Yc8wSGiWxjA5GTrJFApnCtsq8WsXKkFiph8JAx7MfH4WBp3bb0HL1A9cXEqQo38VGCBkSxxE0ToHlvoAW2nNyvC6BNqLlpQi0CW3sJ8J+BJEPXe5A4StYzmnQYqn8QFcfLZZK4vVAmQRYxh4o5QGbXT+KpfIy4VGyBImMXAXKpMByiw2UZjlcfopisVSBFzcCJWhga5EyVyxVHDq6iqWKMu9R3gdWgmBKCsHK9aWMEaxMfZRcwng0DFJ9XJVMyXewuCwJYaqXYuC6JLSUQQqJC5MwlkUpBq5MJklDAufCDpI3n7rHgkgqEjiTBp86W1qdTJKOkMeiZWlNldcnIanL43EsXPH9utIKZSqZppbQxDcOk+LQfaJeER0vYSZJMqLvnhtV5EgWR1SqT+RA12XH1zNyHAzthi9o5DAZPAcxMXB9suehkbxHnygIJY+7QZLIRA7RYcjmeDfyQjaMaKnbipbD30ihP1ge0iLlBWD5qqLFogXutdHzyjOkN04sLUv7XlFsETnyRY6UmoDlPh0pb6E1bDmKbS1LjhMp43EQwdI1jpQOgY1OToStOEuaEymRAsvRU6QsCyxH4ZFSMJiTnOsnwlJlvrqRkjewvBgWKbMDm/t3sV9Cy+nfxV4LyWvqFktVfAeNPR4m2SJlxvEgQxrVj9IqPc9CkefkLMlP5Bk7S/ITeT6HFtqx4yiVJfmJPLJmSX4iD6XQumnsRFt4A8CLxVI1z60TLG0PZC/nRVuctDUnVpraIFYGjyZ2HDzY8mSINuJ2gkS0YAPaVvqoDBWErHUcpB3aFPpP4W5DGrsYWUk9sBhkpQM5sTwqYxVop4JnTqwgWZ7AYxTLw3BMYnmhKWa2jYdhsAn3QBqPu7GQhWvCP1Xow1XSALS4JSJ5AFrcEoH5z4nFLRHJBNDiloiTjlMIHRR56D+V0UrHKXQFq+f1qMjTHa4IeS4kNgYYvLhUlFU4XLePYmVnRiqIzawGn6WC2AiBDZ+XshywvIQcKQXC7RPfLRYD+ly3WAzJPCJlVmBDk2Jgx6mRV2si5WRgpR9RwgaW09dI2RywcEO3tF8k/YjyQLCcgERKEsHyGmakDBIsRzeR0ktXJQdBi6WCJESqj4MMZMKDoMMhqEKPlFLhAFUlD4m8qwbJcOeMgxtkw72QjXexOnYcJ6Ed9TLjKAqRT29XOMZCRhylVDQE18rze+zbaZVXMWPfT6uxXybeUKuls+YdtSpTRN9Sg0QzdY+MGk/6UTbVapMpQnbVID5s4+exfK300uMyhmtubHHsIaKm8tTueUqNvEiOu3Rc3iZe9ifT0D2vXiQnHmYC/Hzy3SeaGlIQD3Nq6x53AWGUrt3jJqHscZLHPUQY5EYPPJrsc5KP6MP59wL6eP6+R88RVZIpE7JHWpohj/WV/U7weDlb5VQl8ZTZZL8z8ZTZKsNNPGU22e9MPGU22e/En8KSNZ590WLBZL8TLZarcYiFFovVeBBBi6VqPIgkmjJhCBloVkg0ZYLlyTjRlAk2+v7hDNbxFetHJVPr34XkK1z8cuHmkWiq9rgjlMU6tFJImubBFtdtACtbh4lCBLAcBDANsDyWMSuwnOQm2kcHCzzE4n7u4C6uAlheQJGLBJ4bZeK1R4/pduneox+rTJu4kAcn1z3eOAPh7/h7+BiI4zRZGhl4nvylEUKX4LxZGqmHNCy1y+OpUigxfj8PofXz4e9nXn4az4/r3ZflKxyJjeUvqZbL+hVpDV7qD3NbJ4146hk8bYafwWPhcBg9g8fRzZ0vCw1A54sG40NN50sK3T3n8wV3sg0pzQG3fPtRLFWr/qKRunZud1AqaIcunpuhH8ZLgo0UJvJOBCYC7x3vGCeK6yCrb/28dLTwpAljDFvOIdDiT5U6fpcsz5L9vJVnyV4q2W2To0F21OS7cNV6m8UTBZis6rlUwcfxlwPaHPqHPdrm+1FgFZIMBnI0nTs0HpW9pkSxqA9FsDuxgt3h/AuWsxq0lW92qGKxClXAOpzZwfLsnCg0BTvWt2AVauvjWcEqNN8HsOL5vokkP4WFbDwdJ1pLA8tr+onWAHyUbDvRSgxYjmPRFrQc16HNaHknMVHwiTfR1H4U78hwvBKIFm/BcL5f34y3YMh+F1q8BcPx/JsoYoT4aaD5D2xqfK9HEYul8jI74DYdWpkM6G5qsLX172KpJL1Gi6WSJBhmPrKJx2C8R5Ys90EMIMn6HmCwlUAuieXLDZEq28oxUmCbebkIHyYiy0kuPh8T+YYUOiql6imulEq23IKUqsjWQuUaVQ63+lFIXy++m2RTTH4Zjl2eNzkJZHAnFm0IkvKwlTAmiC0c9Ea2ntf/sPpkJdfIYmOPkNkmmgoQLN486znXQOxoA4fieFHQRg5foDGQpfTw2//9P24S8uY=',
          'content-encoding': 'gzip',
          'content-type': 'application/json'
        },
        gridID: '6',
        msg: 'MAP-DATA',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function seventhMapGrid(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        anchor: [
          11,
          100,
        ],
        cleanId,
        gridID: '6',
        height: 144,
        msg: 'MAP-GRID',
        resolution: 43,
        time: `${new Date().toISOString().split('.')[0]}Z`,
        width: 144
      }
    };
  };
}

function seventhMapGlobal(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        angle: -90,
        cleanId,
        gridID: '7',
        msg: 'MAP-GLOBAL',
        time: `${new Date().toISOString().split('.')[0]}Z`,
        x: -795,
        y: -2537,
      }
    };
  };
}


function seventeenthFullCleanRunning(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 35,
        cleanDuration: 49,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: 'scheduled',
        globalPosition: [
          -422,
          477,
        ],
        msg: 'CURRENT-STATE',
        state: 'FULL_CLEAN_RUNNING',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function eightMapGrid(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        anchor: [
          11,
          100,
        ],
        cleanId,
        gridID: '7',
        height: 144,
        msg: 'MAP-GRID',
        resolution: 43,
        time: `${new Date().toISOString().split('.')[0]}Z`,
        width: 144
      }
    };
  };
}

function ninthMapData(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        cleanId,
        data: {
          content: 'eJztnc1y3DYWRvd+CpfWXBAEwB+/ikoLJ3HNpCqVpGYm2UzNuw8BEGz+gP21HNmtsE8dydZ1s2Xi8OICBKjSf59+/OXL51+//PT06fm5ruAKL9UHFF0HRRIUSVAkQZEERRIUSVAkQZEERRIUSVAkQZEERRIUSVAkQZEERRIUSVAkQZEERRIUSVAkQZEERRIUSVAkQZEERRIUSVAkQZEERRIUSVAkQZEERRIUSVAkQZEERRIUSVAkQZEERRIUSVAkQZEERRIUSVAkQZEERRIUSVAkQZEERRIUSVAkQZEERRIUSVAkQZEERRIUSVAkQZEERRIUSVAkQZEERRIUSVAkQZEERRIUSVAkQZEERRIUSVAkQZEERRIUSVAkQZEERRIUSVAkQZEERRIUSVAkCYpMZe59Gu+ZrAhNhywVoanIVtFrJD2I1L2iaw0vHXl6SUeKSk0/Ou7kkm5XdCzo5JreTtFpJWlFWs3JJV1X9Bo9D6no9dy9OSj62yu6e2NQhKL3CopQ9L4UnVQSilCEor+Zors3BkXvRNGt60IPpmj/z7dky4MIOt6Nfa2kuzfkfSp6EFCEIhS9U0V3P+n3oehY0t1P+Xtz7RGsB1eT4Sk1CYokKJKgSIIiCYokKJKgSIIiCYokKJKgSIIiCYokLy/Vxw9Pv/3w7y//+vPLT0+fnhF2HXJKgiIJiiQokqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiRB0QP/WvhbSIoCdz+V98pFEZoOWCtCU4G9oltFPYzSI0XbppePeQhNx4pey92b8v4VnVYTilD03hSdUhJZhCIUoeg9QLlGEYpQ9B5A0XdXdEJJKEIRilD0HkDRN1N0tOB/9wZ9W0Xpn75e1d0b8+0Ubf/xtXLq8+opK3pNxTlx7ryVoofg6xTd/bTvqwhJNygqq7r7qd4LHuSToEiCIgmKJCiSoEiCIgmKJCiSoEiCIgmKJCiSoEjy8lJ9/PD0248//vH7z19+evr0jLDrkFMSFElQJEGRBEUSFElQJEGRBEUSFElQJEGRBEUSFElQJEGRBEUSFElQJEGRBEUSFElQJEGRBEUSFElQJEGRBEUSFElQJEGRBEUSFElQJEGRBEUSFElQJEGRBEUSFElQJEGRBEUSFElQJEGRBEUSFElQJEGRBEUSFElQJEGRBEUSFElQJEGRBEUSFElQJEGRBEUSFElQJEGRBEUSFElQJEGRBEUSFElQJEGRBEUSFElQJEGRBEUSFElQJEGRBEUSFElQJEGRBEUSFElQJEGRBEUSFElQJEGRBEUSFElQJPl6Rebep/69uFWRucrdm/EeFG0x85/p88Sa/oqiB8mmtyzXKLoKWSQ5cVd7u452UkEougEUSVAkQZGEeZEERRIUSd5W0SklvbWiE0p6646Goiuc9i4NRZK3VHTSgv31q45m/tuce4G2pEg1cyniAZZmj7LouLHbceuyin33xnwbyoquNbis6O4N+Xa8vhadXsmW64rM6u9lgb77iX8/bqtF64L8UIKu1aIH3qK+RdGah5OyhodnJCiSoEiCIgmKJCiSoEiCIgmKJCiSoEiCIgmKJCiSvLxUHz88/fHrr5///Pkfn3/45cvTp2ecXYe0kqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiQokqBIgiIJiiRB0QP/kthbiIpcZWxl6srf+2zeK8FS46vGVY2pTIurA4In6yprK1tXQRi2DgimnKlcXdlu62u46TdbP0xhC6p8W7m+cs1WmOmrYvVa/uZ0H14Mh1wOPP3vDo9lva6rYai26pqYaItiNmwcbT7acFg4eFn/XFn7qUgOx/aOHrcug0hXLUvdlIlLW7uP8chw/LJAri/FWXVeVObPWemUm6a6VMOmZGz/0YXjL+9y2ytyXqW5d6fPtdaUpenjkqt2J23/0YTjL9XCrC/Kdoi6u4S3Fjq2fHRXUrr8vGRsvfVW/Ji+5fKqXBnLTqU1TnGinNnr1u02bZciV2+69rm/SNdGvjF5T1QP4oS7u+TgTlqx5pYdfu1neXSsY6W4u6A3Vt3kCrro9IfZXRL7mmQ2hb93Y2ccIJs4Gbu7ordUfRn85yHrIMkLCb0pt+mjLPN6ZoeUNmHgPFnliDOINFddTrJ2ub2pyYXZgVkU+JtK9kXsKQtFnEqE9ixvBbaZnCdWK42baWy3mpEd5vJqCD1nQcgr3fNN6j5x55m/K90IpFuJZnXLsFRfFFooAO4sywbrvYNdul7uSn3pJtWFqer2djZbD2r7/Uz6AdJzK3S5mrJZWZmXoYbd+sp82xv7/y4zT145Sy+YabnOzF+VPNu8/OJjFhY69blH869542TV3S7tNBXwr+2fPrS0S2Ne2ywTK/9Y/wuDxMbZidZCVlusu62Im93Fal8eGJaDwskWOVY7r/uNCJ0kacTwx0PDYk3vdIPqaj92v/twtZdtJjE2z7yvVL2TTvHWe7WbbYfc34qduTizdoubwvIk77xjyHovd73jMPW+ctceDu767OXr4qBydpmrpdfLMszUDY/6+rC+g1uuS9Srm+MHuzMubPnuk2jb95NYc5FaWiQOF6QXSzhn97peAVwl63a/cq6qy9u6pjrY6hi/PFx63C+en1fwdnNgVRXKM515PjWPRkf7dUdbRY8meZluyadY7FrNu4r3PgcF5/rWxIknW/v9msJkc1OU6/Xqbl2Y5y/fcTBRKG6xnfTGYLt1c9zxVzs//XY4O1K8u0m9tl18wtvWzQ7O9YFr5X/1z8ez2d3G2+EzDydbTdls5SxnW2Vvu0Qsfb0dvDbbbw/wzM563/3KgHVtb7c8Wu1Hrc1e3OmfMltsuc/bPOWFlL/yuZh5rfbmzv5Q5GXbPd4xFJeZb8na4+M2863Ldt25H969bJNsVqCLDz3uH83ZH7OdUrmvXBH/W3HR6Ne3s/uncXeP32yft9mtPQZnp7jD0vZ2a1dHD4UXn6nZPbJ/qhFGu6u3DxwUfzih9PTM7mdG7t6u722u9FhH8Qc+Nj8k8wg/KnPdW8IUfhxr8zTM9vmO03PLMwgPJ2UNP+YuQZEERRIUSVAkQZEERZKXl+rjh6ffP//nn0+fnp/NOAEaZ4hd01j3Uj2bJoV2MD6ENoXODHUI3RRaa6ewHsbQdSaEfgpbE19tp7DzOQzv9abzi1d933U57Kuu7dt4Gl0K+yF95z6FQ+PbEA5TaIdwcFNPobfNFI7/UW98PYdmDLve5rAZw6HJ7x0b2Td12+bQjaF1Pod+DF0/v9qOYdt002mE0PmhnU4yhlOLuhT62gy5+SE0bXY1HtF750wWG0LfN1l7CLvG5Isyhm0dWxTD8Vt1TdsvXh3bV+ewr/rxLOdwGEM7DFM4NqPvk6vwH4Vw6Ot8fcdwqFtnp5OMYVIXmhBCU7d+amAMm7qbmh9D67KcGLZNP6mLYZfOObxqqsF2dn61GcOkLoa2GlwTzznc8cUw2civ+qGf3+uqoW1Tig5T2Fu/eLVrfLN4tRtam895DMfrPeQWhdDOORnDNuZGtBHCLpl0KRztDNlkCJtmFuvHsPXm8qqpTe2Gy8tj3Pn58NAFmil53BTbpCD+3zGeMqKdYmfcfC1i7GJax4aF7+98NzuLce/arDTE3jTd8nXv3bB6PXXAGIfzbwfvL9dg7MFd57LXWCu6i+eQeWNmusv1HkMXszK+P8ap70zpEQ6PVyJ+HbqXmTLCxKQee+6UAynnx66bukRjc9z0Psehx9nkI5x7iicfNnbs8TPlRYzDMX4qKTaWhTGeaoqNVWOMezN//3BOfvDz8bE6jEamuI7lwXZtjmN9aH2f41ggUseLcawQg5vjWCKaub11rBGty+cf46Ef7NS+EDcmVZt0rUM8daE2x5O/boqbyV+f4yYWoGbI8ZQvIQ45anu7et1N/oZ0vo3zcWSIcew+bewdMY69a/KXKviYY/Uwf/+QI76xOTYxJ73J7zcxJ1vf5TiUMt/Xcxz71ORvSNdzrIH9HMc+ZOP40qTSPcbTIDGk/GnaVPliHM6vnYaJIeVjM5g4Etg6xy5ef2tSbGsbv59tcux7l+Px+1k7NiPHXYhTftlU0se47ebjfYh70+Y4rObYIbY/xuGpYZf8xbgJse2HHJsQe9PnOKwMuZR/4fyDb+sGO8fh/Hzdz3E4P2/jcBXjcH4++YpxOD/fOre43rZtar88vq/j9Qx+QuzGs8znE2M7nf+U/87XXZPjLsRpqI3Hh7jv2+wvnK8b6tl3aI8bUn/P7XVj+WoXPtwwxP6SfY2tafuFTz8WwHrh248Fb3k9xnSN9cNO9cOb6XpO9caHjF1cX2/ssLz+3qT+kfNjvDomHx9qnh+nYf7l5X//B1Z+1+k=',
          'content-encoding': 'gzip',
          'content-type': 'application/json'
        },
        gridID: '7',
        msg: 'MAP-DATA',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function eightMapGlobal(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        angle: 90,
        cleanId,
        gridID: '8',
        msg: 'MAP-GLOBAL',
        time: `${new Date().toISOString().split('.')[0]}Z`,
        x: -336,
        y: 2580,
      }
    };
  };
}

function eighteenthFullCleanRunning(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 25,
        cleanDuration: 53,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: 'scheduled',
        globalPosition: [
          -320,
          5180,
        ],
        msg: 'CURRENT-STATE',
        state: 'FULL_CLEAN_RUNNING',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function tenthMapData(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        cleanId,
        data: {
          content: 'eJztnc2S5CiSgO/9FG11zoOQBEj9KmV16Oku2x2zsZmx3Z25rO27r4OjCAkcXJGhECjSzbN+PKVQwAc4Djjof7/98befv//955/ffvv+XX3E0onc5cfHL4KoLIKIFQpR9US1JYKIlRRR9SS1JoKIlSsgqpyysxGlvefyrWkqGulpY0R1AO2X6ohaB1QN0VlfdmFEZyXm7RAdnaAjAAkiQSSIviai45J2DKCmET2XuKMANY7omeR9MUTbpO5L+pdBFCf2kcRfGNLnEfGZOPq5b4TotSKIBNHlAAmit0NUAdC1EFUBdA1EldDwiLqP1D0URESyauNpFtH6ltqAmkDE3fSlAbWPqDqg+oiqA2gTUfVMH4/oWEjVs/waRJeenj8LUSmUZfu7N4DyWURfVgQRK4KIFUHEiiBiRRCxIohYEUSsCCJWBBErgogVQcSKIGJFELEiiFgRRKwIIlYEESuCiBVBxIogYkUQsSKIWBFErAgiVgQRK4KIFUHEiiBiRRCxIohYuQKiynFK7SOqHs7VOqIGQgHbRtREvGTLiBoJKhVErAgiVgQRK+0iaibAXRBdFlEJkCASRILoFEiCiAF0UUTHJr8MqAIiOhGPJOXYDHCAGkF0TwqfqKOzcDFEfHKPzsKrn38wos/KawGdDOmVx80Jopdk4XKIntl8/kpEJwJ6JaLPZeRyiI6TIwG9KaI92WsSUL2z1C4DaBmj1YfULKD7MPaehG1SasBrChA30q+NpAlIJUS1cTQC6XqImlrTr41CEAmi57LfDKD2EO2HKIgEkSB6J0iC6ClErUKqgCj/9bVhNIlonZCaAJoB1ObrCUqITgfUJqKmXs98JUSVAF0HUTVAbSJaEtcEoBYRrZPXAKD2EFXG0T6i6jjaRlQdRcuIqkMQRIJIEFWX+oiqI2gdUXUArSOqnv3nEb1yWq161o9C9DYv634OUf7yF4cjiB5A9JqmJIi+CqBXbZMRRGw9EUQFKMuD3wjSUYhi+TKIPr8N8+0RLZksZ/ZziC4HCRHxyzFHIroYpCNerfs4oktB+vHj49dfvv3jL//987/+/fPPb799zzS77lrZep3kLZNIEEHEiiBiRRCxkj+H5jVCuxtrl/X+u0YK68xaVOMUgJcgetVXPQOnKiaqFrULqBlEr0jI2yFqF1JDiI5OzhvWomOTcxSg5hAdl6A3RnRMso4D1CyiZxP3ZRC9/rS0N0BEJ5FP+BdDlBt+CqJMQvcl/UsiWpK7NwuXhvSqFf5XAXoLRK8WQSSIBJEgakEEkSC6FJwrIaoERxA9jailg3qaRLTcUBtOc4jiy7XRNIOodENtOBdA9GpI+Sm5RgDVRVQ564LowoiqZ/kViMpHwb0hlEcR8fIWIPJyDKLq2XiltPpyy4ZEELEiiFgRRKwIIlYEESuCiBVBxIogYkUQsSKIWBFErAgiVgQRK4KIFUHEiiBiRRCxIohYEUSsCCJWBBErgogVQcSKIGJFELFyFUSV35ZWO/t5UZu4lEqpaB9R9SCclhE1Eq/ULqJmgrquiehUSK0iaig4UBAJovdFJLboSUSnpkQQXRZRM83sqojEFr0nomMTzwG6OKIjMtAQoFchejYTzdaiOAHL/3JJ4rZhvQ7Q6Yj2JOlxEUQvy8SrC+DyiF5dAILoSog+myBBJIiezUqDgM46v2gNQRAVslXO5OeQvxWicjaf+ewbI9p7WkRVQFc5S00QtQ3pmojeyi96E0Rbb6V25htFtJXa2RdE74CofUgnAromolMBSUMTREchohaFWpfTEV1PBFE7gK6KSGqRIBJEgqgFORGQILooIpewZgC1iGhJWkUobSNaJ64RSG0hipPXAKCWENEJrA6oHUSVMQgiQfT+iKpDaB1RdQSC6OKIqme/dUTVM986oupZbx1R9Yy3jqh6tgXRxRFVz7Igujyi6hluH9EFIR2PqKnDLNpBhA/bh+hykGoguhikIxClj30rSLUQXQjSjx8fv/7y7R9//PGvf/7155/ffvve5Dk0DUmrR/U0JIKIFUHEiiBipQaiUp9YHUgqZyN6HMLdNT0znStpv6FV963aR7QWQcSI1CJWBBErXwDREQcbnpXWlZyH6JjjMc9J60Y4RG05cw0jasXvbRLRPXHPnZZ2THKbRtTGGKpxRKUknpX0CyDCZFIx9+ck9yKIqE0IZzXCKk39837ROrl3ROu/X5GxSyFKz0Ba/n3dAtHFahGdARqMIDocRa3nk3Klkf5b1KJXiyDKStXZ6ysgqhw2kUdUf0S2pKNyCkq1qHriQioa2ExcG0JZLoCodl36wogWE7yNldwa5ybi2/Ygek0i94YMXgDRa+SecT56siIeJ2c3tGYyvl8+h2hvNvMx2dUzfiSiLapyhkvm94JwHkFEQ1HZqxeH8hlEnLwBiLy0711XF0HEiiBiRRCxIohYEUSsCCJWBBErgogVQcSKIGJFELEiiFgRRKwIIlYEESuCiBVBxIogYkUQsSKIWBFErAgiVgQRK4KIFUHEiiBiRRCxIohYEUSsCCJWBBErgogVQcSKIGJFELEiiFgRRKwIIlYEESuCiBVBxIogYkUQsSKIWBFErAgiVgQRK4KIFUHEiiBiRRCxIohYEUSsCCJWBBErgogVQcSKIGJFELEiiFgRRKwIIlYEESuCiBVBxIogYkUQsSKIWBFErAgiVgQRK4KIFUHEiiBiRRCxIohYEUSsCCJWBBErgogVQcSKIGJFELEiiFgRRKwIIlYEESuCiBVBxIogYkUQsSKIWBFErAgiVgQRK4KIFUHEiiBiRRCxIohYEUSsCCJWfvz4+PWXb//6+99///df/+P3v/zt57ffvn9X40evP4bxY1Qf2nyoroM/avWn2/7aq3Dv4D8Hn372LbLwEHi2+ejVR++f6X5Gp8IvZ7hhTq5GNyzPGHxy8O9x+b+7eD5pqIzw5ZDKYfgYu49x+pjniK0HGX4L/8AtcOPgPwQffS4BN2RQsv6hUFqDTwyortgmd8uUXI9vidBTBXA+YA+3c1/v0m4/xj7hi5V0Cn/DDXDb4D+inga7pmZ94Sr/49My9J6d8ff02xuSe+ICoIphXctP5HvLA20XFiOQ5umZupCC60MhpuW4Lnq8Ib0nLoO0JDaV/VTAUQXOEk6a5DPfnLaauwkizFBqu+jbts9LC+TAKvI450dyckxjK30lUbw7et/7jdFD0+wcbvEe5V3KEGZmm7Zna4P6yH5lvsbuqQ7ZR26L5xXd9zHEb+nqV83vCENHV3CsfJ+x1MUGEP853P88AnUBt6/in0/j2uHYGln3/8fdjkLt3/6pzpp1n1PcwSP9vON/o526DcjwYR861wBKHf75rMveNIH75p4mQ7G9ElXXbcW+GYZHBoK56t8U6iTbmV7/Rvte8bYVi6zfdEYWbkQBpz3X6rnrUXU0nqZqfmuoV6ksdy9bx4mwmVH1jmYb4oLYsiHMFwtFeUkLsOedq1qo97m9G5eJcAbS+p3OQKSFkbR72g16yD3DMm6aNzOYi1NLOLo9Uc1djU7nJbbjT0V/eTKf9ZlRSKnfr428OEGRdJl5E7PpQUfC4djMEg13W5NpYPGzHx1plyYjajPPz7n1pA+bHZ0lPhzjdqymPDkL8KnRVWGKrT7zzBQy2dgzkxbpKCUeLgajfCuSlbXJ1PL16PzBoWxxnmCD+7QZqu2SVbIOsrEJpIFPJjki7EQuVyUSD2y2hfvcLBJLu8IEVbpCeHPkpqSadwXk5NxSYjbv1wLNaOCuzsJdaXYqtyAbObNro54QJ7tRvCczaAr/xHNSicF6DkqBdz37nV8Bv9XzyKKvqBccR2q2QkV30hap8MTP0tENOog58Mswbm3Z1+xzzuNiqYlJGH5+9OA507Ub0HZlz85WRPiTXjUes5QGr3nCB86eJpMr7WJnYzMQftq/Jo43bfxztmRl2p8MoSCnfFvqQ9XHIrtCNtJSIceWhO2POsuNL0mOk3a7KRHj7Ux4K9X6bjfwZ08MxwI850guJZPa/I0fGD07MxGwjEqWbkXd/q9u/6dm1cllnvq87/Zi+WFWCtMhdmZykAz9uE+ib9tNdpqL7E+i+LIx/g23jHJgz/wk/hgGTuEXVsVzhUDMhhMmf71gtO0o6MndXMcSRaFp4jf5NcPs4K2GXY/b/jIk5BaE1iZhOwFyrzuZhhHX2i4tOnpp5x6dNlCtKKpE2TAMYpaiyixA2s2RXngWfzIbHq3XUE0iMtTBWN+GZZmuJg5bU9Rv0jWPwiTc2WFYuL6c9+PoqRWy7kfz4l1ii2+s1y0C/+QihMdkNoLqgW5/R65R7CkRMxO3UEk6gODEWv/IeLEQtBbFWKrEIK+tTWpkbn/nAot1bHxynVKSzuRq9CsuEo8IOahePLmBT246Jg5h6xIDnYRuxMUwJmWQGv21ocp2WXRzzjXz0gRHMkNfIbI/Z8iiYROagfLSSKhuY2LwI081dVDXhUHGNScTy9zK5Gf/1B9ExIVDj1zXvWRmlfzWZqK1FvpRyRhtUwRUFDQ1/0x3e3SN2d915rrNk8NSt+VCD3LX9shmF6vW/SoNMA0kicqAaAnFwMciT7LylH3JJLnV4rO3xUKOhjd9cGbtLlllKdltEtW2GLKt4lGTtZ3DzRoCykTScw0n7lXYlg05UL4N4Ojg65Td4/zyZfBIL55/7rr2E5NqBSejOP1WoYSIOaAwzKMio/aXBVWGR3TdeWtHDMj7zF60jINemImuUzbJkDwN3GKi36lxQ2T+H+mfix5jYZhO77ikRq6kuahUBsW1f2o3Q+4njpoggt7Yn9JEexRRTawdqu1EDd3oK0Eurfjno+GJn20oUBqVUvpswXwP1IBaRVNli6Rttjmm+anR9GcdzRYVAcmKfEJmwrzO8Pc1RDMT/elPFJBJbWpnfvKT4BfhuIcmtWyV/qwiinMHL5Q+ns5qV6dyNEUUlQj+NqGcTPI3Y87OAbUHZPWsvErk5BlWBBErgogVQcSKIGJFELEiiFgRRKwIIlYEESuCiBVBxIogYuUqiCrOJLSPiDma5CRpm1N64GzF9bKWUZFHANRc4m0XVu6MivQoj1ObZKvA0k13e87/+sLIMqelJXsfa4VhtEmtcK7dNtCQOvXrLHgtoivvsldkDEaV4Ln24O3ZME+HeNUKDm2NYbr9nTgJaHfc8zk1sS2GuaNgSxuvU4z67GN62oKYO+JuExaVnD8SN+/TY/Abg7jvvO34HIK0ap69xaQljKp82tAGz/aIAaLb/roY87WRoHO3m0JxI7e+JXMA09rgDV3eB7qsYTyirPe9/ePW+ZaPu6rWR8dBbusAuRy5ZCf2s/MhD7Tp7LamiiyZ4PJNiGHEbIGfbm3Pz21+JhCPeRcQATlfX6u0enarxCb8NToyI39YAL1RenV+dRIfuj/dvIGISO59mdBp/RW7TWgTwB0dElM4D4Da/rw+wpp+h9xO6rtsyfb0g7aoszvn1vsQiDdb5HbgUfubbyDyr5jbk272VWPpcP4Bg38OdnZP6WZHTfIGl9zeVHrr8t29yr9+bo+tedAJYUYhlWz7jv3WOj74NT7vrrRXv7A5OR1xxesXu5yI23TXni5Th70Zj3TH5zQBmlI6q5Rep+dDd+3Rz3+NXg7dSWYOH6qj/OtaKL+x0QLKTfV99k9p+pD/rF7eIPlsx8m+C5SaD264iLgNxKW2tLdYuO/Izc2RbPiFbc6xygB/bGx3np0jtx4nW5Mzx00x0Jl9zPS7dfKHWGWKhenMS2XSYsOhtyqnG5mJF54+sGl87UYzmU83mOc37s57xtdUx9Vi86B3OCeO6jrko8S7sFm6cDL2nLwJIzwo2ZK+KozkbIJCAcevMH1wguS8sWB6qE8yVqMHEjs3pC8j7YLzmrS1myWjW4jjmcYHlV5JuO3YH505OWeccp/VIM/cW51ju2tve36bNlcPqa5onouNBb+Jij/a1z7KlqpO2MOqgeQO2ktPsSizTzeB7/B68s4Z3V5WFaIY11RoIA934SfFT9znQsgj9hRRNcOpLfEBu1thosDioWfeVd40k62ZZA+cphcXWuvB197UPasHvdk9E8OosyfKcW0j9RoKjmwuKGjfLNjZRbDqO0PCj33nO/nWi/XcLv02SXo0k56AWnZiqYTu6iHOPMlyOx5f+5Vbk0FWzgcTm54rtLZ9W1eZmO3Mjy2zVSYXrMl0Dms7dlqUZzozcktK7vTDJxov+Xqy2yJVtLBSfEUc5Xd1sQWlw43b9Je2Nvpml4pN/hPGM3pbWe7Yp1srYd99mwzOi4Hej00nnrjOES8d3QxEPq7uEymMogmSdXPyhX05+ut5qezCYPJ2oOp98Mob7bZe51h8Q81n2yJxohT5pj6V8ZqiQIBkzTuNB24EMr36z7526blOR91iZsgX3Ka+UDzbEz2u+B7umnTpgJbU73vFppf0bbaUb1Mu0v1vlK+wDJCEZJH14HX9NNVL7tyluWsGoFpPl8ymkLXg1UnLOSGEdb0CVHLqik7tKa1pdxDo3jm0isFW0QtVzq+rG17lq+xkR12HK3K1yFfBV5gLWNGNXvOVCYWv3fPHS8arZZ0qzT0W+vjU2xXuAP2a8U3RsHdMX7RZo4Kmpyjf+6rYEevpF67U6ey3ZbolSJz4fjJY8sDvlW8VDx/G8sRZnWiidNZSN7E5jJyD3g4LyOXpdviSk/OHzoUdxJYMUE3O3rhdbQIvuf5Ert+daRcosKmJyrzRfLVaU4qnOzlQZzseP3xya5+sXxpMoSUWkfIO12ZNoGLvRocWvHJuKy/rcylKgGPQ2SXVzY01lyVyQTXEC4xfeypOeghUrg4TdbqwdL0pjNzK3Yshr6e5CqsDLz6vKXNuFG0lmN9m4jpqHAKEG0EXlVgNeO1pfuXDpQjbu6tOFyL9ah+tpF78Qood507FfVnZUqz7LyqM9a1P+XrgMKq7ccys3RJOTSk0+81Y7jmbKo4n6il3IaBODzg8/cDR8zHu6eVvp8qt9yYnBvYOue57E89lmHr6hW78Hj5wi/csrl9WfO/hmRQzznzcadPDY8cnndjaHl1Q6ZVo5zHkWnGXNYjrkc28f33wzQhSISXceJ0e5rZxZPVx/PY5lnujRjZOTaGbaOD9as+gy1YC2lXP75kv0KvdQbyOX9YMMYNIPvIuh7FqL/EqkMVOsTzRUZwWzgxRqr7D/FmSKvn7LuTxo+mqDj0xR01GZBzDK5FMWcbTPAP1n3UbYwLMuGllJq45nia/CtY72OQktMgmpv/Z9AXFOLNkGY1wCdlJiniC/Apwl6XOzNFy6Q4JKqI3ZLMQIrc+b6K8vrd7s/V2krx5zGEtY9XLZBeNOuI/27wywYjLuJql1+Vni+iiuUB9XlBv8GbWQDeRPtT7KXyGy7t1CNSZZUlyJz21uHYRw3wjva65ucX8zcaiJJIq34+RwWzZXXpJ5eb3+VwMuV/b39bffGxKl/0/OeVBhzveJoML1ONNKvtOOzn9dRJPg19DKQVdbUNCyKa+ClVg63tuBJwGl+w4hqnGOv7z6FMuRDAh6XOUw2EzO1Du7kp2D2gEnvG9K4WpPA8+MQRRaGyuT8zGy5Y3JtyW93btYssPJWuGtz0J/cfHr798++fv//Of33777gIzenBgJmXGHx/fnbFwtXXuVdBcSENnO6f1rhKOqu+GoLlOeZy005TX+m4092u91ctTQBsm6+8cvTZ2ymsaNaX95wxq42CDBt9nejsEDdJi9LjcCek0ZlruhPI2Uz87zbpSGK0eVLgG2mTwGzRqdjYhLaDNnZ1COp3W90tunTboKWjjh+5Mb0JuQVOjne/X+slOt8/pIeRvQC3kDzToh4b+9u1OC7nVqGl8JmgWNKuW/DltMl3QoI2MXddjbsEuaN0b/8wJtcBlRm02LmW9jyPTRinHE+2Jtv0wotZDj2NnnxanwfdNvZqCBimbdNcFDXI0mXHRIO8T1pfenzVvOqPu10xntdc8JbjiS6UfUFM+tzioNmrUQ9AG0PQ83K8Nva9n2PebQZtFM6DZbgyaBW0al89NHwaq63LnDFqPn4M2rUDT46L1oGHtQQfF6KB5620M1p5+QC1Q6r1mO0yZQq33JdZ3qCEzVw6gzZ3/PqdBjubRl4rTIEez1X34nP6w/diPQTOgadOHbwBtUL4OOg3uHOCpIS1OC6ke/J1j79ufy4PTsD24/DnNWP8NBrVZLxo8RXdI3qIWasiE2uBbcT+jFrj4HFnbYzn43Fp4aBe0HjRrls8p0OZuClr3YadudJrzFWbQ1KyCNoE2etvjNAua6eagQaqnULNmVyfgkUMfNEjLrKy6XZs6bH/4LoRJ9ZP/BoUa0nXaAJr11s05jaD1ZtZBgzs1tgd0KCetOxU0AxrUtKBZ0Oy0aNMHJBq/oXf5mwzaT9dZOW3w9gynOqBRzX3QFGjG110cvk/GmuVOSBlU+TlokDKr5uUapMwO/XINUmYDM7w2T8anbHQaVMh+0cAB6bAdDb6GzArr2eBrCLRvfIqvE/MYSPiwrHnU3hLhChNUa28Z0POeR7QMTlMfsw4kjMvtrAMJ40hADZzGoEE3C4US7gSesw55N471bIYhfIPTwESOQYOUTQM+08+mz/CYRXMOVsjfjNrs263TrOvDsWMZu6CiBR19TQN18o1gVKhCwzJBdTcPgzfMTnXrGgMSGL1NBRUrw+gNrurGzrcE56843wH6zEXtnTp464XeDKhYBZw6OtXePuvmKjU2nNEbBuillQ5p9uqMBbhchez396vguKjuftVlyat9UI03zOMQ1Nlb5nFEVaFJGDWq/aDnoEKq1NB56KN3wkANOfJ2FVQs5tEbXVChew+qcioahtHPXik1oj8BqiOpRjWFq46zGsGIBdU6FSv66DtSUNGijr7HV0pj5zIaVC3agNEG1fpOcZxQnUJl8NYDHB6wx0E1TrXeKoxYc6BlIKvZp6pX2DacOjs11A1v6kDFHmH0dhDUUFW8kQR17sNnHStwREz4Xkey79FWQCJRDSS9KYZCML7n0VhV4KI31BqrClRYXze0Ciq6ILoPqvU+j1PdZ6GvdznSA6qm82nWA6bKYiejB0yzne1y1eVowoqkB8zvNMxB9TQm7ISc6lhNYOyD6khOWM30gJxnqFhBdYmcsevR6OX3s/actfdYQUWXTnuXFVTso/WAU+sddtLau6Kg9t4qQX6Vm35TSBKuerXHZqXHoGL11jqoWOvcyMGrWM20wSeP2nuB2uD3joGkd17VAIWvgupWacCD7ILqxmp67IeguphFjR2TtkG1vsFqb/VARTfHqdapM2bBerDQqryhcKpLJNQyg6orlCE40dobZVBDgZqgoruovQFXwzRYn98J1Rk7Oqe6m+fZeyoa6zP4pD7Npgsqjg4MGrexQ/NlFF7t0Rc0aBjHHmusQbM5DsrbfINGdYQ+bQ6qdSp6zQYNMij98lk3ABsQu1E4IByxAhvfxYMKlTSoLlWgW0ykf0fBiH2GU12qRnQIQHXFPY7I2fgQNHBY0cY61aUKnPLwKL9JXmvfSQMc5VKlDVbRGZfTg+utp6DqPhQoquha6bDZPnjmGoeX44RDFh1W5qd7FfUqWpVwFSodNisdVHSbtI+LVTp45E7tnaq9cXPfCzR0H+qzRnUIdTJcBV87VFGvjsq7YC5HXg2VcA7qHY5XsYc1KqjT4L7I9KhqNCNmCKr2TzZjUI23OU51abbYnA0u22mLHa4J2bfz7SrkAhx77wUYvxgF6uCdcqdap6IpcOrkVDMsn3WFOM2YBT+GBVJoKAwuBeoZPVenulQFB9/44Qyo2D0ZP54BFUvBhHmGrhtCMlx9Np1aMuhqu4EiCtl3bcF0el5UF7rRwSgAVdeOYIilwqNcKzMqtBTsYU0/YX6xscNoyVdCg6YA2jYmcgrqMPssYOs2UEMX1X2Rwa7NYE8HgyRvGQz2gzBKGperLgs2WAbsQ2H0gZyxwwXfFtvRhBGG4MwvqluHnRRWlcmDNdMwLKpLVRgiONWlKowRQHVFZqZpDlddgcJoaViuulSFYYJTXapmdBKc6lI16zkkwy9dzuBLBxVSZbvAefKV0HbYwxqs/FapJfte1dhgbRdUbLBWBRU9aNsHFSuwU92TLWbB4sI+DJd8W7C4oAq3+iw4VTsVs2D9+rVyI6blUdapaGOd6gjMmAWL/SAMmnwWrJ++ARWdBOvnfUBFU+BU97QZ5yasHymBiibX+qGSmjo0uTase3Rocq0fLIE6DstV61S0KtYPl0BFP9368RKo0xiS4Wrd1KEFdiqkCvz9OeQXVY05GoM642ex8sODfIO1WPknM/kGaw0+eQ5PxrYwD7M3yOHmebS+9MPNs8EWav3gBdQBv9ePXkAd/YDa+uGLAs97Dp912Z8tNkmLHf1s0fuy6AbMwG4OqnNIJvRULboQgBlZoYMBVbILiXRFNs843WHROYHO2SyqS9WMFtipLlUzWmCnumTAN4VE+jm6Ti3J8BN4MEBYHuWqR4cG2XrLD+rkDZSdUO2xA7JzUNECTx2qYYAwKVQ11menuieDh7hcdd+rsT+acEm609bbK/coN3WosT5PuLbUGazP7mbrVMTuVGccDGJ36uxUrM+T7+hBxQo84Zo11EFfgZ3qUmXVGLLgpq46Oy45cjNLLvfhyW6mHkZeHt0UIgYVtv2px6ARaIV9UJ132uMQbxqCij7ScnXAcf5ydUDjNnkvF1TsRKYe1TGQ7HE344i9xuSbM6jYa0y+sbuBiApf5KPTwohg8oYCVJyHdKqraW6+L6guRybQ6H2huE5lUV0ijV6y4KeMDTbnyRu33tnrIaguVRZ90UmhCi1WoerSOM12+PHj//4fgfh4bw==',
          'content-encoding': 'gzip',
          'content-type': 'application/json'
        },
        gridID: '8',
        msg: 'MAP-DATA',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function ninthMapGrid(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        anchor: [
          12,
          24,
        ],
        cleanId,
        gridID: '8',
        height: 144,
        msg: 'MAP-GRID',
        resolution: 43,
        time: `${new Date().toISOString().split('.')[0]}Z`,
        width: 144
      }
    };
  };
}

function ninthMapGlobal(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        angle: 0,
        cleanId,
        gridID: '9',
        msg: 'MAP-GLOBAL',
        time: `${new Date().toISOString().split('.')[0]}Z`,
        x: 180,
        y: 5031,
      }
    };
  };
}

function tenthMapGrid(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        anchor: [
          12,
          69,
        ],
        cleanId,
        gridID: '9',
        height: 144,
        msg: 'MAP-GRID',
        resolution: 43,
        time: `${new Date().toISOString().split('.')[0]}Z`,
        width: 144
      }
    };
  };
}

function eleventhMapData(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        cleanId,
        data: {
          content: 'eJzt3duSo7gBBuD7eYqpvuYCMMd5la6+mJ3pSrZqa3crye5NKu8eMGALnX4JgySbv373JrjHWHwIIcSh//v247fP779//nz79v5eZKbkxt88ltwxWz6zez6yLwkTJRESkehZiZ4KCRGxHjkQHYMUfcH3JVpCIqcFWd4nkcOCkIhELkTowyQ6+KufhlNHdOQi++G8FNEjG90piF4YKH2i6EDpEyWApBJtmc2RRNGRZKJtszkN0fbZkCgq0AsQHQ2UFFFq+zISkeglibbM4OWBRKJtM3h5INYiEu1NlGZz/fREJ0AiEYn2Jdo2ixMRpQqkcgWmezaiCPXruYmCIJHocKLYSAGJHpnJiyPtQRQXiUQkeiKidM+ivQBRXCASkegpkEjkRbTtC2MDBSTa/pUnJPL96hMTiQNYJDJ+uUtBXhoIEbkVJg5HICA/oiOR5LkEWPSwRH5I0Rc6DtELh0QkIhGJUgiJSEQiEqUQEu1MdEokEj1IpB6eRi8wiRIMiXYnOiGSb3NNIhKRKABR9AKTKMH47dGiFzdFoiknBnrkiXynCYlgSARDIhgSwZAIZp+nYL10RqLohUg7JIIhEQyJYEgEQyIYEsGQCIZEMCSCIREMiWBIBEMiGBLBkAiGRDAkgiERDIlgSARDIhgSwZAIhkQwJIIhEQyJYEgEQyIYEsGQCIZEMCSCIREMiWBIBEMiGBLBkAiGRDAkgiERDIlgSARDIhgSwZAIhkQwJIIhEQyJYEgEQyIYEsGQCIZEMB8f2dcvb3/88u/Pf/39+fPt27vnw+f2ib2QST2sw//5fCGIkgqJSEQiEqUQEu1EtP8fjUMFSwjThWhd5OOQEq1tmOi+AGhxthL5YyZFZPrg/rWKRM+LtO0P7YYFSpbI/kESOReLRIkQRUUyEbnO4OWBSESi44jcZ3BSIp8ZkIhEOiK/GZyQyHcGJEqCKCqQTOQ/g9PVIhKR6FGiLTN4eaDn2OknRuQ7gxMS+RaJREkQqSUKCvecbdH0biCmR3f64ZEeWaFRiEIA+dUwEoVHIpEX0ZYZxAYKSrR1FiQi0R5/lvA0RM8J9BREsZGegCguEIlI9BRIJPIgki9AJ5GGaNuXxwaKTLSuWSTaWJTYQE9AFB8pKSJ9cUgECxMSIzBPykQBFj19ouiLngJR9IVLh+jFQyISkYhEKcT3UQbRC0yiBEMiEpGIRCmERCQiUYJEJ0QiEYlIlCBR9AKnT3RCJI4X7UoUvbAkSjQk2pEoelFJlGxciaIXlEQJ5/F7Y18+/KsgMCSCIREMiWBIBEMiGBLBkAiGRDAkgiERDIlgSARDIhgSwZAIhkQwJIIhEQyJYEgEQyIYEsGQCIZEMCSCIREMiWBIBEMiGBLBkAiGRDAkgiERDIlgSARDIhgSwZAIhkQwJIIhEQyJYEgEQyIYEsGQCIZEMCSCIREMiWBIBEMiGBLBkAiGRDAfH9nXL29//Pjx15+/fv58+/ZOMHtYp2BIBEMiGBLBkAiGRDAkgiERDIlgSARDIhgSwZAIhkQwJIIhEQyJYEgEQyIYEsGQCIZEMCSCIREMiWBIBEMiGBLBkAiGRDAkgiERDIlgSARDIhgSwZAIhkQwJIIhEQyJYEgEQyIYEsGQCIZEMCSCIREMiWBIBEMiGBeiQvjvCYOJChK5bmjFWZFIBEMiGBLBkAiGRDAkgiERDIlgfIhO2sP2q0Uksoa1CIZEDuGGBkMiGBLBkAiGRDAkAmHX0SEkgiERCDc0kOKW6EUOHR+i6IWNE57ThyERDIlgSARDIhgSwZAIhkQwJIIhEQyJYEgEQyIYEsGQCIZEMCSCIREMiWBIBEMiGBLBkAiGRDAkgiERDIlgSARDIhgSwZAIhkQwJIIhEQyJYEgEQyIYEsGQCIZEMCSCIREMiWBIBEMiGBLBkAiGRDAkgiERDIlgSARDIhgSwZAIhkQwJIIhEQyJYEgEQyIYEsGQCIZEMCSC+fjIvn55++v337///es/vv/y2+fbt3eLWZ0VeVZcsqJaXpfxHfHt+Q33MhTjx8o6u1RZVWR1M3x+eDMvrv/b91nVZVWeXS5ZeZ07/JDuR/5dPn5k+ODlOpOx2AHJLbWyz4omK4txUYdyza9qfGf1/vJOvwey1aO4sg/4wyoYVsSwOqYPqj/Sr0wrLiLxrZZ2V4lr0YYFnl+X8Z3V+8s7w793hfZnFjadYZ2OX95mVTnaDf9Ueolvjrjl+I/HMo7zju27rrqXa9nGSlMsr+uija6396d3Si9jb+FKs0lpSicUUy7yVMDGq1E7gLhXKq5SU6ZKMZZ8ef9eTXyYvZBvsPIWVRmQbxWh2rqZHWR8Xcty9Sg17d4weQOffzW3d6VnjXGXXumqrromY9VGbNptHARd5PrG7sYs77ulXUyhki+toAO5YZcme4vGOl3DvnC147vWAs/+0CHmt6pt3JPLXSXbe8ru3a1eSatd6uqN8qVG29LnMHX2pllF2jnqV4Bt2deFXy2vqUMrV1bnRvRWEcy9bQu7rTja7VXaFKJvB7aNv1AdYN1b1oFfbwG2+sat0xU/yW1CXBW33oz5AE5o4bUNsLQW/PvFcDWYLOWKrqsXpgPPqAdG8nqQejyadknazUodD2X3ufUQ0H+LyIWel2a3bG1FYx7+y6tA7QvZujnlqh+otuBwWUHDZDzcF3cN4tHo/ZBjfdRhLJdXeY62t3YF1S79/fDWugOxdE8t9IY+8QpYPEQVD6OEQ2pT3y6BPcACb+mDyCVdjlqnQRLThqIuq0/boz3sU49TK3lcSDzktQ1CJtDmL/L2HrjcX5v6kso+QmwHrNu2vV1VxzPWtvfRT+HQ9TaWY94CNcdyURr5ydx84GXfKG86UqXUNMru9UodXr65igP691H82m2YPZkajo6yLDsfw/Ca5iDVo0pJ50uWkyXzHxW5f9rtsCSJBtxeoaGSUAU1TfnWalRY1gSuEwkcp46yngXV8NSGiuTZFXEN2I+n0AjPsp2tCXbQsRyeHNSfRS1CGrDg0FHux6k47rB7LaHnLiISrKWbq++lxWZ16tNHV7V0whzaR6rqVUHhwKBoWk1AAl3ZWdX9CO0R03Ptr7QDbK7daoi6d/8KDIomxIpXuLmeWVmP2Rg3V9b1UdvBqhsOAJxUD6s3TsOpq7JLFw5uuXxwI652IEW4WMJyIjU8rdM3iq3PBCldMhjqCpB7QyuPYLucIU1U93a1kzAqubpUMNiVTPfOgXpCxmEXlJSv2CpIA+3l+qxGuKvxNP0E33OaaRorZ5HEWiRfLnjwCKOp1+BFlBTzjVq6SmGWF8figo45ansShWeFRvv2gJ2JfFV8zfapXhwSePx8FLeM4DtuYvhiub3BdSeg9VunthrpxAMdS0+j52rNuH+9w47Do4bvs7Gq553BZZlKU6N7O8Tx4OStbUh27ELvv6mK5/gMVx5fF0R33Us8Z3DMb+/N+x+T7QRtuJL+vlNcHx4kI+yLEu18gvaEtXDxyur/Rxs8fhw43hi4dEuTdJ2KcKtLSrwbvhtd9XfU/vs2+qNelHK7pygtXJ0IGK8yXt8Zom+kXn9yiTAu6AS72uM7DVUFPyTRFmKec/DeozPq7QYRlxGqGMPGVl6HW4YOPqBTDueEE5vGkam5UiTnWswV1uE2oIOP2pRxidsdYOq9q/lyleJl1YzdG1/HIcZgDe1Stuhn7zXKM4d2P3Rve8Xd8L0pPr4aLxuOPubnI9jvoQgDrT+PqB25uTUZ94ZZ00I/sIczCU6RzruJ9dbysj83wTJSF4bf0A9W/cW25X6GQdeU+9Z2Vdf9JZ2tE2u49WV71IL5FqHg28TqgEJsfORrVO+Nv3Yv4P30By2wz2t1mk96wIHlZXk6QxF9tWjKJRRW3V2YVpdw/sLzDKoW2OulbryOL8MlleJZm4hrRt2QbwuqG79Tx721fWOnRx7Ymb1eynbs/tK148YbsWOsInX/N9dg9Qhf3hAs/VJ4tgda+76Ujfrxn0Ja2gL8hFpl2g7jfecp1FbrrffKkbzpmMPVfj91XXEf+XGZX4hBQmElao6vct1tcrYnyigVzrQGH6v7e/+Y1rbbD97Cp/PmQS7/mO/UE+7ZspzJcDmhYl6DR64R3ZpBvvonFmhflv3m3F8LcDmUw3PebDdAO66p/bXNa8DGvR53Av0bS59zWjHHXwwInnLYLLs415Xj9pzHbb56bjOuPLZqPxKwHJfdxrSKQ697tT3UE1R9dUWg50B6e2orqAVTeQSm7WUZq7jdXX7oJd02+8LLvjE8CeIRTlN9NIPJjzywjBlZxu5CXUj/kPy6s6EfYNyMaaqMZq71aYVCybxc0jsH5yFktW+uHULfzKavdDqj4DcmHaKamxptz1FfYxULVq2iMhr3fffa8uQwh7BZal/0RSJbsjlqI33xkI1sabDlT3B7fZJqyT/rJUk11jVvNTZsm9i8HyYX4dFs6amBRwabhldOEbMbrmumIZPoCxUXzvVPF5y3kbuenJ7HJ5aRCmE4CD6uNuB59PRyxRMHxszX7NmGOcUTIads9VZDttYL7Gzj8fcTGlH+7FVcwtWZBXQZnO1oIuIftYoruDpB7nCZmuWS9kh/qyqun3hG1/OiMeMu+CQb8B1R2qnar1zQ92JO3hlca+potvyc9qDEzKn2Aj2ut4n90PIUMDdfiHTSnuLab+NlRKfsIK7lNl4EdMZ+4Rpu4zU8QZ5cmWrWh3de1+WEuAws1axHFuCFN9HLG1mKELbYz5kxOYkcQiIYEsGQCIZEMCSCIREMiWBIBEMiGBLBkAiGRDAkgiERDIlgSARDIhgSwZAIhkQwJIIhEQyJYEgEQyIYEsGQCIZEMCSCIREMiWBIBEMiGBLBkAiGRDAkgiERDIlgSARDIhgSwZAIhkQwJIIhEQyJYEgE8/GRff3y9uf3//zz7dv7e1FmTZ/1Xdd8ZONf2xomirJvi2mqHZ9nVtblOFVlbTFM1dUyVQ5TTVuNU3XWjn+Xoq/reWq88zbvm3lqfCbNpe3GqWaaqot+nGqvU03bLFPD59rLJZ+nhnm2VVvOU8P3tW19maeGsrRdX81T41+SyKv5c+MydEU7/67phqlqKtkw1Q5Tdd/OU80w1VbzPJvxJv2uXX43lKXPi26eGsrSl/1czmYoS19VyzcMZenrbvn2oSx9WxbTVD2Upe+aZarLyrxYPle3w1TZzSWrm2GqKutJqR6fhtRNEtcHNZRVOc2zuX6uaqa5NNd51nmzfK4fpi5VO8lfp6qum6aa8a77pmynfzmUumzyqp+nymHqcinnqcsw1RSXeaoaptp6+d1Qsjav5u8bBMt2tm5G3bKt6uv3daN82dZTLejGtVK2s243rrGyy6da0I1rsxwWNp+nhrJ01bQM/VgLym727McaMlTPfJnLUJa+q5fPNdklH80+/vd/QpaZsw==',
          'content-encoding': 'gzip',
          'content-type': 'application/json'
        },
        gridID: '9',
        msg: 'MAP-DATA',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function tenthMapGlobal(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        angle: -180,
        cleanId,
        gridID: '10',
        msg: 'MAP-GLOBAL',
        time: `${new Date().toISOString().split('.')[0]}Z`,
        x: -4937,
        y: -1118,
      }
    };
  };
}

function twelfthMapData(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        cleanId,
        data: {
          content: 'eJzt3duOo1YahuHzuoqWjzkA7923UvJBp7uURIo60cwkJ6O59wFsMGvx4w+7bC/E/+qt6pRr06GeZmdY4P8uvv/x8e3nx4/F1/f3Ivts+Zw7Zm8QQQQRRMl7BFHyX2LqRMl/hakRJZ9giCYYRJ8gygG6TpQbRMkndlpEMEH0LKLkkwvRJGNBgwgiiKbQrUQOkSCC6PVEySd4+kQOkSCCCCKIphBEEEEE0RS6ncgdEkSfJBqmc9TtRMkneUpEzEMQPYco+QRDNMEgejiRQ6T7hoNWP+oGjBGzLyCaPRJEEEEE0RSCCCKIIJpCED2UqPuDboDuJ+JprM9F6hFELpEggggiiKYQRBBBBNEUYu/6wUQukSCC6NVEySc3LVH/tgUAtURDXwSo7hoR1UEkg0gGkQwiGUQyiGQQySCSQSSDSAaRDCIZRDKIZBDJIJJBJOsSXQ7AOj8UG6ZuiEEjiNyD3T+o2E2cjX04kUOke64BST7REE0siJ5AlHySp06UfIKnReTqqrN7iXi2FhAx5Gog62AIQEEcL5JBJINIBpEMIhlEMohkEMkgkkEkg0gGkQwiGUQyiGQQySCSQSSDSAaRDCIZRDKIZBDJIJJBJINIBpEMIhlEMohkEMkgkkEkg0gGkQwi2fGYfXlb/PnLvz/+9c/Hj8XX90+/tszMx9o+4uV3Zs4E0YuIkv8aEEE07SCCKDVR8smHaBLd/9LxboIIIoggmkLcHuMpRM6QIIIIIoim0L3P0ZJPOEQTigUNolRErpAggggiiKYQRBClIko+2dMncsUE0dOIkk84RBOK1fWTiFwxMRc9kcgNFeOLnkzkggqilxAl/yUggmjqQQTRNIhmjsRcBBFEEE0hiCCCaIJElx8tzu+zjxe3hAgiiKYQryL7cCKHSMxFDydyiMRcBBFEEE0hiCCC6EVE4acgGk10+jgmSz7Br88msr4VIhoKIhlEMohkEMkgkkEkg0gGkQwiGUQyiGQQyS5E4eEOp8/qrdRRx+QTmL5xB2aTT+b0iVwjMRc9iMg1FTfEgCgdkSMkiCCCCKIpBNHTiJJP+NSJkk/2dImST25Kou4nYQniwKwMIhlEMohkEMkgkkEkg0gGkQwiGUQyiGQQySCSQSSDSAaRDCIZRDKIZBDJIJJBJINIBpEMIhlEMohkEMkgkkEkg0gGkQwiGUQyiGQQyY7H7Mvb4s/v3//+6/ePH4uv74Bdj3lKBpEMIhlEMohkEMkgkkEkg0gGkQwiGUQyiGQQySCSQSSDSAaRDCIZRDKIZBDJIJJBJINIBpEMIhlEMohkEMkgkkEkg0gGkQwiGUQyiGQQySCSQSSDSAaRDCIZRDKIZBDJIJJBJINIBpEMIhlEMohkEMkgkkEkg0gGkQwiGUQyiGQQySCSQSSDSAaRDCIZRDKIZBDJIJJBJINIBpEMIhlEMohkEMkgkkEkg0gGkQwiGUQyiGQQySCSQSSDSAaRDCIZRDKIZGOIirLmT4eNm4uKNodUY4mST2i6WBfJIJJBJINIBpFMEbnelp3SRFbJJ/uVDRPZOM54qm4hSj6xaWJ1LYNIBpEMIhlEMohkEMkgkkEkg0gGkQwiGUQyiGQQySCSQSSDSAaRDCIZRDKIZBDJIJJBJINIBpEMIhlEMohkEMkgkkEkg0gGkQwiGUQyiGTHY/blbfH3z5/f/vn912+//PGx+Po+yuyQFdtsWWTLdbbc1H/WH6zW2brINtusyPPyvei85/X74ZCt99k6z1ar6keKVWqBB3uOmuVKvH2tVTOUZtWfzQelTSlUOoWEefvfUrc0XtXexXpeo73H+J1mvVJvma12NVdR/9n9wAAsAGz42pmv5FtWWJXIvvPBkkV4sE1v9mtntvI/l49KwQHEFrn8+fJfYVl9NvVv/VrDov6tozmwu46LP26X5txaknfVP8ay/ofZpP7VXwxZL4LxVqM3O4acYo1Y/jWpf+3XMhbVVsDeeWkWVnNJ9rVl0Y5XZsd20xLMkP33mW9bPmUYa/bfZr5V0XpXl+T+PrTxls95a6IBh2a/rt1lGV333lb1rDfPjcj9esGSe1lCN+Hbul5mt/OEe8A2+AzXLJvV9rV9W1UruxkurWPorm92gzVatVS6uEb8ricg8Tw203XYPV7Rk96+2Gx3OO7z0svkbPdv7UbcA+X6+n/OT6vuIxs1j811N/ZOMGu173c9psHa9T7rsXFgrMduJmNbGTQK7PruKyuxKGawTqzEnkDGPBbESuy2mMFujJXYE8iYx4JYid0WM9iNsRJ7AtnYeexy1mjmcI9Zja2Dc5Scc6vV5Ix2Gd6bc5q3URuxTuuM663mPwYXtHjXZ7lo2WVQS9PVE5inh/HQXR87cvecNAmk4rH2DOjrCw6MqTLGNzO2dBBx7Ng+IO+dH0eNHGeZPhmGa0V7xFq9jTEkqy+7367Ic8vd/ZnB571O92fkU7ZV/Xlz/9r9nGddxBXuJpsbGdZ758zrV5snuAOXALPdbe26F0533oa2Jcx2DVw004Vv5r41q7tsYGXXe1NLrNMN7dCQwf6bt/Xd555p6HcUz4bDT3zHvsMI4/MZhxdiR7eP+LRiO6NdDh3Ex1vnvkvzybmwvc6yfU58OdI/+1Mhj5n3znfSKcKnxnM/Afcwu8sJy+5z4zmf9n3MIttcsxSPNpjxOINHznPnnZLZj834tJu1FzLDfTnbrLkAvHsheOfxbfNa6l/oZWjduw1Edx5YGZ93fAx5iDC+30X3xhe9g6GDx5JdHNMbIjTuuNLuaVjHRM9HRq+PHJ3pbrAtaN/1p3sDyYE3xt6eBYcOf7bnu4e+iuBZUB36jEdfDH4Xoq2oPnQ37nCfxwPQw6r68PytB1BR7a8bw6N89pA2NG3NeEPdOehn3TlxYEXqeM/c3rFsTxNbt4c2N+0uDksP8RkjOVahYx4eT432PD2M0R/WC546dwbHmLfHN575OLg05Are+bjNplrkqkMR1ssyGM+vwyOsjtCsA4ctXveATv+wTvv52R9b1WSnivbP7vFEJ/sfYbzajAwiGUSykGhoXeNyHdTUEEXbtWazdbIZ+KK7Toc6rH2n5uRzF8n8Hle7T103e1+9Ge7QVTK/p9xd93AHBkvOfI54egoYOQ1do+LlTgymnnmA4nT8IbIaujrKyw0ZLD77MFn3YvjgFgLxNzm7L4MlOHjYNr4ovvtyLdEhW3e3abAg40Pd6uL43DiaG37o+a7Ol5NhwZxmv4qLyR2fXChsYVfHyZv5NFwL7sNzN/2XuBpz7sbn+ZvzGjQamW1seZYBqH2hAfOmMU67fwOlzn6Qfacb1yvN/qmE/rOVy474wF2WPG/H4zML0VmG4ZfrvexHunkVMcsvPtMVnd1aWbetGh7L5nAGjOes8OTq+QPzLl/2WDaHK8HoVH98et96GjPm3elWeeBeSoM3WeJq1DGq9hCp/pCo2z2DJ5weZ9jbucaPpnR07eo1WDWY0pqfh4b+urqo1UKNMOLBlP21rrmV93R5a1/ROAzZG/JnfE8R7rw7ucS1zxdDmEP++t8TPs1xcp1rXy92MIf89b4nOm3j7OxXYxcrmMP+ou9x8nLJ1+FCAnPkX/TqAMX896yvgkW/fveK4eQTmD5GbMkgkkEkg0gGkQwiGUQyiGQQySCSQSSDSAaRDCIZRDKIZBDJIJJBJINIBpEMIhlEMohkEMkgkkEkg0gGkQwi2fGYfXlb/PXtP78tvr6/F6vssM+2Rb7dHLNqFFL9aH9YV4829aNlscurR9vTo1VRnB/tst16u9+1X9ttDpv6O3f113b5bnN+VH5tn++arx3KR9vl5vxz5aNDvj4/qobE7g7bfHv631cP90VerJuHRflwvWq/usz2u3KKOl/db/N981eVDw/rTXH5m/eH/T5v/7eHfHnYtJN02K1Ok7Q/PVpv61/scHq0rSdheRrBu9+tVsfj//4P2N0Ddw==',
          'content-encoding': 'gzip',
          'content-type': 'application/json'
        },
        gridID: '10',
        msg: 'MAP-DATA',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function eleventhMapGrid(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        anchor: [
          11,
          98,
        ],
        cleanId,
        gridID: '10',
        height: 144,
        msg: 'MAP-GRID',
        resolution: 43,
        time: `${new Date().toISOString().split('.')[0]}Z`,
        width: 144
      }
    };
  };
}

function ninteenthFullCleanRunning(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 5,
        cleanDuration: 62,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: 'scheduled',
        globalPosition: [
          3,
          40,
        ],
        msg: 'CURRENT-STATE',
        state: 'FULL_CLEAN_RUNNING',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function twelfthhMapGrid(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        anchor: [
          16,
          72,
        ],
        cleanId,
        gridID: '1',
        height: 144,
        msg: 'MAP-GRID',
        resolution: 43,
        time: `${new Date().toISOString().split('.')[0]}Z`,
        width: 144
      }
    };
  };
}

function fullCleanFinished(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 5,
        cleanDuration: 64,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        endOfClean: true,
        fullCleanType: 'scheduled',
        globalPosition: [
          3,
          40
        ],
        msg: 'STATE-CHANGE',
        newstate: 'FULL_CLEAN_FINISHED',
        oldstate: 'FULL_CLEAN_RUNNING',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function thirteenthMapData(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        cleanId,
        data: {
          content: 'eJztvd2OKzuPJXhfT1H4rs9F6F+qV/lwLqq7CzMNNLobM9N9M5h3Hy2uxbDTDv+mM+3MbUSeg70ibAdFSRRJUeT/+4///N/+49//+3/8l3/82z//ufx13RWuvK79vfuvayl5yPX3X//yZtHjWHQtaW8WvVn0ZtHdLLr+R/9QFr0Sg16SRX8ug+b7rmHRazHoBVn0Oqz5fgZdxaJXY9C3j6FHsOh7GXSaosczb7mGRTf80AuwaPvZte04+aufZdF3M+g8iz7TkrtYdPWPvASTvohBj2DR64jrL2TR1gvu+KlfyKJ1oh2/5C5u/xks+sQPPZ1F97H1j2LRaSo+2bI3iy627c2iN4veLLqGhk+27Lew6B6mXtm2n8ei08bqm0UXmvxVTPqBLLqPijeL3ix6s+hBTLqXtd/CotcYR1/BoDeL/iwW3cfWK67fw6Iv04z+FBZ9gknbLPqZrtkvYdApWfRm0QUW3fejz2bSfWy9i0X3/uh3suj7rP3lp46ibQrfLLrY6Fs//2kWvfKO7C3q4+7JnUz6TbJoi4qPT+5i0m9j0efa8KUs+l4GXabugTFrhyy6nojnMulhDLiVRZ8h4s2iN4t+Gou+kUlvFn2CRbcR8WsZdJ5FrxoQ+iNZ9MeMos8R8guZc8iiz5Lz+xikdzqLPk/Qb2PQD2DR97LjTJseN9Eey6RnM2evTY8T149i0bOZ8vIsejZLvpRFv4s1J1h03NzvYtGzGXEHi+7bBfl9DLoQd/1m0RUsuuPn3ix6s+jNoq0WnQvB+j696NmsONum8yz6Po/Rs1nxCRbd0ozPMek1GfZgFj2OSQ9q3Ad072+8CIsOv/0YBp2n/xI+8hc9k0WfZ8hX0LHefz6LHsmYzzDo5HWdAXJdYx7Rd59j0Zdc14amv1n0RBLfLPpGFn0Rkx7HontJeCSLvoRJv2sUfQmTHsmi12DSL2TRYxn0ZtHPZ9E9BL44g76XRduf+ONY9PzrzaI3i94s+m0seiUf8kuyiB94doNfmEW7jzy7yZ9l2GkG7u6cZe65g5/HP/XTGbbFtEutvxBfdHw9u4Hfd71ZdD2T3iy6yKZrZNGfzKJw7hD6K7HoqV1z6yj6LmIP9/CeyKTbWfT1xL7S+L2TRV9L8mtN8Ttk0deR/LxOuUDV6VQG30vwbe/8+mwPe28li65nzFew6J6335qc5xMU3VbO8vEsuu/dWxRc9+nLI/Hom89j0WeY89jrAj33sui3sOeK6z4W/THsuZdFfxSDvpNFT2/q67PoxzLpO2XR0xv7ZtELseg90b6IRU9v6quz6OkNfbPox7Po6c18dRY9vZFvFr1Z9Ozr61n09Ca+WfTjWfT0Br5Z9KIseuWsoS/Coj+KQW8WfQmL/ig5dA+L/jgG3cqiP2wtu5ZFt7DlzaI3ix7Joqc37NVZ9PRmvTqLnt6oN4veLHq16xq96A+XRl/Fol/EpK9j0a9h0ptFD2HRHz7V/v77r3/9l3/8j//0f//H//W//+O//OPf/nmlUfsqjLp3hN99XW/3v1n0QML+SBbd9qNvFr1Z9Hmi/jgW3f6jbxY9lUXfzqCvYNEvG0OXWXTPj/4yJj2ORV/LmB/OouU3M8iPpH+OoO9izxNZ9DlivpNBT2HS51n03Ux6Movuzdfx61l03Nxbf+bVGPRQVm6x6PafeSUWPXzMPSYR5uuw6L5R9wez6AEMehSLXkd1fLPoYrNvZembRW8WXabkzaILVDyiZb+MRS880b6PQbeNoocw6XeNopdm0WtoRi/NotcdRZ9m0u9n0afb9rtYtEXHm0VvFt3KpDeLfjKL3ov+C7HoXKPfLLqLRV+kF93zw6/Aoi9h0ukc6r+DRZ9izu9n0QPY81NZdK3n8c2ii/R8GYvuHaavxqKHXW8WvVn0ZtETWHQ/GW8WvVn0ZtGVLPoMGW8WvRSLvpVJp1n0yiGhbxa9Dnsey6JHM+nwNx9odd3Pos/21o8eKz+NRU9nyykWvc5Eezpb9tt0OIr2m/r9LHo2QzZbtcWiw39/D4uezYoXZtGzWXATiw4b/B0sejYD3iz6PIOezaL73vPNTHo+i177evoo+hFMelzYwx84it5MerPop7Do1Zn0Eix6bSa9BIuezYQ3i94s+jwtbxZd/X7++5CeX8qi299/mq43i665fh+LHsygN4veLHqz6FuY9BtZ9GAmPZZFr2Lrv1n0fez5Chbdo7j9cSw6RfY19/5gFn3n9WbRm0VvFn0jiw7ZtfV8h88w9/ex6HPXm0VXXm8WXXFdYNHhbP3zritG0ZtJOz6cSWXwZpKuU7LozaT1usyiP55VbxZdvC7Jouez6Ondcz2Ldptv38uep4/hW1i0/HVqW/frWPPjWPQ95L7SNL+DRf78u1jzdAadyzzzncSeH7Uvx6LvJvUSe2598yOptu9fX1r3K1h0/Zs/d3185+GbL9D0PBZ9F3s+fb1Z9HIsenqDX51FT2/u17Pos0x6enPfLPrxLHp6Y98segkW/YEMerPohVj09Ia+WfTjWfT0Zr5Z9ONZ9PRGvln041n09CZ+L4vuYdLTm/jqLHp6A98sehkWeXP/QAZdx6J7hPSbRW8WPYZFT2/cq7Po6U37Phb94WPozaI3i94serPoNa43i57Iol/DpLfq+GbRm0VvFr3C9WbRZRb9/de//ss//sd//s//63/+1//4L//4t3/e5IP8A69b3bR/4PVm0cXrzaKL15tFF6+fx6JvXzevUwGew4wXoeY6o/ZZ7Nim55vfeA2LnsWMbWreLLp4vRyLLvfad5seL8iiSwSHbyX75SbadWdYv5Pg0++7dBj57neeY9GrrWXnWHGe1i9i0TUy5hkukO03Xqb04Sy6vvGvMdW+kIZtFt3mIvve9ewlxPWtZLwCi759FN1K9tex5PhdLzGKbif7+wi+j0WfovAxLPqDVcdXJPse1fFT129h0cuL6+ez6LHf+HD9FhZ9IQ0/kUW3TrVP0vfTFv1z4vqLpNRPHEW3UvEisuj5mtGX0fATR9GbRXe+7RwVLyCuX5tFnxQEv4dFX7ayPmZP//nLfvg6On4Li17cjH0Fd8gXKh7HLLrnVa+iPJ6+/2Rx/RNY9Inr/D7a58n+iut2E+RT12NY9BrL/hcpj+dY9Hmiv+a6nkUPoesnTrRrHR/7YvoTzPpNLPqiwIefyqJTFH+BKP+pLPrG9z1GXL/GKPqi66ey6EVG0Xui2fVm0cXrzaKL15tFF6+fyaJvfd/5oOKXJPm73/cTWfTNocyPY9Gjid5ixFMOvL82i/YZ9bRcAI9i0fdLo2+73iy6eL3WRHvJxBqvNIpeNP/I4w5bvVCjHnu90ih60evZLPoBjH0ui34Ag94suuJ6T7SL13sUXbweuej/iAbffj2fRS+qMO6uSxPteuLvb2A4YtNLMevSKLqlf+9v1uFbfimL7p9oT2fC+etSzpCvH0Vf46285dr61hew6H5h+1Usuo+tG/Rc2tP/uaPoYb94Ka3KT2TRY3/vwij6Hr3okQ36grXwkQbIa7Dokb9m15tFF6/HsegVZNGXqJy/axR9iZ33+0bRw5n0WBY922d0Qvn73PUKLHpck15cFn0Zic9+/2NZ5GRebyg+dmL8GBYd23eH7rLbDJtb3/5tLPoS0ffl17ePop/GoJfXrl/heo+ii9ebRRevJ7Bon00/nWHH9J9q0Q2+693Hw9aXX/La7cOFPXyKCVcMgFtTGbw+i65rQzjAH5/dOIr+tOuISW8WXbzuZ9F3Tbmn7/G/8ij6Wmvu6ut2cf095B56CZ7EHlz3jKKvY9OhJ+CJjNld9060r3BjvBhr/HoVcf2yDHoFFn0dax70i/fJosdNtM8zZ9+EOH19pHjfK3qKlevd+0bRIxh0+9g5x4JzY/G0U/iq37l3FD2GQZ//nduZe9zBH8fU0fW9quM52/tlr/sX/fu+9ZJr1vnrXtXxnpf9OObwun9Fu/1lP5JB97Do/iX/j2HRZ+TQ05t7z3WvXvQ933mJ6ztH0dMbe9/1XXrRj2XQd020HyuHcH3PKPrBDPoeFv1oBn3PRPvjWPRHySFcXz/RfjiDvl4v+vFj6FYW/XGTDNfXTrQ/mEXXNfxXMOhrR9GvYNDXyqI/lEXXN/uXTLOvDOT7Y1l07fVrGPRVLPpFDPrKUfT0pj3qeuVAvhe53qPo4vVm0cXrzaKL15tFF6+3uL54vVl08Xqz6OL1ZtHF682ii9ebRRevv//+61//5R//67//93//3//1//j3//Tf/uMf//bPEzwrf4X6Vyx/pfhXan/l8Fepf4Vlru9L0H8Lbs0HKeNzIe9iUe3GvL31rTPf/Nx14b1f8s7jaxjbwl/RXhPzmeQRCz6XFmNv/Cv3jzTPnxq4mecHEn4upN1X7ca8PR/Oj8wPfkNbX4G/s6HdaDCmJPDgFIPXjx5xadnnro3uZF0RDvtw7ZvvaezzGTw+zvkFr8sn5OqBfDjJJDI4Gs11fmuzGzfG8a9k8X7D92Z97ts8PhYRByz6ICIKfnyc6cjv4fF58XTwSh8Rj/KFlqOGr+8fA0zeSApwlTA9lhMn+/KbxtXa1HNj+SPZBf+b7QUFtqDYPef//v+vJfDSImYsP3zVFfPwcGBznKws3+rdb5Igy6WV4XjZ/qARFPtHwM2x1x3p6B/HXDtJ0+YI/jDsP7727gF01AVbesuEx3ce3g9nZczmiw80B/s2WsZHO5Xt6B8bvDtB1Olhvezk/MGbx8VxtT0dtnrig44T9v69fJwnW0rlJ/rissZ/3B2Hq5MtUKuqsKfgHf1ji303Tw0btuiNgxd/ZO2ZJexIstaTnfGRCYcMOaGEft302BRTByL8A2EHSsTBPzb4d0d/8F/okE2eHAz1Syts5VJ3Tnn6wIkDo+D69jyyRz6Mqm3DZZ++fY3j4B83DauzQkv/tmX8eNBAaJ4Z89vS5jIjPi4kh425YtZf3Sc3qiGXDcLtYb33r1tl8ClufdSwDucABeTljjmYZNd4bva+vTX5H9MxV1pqK/nXOENODu6Df9+oslyhfJ1g2UmxtG04X5yn83c+zJVb1skbO+es8nU4aS77AU/Pmju75TpFbJUv+1Lm7FJxoE+OywN1kyVXqpM3d8tF22Sf/nPj6Pwic6+VdU4Zy4vPlMP/1mF9evnelKaXJOc28dfYW4+bLFf2ymVtLG4w9KrlZct+93ftlpZjVX5zfl5ymZx433f4pPaafNOif3q8njVWNrh5jYK/ZUb4T3zsjW2N6bCHTtmOm29bNgzPE866b+uLQ5341OzYN/L3LPkTA/ta3f6MwNpXuw52FjYE1GWdfN+beKqRjzcTb+yJ1XY/Nyt8lK2+rUufvuiFuLCaf1SxyOpTmz2XrNVx2if2Nb6T+/vgSIxuG86XNKGrJe2FybBToMIHkm5QSW9q/8NXhs9w/+SwXkXspybAiXVzVWR3ozp/9Kh9HO1n1M7nsv6SRXFB9pzW5a6cJOev03146M394Fz+ONZP+/KfzvqznudDl+11cvG8pn+siZyk7Yz0OtzW+LDFcpWN9WzOn9mAucK/sbnmXvl7VykQpzrxYE/vw5biNZtWz2b72SXxOufFETtPeZrvURpO2GSHu9fhqm3a25j9JA1/NX6v8ShtKpSfGD7nhvnXDK4n6fGn93wuKTtXOYuuVGQuSPQvGGFPVNuPh+79FtbXCPKH8/vlNPVHaPS3OA3OCvCjhny+sa9llV6/0mzvHN4hIq8Z3ucin+5u9Ov4Y27g+pX7Np9yyuw4P44080er5c9WWS6sofs64iNl5qVBfxwp/TX26LPZf3pj/Cqt8U4l5pyAP/aq3+KIucYV8pVy5xZXzLmIkUvr64Zz8BHS/rLP59Ku+A2BeF+w2N7E/pPBj1uBOpthyrcx/7LIP73neiZk5FCAXBWX+hrK5UV5vi/+z0SJ3Ch+zjlmriD/uon3dDfNJybCCXl5envtvjX49DJwymes7r7BP/T0brjoHnb2XnmUbX9dfIRD4dxkuBgQcyMzbzjZ8s3u4kO/5TWnNg/WyDNC6XJc74Wl4WLE2E0d8fQ58fmwhQ90bruNP+XAv+C8v0DULSrPy3fGLS79zWNy6+M73fono3qsMy7qOj9JRp1//fU6LAf72XX67mF28mCAjkqd1f7v6I+r91e/pj8eHBS6pb1+qjfODVft8Z4ZJbe/+uKu65d2x02L+BWR00f23P19cW4N92CH88PojsnxsPCku+bG511NB+HSa1ju0ULyqGMf6ojz8vVmr8XL9sMVB9UObNldKO6RhnuLu+Os2Fbgz9nZ+Xv64JiCDffmsYfT/3/GEXrBqrgYeXVO4Xt9YRQ+bADepkcfO/eP/fvc6Dq7DXDpAMoZF8cF4+eV2X8maKncas2cDzwDnffsgR3NgKOMLOfc0/eYYd/F/WNu7DFklyzh+h64IgLt1t3fD9RuJRy6wWZ5IeZv7gnuyYOYPzy9oSUfJdlR2z43PBThfNpi/1qO37/DeWoncD9amyeG1qdbZ3W/dOnfHhPG8muPOT+K45/b1tyb4+e3GvN6bu7Y9/lNbD85MIzvZwIz7vAg3KJb37ydeTRbzxhJuz7oRxw/Zd5+CfdP7RHzJNFZc/se+XybXndbU09I9NN2yV5/HA35rf++tiPOnNBdNvtif8S+UFeEjcxUYfe38aLtfjnm/raj4fGxBxc4Y4LphCvXwyruUFwe7KreV/O2HO77C8FHv8y5qXEgwG50I3xFR5yRTyc2Vi8qNA/tiYNVIW/52s/w9gOfjzpOK/hNfoQHdsIqm7a9gB/PPt6i4TxWNm3YLWf2ZD7Km52rf09dPRBpOwX267KMXObJ0Rqx5/s52qe4wRH3qMmwuSosp3bmL6y/O2bv22775tuXJRa5qEWeXqw3OuBWS+0x+2fnUits66xn19+V3ftujH1Pxm1+hkfPiu2dgYN+2bh9YWY8Zu/szAasS56jJfvk4nuQMvODh+erapFc1QMb5vOHMb/VA5fN4Ov9Fndvuh7qEht/W0tvuPJE4pex3gXRiVjAi4zfU5C2pu4jOH+1MX4o47dc/I+W8Xczfh3zm0l9tmKZtth2avm6nu+nlcAr+X5qVf3ChfUTbF/3wfbS9+xvdV6UMWdG1EWmX5lb8go5syFY7tmp2mDcjdfBty6O9p16/VFSnuD7hwj4k+bL1RLinAfg0o+cWlXv2aPdv463Yj7/d7y458tTYreJtOcYO6dzLxvc2Mhe9iAb7WzOyDPq5759fJ8TYnP37fN/x3qw/50QIIqRXz6Qc7GPzmrke/PhYf10Prv9aaPtoDvv8dyd2qz7/N8Z7eJ0Z+2m96WuOsWd01uHD+mr4z2VM46OrR67+Y2n37ypM9/3d+x5+fh3srMuuxLP8mOTB6dPtdzIuHEywfrp0XOH6+Cq+fyov2Nv5cHTb+2q28J+zxuLF7yhJ1aqT8m/s1tDX/bftut/u3l3duW5bj3VlZdSgD+oK7dVwnv1jU8M6c/+d06OnJMpX9uRVzDkgT15bFXdq9xfOI67xcxH/HeJx+eW8Y8deXoGXRosz5+Qh67/O8M4L68HH5p8x98lV/+5df0J6+EndJfrumzfbXRP4Od1g/bYq3Dl3972x67nztuh+z76m6Xk5s9t6gSPO/1/dUetOwZ3hIheQ+6H1fMmU+1jXOC+knmsau7tZ900kY48Ql/t/bixZ/wbV3TGZVG3P2H2jytc/beTwJslkY6tN73oxi4pdS/Q4R5n4Qle7i7n6np9YQDytSlt9s8vXOke3HcffYx1SHnLz7HeX+7okaPF+Hj1van25fXuzq/rnYtjaJwoFXTiWn3AH0OA9g8EHboOtTDe2yFn1tarJsjeQf2bXJtXdcjtgUnXiuwbLep93/x+scnj8afl76ruuLCgHrb92sJBd7kuT3bGZ2L1rtRE75yGu6Kr+4LZ7n5iNpx4/XUuofhxBbveF7kVd/jZkNWrtctPL1EH12e4v22OXVQUN/flPihlZ72Ln3Ty3cn9L4lKvo/5F1wa15QdvPzfLR7Ck+fEv2gu3OGku6kzypW5Pa6g4Yw1fq2dH274/HLw2QfJjWvV7xMjcyMe/uSLNjpkbyv3Uoarw6G3vTyfiJDcYtvt3XRdF31emNygf/d9RhyoR86bE7fDuYfqoa0N3OsXsX2N9qCnjqOKN1Ky3LBjcrVA+7x0uaHWSP7IggNrwvmyYWjsP1odO1ufsfPZH5L1HW3gXqF971mU9utHNsj+ONhMwHlpF/Liuv/pkxDXhfV+sK/dXNq4uWmPf3wkv+jWZzx3wUb+vpPBbR96hSP0Y7j9KWt9bB6vv0LZ3XfvnImQ/WS05lnDa9cvx209vhm3PVeHj/brBn/8jM/Jc9n8tpNBHWgGx6H3pwbv5iGWK6zAbc/PkefmnCveTbJP9Myh23YNrzuuRrjp5t0cx5vFDMWPK1L7ba/d+2tv2d/K2ZD5B3rD0VGviy6TD26fU4+2lr1VVC1H3ot7cpkvH6ToYZsP7x4cSzh6djysw/GHdlLlYlddVpc2dh2PNKhNr9gxB0/7HA/9QqceHXP/lBvw8j7YWeFy9M9weGtD4zsY1fuPx9jqzrJ1em9vtT7bfZcUrkMN+vjZB1fzySVk9RSdCGY+vvPxu8fhlwdi9tIW83llbkvB3TIlPjB+Y1Cf7unjX9geKETn160jmfBhxHyQDhuS/milfMh51IOJehzSfKCxHJBw3GWnwveOOXe0GbzOi7PnCk533+aJs61/G97Li3raFt0J1a3N0S0JfUfK++v66EjGHZ4L+Kj0f6Di1Hg5VZl7XwM+2A3erTynk8wfyMLD5ewKg/0wR+qHn9z0fmzskx4slnfXRT13ja05sz9zTua+PrImtofKdnKuw73fA6sirVVat0oxbPAmfOTQdu+si4j1zjkZd9Q/x2r1VnDs5vp0v7N9lW19a7p8nDRH/oljuXFu2AQfDJu2xHHrP5qjm5ahlO52rlc3vCru4ffDQ6e30zY6aX878lxc7AMn0kfl/viY2eHsuU6F3YxKOHKlXdAej/eUqVKesheXo/vLdpsOXSvB096enVCbLtbr9g0/31mbc2lzMG7OpFOi5DR5W8EX8uicOFm06To6tggOcwIeBwnkjz94fNxCU9FPoy4fDbGtJeogsOGCinhhJF/TX8c5IT8ImW1hf25mHWi6W5b72e44ecToWMDlI9G5fPy1dPSx/ZvHq+SHP52b37TATrK9cgKcUUnOfPmq0z37kun0HNtSgk4u0wf9dOwPOx8OuDqDNvl4rEAcuCsPvn7g6jy+eSTwP/4p+8eWkDkyET6y/liD2tQnb+y0LafUhSl2vtPGUfKnD3rdeT/xQW+cPnb0Yfbuv+/Di4+MnU2D4JRWu/HfLqPUJVmz2Q2njnLd34Gbqlq6aa4dzbMPU2FV4I5+9NTfYddctbt2TMixiXHC03CszG2+8SpbbZs71ygg4XLc142de8piWq6Zk8eK9Mdl7qPL6v6zY1v9dkmpPSfwtwflsbJ1+J27372dI+54Jp+K874Y5XGLvnrKj3HCbXA0Lj9q40dbUvt7iV930uzaYXOq67/ivyMhfmag7zpsK23PeU/2FY6QjQl+vBqc8UTsunrfhbWZomTdr/jC46HjRCTeVerdpSFzbtafJUVpvY7JOlD0P+ZxOZdp4mCv6KzT+pKz/3Bh2OjU/ZzXGwnrwilhcd/fofIbzmh+51fny2v06Vd7PqpTAQI+F8fFGbTP2qvX20ui/MwU/BjAcDqv3QOzH2xaPqcHwqFmfKxub+b+3nidKuqciRU4POVxMZT6+iXyYmDwNd0T9rOWHY6HrUi0e/7SlivhTLcvWx172uo87Pzd6+TUPhMrcLA1euHo5E2z6OJxsgO7cKNfzh0lPDBW7/vbn538O9/hRxu8G+bWmW7fvU67qtdFBVxQNcLNPXPFsbIP9t61fbJOzOVz2azW+YjrfEcfB31unp861d3+JpXhPrHtf+38uF58raLl4gnE2/n/YeBccV3de4dxQ5tHozZ6UD+9n1ngDM3XHDi/rFQfBWUfy5/9jrqDuZ+7PnbAYTdcqA5zHSfPRqdeq8Pe0R3fxcJPX9ex8fS0v3a6X0r/dIs98XLX1Uz8bDrqa4/hPfQUz6vx8Uy2tmvG4q8eifevMKXewsSr8iT82HF4HRdP7V7cMKdP/8ThbP6KUhUvwceD2XzOjXhyRJ5XdDZ64rsTYH8LJy9tZF8xKy9qOo+MsXtdRp5Psbsxng7n5fk15hes1New8nzK10NP8mlW3Ogw+4li8hIzD0J+jkfmdSrQtaewf/iwPMfLzQCBo4DaK5bw/QjhC+lvfva4PM3NMzvyt43NcRTL+wtVykvcPLnZfYIXJ3fhN7vl96mWF/l5ahU/LfdOHMQ5tbP421TM8xw964G/tE1ytBN2hY51437li183M3Vrs/9YhO62r67eIfkFOvzdTD3chD+7rFzonN+2MN3J0I2dzFOrynXJj3/FqnSamWe3JM6sRkdneK914f3sxeg0Iy+HFe2z4eRh5EsOqN+yAJ1h5MW98AMBt334/sx+7gV3yQ+7trh41UJ+3cp7Vtr+CvP8E4y8STG8pAj8FjXobk6e0ycPfuyx5dZf9rpdQh7z8WzepgeVJH756/Y1+ypbfP+6LvXYL+Xk9TlhL5snF1ec38zIK03wa1fwNy8v+yCuXsT/FGb+/de//ss//ue//z//5z/+7Z/IXtPiXyGUv//6Z2j4d8pjAegAeSkJYBjIrU4QFwN9NIAAUBtin/8ZI0AbIwIkgB5bB8gGcggAxUCxnwZIf/U6MoBR03syYOT00UFONHIGCY1Gzog4a4KcBwAFpSv+mYycUTt+Ohk5o4cBYOSEpdYolGaza267Z3GpQ1+bz2JNQWj2dxzRfxIbW0vJQohmT8lRm4NjISV8lusSRNdEJfDtC1HOWa0BEg87Ua+O5m/WYHTqWeupiD9Y+UNwbiGcmkwBwiFwcoWcDL12/+Qc1J18AZrDvI8iVPtfYcS8CM23j2Jtn2+fz+Ky+K/MZ5NlThlQIAdniwxxJM3WGhI/A1FrkVwCiis/61xnMvkChBjiXh1h9pW26FcmaurbhUhtGIZ6WLooA0rd2wekFlUijok5Eg31JXHEAo2ldw5mQzU7wgbS4HBOoDqF4MN+tigFzZUEnqXZgUVoTNSKnk3Op6CJZGMwTcboDbP/Ukzso4S+TTEXR/AgcGQBzbfHln3SIYR8if7J+Xa8Xmi+fT5MRB0ri8bgRBBvGmfZUMlBfDGk+VqJ1jlqaDSNVqC6zlJDydo3+8jQOk8NVWvt7FtD6k1Ijb9mf8XOuQk0UglCky8jjyQ0hfUShgmCSXWZUpocxLOJ4hI1zgxxVuHtE6VlkQAxFIpmo6E4NI8MFY7rStTLIr4AjeE8myiHJF4bijbKI98+4UKRaagOSVNDnNNT0KJFk9VVaD6rwXpaz2qtjqaJMOVmFpoyuXFuBuN8bqn6b05LohVr7ZTrQCM3iXxDHJFzMTA0FlAdClBZyM9go6BMIdKEpkYdOV6A0kSxO8oT5ejfKxO1ZQhNPTGSg0BtojEc9b9K4WzUszrWX5nfa3n95PzNRhmi93UtVaSlUyaLzs75pzb0sbRd+wbl9URzdpTBGQc0aRmccXPBN1SXIjQpG81kJJ9N2eZvMEQpzN+siTOA76ttylPxGqJQlFXQWZv6oaENtYuyhvbNycF+6EQpapWenJhynXR2cKn2apIP/Q7UspbwDuE+xb5G3aRzBM7biSado6yjZy5Ji2TBQsS+xdgFohTGuAbSfEhE7E3Mh4lCSD5XgHLQMm9IEqURSQXoRJIopCWSu4m0RC38pCUWX28N1XX2A80uA+LbU1gM8e0pGScS356kF/DtWXrBtKrnUo1hLjSX8aJVbSIoA1rVJoIgrqHzN4Fa9fcZIucnLUBdcikTUSZPqg2t658h9spsrSFJsMXQWIJklqFVSzA0hR25ayhHSTBQPVpoQlgGVuVrQPBrFS2GYolaAwamktbGiSYTk9b+iTBEORvxKxNVrWONKMUgyoDIF1AN1LLWB0PiUjA0h520LkM7LgHl7BwEEpdIWaMuBc5PNHmd1StAwdZw9BgQx3UaRJRZ0+QyRJk19X6gQcmuZ4M8y0b1lJc2IvlsChSb70CTnIUjEmiSupC72fpv6g/GCSAot9S6gOYrA3UUINyg1M82A0ZgP2SbAUNKeo5ElOw5EVG7yDYbR2Gv5EKU696zxtGTs72vcfQAoSs434HQFeKLjd0xB2gQylC2k6PZ9s71D2jyRep/th4bvbBF+Nj8Ez+L7ZnKAshFdjCn//wiobidzTobo7bBBsvYm/KI7BBOporl1RjkQphXI7FS+RyOR9IEJ3azoDmO1vxUHUsgFMcSCdmxVF2nL0qxcXrmipM5AYTZgYbRhqKhDYzfqLEPx3hHpXJiGDRMadL2n3eJxOCYQzBFYSn7Rp9hCcbsuJDerN8fEpXFMQej8cMwVRLj18RBBoPhDBxdZApTTTH+G2b/GLbvU8zkxXGzKWv9aXik5P0NLCPExoPh6MNVmP2Vi+Nqqleuer9Mk+z0Bqr5uevzkf2Vnf5YTZiuzxNViPV5pqg3DJtT5o1hmN6Z/Zs13oIMHsMLTHOqqXIFTxvS5BdgB8zJnzZAzVSTbhNyaQYEaYVrMyAoqyH5T5kxrNlqKxts4+AQZNXqzR6gqlJhzbZ8Tp5pTnbCKePtw4NQdlNZBIfRXEx8ze5hB5UoyP4pCSoKjPI0BPE0kPv+YdlLE4KqKDMIMAByNQGEfZ9ofhdbWCfkegIIC1+mEGAF5BpSbO2BaLZeAARVspQmVegU6LqC1mfThI0iy/o45uZNtDEQMzuiaA7GEtL6vACzKwyDmEJjYfe85+DvM+/EYqK8ZMfJ+r0Ux61ZUyvfPy0fWzJKc5xy2X8eGnlRSX8KYqRk3lzO6/ocOxZRrGRQQIoc0oCokjEbUwUbIHWYYqszpgtZWW2MpsSVp5g2MuG0oQRBWBIjTY+ZkOMbEGQlsbXaaEgyxoopfBNyQBcO2TSoehcO2dm/JjbLEKRKXRdBaimVQzYHKiYT4kU5cKxUjsIsQ6hyFM6hYCtk5SichmZyiBO8iati5SicpmbxXzbIWQcIx2PiwlhNJZmQWnI1bWkKFk5CQCSgydRDAEFzpqJc5WCeBqhNnap1Z5qgYTgGnfPx2H/eOE2r1i18PJNwYfLTMH5/6mT2Ps2JLEOrZsc0YSY28mRrVdMAptATS009mEKPi1w1XXPCzpaaujch+72aljoVJ86eairshOIwhxBM0UUwAoqlHEJFhlzlECqy5CqHUJEpBwiqUrFpCwiqUjc1t0KPnbBwIavmQ5gN4LpUzZybMJu/CN8FnKafPwWRsu/w1CCNTX86V9iy91NDvOqCnF/VbEO4nkzRqmYcTkjBDWieRQ2hbi2qMZrFVDkpakyjE4IbNaq9XBKqjK3KBWPqJcGfIiOuzK06rBemCZAdgqpEw6Ka4j0hF4zKZawm6hSVi1zNVCHwYYPUCPzpNKLL3lOZVnXwp6Zthe+2RZBSvnEGTXPDBGej0K8yoVoyIttc24tgBiQ3mplpE3JCAUZAcgMwAI7qTyfrpglr3GhcxqYNmx12QOpcgA2w+nvRZU1mVzOnUphmUPenoCqlqvdibLTEUdfMbzZhG/4iUJW49Dbz/k0pzzndzDUYWqXqYk78CaeVGMgrQrFuESzeKYSdk64T9sUHP6FEYhWkOQo4mz97jKKiCfao4Q3ndw8aKlWw2loGWAFH1gw1b3jU2Gh8miS06Dnv2sKoQ5D+lrYIdvYvnfVzaSGvoqCYkwQpr1oW5AydNBBywWwislLzaGrCoBhpar4smEbmdO1YNPPGTUgnRjOHUZi2ZnYYAZO/CL0/tL/RKFXGopFjjssJqZoDTqpG4LRqFF/T/jS2Nwq3ITupUfQN7ZW0IkgtvBUb3iPQqAEEkXExqQIIImUyNfN9YHfB3LeN8nlMg0k0Y9KNaS91QRCp3ZXWCAs1DUB8uFIfaFRwR8smvdsQ5OZRowiafLWnfRFkL/QgyF7wpzJrulnqcZHV0c1UjzAyo2AApL7VzVifkCpsN+VhQlqE3ZxuE05eCmIHRZsfgBUwZf3y7LIJc/enGbB6EzqoijTvAUHV5I2a0EHVVNv8u6AqccR2m78T0g0DCKpS9xY1UJWp4XSbGhMm50YDVbJhAEGVTBhAUJW5HnXbmZvNoygABFXaYGnD4BSLFDJdkJxsTZCrJKaGQXKy2T5hhK2V955Gupn8aWQD16e9972nicuEP5W67+/V/kaz3bGp7S6LqJqiA1tZnL+2WzahZmjT1pYaqD0x6eLwBOPDUsXhCjY4zIUHX3DHRhiVKjiDDbKB5g0GpGvAHMCAFF+Ak6qpddqKMyFojtreAAyAcei7aD5GWRdMgNSvADMg9WrAAjiG/9QcDFOfsgnbzRE+IdWtbp7wCanBAoIq7XN0W3Em7N4iDNGYqX50W62m/IneIgxvqBsiEoM/Fi643bZa4AUmr7JNnGlzRNGMaTUnyuIQVFWKzW7OrgkpNrt5uybkEtPN3TWh5r75uyak2Ozm8IJyGUXkAFUtVX8KqrTP0s3nNWFzttvTLkFRCIfmr+mxs8eq7bz0Ksg1pZvdNK1XGl0dzjNATboGhXl2r7q7mXodobgOfllYXerPZUr416G8AnZBeh9757u01dI7KWl0ifROOifjs2AB1KA1L8OEElmmFk6oQWtK4xxZXGS7qZQT0p7vpnBOmKK+i16bJpu/CH2atAXTTZWFhR5FJMZDXrjmdlODJ5TIMiV5Qm6fAiK6RuYGYASkCt1tQxP2g6193fbOJqQHrpuqA1dAFM0Y/1mmSret+wnFd9NAJqSpAgiqZKoAgqpIExUQVE1TRURizmaZKp2icqpQTURivmeZKravA5gX9SBkxbQrnc8QStOudD7bjnuiTxAQVE371n6ZO+nTkOwiw+BItr/Um6DGYRVsXOyKoDjJ7fSy0HvVkyDVwq7N/UWcDIJc3CEqcbRA+0y2pQZITtp+24SBZjWEsEEydvKckEJp8pyQomPynFDSPhFquwmBLQapnQIiPjAnV84NFon3TNikFhZBrqqAOL7buaqCKoMpOJEI49RWD5pgUEtSN1gXNWEI0o4DcwxqMATCoMEQBaXqJMFVkBJS4USXGdRgIFVVlktvgrvBYFCSU1RNGxBkDFEVhxmqQ1QlKpxDVCVyEnByYxrE5kgd5HMt3Ewb7IVaKDcGh0qt9CYMBolU7TcNRpDUqVFGwQZITWBwVZ0Go22JAg5A6urYGANVbfHvYpZV7ToNCyaakGJkWJDQhOyyYTEZsIerQ5DRKTds+w+Qri3bxwPkaLftOUC6bgcXytq5Rze4UE4aiz8FGdqNGmb0TRhNqRhmEmK9sg4dZjBOyNE+zJyckCNnmLE5IUfOMFN0Spy4kjEAuQQPM2Mn5MixzStArrnDTGDs8Vd/Oqlq2t4aZj5PyJEzzLiekEsMYAWk5WJbSoBcggFBVVycz1ie5gocRbMtZnOiLNUx6JIVaBiEJYaPrM9l+I0sXLJpcKMIV85Tw/i+7LdRHWt8NcdUvUbV95t6y39PG1qjiN4unhc2Z5peep01VubhKGTFoOgeVBLaqCYzBzWKaeLY7uagNtIGJ++grtJlLY4imLq/NwKWKLLQm13Go+1aAlLtGWbxWTyVww5ILWhYnAam/eKwAtLjP8zwhGMoOkSIlbbUbBMZkCJ0mEk7Ic3DYQbvhNT5h5nDE1IVGWYszxWRnuxhpvSE1J+HGdpYL8lnM8MnpEtymK/AYsJEFRbfnqmKAIKquaBW9jEmfs8U7IMGQldg36CB0DNVkUEDYfa+ea0HDYReqLkNGgh92rD6KQirXujNAwRVpa3ftVA1LqCD61qvVPMAQZUiAwFBlfYGB2X1FEZkO8JVAFsQkQVUVQ3SCUFVZdwmIKhq0flsULuAs38JNVSy4DpbAceyynnCmJa9p7G7SCmzvSNJdE+IEDuZpYCzvWN2StCLImBaX2Qw57r3tNDTDiIBtWWFJhjUJJwNDICag51QO1iIZMeHm4J2EPtrWCFZwDh1oWA8wzhyrp0nw4gN1NaTYocn1ZQghhEdr80nwx2Y64rhAczFH7iidFHiMDCMQw3ajzKMwtjagYK4qaBvKm0rBn2ZWqlh0JcVggUM+jJ73zDo0w6UYdBXGLpjGPQpHBAYUYyLjDGIQ+KehmPQqxDB9XktDJ9aImMiZfhi+4k4iZ9ZWKFRS2ZEZeSkMYx4y+j8yozGTM6fbO0N2mUynIGdX6YCTlzqiiOwAtaAA7Dzy3Sm+WtcgIHRX9MGaMkx6NNmk2GLB3V+2YyZuK/tw3gJ2fmTbTwFWcLgB8bb1Ng0Hm3kT1zU/oThC2OQgWvAoK+M4jgPi0BdFscWkZo0PicGfU2Bdv68K9RuicJV7Q3Crfn4JaZJyDMLCQ5BTS7C5FOPcBXLhFqNiyCN4jmpAeMSmpQZEAqvlkPEvcZVicrFAmaT9ISMiFntpgEinDav+ok9LVJIsqCEWBEcWYuUwRp9ObAXVemuXVDLX9eHhwteg21py953FcboT2Uv+4uG1NFqNKdFEo9nMdOyaho4j5BkivpTmaLr0+LrrJ0iAWf3ntbJnb3vVvaov7dpJW2C6jLSnBS3qBZhu6buuJEUq4jBYFBr5bAuS30nGg0PKtw2tCqCkH1qRWGfWkm4lHWoIhV7cNGThSnnsVNO7FOrEk/dkVODo3Ea3RLFXZi2cVg4lnOia8TwnEpzycn+HFMtl0WiddhUzIXe7oDUvIYTp3KwpWRiTa3ApSQrhtAwMpmXQdERuJRMTaOsv4f2V4nKwKUEu2Hr74PeaQytz0Fv5XwChujKTXGlgUtJbpxghkFfy9XfD9GYFcdoGPQ1Rb4Cgz5FNhoGfT3svg/6FN1oGPTJzA2BS8s08wN/Lwqrv4JZb6nIiDQ8f78kKv+GC3Ci6A5mGsLRwQjpYJbjxNzINhyBJeqC2Z2p5Lh7/wJMYwH0gX9TPVN/BePvtBXS+hz05SF+m7ozJ5UikYFBX2F8umHQV/LaPvR/KXX3fdBXvL+ijZ9Svb+ijbeiXTpgjMeC/VDHoK96f0Ub76V6/3A+FUVVGgZ9zfuHz6v22+Y0JI7cYDTcgLkVEkIWHqKP860mH/9V2PnViPOi/qL4n8a63l+tPbUWqiI4h7nYIQH1tylus7O1tANH4CB+N+Pn/Hj33we/p43eguMC7OPNVFt4wsqKQV9TPDpwt2MJaf0+6FMcIzDGS9VJJsN2jEHyKJiOnmr38WwqfKoKZzQM+gb9S4ZB35BqFMw+mLj29Tno0/6g4UlfW3Q+IJjxMXHU9ztx8fk2iGVChLgIi18xCDO4wjB+v9MFYBjlGLVRZrgCS9WIVC2nNcvxF6l6NgUY2vtwNnSaEsMx6m7LeDCMY7gKIAQGv7usCfwe+qMrYNBwA9bJhmin9iam8W+4ALfu7cV46IGbk4YTcCpOD8ZT1+6d4QCs8Qu8AEs+x8XGa9cGnmHQF1Ny+jDepz3bnR+YT9OgDd4ezL8ue8gw6EtcTKeBTZyl+keqLdNepPyKWViqKjB+z00VYLyvef9wvk5poP6hgtW7VEFgtKdr/k9s7e3eP4X86N4/hfwajEcwnOzgTlwx6BslrBj0De+fwv4aksfAk75pPXI9Be7AkifAA9j7x/yVE3v/mJN1Yp0LieaDTXM4Fqcf829Ov+H0YHwO7SIaLsC5r5+vwDI9gEGfTmwZBn0x9PU56Is6m4HjZcBu2gHj85mOvhA5Xyd7ND+6sEytyPk6qvfn4PcrtdjAA3nTLvD+6KSv88yaYdDfvT8629d9vnS2v0ufiZ386d4fnfwb3h+d/B3eH538n3qy02P9M7w/OvtPO56GkSNw8f5gXsHFTT/gAuz90TF+YOgGp78gK8LS6vocR1QXnz9myedFJ8AMI0Nk0HoZh7BM92j6HPaJNB903jpIvwEGvXFRe0x/nZjn6QyDXgU/Gga905BYfx/0KvzRMOidpun6HPS6aQoM+lKOzl/DxeVtE/b5XIndtItFOI3k89mwz+ck7PNZ5WGr9KuojJpTnKzyzbDrW6KvFZ6fCqKvaT4HdPZs0iL5FZqw+sOfB1/v9Xzaqmn/uXYuDc9XhMQwZfu84byun5ZEKfn624lli4HeBFPHz5DhOXBzfUTPh4+XRTiv6xkxzRfwi9jHRxLuq/wERryHyxNiHx9V2OVBE27r/DQcfH0ewtwzC2kRps03+cLMFjL6gmckD9w5CJ7eNIo/wEjPkOjDt88b1ngCxhBFdKuwFTzV9p9hFC7LdNwbxt9U+NfnCVjyKNn6lbFxl/V+YlrShnEos2g9T6Z/Tqz+S6a/ZmyiOb2WXmKqh8O/D6zASsOgv0o+JXOFZGwsLY5Bf1X/J3OlZGymRMegv3E72zDobXJFJXNY5uhn9lJihiQ/tQcMejujcAyDPj+5Bwz6/LRest04HEpd31dBX+eeoWHQN2RPJduenDjp8+bKytHP7SVzdWFbWPw1VxiOuYof5iqbWPImmSstJ23CGobIWsrKT8M6RQfcJr1JoZvr87zrX/u9Qm+HYbxPO6SGQU9hTI9hO1yr9TrZpvDEu/5Ae1Nd1vELfiS3f8UvHEMre/yE+df2+A0DoO71R3J7WP2V/ESm+jM1rRfq79R2/YXxkJpOVGq8JLd/NZ6gMK4Y9HXul/t4TF2uy2T228Q6U6vxOxWY4PzC+E7D+8vsr5x0ss8w6B27+U8s/SKZfYco8LDO52wHlsf6HFNykX2j+ZyXvPs8VFKdD3R5MLs7OYa8yG6vS55k7T27PMrB5Q3laVagrGE7Mu3yxxJsZAjAFYO+6PIn/GVNiDv5YzhpvfLn03wP+9/Xhm5gxuc8tXHpE0PY198u7PZQE3Z9uwq34PoeMXcdgrI2Ze3N2vpgWK50rB+GtX5F0TcVOrdHiMXfIPqG24+kr0DB8fXPsFzDwRIY5RIkf/F54NX+hnN14uz+Ivj1gd3f0P8ylaak6O+DClKK+w8acRU//flq3w5inc9De4hFHxKYG9Z4iFGYO5jgF/HOXgGer8uuzxO7vVKFdWI8NmHXb0hfDW4/DuEYm8YDsa8/QXi3/hDXdf4Zjrv1BipZ1b6h4Qq8k/dQyWreyQ8zybL0Tfw+cFnW8Wyf1zad0WPY5+tC3OQPRXsMa77GLqz1HvwwrPEEfuF9Tf3FCn25du4+B1bvm9jtfdHby2qPEmv+skbAxD36+DXs9j/Gg+GdP4K4RNfniN1fR/raInskkL6mA3nrc7efgSES5wgsjqEyuL3MnPlzcXB/UBGWvQxsiRF66fvP687fQ6z5C3qBEQnuGN/vTn833N1+RfuBV/tzEXb7Mwi7vyAKa36C/4bd/szEUf7AWIRdf63Crr82YbdvRF/c2TfEvn6JvuTyUvQl6bNJ9KW86iOGYZEJw0TovhUIDJe6b/0Bw+Wu/CCG4QJUNC1+33B3fS8Iy7+IE63Aq/05hKVfxC6cu7ef2O3PKuz+G0trNpVD999kYbdHk7D3RxSuq/wmdn8A6ZvDX/7QIbyzf4jlH0ABxALs/lrSN6LL1ya8848SS7/z7ye3lxpxDtnHJ1y0w7cSWZV84rqjByqHbx0CQyWpu/lruO/8YcTuXwnCzs9IPHy8J2Hpz+Cv4dzdX0Ps/KzCbs/j3OVUmZYluz+B2Mf7EM7V5SOxy9cg3FZ7JEAFC8uqvxpOYbUHAlxe2fWTKCz+AJeJS5T+Z/0Pb3/3+QIX9OIZTIDhgihuHy3CsreB4ZKorj/b/JvY/TUINQcuqz0Y0Z46DXqfz6C/ar5Ek6/Qvlf/ElxmS/P5gmgvYO1vcP2c2P01CM4A3s2PBHr6zn+WQE/3+YHdSmCfH6b/TOz9aTVPJla+FWDQMyfU+hz0rPa36YsTu/wyfXJil1+mb07s9rb5K0pQ3K9hpKhZ3D+zCKfVv4w0kGFh0IhhqOiL7CfgDNx2/g7Dvp6Yv6cEt8+D+YNKcPscws6wrzcd+vrEefU/IHF2CLv5mrFlGHz/AY2fOO7tNxiO6/pX0J7o/g/zv07s/g/zv07s+mMWdv3MQgtK8Fw1weybEjw/TTD7BzkidvtPwH1v/wntcXuW+18Ing3r/mS3FEEKNRjCWm+A0d51/9f8hyWs+78WxFSQf4H7sRX23sTqn8VieSf2UI8i7KEx5s8tcdkL/TAcOF+WJCz7aTH7c2L1zxKF90JlEDq0eCiI7X9M3EtxDBdk8NCOSBw9dMP2U+Dt0H6yhcpM40X6EHAArmtoEHFfQ4MatgTdvl7seA2kD6PJhiCnPyBap91nQDRGxvSwE28FMbf+XTSlaht/WIKjWBWZ1Qm1UTwszn1C7epbFPyECsSwGPkSZQWPQThCEVVIwJRkw40uqPAxi74vSRbcsNj8CelgHBa5jzO30d87mzC1j+QfxoZfXhtosChsYVgyKWwOOxyAa3sHNn+K2ouUKhN6TPFAdKSZTeKkBaNpm3XYYbKSfBd1WYTDGqCF+LM01cr+4fkuoMuee2zwEFS4r52HLdgUWfzTaJbblMBo1/BwLOZenTZ/FfcJq3OBAWtuUiqzaMnLbkzyhiJqmXLEbmhXfUl+Q8vQYjG4JSvGFtOMuK0RVfyC253+gSw9Cxiu/SI9aLGEMBNrH3uxozMFhmZ3jK0kt0P9eQ3rvDeMA4ySE8TeRjs6WLDwZ8fYilrkx1q6cNW87/x80rrm31fI5/r94nJKz6uHmPH9xe0e0Vd8n1P016UWj1OxoMvkIYSWNqFMO7K6XLT8utlDBLtSkmYfUurmulo66uaqHB4Q1bpR11gE3RjNb9hLxi64wojwzbsQSaRv3oXIRoxdMAWY0pZlDW7A86mLdKfAsG+2+XM3bkIi9jjHQCY1j2sMHAQt+2JahNsabGC4+OKn525s+HM3NgI7qemw3vq8p5r3n3dtHgc7hFW6GweBndR9sypY6ouJ6/o+xnZ64GJgbPi80VcKMNugHXbfL+EnqhweofsNnnE2jdxueGq/uPgN38Nb9KOtDd905o3uWqDGxlh8W4wiZLjaHC19xMRuJlp2iYJMmL4tAs6M4Nsceu7JAP15jMPVNsOuRkf29NARM6iNhmdP+TYvsbeRPTHablvXcN9ty9r7VzWykP4hsyAWtm/dtiliwKpHMkofZzxjWm8gGCgwC6RZJnYjpZ1pwhs9rran3chh3Uw0BWPx3RO7gU8UD0cYem1z+5/TeN6oq4Ny4FxNU0LGaBkOplTcGUAW6e3JAKMtHhPX3efxfPiGk567wpQsX9nkuzt0rSdr8IA45l2buK8Ocft88g2exOdZMsM/vzr8TYYgAcXqYEZ7wurgpxDC4YpU1hsBN9LeDYRtLXHdI9ANJcxUWq0akaPFbmTdwGnh9RMIPfG4rZR1o+w2FnRjt5MAQucKt+6MEdfVM03sOwG2KtfonmZ1FA617HcEVCa3ZIm1ExBtBZnYLZEq7JEfhXhKHx+xA8ENq+c6C8tz6M+HewL13D3JnHI1LXE3hQy7Z5afT8vOE2vPQ1knCLEsW38ey44+4LSL5LDfS7tIDXvunmi1N3kklviRPBIrduKedxPDsBYvDeTkkVVp4fs8smp9vttpAM6rJ5cDH658t9QNl72JgmCJqkgyfz5Xp7zXv3n1vHZh97w2Yed/Je6ijyJsYveUZ2HvjyRcV0vZ6OlMOCORjQN0i1u+xDG5J5JYsih04RJ95SHeWwwNe+SlidRaPBIrZGGJ6JCEfec9Crsnl/ydprsWb/IPpjY1pC6cyqqh4ftBnjxqSBMzyFwa41R3tPguSbiuSiyxltqF/V2UH8G0ZsMKLR+C3PgYXVAnUZqgjvhUwTVMG0NtatS2KT2KoE78ZMG06Lwa4XqIkJAOvREIC/drh+WlmFCGwiKoM7KDsK6HgA3qFAtOdRvUuc8qqFO/RZCOex5kr0VxYT0JUgvpUTDx4LKoUtLsLqoUE9ZE1VQW7Mgsqaqe48EUZxzdLErMQKikDkWQ63yjTJkSl2dkufZU5X9jwp4JlQGCA6wGnhllsh/sAxeleyHkcGCSoUm/ElJxsNXI3q9VcJePyiAHA7NVTcjer1woqhJu1yhIy4CZt2pVAJgyb03c7DxUGY67BdiWLuzJ5ZpjZZejBlmrzu4UqSlVOq9lf8Mee2VKeGWHm5ijQNnjJuYwKMGyy8EmsKaWRbhx5TQMRjUlJh2OKUYsQyF400paMyYCS4e2jIrActhbxkXD7H/LyGi4Mt1nckyHvWV0NMwhYBkfEeMmHdwyQhpWAv7hmMtK6o45DCwDJVLTSwO3jJXActhbRkvDSiGeHXMomJpgWEn9o2NKBsuwCexJxxfHVANMvzOsFOzIhYgY+dTM0MXnDUND0+8FyxHNDTi8z2LiFcBlGDEWjRrl+rwzwBr0EisDaXZMMZqK8GDqevCDOJmYBL+Is/UH+EnczAEGfhN329BCf1gM/7JwvATHiRkyo2PyMyfHNHxzdszJlUVfl8M+V8fkX26OmQo4d8fFAjgNIyZl6oZl/3khf9fnRcl1m/CcYcVxtgzatgzZ+w03Jtm0DJvAyhAKjBhPOciNfsPkn7UPMaGyM6z9hpPp68Yfw8oQGhyrPYvjxrTIQ1gOaesfw2yv9Z9hOjxTdayMscUxbejk9MkhnZw+nRuy8YWY1cik3jY+DTMg3sav4VJ8vBNXlkbojkdT4m/GqCeVsOjCUxx0x4iBrdmLNzBmvS38fWD4WHX6yPaH8LwX61/bHwIe4tfiWCUlguG2ICJV7SMWv5LjvGbAJeZZQfCPmAEg4C+x+NWElQQ9dcfi13BMfuXFcU9K7yw8VnloOJJf2elDBJ/GFzHVxvV5ptplGD7owg0gwziOWTR+o/BcGRfHiPnSgSLDSC7UyG+jz7Ay3gIX5IOnX8raAzzE3+GYBzaMH4aZ49P4hd8b4m91LPlYHIu/mRgpnn28EjNAIon+oMwZ1t+GyV8bD4Y1Hoe+H3bjkZj8jc1x2Y034Eg1O6o9iCdJjpFeKvOAgWEcH4dd4L+HXC5FpUOALRN+S2P/ucxI+75hmolGL3DXfF8c5zXDtL0PAs75YVj8TI554MP4ie8P8bM4ZkoI6w/DGr9On7zNyekb5GcSfXHReF0ck5/2HFn6E80AYKvmk9ak+sJar4BR4CcvpTtGWo6c1/cRV0/YbzGLU3WLy/7nK+Vpao6pf6F9hpvGa3Es/mbHmeMnOdZ4jY7F3+B4rBm7jZ6uogjDMfkbnb5eVvlGzIyl0enrHL/R6Rsar04fMurtPcdh1cVxAOb6ZRh7SlGFbKpw2pOvVikh8X3q/5RUskb9nzLXdx/fU3p7zSYb/zhBt45flHsq5LfPH9hZxTEK3yijuf2+4b6uB4aVmsDna2qav/57XfPVPy8z2H4P/JgLxkoPyjstaUfP5E+WWar1ZWKtP+tz8Wvo+0pwt/5e2aMXn29lf32aeKpsu/Vt4rHKB5Tdyp36idbLiXfyBfW6pvofnb8oMZIVgGQYGduQrHDvOSIYvP3Eot+fB+pz6/cz5XGkfjQfajxSf5p4tJUeVMSoKjrVTX+duAcfH4ab2kP9Gy753feRXzBxfkgfbohv8Plhn2/UxyL1eyRbSf75BXk+FXCSaL9MLHuAGeMnljxgRvw5+Ll+KaP+xPSfJrrKJ85lL0M/ZnN3/dxwkDyhPTcxAw6SOdKnri3xYU6YCcNegYsJVZdjCOYkWW45S3T2IdquYIMokiYzQHjUwGK9j6bAlWj7lQ3zWssY6pQ0JZaOttc54dqJhKtOhoInmDOaQh1UpXVGdVCV16piHVTldT20FIBZw9uSE0+o0d2w1TvhOhg6qEK4EWWJZZos9FhFS98yYSJzLH0L4uqrP8Wdonlr6VuQMDk7zFZthaPI0rdMGKOEICFtUlufAQtFDtQPQGbp4wmuCQcngB3wgrZfHYIqJCMSBBmyDniarDUZB9HySkyguWZpJbDbRNXAsko01C+LgiCjS9BaTglUjHGqADv0NDaQUAXjGmFYhRBh8MFAGL0WG6E0riCoBSsSRqaP8qdS15MdvwSNw7+LfL05dU2egk7M7JRkZy/b1MTXF+FpHW4pGFTkfRyCyQchoaqmNUFJqSq4Yw7gbEMT6wxqSUqCnNExCspAEFVd65WoGpze0Y7XtwF1UxCBEEGLNTk5pMvz7OqEmmV2tHVCTSs7+Qo72+stVavQo3XbztW3oWTb0Y7VT6hp5U81rfwp5xHsI2RC8jMZg08TeyF0PlXECA8UT0h5CtvOoDakKr+b2WWh8Cn20fc+LFvBn3quAT1t5LP/1Ajri9AE6fHB4nhwrNeYEyzMB1sb608FwOg1yZCDf5HOzTPYE5IbPKKNTVk69u0E95QS8oPbUEE+8ORPUYMrcsTyeDhyT0eHoErJthlsNaE4abFYffFcAE2Qk93C+v7CQeSiXiDU7F4EqaZaTB8gWWchfYAa3kmQe348g9sXhaZYPB9g8gJOhBreoqqyUBnPN064Ll8Gm1arIEjXdLK8LtA8kiY7SnuEZa2waDBKig5BjpzYCZPkZBOk2y9WwVUCE0psZkH6/CygG1BiMxJmaaSiKq+MBc3TvuJAGoIak11wLapG2L3+mkEl+Q5FkIy1OFlASoaQ+bRlnxqEyhnkT7VbpRZ1HVtUe5UCDhCJzTqFasiEfibEjnCjBlfyX7aSXJrslpmmxxh9pqDKU1T6tNAEdRy3C2qIDn13uMwxmHaMNSjGRn435bVTDFa3Mwg1YougBLKo0qmIKKp2NgJh9lqdhBTIaRGkAYxBCCj7wZ9KvU8WzmnR6tL+UA8rTZW77T0N9CWtT7ObYiilhTxFWp4MqrQPDg6hvKJScePcEGBeXC0lTG5FEGaLUo5NkDYmz+hO2NfBD1hk8WRBVW5LglIRI3+qSB0XVUU6oahSiR8ezp2QujTP5vbZY8nHhsEcJDYJuVoFUVWpEwZR5SnQMl/kx/5xpH/Cob0/ZHgA1JpiMb04EqAzbQj5nVCrRiaM7N9Q+OGkuIsqqF3zJph9LSMkY3l4d0JpQYug+BwIs/gcBcVYEalYIp7bxak8V/MIm4svQlU5FFVFNryoKtIKRFVZPaKExQ0Ua772RwCR2bpXd99YQVMvfLogurrjOEDfPZ22nOt1BsPq+iRMrmwTVteuCZsX7DOIQFg13+BaJpZQwzsKMlSBZ9Y6ssMNsd0g2Y5OQa3TpDiFLriqAYQ6314EFcGVBbtOOwtqPEfCTD6HIKgBrKdFchIJ0ibUPgogsulXrftBUOs+kv8B+ikeBMVP2BVDI+ghL4RDC30WpNgMRZB9FKqg1qMmqPWoC2o9ssNpyJTVNLsJ1aFBUIprFJSjMAlKjyVVVWYqj831ql0TRv0jcWSWk4dQ8qoLah7ZkcBeZdLiBILB4Aa5fdhtOn23rO4JGH3VzbRB6JYXUt4CNjeHUSO0tuQWLgqIVjemOuFYHaOEaq+dC+tNcf881jZhcg2KUA1MgtwT4Jk2OESrMxYJTd2YCoJq7yIY1y4zSPdq6IJt7W6DOh1aCeOqFRBqPGdBpRZKghLIoioqp6KoShrtgU1oGnVIow7Yqj+dU6NBdRUMgDSXgh3xmlBqD2p3TCgLSE+7B9RHQT9/LLhqQYRS8oug5m8lRPk7McdgdP2ZUM0fgtXVPEIZU6LKrSdRhTQH6t8AmL2MNyHTJcYiKGOqCq7Vzw2m1f9GyPM+PI4D32uXX8Xeq9PpUU2oefVFIgRd9hEgktbX7gYvqvp2ZVbDU0CsOYKowaJtEx5jm0iWZrMPD9+0qITTepJTglAumiwoWzIJ8sQtDmwZ1PwNhEr4jUBRg/I+2umlCemiQSiqweKFiAklRUXkZKwkocFEVS0UflgWQciCxesr24ezgrMKm696r4BISAwHgH4ZsJNXgKjwoEzbgMhj3aW3N0GtOHa+bEKd54C7ElCZUPV0lJLXp+iSpMluUGmqwQ1ApakGrwxK1gVBuZUiYUpkexKUlz4Lqp5uIcxMAojuNkhNNYoqFX31p5XRR/60rqIegVfTHqwaV3ANqtIr/IJAO9cvStssY/+bQSej9bsIAF73xQBHXrcRUM12Nf8QLob6ij5ADdKQjtadE8res87GYYeR19E8UMHVP4w6EbIWAOHr9O0GkzgoW7Que1ZGd7cqgMiiVQGFKgC1lWaLBM6sRJcaoErxf7EQ+j4FcqoDalVA2m1AMdJWX5RLinJImUO2SrlELl7AXvUixB5OQefGMEJNUWlMQwGRqFNtcx8iqh3DW6+FDeWOQ6vuQ0S949C6+xBR8Dh0HoqIprVj4q/vtTrCkgum8Q8UdxeRKHqMoo8OQRUSdwmCKt9VMRt1wtSlTaKI7xQpUU3IoEqRhbT2J9S4MkN6arhLl6+2oL7SsnoKSgBc60xbIU+lc2O2McQi+LgqKNcS170qwuxSsiDnuY6O04c4UKzFXYoNcLVgC6hSYmr6WydMQYO/gkjk2RQEkan4HofVeVSwYLSUf5CZXbIbAWQxB5+BCC+Lbkmae27CdboiNC2quBKzhU2oIdoFmTwkWhGQKY1XLzAi5GLRELV6IhOusxvRdXPBrNqHQSheLOvuAgL1Yll3ZRDVF+tq4VgNLJmOyUpNQewPh3hvXXfYEU0YqzzIVsQK55fXAu5ofpM9H7DZMKGinaCmAHJMAqJTGsck1H2Dw7e2ERAZtXOWrBDniNo4S0mQOn1iieukbbFUBLXrXAUZdIZJBKhD/CwJj4gVttfK1gx4GQwOQdXgXgTX+AlC1eROhJGqmIprI4WxwSJI116ughxXmEQGKfpYsneupxR92faHUDnN2lsWQY6rEgSpELMa7oQcVyURynIuWZADqRTBuBZaNagirU2QKmDpghxXRVTl4XVFDZZdSVNQor3DKl5p67CyB1OjCVfVg02VAVnBPEHJ/dtCNgm5HlUrWDDFE5UP1u4cyOetcE8Ms6QkeCipaZBLNUt5DuQUtKddkDY5K2wOZPgzOAQ5rhpHexqqZxcEudK1aFMjLxxIrN6IbPc2cVi9cUJGMrYsyIEEiDDcheILJfkQhitfAMsIDhTpsqdNUIVp7GznhDSl2iCM7P3GuZ+VIoDV33Aee60ch9Bh2dWs/jahSkcFQUp+QLRIdjUgmiC7ukfB4UHGCOHFwAEZ3TJNTcguY0UzWNVeacuKijb2IKuhTUhtpNsu3YRcJrrtTMwuoUOq287EhAyBYg2jgRFqDbQMiBO2tToSiOxruSsryyvjHyVvDKpkm1XAmZA9OGx/aEIm/xy2e4RMicy5HQS54gzbSxtlWYvaECo83XatRvHM55ZWeEJKBpZRGEWuAZZRGEUliVlGAYuxF0jBfC9xrUQDaVAi5++wzYZRtOs8zAmObC3ZIU5KRmUR51JekurFmF97QuoqgKBKu87MqY5CJsMhqEqszwgIqjITGwGCKu06M135hKpEY87KUbTrPKppMgVpawRBpHadAUGkPBTDfHk4eWajDhBEal95FEL5IEYWVGb7RNjZ+yMKqrsjf1mhniMQDvWRZboa1XO5W6IrFDMzATXMpzYh89gM820NHOPKgji+ENQLkTByhQVVBrnCDuqTOIlT1CKDKihTCYsOkFfTRWulk2VY+gdU1Cxijh270T6rP1V5L5SeQQTDso4cQo2cJDiGxrNBJXYfQVAjx1xdE6pFgTCpU4Jp1007moA4PKMiXMNcIRNWrzNlR2e0hWkl5CbUFqYVmANUh0bCthYUIFSXZcLONdQq/f2FgqJZg5BwOGPtCM6yDn6DIXqCfTuQgyVWTw2KSFoxcGzrxIg9LWPRqDNYlWA/ClJJsNqjgKJ5EZQoWPjLXt9KT2VI+1MvWWXmPXK/eqfYoaCh+WupbWAaN/HZzrsGLsdW/fkv5IXtGpOEyc/AEK4VggjVZZVQG37+FNXC955qW27QWhzKQDZoS84B7ZWJYHgO5Qcfll4Gwb/dIc5j9LWP7GlfJ47B2WkSjISSG5bZJtgBUR3NEU4u3YVV08rs/4m9ZpI/D2lltj33ykeIJAJOq/iwsyFtnTB8rMxaI+jr44AcGV3DEt0Ac+21svOGNaesv8Kkbq3Tw7PHMryA7egxdrCF7bDqoj6zUQjMNRbYYqUWDWlUyQSeX6jr1AKWZDUfLHD1KmY8gxsleiyRmoX1erUx80CEwh1WK2UFXNdaZTxMW7mLCqFo9MlIoZAEVgUgM7OBd1LT6Gth/b7R1zSBzbQG1ppjtjUwbZNhxrXluQ2+NNr7Ow/SDLOngdcKaHa2JSiQj+VJgNcaaHY2ZhoKWcKHeGiCmY0NrDJoZmQDS8LY0gys/rKVG3i4tLazPFAKRJ+d9UEtYsk+OxuEksGuU9hBXFlMVCqAVYvLdA7g4e3h2aSwrpV2dinGtUqfnW2KcV0ehSW4TDsCppJK5Wli2RlI5WGf1xZd744pndfnOlK+Pm/SRf15Fz8X4UElcaVHmvxK71hXDmtPkvLOwlbAWiv9eaYfx7+PGkP7v5+Karfp/cmV48H2JteOu7BXnm2OpRBXx/SUdAsYA5YOnIVdCU6O2d89OmZ/s2oksNTi4HgtZC1MzYElKYFl25iiPLH2xVpzTPHG8pfAsmeKY/KPtTQDalx5tXphLgOsxAksm8ZqXgXzker4nzDN4zqEA3c8q9U1BaZvsHbHXOxrE9a2VzU/zcSJy181Rw3ySFEe1C6sTFx1CLfVEhQWf4Kwwv9adEy/QUuOV3uPWMVyW3EsflSYPJhs3TaXWJcaHgtukTTzFU2cuV40cxYBU/6w9jYwxxuLYOPYRFxNSDs7WChPmoVYAlOeNIvAnLhyfCE6sxjmeAKuhsWP7LgmmbfN6K8yla2SNLD4lYSlmjarmAm8M56tPQhNJz+FNd4i7GXgyprfwTG3YoGtfdJ1ga19yuTcFsd01dCyB6bqSMPf8ogVdwRY+5T5im4DYI1Hi7INFuPl2No31vEnTMUG2Nqrg0zwWQBXbczR4QFMJzP9IcBcL+k9mViVlGp0TF0IZiwx18u6CGuLrQzHOkjaHdPKKk04UT8p1THVo1Icc7wVqzcJTHlckmPKvxKFlfkKNioxx2NZHDNHUbYAaODqB/2EOR/pPAPmeKVvLSD/Dg/2ZcfcgsnJMdc3GKfE60Epvr9yPGanr3I8Zqev7rsFgdeDlMIKTG+OOf7Mjw/cuGmbnF9tPZgqzPmfvH+VqTp5/ypTdfL+VY6u5P3b14gm4eAHCYR5EDF6/6pyU/T+1UGv6P3bu+8QEA95o6PGozJXYxuVWJsii2PtHQ/HigXpxF6bNogenHzSvh6fl+QRpcSTn8vec0Swlb3f6x7tp/mHiBHtPxPr4JDtiRuWZ37R87ZrD7ECiqJjhWolx72IP8LDQ8eJVWkoiv7e1y1f4eIHNYS19TMcj572+reP1bMuvB4kFu4fxk9Xfoyk/huL3OnFMfVXH59whC1743csnM8+voe2D338j7D4QVZh+vd8/gyZRz6/sJdX/nZPebBEkXVvvkK92J/P2Cran++2K/O3e8uBueno8mHoYJTLj6FYSfrPgTnf6UCfGGUv/3YPOjDnO13owCm7PCPmfKcTHZj1Z+lFB+Z8pxsdm61c3+lHB+Z8pyMdmOtNcfqUSbo6fcpfUp0+bXZWya+h3c7q/NT+pq8PQzua1ftfW5q+nsA3NPbWm6GDWXSp4xgl56evX0Mbl77ejcZ69nU4pv3l6+XodABifSWOrt/Z+jq0XYn13M7ma4OyJcccv3SmTyx7Gf5yYuk/mev9GFyf4EAn5n4CsPFH9nOrjrk+tUr9YoxVX+p2dl/2NF3uwex70z+6Y45vOt2B5cFeHEt/X0wfggOAruXomKGBwNjph0EufV6YW+7wlxfDdC/g0E42zPHeqb/BIAzyghNHBhl06nsw4IbsD2HKV5wYCoblrlhMnzQDS/6Kau1NS5Z/QJj6E+uwAyevcV6tfcjt+LfZn8LFXb7V2rfLuUKsiFzLx2J4V2eZWP5W6vdQ2GlP0x6AQr4rsmsNVFwuE0jZDc/pF8xEMR2bSQGj38hr4dRqTCi7SrLFWq2qxDiLphue3NEKXMFL4tkdq9/QQrfYzjRuKDJq6X7Dk47aYRnztKiA7/AbnvNxmDVuyysDxBbdaGFXKNL40xgdYJldeYMLiN0wjnltMdYqxA1PJp38xtBv2N7CvKE9WUs/yxuelYhuBywKSktU/IZXIKx+Q6cwWCIwQDSvmYt0o6xFAS2vwzLGftbdYLNp8RvNbsSwl8c32HizTyA4vtiNXeU3S94QPI6Z8fI4KO65kS1UHTcUChqtmAawV7Oyahq44eXfLNobN7gnFBiUMXH0AnD0K4UQd6ntLKnDfKISYBarjBue3K76DZ1ciLbHATKkyyA22EhPgWGLDBbGDUVYRQvbxY2yFhJjLhIlqrHKGUa6doktdbWRnj020Jz6uOHFScyPjxttzaFm4UrBK0+z3MG8Ucqa3xpuzTbgZxWlvBE8ztcCulC72LPuWcpz3NAYi5YTHTcU/MtoOtwoa1UP3VDoGiPqrCCyQoCruUPbXP5VGK6aP7Qhns27waLl1og2Bh/hhsLiGX6EG579zwKQcEPVjBiCNG9kOmUCg5BwI+X1hlHqh5AYeYQbOuPBQGDc8HFKT2gbJQxPvmhpykfxYWlx1rihqjkI+DY6iudXtJ2ueaPu6hImo6N+SNyNG9KnY/Abe6W0jLC6SzKZjIXKdaBk3rjRd9USjdK2rOUis1HakvrFzoPghg9cTtOGPZ39iTxvKD49Wsnh0NZ4Qc39ecOLiNrRJtzQSOapItzIaZUfvCFBjxvWFk+/x+NguCH7hZVLcUAopOg3rC3aXzFJxxt1V/zUGrdWf7F4pzAJd6ltEU+4QS+18nsHS7+zimDecKlNxywqVCj8N/oNWVLM+Y0bHlwcdCO4GKezEgf0lEyN3koc0VM+XzuCiht9TZDLG9EzWVsMVbCTeUnrnG5oAi10EeJ03i6Zd7QbXn3ZqkPhxtBvWPlfvFMTaAl+QwnvFytQihueV27xG3XNj1yttUl7osPxWt68WuMz5f7ojoNnsK7Gi7wWPBdey2dX44TOqI3quPmGrbA8yXY+OeBkoe88CCs0IJsmhlPFrkk141LJvnElLE0qmqaH04a+cyTcfSOrGcu0w9Ytizgw5xw0QeLsnulm/NEWHDRHYnqq7DC54e6eamLt0UEzNf4oxQY0V2t/y0mabbf2ypLu0XF3T3W39suS7kF4WtL2+xaPBhzdU92tvUqx0SzjOHB1T3W39mlbEJq+YdVwgmVg7VNNI1gS1p7BGo2Nlj5UHvdUC4/VkkH7gjYS2yIcVs+xbcQGWbK1O04eniTMEVyr42qez1occ7zKksPRTHr2aPnBz2Dji2FWwPSc1uCY47MujmWpDmGd3yvdMT1NhZZ29yDcUh3Ts1SKY3qWSnbM8VOSY0WjRWGF3lo8qGHFo9Hy7R58K8u9I/rWwua6Y2oduTnmaa5cHXP85CKsCNycHVP/zskxj2Lk6JhSOju9XSlvnN5OGQ3PB/GaAke4ZKWsIT8ViSvPCo6L0jNThccHz0xH9O2e5wYHSJmCLjmWZyZpvMmSZdwk8Hps3zAy2OlMmo3fqEyfwM0wtes4hMPieStsfkTt7EZ6xlGiyFO22HyLuxQqNh+jLM04OF+jjl7GwfmMOF4dnLD575G7oIe4+5lQkx9xWp6KRzX5EmVpJlrWOLvqSTlMXnmAbloo32JeU/QJs2ZIHJSP0Y93DsrPNSp3UN7OVcvPfZh8jn7CkzsvOI4SdcTE5H2U0Ri1XkTVqGIMMPDw5Ba2HsW6egptAYuyD2PlkhdlHrLkMbA8hYWraFTad5ZMnlg730xEAczMtMxEAbyeJ7G1frI/+2l1o89T2nAndOLhZxtMv/DAXBx2N/rAMWGjr1M9x9l4o89T2thZeWCl/rAz7BNrJ5wnwIGjHwPNRs9IfiBRWGdLqO5NLC3CSroASy3vjpUEwY4rz9nqNb266aAICvIjJMK7M9jBcPZjT9BqO4KLlWQAau/EzY8IQi/GiSNPq0CsnfNAg68npT3liVlgqe/Zcfbj5NDVu4ccB0Yy4HhX8eO51h6vYU3tHwnEpdubedA9CplHJ4FdkTc7ryc/wWsHERG5tTsxZvQmr8Gj5zn23ecjXTt8P3HQeTSL5IgwcLMOWMKSnFiZsS2+Dbj70SFUJYN1u+hkPqqSwbil3VXQfmCZoRbiByzdvTqWVWrmM7A0d4sfBNYxNitBjG0x1eCz097A8s/YOWtg6fWoV2xYzhgbP8Dyxdj4mrhIh7dT+sDZdyKy0Ten81jHL7COZth4Bx6+k5KNvroe2spGX9WhteCY6wPnFzAjT4CNXqUYjebmAdaBDZuvEzcddLL5DBz9OGUxehWZwPkPrJNQJh+AOf+YrgWY5QqArT1t+OmZYu3pawoeYR0uMpdatAz2OgVSrT2KVKB8A17PF1Vrj1JEMWkPcPWjLtXoVZxutJ33ONWxxQ/K1GZYZ0Os5C2w8hlYSVxg1vS19ImGdQAE6QaB/WxudKydN6QPM6z1JwhHmqfAxbDOavjzzLi1aPp8ROBhXPafl7F+356rpli0qOwI9c9zzxB7zaVBjI1Y7cQRzwGp+UCs05dIT2ZYO718PmVF0hHS7lhnwIdj+kijlTiyYM+2oxcZ2rSeRWEVk1ifqwbb+lw12KLZP8CsoEF+t+w10yzdJXCT/mKeamD6BqN5qoEp36N5qoE1vixSA3hNX4LENxk11NT/yHyTvWZaNKMVWOPPIluApd9YSWPg3Xgk1gk4czIDMzxUmaIytuZ28wV4HZ/F6BvJj2MJZ/+9YvQOpuCN5jFABjzaf9EivYA1Hs3LC1zW+a+MeX60sSyGtRNsJY6hPEsfsgQuwExpTX8MMF2NdNgAq8auyUNgucwtXwgKnspjjnrahlUfwuQtcPad4Wz0RNWkNHkN3JPLf/ALNdF28r5Zjfm6rrfAZc1YYfQl2nvBjuQBKwGRRRIiY6AyENn6BKxaQMmx1zTGegbsNbWx3gFXOawce03bv0wFnetH9iPZRr/XWGdigISi6mldX5GrJI/d+go8Vv3A0pPoPCnXV0tmwsoKTMowsbyDTB0xsWo02vqKbCeqEWf8AU6eVAkJMBJqOy0rf4HrUlf+W7aUHtb+QYVRVdJQ3pKMWhRr/wKrpElnlhcUxS3regqsyg1DuKkGOrPgTBz29D9g6Se2vgJnz2+Rjd5G154yDk28yt9s9HY69pTbKaHI6m49BWZkKvVb4FWeFaNP2YG4ngIPlwfF6FNNZcxfYvofOJ+Byzr/jV7VvsL6SCx5aesnsE682/qJfDRrHj7h6Ln1KvPVpNX+CIYZ6ByZyiuhKOTO/sDhGtqLtE8mLooUMftl4soa3NEi14DZH8DdsOwj848B66hi/cuCsUqlfLD0uoYpH6KtP8iRs9pHthlcWvb32WZwUWUOphJEEp3gCbWa0aPKHMxLCKz10c5WWdKdpOfd6EH+AWGjp68ZBbrRo51o5kgEVk4BS6EILH3M/E/AWr/s/Jll9fH0SR0/UbXTHC1yBVj2j0WGABc/Bzoss48qcdjJZMPNU5qOZGmCVvt9ZMNxTUsJeqoqbTBNJTAjGVhYpePoenJs9OnsLFNiAnP+K2UmbrRdzs7AzESeRBOeXNzgcRhl6USyouyJceANxg2l5LHYOMtfxFmnRKC4kXeZRHmD64BSh1rKo+a5S3mjxfXsvG6UrKHlN9a+1Y2+di5837ix9p4+Mc3Psf+JJvsxdr+hos/KcYobNdZ9Sr1SWlrWGypjsPhXlDgpBb+hQhIqTIAbXHZVaQA3lAqepQZwQ7ngmbsfN5QMPlgyWNzgVLMb6IZWYst+o9gNTrbEfPa4ofxHTCCPG8oYHS3jLW5Q3tkNo7SyKLlysONGXIswRKNUVS0Ss5jjBmWe3TBK/RQy84LjxhrfhP23eaNR7NkNo1SlLuyGUdrKLve7UdrYt3bDKG1jlx3aKIV3+e+/mD7aKO1ynmW/MWpSnF+wlE+zG8ggZniGMqi8e5VfQVFyG2OJOZpxY43FMjrmDYpkZenGDeYsshuQM8g9669Fa+cNSgm7UewG1WJlXscNJaMqfqMvrK1RyOR5I9rKaDeMdB2ttBtGuupHK10+brAmoO1LG+mewLdwOGTb4fUbRrqn9GXJAtygQmI3jHSlVbIbRunQ9CgcuBn7Uetbot1gOBX3x+0GdyftxmI3+kopJlA2x73f6HZDc65wVqJ6u8dsmsjJSCK43rDsXzqLaTcs/1eMfhDdJB0quCcfY7qhaZopHKFerCUbFiM9apqythluaJpawRrkHIvNPcRGuCwq1nCznGRkudXZAR5dHuNhVGd6CJNVBwJe5+MwinQ80/JXGOYKyVhDYFbKo8cZeHisajd6igZ4tBUOOa5Xj7TRMy08zxJg9Kh8oM0OwyqKYrHbwBr9FtuNJGuMGGHCAmDGDtODDKwVczhW1glLf9Ct9JZjo7euHvBm9KrqNj3GwJwG9BgDK+GJeYyBVw+5hbN1Vd2mx7gjp4nnFbJwcy8qEm2HEVgrsu1AAuc1xQTxmj23Gv86V2zmnABek7hXo28snlurMgsdJ4c0vImzJ6CpRq+qcksjhMLqy2Q1epFW5+9Vo8xe9ITJNYCVo4YpWzMS0Uijg0abvagJM3cAy4POrK0TrxoiNOjsRU2YJQS4rR6iYFjKAjOo4tT2kIc8G31RHgfm9sxI0uEpSoyeyDxA0Q42A1dPUZONHlXdVjZMHM0Iio3O9v4kDZ65FnFM3JPYJHt/Kp7hNdn7oUUIG39kYXqywqFCk1EpFYcsTCaJAV7zsdgSM2RhMsUMsHQUWnwZJ9UVm2zry9RY/Pu2vAydBWHMCbDmiwWlAK8ePFtKJrWeXdAWjjlcqui3dQNLoDwYtmyMmtfvGz1VHhWmFcuIi/D0SEZPWzNp25IxmuYHs0dNLI+KnT4Frr7DYevFUJVzYKOvrx4UWy2gcIteWyxGz85/WyvGUFEOBJYiL+NCDzRi3y03W/INFjyGR8i7H183j4q6Gz8PD4JnLcLrm6drYLokYE0vpr+auHvoOJo3FTs5bJj/a2I5bJhqC4UKs2dUGobXUH5LAIcEEJqulh8up3X6Wfq4nNbpZ9nl5gLVHRt9aTXQLDddTs0Nqmj0uYOv2vBBCnk32MymyXk12GxPEZGaem57jjkXTzZriWFyrp7+21aMnGUw00ExFarg/EhGX1mnXzJ6ym66GT1lNZiT0VNYWlHTs+Wpf/Xd9J04en9ko6dyeYqWygBY04vioGUpYxIXULO7Nsyy8aslzw+Vjb5WfDhlo09H+yJzDbcsTU3polEzwqdXMXqklgEbPX2dXsXocQe5xQwBa4OWDgUcXIv+3N4/1ulV7P1IGCNs/BnrdKv2PqljTIMPTIcRMN7nqS7ogAcOzn8sTxMn7z/L0FKWdT5gOQR3nP9YLlsJ63KC5XTiNXUXlufmCTBk4CNxjI9fLOewYTxbGZZ7aP2e7rXb+6ROsYxBR8W7puVaOPvy3e397rC2DW7g1fDs1t5Eu9MykQLPr7s6Yu3NuxxJxGvW/W70u3plR0+AZfxQvULKg72jQB0pw2jYJMcyH7JjOhwZUIDSylKXLOAAWDZMc0yHAgMWgGU52NEK6Bos+cijH8CqAbg41lGM4JjjkUc/gHX0KglLHeLRj272kQVYFMdUh3j0A5iVIHn0A5jjMTt9Und49AOY4t7yjxlOrNEYHOfqRz2IeVSIASvAtBSK6JvKkanwpTimPC7VMR0ipTlWAEx33Mx2LaKvSh2qi2PK4xoc02So4l9FBM3fDPjhc8rj6vTJgV6zPi8Hei2OOX5rdcx6y1X9X6Xe1O6YDrM6HHN8toXjqapQdouOFRCl8VrlMG/JMcdn03itcoi37JhWd9N4rXKAt+qY47U5fVJXmtNX6ADri2NGEXbnp9SVHh3rKIXzE8V0/2bt1GSY6oolgDJMB7uliwKWumLJpQwra5BVSAWm/4dlXYE5fi3NlWGOX0uKZbj7UXl+vq+pg4R11D45pn4xsmPWCGf5WmAGcI0mPJRiw2rhAutoxXCcFNu5+A2666zWbrMbTYcaIm80aUiq1osbfrgi+w0KZav32+0GtQpY0rqhEFtWCLYDdsUPRvAG9vb/Zlgqb0SVaA+L39BeTQi6oWOrKluMG9xNVF1j3FDhaxY+xg3PFu2Upg+lk3FDGaODU5q9eLJTqtOrqs6MG1zzVb4ZNxSlEp1SHWhVAWiURKGYtQrRvKHAlOiUejH26JSWuh4V0A2KWqtSzRuDm1/RKZ3SibtLTmnl6q/C2LjhldOd0tpUetsp1dlVq61uN6ZCvex9wkyu6jdwgqwobZHKc9vpR5V051s6DqTv0zFvOIOGblQvQd79Rgz7rUVa8LLPD0R1c29PHOs4dt72eNpxzrzscX3eyApaj36jqlK5U6r56n1r7orqN4bdCEpPPvxGYnXv4KTLBPEhNW/ovGpw0nvv+8MSWYR0ksZJH2G/ZHc3N0neG/y9+EQOTvrQXpomUPdj5z7FQIVisrvf8ENRzW8ovkYTGbnKi5+B0g0/FEVh0HFYPe6JC+RCIE8lULDCK/Jb4wPn1f08l26oiIykVK+qsSMxNjGXKRb1BlYaseKYZgDreHcozZ6WSZj7OKzkDUwrm6W8gZXRZXHcWQjcymYDU0qszzN9x304VsaP7pgzgPW9gXVisDoeadktQ+bUGrtlqleJh54cUw3rUbiytGIPjrVMLY5zV55AYY7z1h0ro0ITblyWWnUcWBjc6dOJ9eb0KSN2c/oa1fzm9PXFMzgI0wpvTl9X5W2nrzOuoDp9OrFenT6dUK9O35Ba5fQNJrqsTt8ozTMqEHP5qU7fUMYF0dcWmgV1caxS3aIPtUYtjplqwMTKsNAcV6qF1TH3BUoRVqWjkh1TzS8an6idaTg65vgswTHVqrL49zke8xBOLKqWNf5aolc2O32JalZ2+hLHZ3b6Er1C2elLaylatj/TTM3RcfJSuMJUsxgnDR+mdgqaY1VsoVoKuyppz0M4+3aFMNUqmTmwM7z0qTDFNOOkLUF7ci81cd73WkNvYm2lINwWLyXYLcF7Y9ww46a7ra3aoYRZh2VxjbM2/jaZrUO4L6tX2vjXg3ulu/EHka9//+X7zBPLK0ozdeLVrO3Wvi6zlmYunM7JzWSjf453uUma0Ttk1lrGFuDiXrdm9I3ucUzN6BvaTk3m5UbUbvG4K+I0FGdlRyNwUMXj1IJhuWGCmfm9exb0RVhxFogbMzwozwO95t0zFIQmrDiEUIUj5OHf/9//D21uWfg=',
          'content-encoding': 'gzip',
          'content-type': 'application/json'
        },
        gridID: '1',
        msg: 'MAP-DATA',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function nonthFullcleanCharging(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 5,
        cleanDuration: 64,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: 'scheduled',
        globalPosition: [
          3,
          40,
        ],
        msg: 'STATE-CHANGE',
        newstate: 'INACTIVE_CHARGING',
        oldstate: 'FULL_CLEAN_FINISHED',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function firstInactiveCharging(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 30,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: '',
        globalPosition: [
          3,
          40,
        ],
        msg: 'CURRENT-STATE',
        state: 'INACTIVE_CHARGING',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function secondInactiveCharging(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 40,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: '',
        globalPosition: [
          3,
          40,
        ],
        msg: 'CURRENT-STATE',
        state: 'INACTIVE_CHARGING',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function thirdInactiveCharging(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 40,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: '',
        globalPosition: [
          3,
          40,
        ],
        msg: 'CURRENT-STATE',
        state: 'INACTIVE_CHARGING',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function fourthInactiveCharging(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 50,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: '',
        globalPosition: [
          3,
          40,
        ],
        msg: 'CURRENT-STATE',
        state: 'INACTIVE_CHARGING',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function fifthInactiveCharging(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 60,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: '',
        globalPosition: [
          3,
          40,
        ],
        msg: 'CURRENT-STATE',
        state: 'INACTIVE_CHARGING',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function sixthInactiveCharging(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 90,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: '',
        globalPosition: [
          3,
          40,
        ],
        msg: 'CURRENT-STATE',
        state: 'INACTIVE_CHARGING',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function seventhInactiveCharging(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 100,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: '',
        globalPosition: [
          3,
          40,
        ],
        msg: 'STATE-CHANGE',
        newstate: 'INACTIVE_CHARGED',
        oldstate: 'INACTIVE_CHARGING',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

module.exports = {
  initialiseClean,
  firstMapGlobal,
  fullCleanInitiated,
  firstFullCleanRunning,
  secondFullCleanRunning,
  thirdFullCleanRunning,
  fourthFullCleanRunning,
  fifthFullCleanRunning,
  sixthFullCleanRunning,
  firstMapGrid,
  secondMapGlobal,
  firstMapData,
  seventhFullCleanRunning,
  secondMapData,
  thirdMapGlobal,
  secondMapGrid,
  thirdMapData,
  fourthMapGlobal,
  eigthFullCleanRunning,
  fourthMapData,
  thirdMapGrid,
  fifthMapGlobal,
  ninthFullCleanRunning,
  fourthMapGrid,
  cleanNeedsRecharge,
  fifthMapData,
  tenthFullCleanRunning,
  fifthhMapGrid,
  eleventhFullCleanRunning,
  sixthMapData,
  firstFullcleanCharging,
  secondFullcleanCharging,
  thirdFullcleanCharging,
  fourthFullcleanCharging,
  // new below
  fifthFullcleanCharging,
  sixthFullcleanCharging,
  seventhFullcleanCharging,
  twelfthFullCleanRunning,
  eigthFullcleanCharging,
  fourteenthFullCleanRunning,
  fifteenthFullCleanRunning,
  sixthMapGrid,
  sixthMapGlobal,
  seventhMapData,
  sixteenthFullCleanRunning,
  eightMapData,
  seventhMapGrid,
  seventhMapGlobal,
  seventeenthFullCleanRunning,
  eightMapGrid,
  ninthMapData,
  eightMapGlobal,
  eighteenthFullCleanRunning,
  tenthMapData,
  ninthMapGrid,
  ninthMapGlobal,
  tenthMapGrid,
  eleventhMapData,
  tenthMapGlobal,
  twelfthMapData,
  eleventhMapGrid,
  ninteenthFullCleanRunning,
  twelfthhMapGrid,
  fullCleanFinished,
  thirteenthMapData,
  nonthFullcleanCharging,
  firstInactiveCharging,
  secondInactiveCharging,
  thirdInactiveCharging,
  fourthInactiveCharging,
  fifthInactiveCharging,
  sixthInactiveCharging,
  seventhInactiveCharging,
};
