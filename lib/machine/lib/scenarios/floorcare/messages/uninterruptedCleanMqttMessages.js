
function initialiseClean(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        msg: 'STATE-CHANGE',
        oldState: 'FULL_CLEAN_INITIATED',
        newstate: 'FULL_CLEAN_RUNNING',
        cleanDuration: 1,
        fullCleanType: 'immediate',
        cleanId,
        currentVacuumPowerMode: 'halfPower',
        time: `${new Date().toISOString().split('.')[0]}Z` // remove milliseconds
      }
    };
  };
}

function fullCleanInitiated(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        msg: 'STATE-CHANGE',
        oldState: 'INACTIVE_CHARGED',
        newstate: 'FULL_CLEAN_INITIATED',
        fullCleanType: 'immediate',
        cleanId,
        time: `${new Date().toISOString().split('.')[0]}Z`
      }
    };
  };
}

function firstMapGlobal(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        angle: -180,
        cleanId,
        gridID: '1',
        msg: 'MAP-GLOBAL',
        time: `${new Date().toISOString().split('.')[0]}Z`,
        x: 0,
        y: 0,
      }
    };
  };
}

function firstFullCleanRunning(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        cleanDuration: 1,
        cleanId,
        fullCleanType: 'immediate',
        msg: 'CURRENT-STATE',
        state: 'FULL_CLEAN_RUNNING',
        time: `${new Date().toISOString().split('.')[0]}Z`
      }
    };
  };
}

function secondFullCleanRunning(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        cleanDuration: 1,
        cleanId,
        fullCleanType: 'immediate',
        msg: 'CURRENT-STATE',
        state: 'FULL_CLEAN_RUNNING',
        time: `${new Date().toISOString().split('.')[0]}Z`
      }
    };
  };
}

function thirdFullCleanRunning(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        cleanDuration: 1,
        cleanId,
        fullCleanType: 'immediate',
        msg: 'CURRENT-STATE',
        state: 'FULL_CLEAN_RUNNING',
        time: `${new Date().toISOString().split('.')[0]}Z`
      }
    };
  };
}

function secondMapGlobal(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        angle: 90,
        cleanId,
        gridID: '2',
        msg: 'MAP-GLOBAL',
        time: `${new Date().toISOString().split('.')[0]}Z`,
        x: -270,
        y: 2520,
      }
    };
  };
}

function firstMapGrid(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        anchor: [16, 68],
        cleanId,
        gridID: '1',
        height: 136,
        msg: 'MAP-GRID',
        resolution: 45,
        time: `${new Date().toISOString().split('.')[0]}Z`,
        width: 136
      }
    };
  };
}

function firstMapData(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        cleanId,
        data: {
          content: 'eJztnduS4zhyhu/nKTb6WhfEmZhX6eiL9e6EvREba4ft9Y3D7+48/KAOoCiqqlQkqzKyZ6b/oUQBHxOJRIKi/vfHX/7+x5//8cdff/z+8+dwMruwX6ffDMm1GZLODElnhqSzZ5A4sc2b/Gpbg8Qt2OYd2ALJEpAvCeURkkdAnoFyEIzLSNYAcVevXfv+4ebdO7KPQLIM6eO87NBI3JPv3RzDZyB5i1ftxAzJLpFsDuEZJJ8DZXMIzyH5DCibQ3gWyeuhbA7BkLwbyauBHA7J64EcDMlnADkUkm8J5POR3J53cwD7QLJz+/xYsnmX94dk91C2mXE277YhMSSvQ/ItgSwheRUQQ2JIDMnXA2JInkLyTdP5z85LNu/ue5F8Sx8xJIbkfUg+HsjBkbwCiCExJF8eybcNrp8542ze1T0h2byT+0Cyecdeg+R9dzQe1l637Nu8a/tDclgohsSQGBJDsm8km3dtb0g279hrkHzLYWNIDMl7kVh4/WAom3ftVUjeDmfzrr0eyYG/wGhIDIkhMSRfBcnm3TIkGyH5JsNmPZJngHwLJN8IyCuQbN6l/SDZvCuGZPdINu/IZyN5BGXzbmyBZPnpV1/K3vOMxi8JxB5bOWOGpDND0pkh6cyQdGZIOjMknRmSzgxJZ4akM0PSmSHpzJB0Zkg6MySdGZLODElnhqQzQ9KZIenMkHRmSDozJJ0Zks4MSWeGpDND0pkh6cyQdGZIOjMknRmSzgxJZ4akM0PSmSHpzJB0Zkg6MySdGZLODElnhqQzQ9KZIenMkHRmSDozJJ0Zks4MSWeGpDND0pkh6cyQdGZIOjMknf36dfrTbz/+/V/+64///J8//vrj958G6NrMZzozJJ2tQfJlnzUwb4+RfJNHMaxF8m2eTbEWyfpnId1D1I667v9dnn9zCB+P5Np3Lv97H+GOve41j01+y3s3R7EGyVuBvB3jLux1v6l0WCiG5NOQvA3jLsyQPIHks4EYkr0C2c/A2RyEITkAkh1B2Q+S3UBZCq/f1E8+9weXD4xkCyA7R7IFls1RPEbyWii3n7A5iK2R3H7C5hj2g+R+1XaXSF4HZIcYDMmbkLwSyCGRfGMghmQlktcCOSQS8xJDsgbJa7Fs3mlD8kFIXgfksEheB2XzLu8NyeYdfjuSb+sjn5+qbd5hQ/JBSF6JZfMOGxJD8kokNgl/EpTNu7sXJJt3cl9INu+gIXkxko+AsnkH94fkgFBeH1437+LHIfkYIF8KyUdh2byLH4nkWwJ5/cDZvIOG5KVIvmlW8uriwObdMyQvRfIxWDbvniExJJ+P5P1QNu/exyN5L5TNu/fxSMxLDIkhMSTvR/ItgRiSJ5G8F8vmndsXks07tjckm3drH0jOr968U3tCsssvpn00kuehHN4MyRuQXOO5D2nzrmyD5L4Zkq9shqQzQ9KZIenMkHRmSDozJJ0Zks4MSWeGpDND0pkh6cyQdGZIOjMknRmSzgxJZ4akM0PSmSHpzJB0Zkg6MySdGZLODElnhqQzQ9KZIenMkHRmSDozJJ0Zks4MSWeGpDND0pkh6cyQdGZIOjMknRmSzgxJZ4akM0PSmSHpzJB0Zkg6MySdGZLODElnhqQzQ9KZIenMkHRmSDr79ev0p99+/Ptf/vLP//jbH3/98ftPA3Rt5jOdGZLODElnhqSztyH5Qs8b6O2tXrLuCRWHfH7FqwdOg3agR3xYLOls+cFzmzdvC9sKyY6flvM6JMvv/pZIlu2gSF5phsSQGJKvZXvwkp35iSH5JCSPO3lYJG5V995iB0Xy9iZ/cS953tZ08rBI3mb3O+kuXrFDGGuQfHSTWznpVef/Akh2aZ+JxB0dycc3vsWOAyP56PB3cCSvcfEdzzOPkLyy6TsH8vlIdu8j80he6SEHRfJK5zYkM2c+KJJXNv7QSF7zkYakO6shOZ4Zks4e/Sz3x37cITB/bvZ6YCSvCoOGZOasm3f4sS3Hko/+uMMj+fiPOzSS9/hJ/yOHh/rZw1ftCZ+BHqQI/Woku76l5j1I3jtwDmqvQHJwe/zj7W8/+UGBGpLOHt9fsnkTP9teh+SwMF81cA4LxGacGfv8Cv3uzZB0Zt/a6syQdPa5JaRDmM04nb1i4BwcpHlJZ4+z14MWgl6FZOe3dr/GXuklB8X4OiQHBfI6JIcF8ppYcvDI8ygvMSQf0rVDA3nb1ta9CHO5v7d5x/aA5LzReWggH4fEGZIeyfnI5p3aB5IvZOuQzA+Sb4zE3bXNm78Vkks49xBt3pGtkHwLMySdGZLODElnhqQzQ9KZIenMkHRmSDozJJ0Zks4MSWeGpDND0pkh6cyQdGZIOjMknRmSzgxJZ4akM0PSmSHpzJB0Zkg6MySdGZLODElnhqQzQ9KZIenMkHRmSDozJJ0Zks4MSWeGpDND0pkh6cyQdGZIOjMknRmSzgxJZ4akM0PSmSHpzJB0Zkg6MySdGZLODElnhqQzQ9KZIens16/Tn3778c9//OPP//O3f/3zv/z9jx+//3yCUT25fPLu5OPJJ/m34//jYvsTTm44pZt3Jf6ffOjqNQf60vEzjkSIRoYThlMIpxDl34Pgmv40bvU+2lm6C4x3ZesfaJik10TLn0I5xeEUHf+b/s7kpj8NIaFtzHrOs6jvA9+ZzTO7HDpX153+Sl0Kgir6UxxPKfO/+e/u4o+y9AIvy7tnkU+vDNfgb9nvdMjOwbsTq9QFCKaXvnH/qWP04mHg/xDFyz9KlMEIb3cX/Zn+cH0Bumuw67Hcg7wfz5pDCF86QF0lCszRDQ7/7f8hxgxKzuaFwL2rcb4iE+L567L3wX4LdSHonQfqPSbuzj+DYHF8IfgUcenKuEu+yxcpLseDzdnO8V2IjZcjuX/ZHRrzbjsNjMVLdO+Stcs186cP2ZtbR/he+LxhdOPpfmYkz1+am/Exd3WWXXfd5+zArtEuhtNugE8ueDOVL0w+94bGeIVpBtyTo2Nzru8jeye/vJ8XLQ2Ii2vRX6AuMK8I7AdHO7fYmZ/A150ZV+P6+nxXtN1y+066uSKEp9vr82Wwrp/Brk9z5yGda/KNi+vxxUiuy7bWWF2VGD/KbB9A3EVK9eySYO2KZna1Npe0v4nhzpKnNYvV+wTu2YrV2fkE91euB0mTFhhe9/+WwjLGJ4KCTlfPOOM+I+ODIX0B4QrF43Xe+qnqOq36UjTvLtavcayJUNOFWZFTzQWULwR1ocZ0/u/aqLV0iW5OMBthvwrXJ6bWlV1Zvk43p7gXd78E28c++8xM+7BEeov2Xhh+WJ09CNuFEfxMCe05rveD8SLWA6VVN6nAnUlnZeF3cbPkksPlpZwt/19TvGnQPuulN5F2LsucLxE9zv2Xt/UmCoubMrcEuzrg7mr7l0Dv72jcmWyWV6TLG81+cezPAp1ZSOxtM+qMs0v/u73RN8wJizc90KEVO35dzHljpeGTcS5s1r1rjr1/2019nubMBdnR7n6DOXnmujD2XMYyf6/Xctp2/wLuby//EuSNT95NsN+TobjLn3VZ3rC+91F78sEbgE85xftTkTWev8M65zy+FcvMmbTwHXnHqg/cX6l9nt6jheR049EHOcMjV9/f4nCB3ELce8Wq7FGcPRK7+yHoJT1ajnhHArditfeRdYCnSm67xram+LB2K2LZHqznDo2tC3MfODss5nU7LG4tYXuwTntvn9zVf5fu4t5h8WqR25vWR08vyUL394e3F+2rXnWf4cptl7d439xXCa7+7tbtJ+6kSHWf4Zo89Ymd1gt+c98huPp7k6tvc9uZ3Q2BM/Pgsyuy+7ekXNXvLoqsB0e4qjL83Ep96QtTl3+/vk/jyBAf7sI9HwyXJtyrvY4rfXCO6ybnJ/q1cGH6rbiZEt/inswxgD766s1HuOX9LeP5HabDZNu3NOfuNLu3gng42yzd0Ti3DX/F7mDp9y3Ii0xlaQm7rl8rR/n1P5ffd5y+T7r7FLzn+HDueWagrZt1Lnfrr9Kive0Rr+H3oQXje6nP0l0oN48u2N8m3Cfym02/ZwtCF/eaXO5e7m8Hbpne8zs9y1nH5fpvRYkr7/Kr8evQrV65rE7YHleib747uzWQt4FbcYPHU+AWp9+P2hLdA7e1BZvVOe3i9w0OkdM9orY2O3mmsw++8bL7/O0Rs1WbO8/19eHX/XadsD0C9nAn5KY0/HjCW/3lvgPZmjLLndluxe1hT32X7zC2usR3G/9XPsBl5Zf3DhXKrrmtK3PcPKPl8UJ81Tf1DrCWeoBuxX0Y6anHrizmaud11VHj3EKtbuHG4ueKoQ9cb5ffPXgLv6VvKdwvTz5cN6zcHTpk7rv6W16r6TVrZ7xTIFn+svJB7O7Xut/6BI/5R7LoTRqLj7060mbPiznO/Jlqbe1OjZvB3N/f+1U8cu3XEGZQLj/Z6nKpcTclP3ZdYG3V+GFwfPT4tZsvwT2qORx+ln6w+7XE8vFjArtHCT6qRRw4YVx5H8dsvtjvq8+/bKYgv1QtPRjNZ0b57NLlmQck3fs63Zvr2Du0VU8Au6R3GQvXw1zxRNVDz+PzMBe/NX01x6x7YsowGyLnYR7gu2/Pwbzzjelu/ojrnpDSs5y/o+jQ20v3SF7dBr1869aSx90Z4/Sf+VvADu2PC4/1X3VL4fU/j9zy7Mudax80p1ym+CAxeuafpexotgB8uBxoCeTyuufWBR+549J6c6+PkP8wjndSoXTzOJiFtHyx+rvTXzj4KHz3kh93XYidWx0u7Tgc6G6iZ4ndf1D55ZP1ZksTO //NgdfQmn/2+NWO+mxNbN+/gPEqVt3zXG6+BnevGLvf32J5HSk16u3tFy5nn8S+5x8BejWjS1qN2a1t3pGtkHwLMySdGZLODElnhqQzQ9KZIenMkHRmSDozJJ0Zks4MSWeGpDND0pkh6cyQdGZIOjMknRmSzgxJZ4akM0PSmSHpzJB0Zkg6MySdGZLODElnhqQzQ9KZIenMkHRmSDozJJ0Zks4MSWeGpDND0pkh6cyQdGZIOjMknRmSzgxJZ4akM0PSmSHpzJB09uvX6U+//fiPP//3v/34/edPl095PDnvf51+usJ/jzllFiOLNJTKoopIAwH96QcWecj8Hu/k/S6PhZXH2ZK8MKhKwbGKqvIo70uqRidKWxGGGlgVVT5EVqOqMMpZqCWFVHKJVBhUaZODU1UDtyV4UXEY+SwhqPKeWxaiqpC4dyGpSoOcJasqI7clFFXVy+eNopIfIhS1LFFjoPjLqKOeczyV4eRqHfG+4k5+GP2o52Tlqr6v8Ct9KGPVT2eVhtjaUkmF9j76PJ9jOrfMl+LGqWW++tzamU/BhZLxSlL0xmF6X6BD596G6GqeSISYXZkohTjKVVGCIQ1yNZVuoEsbJvIEvsbpWBz0+gXxiTjginnuUfRDhir8AKbsm+KvOtcB16/4UwwgQSqQQjvdqcRTjCBBKp1iSuKr5BOiwIVUJjVmeE+hlpWUIhR/WzqXoH7Gqk5eR2dJQ/VOPZIVuV2Cyqfkx1rVd1nFwaXzK2MUH2QVadwU8RdW4ZRyiiM+gR+6oj7hxUOo0UOFGkhhHIlnpRKqm8ZDKrk0VUhhHMl1Jw4JPcrUltEN7ZXUljGUhHHESq8KK2pZzQEERWnf+drSRRu8tIyvn5fBn+ATpByuUVQV8whfYpUH+LWosTYf5BPBl0ZVQfoQqqiYJL7wMWpAHKcxRo3L2nd+JauxvS9xmBraiGNFXcBYIUUeUYq2hZWHvyRRYRgQGUQ5HStBVfTweVFomVM1RkQiUTrevbSlRCeU/KgqOniBqCTMvLYlIn5qWyJ8QtuSnL5P25LgBXwRSOXSIi2pGnyFcqdxcOohco1GB48MfG1HR04PFU+j176zSqT82N6XSWnfvYz+0WvfNbJTK6XvrCqpOqKd5LtjQN8D+/UYosRWVtSWgL4HHg9jyLW9ktoS0PfA42gMtbWTxtgYnavnYxnTkRc1DjqqvJyFOlgujqnXeYkT9HedjwZVeWgqnqpLOUzHqhtjU/lEoVXP6UQFjD85Z+UBCEVnKToecKxctqWWcNG/Og5tbhQVNS4lVUUjURZVB98UP0nDyYynkajW4st0zA1D9HV6I8kyhOmsFAsmx2NJ53HtvZHn8SFkvDjqtF7O73XkRefm0sU4X0eSFNPLOF1ISgK8O18R54NOk3rRSZbcrronSTF5mNyFXGfyXZ5FfQIYcTSSY2l+xzlEQauQfBT4r87/vuCSenZnkvBZx74u+UeT3MgRfup4lJBM7UJmbuQ4qhx4fJGspUluZEW0lkjgKILUERLJDF6cOEfxOv16eToMJTeu+ZZKOAlJflZS0BhKXVAJdw4qo8ZNjhYiASepTL62OCNSgx4HIZGarXCEYpmHwSN8iaQsCLFNZPSYblWm3KKiSJ3kAlqVRxmPAa3KGhwCWlV8yz5EjgjhPI9yAqd5IEdtkUgkRkidwTjei9TkLw6QpTbJTx5w6mYsPad46hv8XpKUyyRMMiKdxk/+XJHT1KUSc1dSSZOXRwdFYvYKkDEPgCMy1TZniNRrFLRVKQxtFlaJS4ZWhSmXUKmJjUerAq4gWhURytGqSPkoXEXkNDRU6gX1aFVEmoJW0ZzL73VoVdQr6NCqRJn8L1kzqNTJzaFVFK4yJD8Ag/5HguTHPHsvQ8NxusVSe+TkyTUu00zMjXSUxNFIyVFHGR9lmTTDdUVl0ZSFWiWSnC5pm1VqKkk9ElmnuKwSo8xDat5CrFRi0EVIBChtVRn06vsMiXhVIKNOWyOkzjFhgMTV12YUN7TMWKXLSK1U6tUPETIVJFcqkeUWQVcwkYcR0ld3eRRjEK2iBQFWDpFiDoFUWSGxWiCZWJYRSVakUVbok4IOq0ijrGBK5aMkORNozWAZQkv6VI7O6bASGTU95R6JDNrICIllQ4DU0c2sRGKp51Qm33J+lbq882hVW76iVQQSU6hKZPpoFeWmDpdbZNBMAK3KuSU3KjGXoVXn6V8lYjtahQTA4XOLupnD5xZ1M4fPRX7g8Ll1kATI6edWXhjiKA06Wn2FEe9lmbSRLB1LbSRLXphmV/FBdNVIqvNTM0iSG40SCV1RyROyjjKVuqKnUaZydJjLVGL14VTSugFwVGJ9FSCxAImQGsx9gtTr69GqMGiCgVYFJBhoVZgut0o4MFoVSsVqRmVtKb3I6NvkpRJLerQqZo/ZitCRxNJ5VElr9QrnFxnkouhjpUgqWP1VTpJFzhwD5Cgr5hhVZp1E+CiXC8og8TnK9SWpoSCKM/hh1MHOMrLURJ1Gq8oyyMSXRTokY1HcjKSyigVS09c4qgxe3ysDhy6Y+jNLzzJoM6o00lM24iCpCz5qJIx6FXzUzI3lyDLJzM6ysBwLzkxsvUeuQs1QSQt8yMRybG0WSblK1h6pjJIDU/dVVv0geTwirWg0N4sRchSvY8kvHtVjqbMiq9fP9ZBJ0TnIKcFQiRBURYbBt3KMSgTVApkGhD6Vij0kSPiktiq4QSaRECDhkx5SZ7rgIEvG0l1lbRmFSI+IhFZ5THxoFSXfCH0Mh1LTgvWCSmSbii4ETHwBEsmnh4zT2BdZIlJilTrxcdxgiZIJRxWRQeYUjjkidaZzaFVEkoBWJXV+B3QJyQkamTWNby9G5cTJw8u40uYQNvmRb+S/cgWdPHePPFSXefy5InUF6vT5cBQJJGPkNos8R0KRuSCDUomqjIOsuorxKss5EorUYp2PkAiMaFUpLY0P/ASyUltOKHL0DvmGyiDJJ199kWloviESXodWjdWjTCeyal03oFVVJ/qAVlWtgAW0qqIMBFYVTohn6Q1TYFSpuWj0kJojxQBZphEqcpQUMeoHJYeokiG17BjxQQ5BZoREkBn1mZFO19EcZFh6XTonPFEyaA5MaapKLTQlPJ4cdZl2tOpYSNqFPOiZWRLY7DR8sRxYFg9JF8JTQiG5KEsu4lLUabKw1MIKrVnk6KgjtL2Y8p6szRCJdIu6oBKzRoWcaKjUMUisWFLiKpebSKqkhFI5qwwD5iOVSYJMDJA6BqO2ihJX8avoIJ14Hc8ClSVWixVyquep1NIKzwIikZpmlR5l7ASpw5mviMgsjsRXRKTmKsFD1oKcUFoVXMsJVWKRXiE1NfUjZG5pvMpR3MyDVUBiA1YR4xesaHJGqUplTqj6qMT4RasSMhm0KnmPMKJSZ2eHVqUckTGqrLKHwQ8j5Pp/1uSEJbdz0BqFPkiUyxsF6zJpJ4o/jmMmSy228VGW2FVpRyllujwasReEU1UdKU4fc1idG8bz0Ro1f+Y280WkYRTOrap0kdLFUTqMtaQ8W3vAqQp/Lq2NtRrQjoYcsA4VmTVHYnQii0OyLbK4NgHJqSq2ohwkLqgX6ehMAVdQJC2BcH1FloqALJI0ArJKROACOa01VOoqlb1OJCZ6bRUXzlAcUEkhCf4sMrYNLJXZIyCrLBl1FZGUYaDCrdKN2ABSiX0ktAplpYBW0QzUBiyhc63ao09X9VxYwqlExrY9JhMWrQjb57LkxBatEukEXYiQsSU2Koue2aukFKqChsjgGiuRMWEjR2VpuzUq1c08WhUHj1KJSl9RKlEZAyZclfActArVD49W0ZRT4TkiUUhBq1LKzev4Caqh1vHsZtTk7M5uRhyjP7tZiHUMZzcLqG/AzUKKQzy7WUByAjcLafT57GYhuxDPbhayjy30iUwTWJGaq8DNKE9VzmhVGao/u1koOjTgZoGSkxaug24l+rObURys6exmYdRYBzcLyFVwNNLMjqKEZJgOW3rt6NjKDiK9XpR2tAZ/4d6UUaQLn0wpiDMEfaRy4n23s0+mURMM+GSqrgxnn0yYQ+GTqWJoOEh1M/hk5pL62ScpINV69sk8TBVXlaXV21XCzbRV2SFeRUhNEuCTmebQevZJSjCGcPbJ7MZWfxbptbLtFF3mGjrCpmeJtHaUB3ZnrilDDizVGfi9LKM6g+NCwUnKwFU/SKWCZckLt4g46VQmxEkPiUgYVOYhY0NBTpVRJE+QGmSIlUqUWYrKMkTsrqlUZ/BoZEGQQSNLljklOEjUcr3KETEnQMIZ0KpRY05Aq0bUcrN2ATWZwCU1lijQcc2QpYYCluSEhQsNeK/I0taDvBgpg0aGkFRSrEcpzAWWOkuGoJIWxCiFqdSdQJb8Xq9rjeBUBozfAXLa9lWJSzZCTosLkdG3grPK0ArOKrHNESFLadeXZRpa6FPpm8eq1JnOo1Upt6lcpU5tDq1KKCmjVTm3JEG6X/QK8i/gidQryJKfhF70krHkHysbsfDkzTKWWlehU3FZYkTZexRVaVWqbRI1bQhyPWPQ60O9GVnp5aGuyrHaNrBYOY3EBEkUks6kKrVKsigdeb6omjbyWXmNul7aMhZ4ljx1P4wjPMtBwrO89JiDHZIIkdUFVBpUxgE1ZqbFO42YvVXCs5RldajIKukahhb7VZaCNF9kxLCrkOOIjIOvYU160eIAqeOMJdf/KK7KWsRBOvkgLm2KTFo6CirJS7FwkVMV7W8UJ43DoFNBlB8eIZl1pZIhx6FJrtI5L4GEa+QsA9apo8qo+QhvXLDMmhfEqqeiBbCsCAf93FFJJukRSZ2BZQ+EpV5rlny7zFhbqYx3MyjkBpTKeDdjwK5XlJBMMk8vlqM6gqPEwugGHZSxqHQ6sGKGrEODw9JrfVqqhiyndbnKksCZ69m8B1rxYuo+TeUJYEUitYmSJkTKknJba7JEEtGOjkG35vDeUSNHOzPqEFK8ZKlZMBpJM2zrglTZaa5HB1XqnVUsuSSf/XSUC+cZ6GQGjhxIM04lEpXRCInKqDzqPQYUAPDiGPUK4lQx6TwZCyS8boTU4RsrZJ6W6SwzLlmVD+KR0S43374Uk9yhxyt+kTWghsHbFwmrSa0H8H1QQlKrBfQ5GmVZ8mZOVnQs+b1Vwx1OlTEF4VTZTw7M76WVZmmNFDm2scBbLjmg+6PKrPNzHPXFWdN8LUtEXqfBReVzabrGoOMO5lJiI0k0aEJO+Qw2jzrHAl1GxI6SFcZcpyGpUtcxLD1LDYzcI95wG3Jt/eUNt6QLpnYUMTbJ72jEMqJI4yBxKic+SSmFDA2u2bCszqPAw6OMVs9CI0laRBIf5GTs05pd5ljZLGWprJKEPq7feVxQjiq07paAzDKwjK1kxRGJ2Ii38+eK1Fw1SeSPY1a/ShFSa+q8Cc+SaymQ/N6C/mbIIsE8adjkPfsAya0adf5LWdtc3fRi7hFW3iy5vxWhT3KoWAdd4icNUBUL8VQgdaSQZLAVC/F21MOfNfRVhOtUITVeJQ2bNNFJM/IAqZWzPOiLEXLzoK2qOmCzxmdyBdkVyRK9uf5c23v5RkEU/rNEfil9NhlY6maU3CdC0mlaJHeRsNRiSXsxDe5wPpVz6ir4IF4vjedmOK++oY0kqdOTdoGk+oZ2kKQul7NTGTVnyOLAJNWf29GiQaa9dwwaKCpkqWfsyVWdYvSiUJqXBZ1e0OSdtiollV7zrxQhNf9KATLJTKcDhyRcVFvF2xMtuPF9CkG7oEMy0Zp+iNOAJamLHh77LJNGMw0FvND2iFcqzwGZb5coIZ2DDK33vSYJUg4hqZVvDdd8K+hQpnDN21hD+yC6KGFAYiPlPZIYkoNKN0yxvbJE2XjgKMp3/kY/xdgUsXzUGJsitqmjFP5JhhY2VWoGrWVyknXaMWOJ9RPLwFLdLCbI0KZFvsMlYjmlG2gk0UHZmyKJrIBXliSxftIdM5LBtR0zOYrVc9WjVSNSqHq0al4XRkhkuSNvGNJE59o9zCqR9BbeXSSJO5Jkf5hnxba64tt/aFyNqAfwz6WltrqS3XKemwdUOCLfweQ0kQuyuUpSCy0s+Q4mr7fp8otF6uqZj/INTR5Fi6Cy4M4gub+DpN4LE/QXnWgmKygtiKSliXwQRyeWuPenQiocnkBFlrbtk7iDmAfZ5VhW3SD1GRKlBbklh6SOI/ZekUpS7xsiidJCgJxKVonhVNzp6UXmwReUUWlEk8R9ru1oajfcqcztbgh9MSpYA47WfHm0FZUrpI5fJ7enkZyKFipTu7FCfgYLKymHX8XyqGHgKNZSfJT/D+474KMiy3S/EsugSSBL/u0tZDJObt1NlMlI3HBJ5aiccbQE3IWh7y247R5npsV0K95zq2j1nJqks5U0LWq5+5Rqe9TUVWqNimVgqbG9Hc1c3fz1f/8P62kPpg==',
          'content-encoding': 'gzip',
          'content-type': 'application/json'
        },
        gridID: '1',
        msg: 'MAP-DATA',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function secondMapGrid(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        anchor: [12, 22],
        cleanId,
        gridID: '2',
        height: 136,
        msg: 'MAP-GRID',
        resolution: 45,
        time: `${new Date().toISOString().split('.')[0]}Z`,
        width: 136
      }
    };
  };
}

function thirdMapGlobal(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        angle: 90,
        cleanId,
        gridID: '3',
        msg: 'MAP-GLOBAL',
        time: `${new Date().toISOString().split('.')[0]}Z`,
        x: -1553,
        y: 2520,
      }
    };
  };
}

function secondMapData(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        cleanId,
        data: {
          content: 'eJzt3ctu21YCgOF9niLwmgveL3kVw4s0CWYKFGkxM+1mMO8+vEki+ZOy7MSmyvPjd9Ecy3Goz4cXUZT834cvv337/P3b14dPj49xZJOeog+SzJMESYIkQZIgSZAkSBIkCZIESYI6kiTq2n1R7qULyRzl8plk1u4L/L4klzt8GSer7b7Y70eyNi+CQ1mS3N7uiy7JnZBcW4l2X3RJ7p5k9wW/N5LdF1sSSf5+JLsv9L4kRNl9kfcnicPBuJ0ksCSRRBJJJJFEEknuJUkkkUQSSSSRRJJ7SRJJJJFEEkkkkeRekkSSW0mCvGjiGkksCUlccSSRRBJJJHkfEnfCKyRBXs75HMkySeyUJEgSJAmSBEmCJEGSIEmQJEgSJAmSBEmCJEGSIEmQJEgSJAmSBEmCJEGSIEmQJEgSJAmSBEmCJEGSIEmQJEgSJAmSBEmCJEGSIEmQJEgSJAmSBEmCJEGSIEmQJEgSJAmSBEmCJEGSIEmQJEgSJAmSBEmCJEGSIEmQJEgSJAmSBEmCJEFPT9HHDw+///Lvb//669vXh0+PAs1zziBJkCRIEiQJkgRJgiRBl7dN3n1R7qXpO0nvvjD30fz9xk+fvvw5wHeXnv5qmO23Yg+W5CXtvuCS/A1IDowiiSRvSXJYFmeJJJJIIokk90xy6PMrPsb5AZLuyw+O8RqSIJJEEknekGT3Bb0/koBQnCWS/DySgFhedkC/++JKcuckuy/q/ZEEgyLJq0gCOSnwEpKgQCT5AZKAUCR5NcnuC3p/JAGhSPJqkt0XVBJJ7ilJJPl5JAGh3EZy6IsnXkcSEMjyxSeSSPIjJAGhuMeRRJI3JNl9QSXZmSTGnQ8YZP7S6emdD5RjSmLnJEGSIEmQJEgSJAmSBEmCJEGSIEmQJEgSJAmSBEmCJEGSIEmQJEgSJAmSBEmCJEGSIEmQJEgSJAmSBEmCJEGSIEmQJEgSJAmSBEmCJEGSIEmQJEgSJAmSBEmCJEGSIEmQJEgSJAmSBEmCJEGSIEmQJEgSJAmSBEmCJEGSIEnQ01P08cPD71++/PnHr9++Pnx6FGiecwZJgiRBkiBJkCRIEiQJkgRJgp4nCe4dYG8hicN6a9zbV5z5WyrvvuBv10u3JQH8TpTXbF4PPk9eu8eRBEmCDrzyOEvQ60gOO0O6JEGuOMhZgpwl6FaSQ8+LeS8hCeTXxdxOcoLZfZHfupeRBNEtJEHMjUu3zRJJZgXF0SUJkgTdtnndfTHfM5/aQi950mL3hX2fPFRDbkuQh2rIzSu6dZYENE9ccdBzJEFhDLktQZIgSZAkSBIkCZIESYIkQZIgSZAkSBIkCZIESYIkQZIgSZAkSBIkCZIESYIkQZIgSZAkSBIkCZIESYIkQZIgSZAkSBIkCZIESYIkQZIgSZAkSBIkCZIESYIkQZIgSZAkSBIkCZIESYIkQZIgSZAkSBIkCZIESYIkQZIgSZAkSBIkCZIESYIkQZIgSZAkSBIkCZIESYIkQZIgSZAkSBIkCZIESYIkQZIgSZAk6Okp+vjh4c/v3z //9es/Pv/y27eHT48azXPaIEmQJOiW34AZ2O8RupDwjhdR0n46i5K8/8i6YbH3Ar8nzBRgcu/Pny6jNInSPEqL/iPvhu0nT197+N9i3hnNEE4CTdv0hiyOsizK8v4j64YzrzyEKdZhzShODkndfcxuqqI8jvKk/4i74cytWE64Zu/79lZeM4sBIh3vfPuHy01plNdRUXYf7R+64fnrz9NuLn5Asg5sijFIDGZZjzF1ato1NI77/9o/zuzOU2+O3k60w62a/Va+v+OjR/u/i1vWW9RTq+G/ZPwvnhp2f++kO+OP+9V09/v6FnbjXb+onCXibo1rZ1ArMTeb2s3H7d9cSF++V3bMGTgYrnN0cyjpFdcnYTz/1HJO8psdeDY+Oxez5fbvvJLOt4XTvz31XM7Eg+1Bxu0gVtbJLJztcCf7h42dysZsPubOZNz1zvgwZ2YHwecDEhzDwDA++N6kx4u39sH93Z09zDp9LA+XT4BXV93+Qcbud/ln0k23a9NDvmElmz2CHz74uKz/Dqs76sn6332 //DiPZ3u6dP3hxXnrPjyCn54GKDs5HhgGM9u2HsFO94YNzowUWweDIWzbNs6QzLzqtbMk18gOvSvdOO821N/h5QmA0/q7ulrONmEHPHAb/sgtc9KvTnzofz5Fcu1w72Cb+7kVS7L1Dfvk7Mj02CxkrPPmamv7tPEw9cB7xmtgi+3VlTMiPPA67o7xmtjqdmt5nLWccJfZdcz94jbYeYKtnq5MpqvgZXs2m1rH2y1uay0m1/ajnsUp8em0OhzVJtbmXpHHDLOnXA47qbasku4AYH2nyHM5yfz5vOM+h/cKq/WDhfGJ4sM+OXyCml8w8BzU6hmfwwqdlDauSNnaVk036Ac/oKLVlYtTcJB1WfPCOGCn1pVrVJr5UTxO1h//ASG5rlyj0l3T8syxQ4Dza/MildO2+9oBBJ8VOf7ma/6wZfkcR3w5abp53i+0fePi0HxlS84Lp6AcyCq5Rpasb8958UBIJ07X2HilznL+rJ1JDXbzv9gBnC89uWzNts6mbj4pfuzN//IQI1k+T7sxw3AZQUjza3m16+SIbPVpoGAPX5ePjorl87N8gtFt/uPsMpLJ07N4AvuZa6HCWBk3LtI/Xx2xcax/8Cs116S2L88vNh5GHv7C4DWmK5fkXz1TGNJmanop69rxUiLVnGrrkV8+u25Qr4vX+v7/cghx5ZK3cMHmrwuZHaFOPvQ6v4zhcopw8fhn8RHqwx1MrunLOs4Pr/nJlZOIxz/IWp7p4jVai4tEVm7qyI7/gsDrbttXaz13JddwFvHQl0W8IdxhL7t5B7fD7zSnbtfMdNt2W3vNnm7PuT17Ja9sr2a7ehwSzkHuRW39JeDzq3pXDneDOsqdaOGl3ssrL+aPrII6qL0wrbyie3FFz+LlygEdwU6U+MrtxWViwbwrzzbS6mu019/m6eDv9LRttPJq7BlHcvz3DNvG6f6QTNp9oe6DxM5JgiRBkiBJkCRIEiQJkgRJgiRBkiBJkCRIEiQJkgRJgiRBkiBJkCRIEiQJkgRJgiRBkiBJkCRIEiQJkgRJgiRBkiBJkCRIEiQJkgRJgiRBkiBJkCRIEiQJkgRJgiRBkiBJkCRIEiQJkgRJgiRBkiBJkCRIEiQJenqKPn54+OPzf/758Omx+6WN3RsExXVWPUX9KI7SMknqbpQPo6wZR0nTjoqs6Ub9u02lZVPG46iK0iodRmX/qxSqIs26UdWP6iZtxlH3q2irJB5HdZTlSZaOo6YdpdX499LuTamL4jTq3pOvbIpxlLajpqrGURZlRVn1y1n3ozqu8nHUfmVdZkk3aoZR1f8L3Z1rR03Sf8+OofuNwnUTn2/L8zqtx1ES5e1dP90Wt6OyLIZR+23zosnLcVRHeZmm1Tiq2lFWnkZlOyrz+nJbHRfjv96OiiQZbkuHUV10S93enW6UDT+HblS3o2L6lUVW1ufvUiZFkZ9vK9PhZ9TdVketQ5KMoyYq27tbnO9RWef9feiUulFdlaNgEpVNHDdn+bLJ4vEn3Y/y4WdbdvLtT6ga50Q/quJxvqR5OxruUTezulHTZOOoiKo4yfJxDnajcRa0o7IdjbMgHUbj90yHr2zq/HJbe/eqcVS1o3G2ZsOoqdJxVEdVmsT9svS/bbNKx9naToikHQ0/226URlX7A0zHURZV7Q/idFvejqpiXJasXZYyzuPhX+hHSTX+61m7ZO03Scav7EZ5efrKdsnKKk8HiW5UJWU5jtrlrLK4enr63/8BWWr/WQ==',
          'content-encoding': 'gzip',
          'content-type': 'application/json'
        },
        gridID: '2',
        msg: 'MAP-DATA',
        time: `${new Date().toISOString().split('.')[0]}Z`
      }
    };
  };
}

function fullCleanCharging(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        cleanDuration: 1,
        cleanId,
        fullCleanType: 'immediate',
        msg: 'STATE-CHANGE',
        newstate: 'FULL_CLEAN_CHARGING',
        oldstate: 'FULL_CLEAN_RUNNING',
        time: `${new Date().toISOString().split('.')[0]}Z`
      }
    };
  };
}

function fullCleanFinished(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        cleanDuration: 1,
        cleanId,
        endOfClean: true,
        fullCleanType: 'immediate',
        msg: 'STATE-CHANGE',
        newstate: 'FULL_CLEAN_FINISHED',
        oldstate: 'FULL_CLEAN_CHARGING',
        time: `${new Date().toISOString().split('.')[0]}Z`
      }
    };
  };
}

function firstInactiveCharging(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        cleanDuration: 1,
        cleanId,
        fullCleanType: 'immediate',
        msg: 'STATE-CHANGE',
        newstate: 'INACTIVE_CHARGING',
        oldstate: 'FULL_CLEAN_FINISHED',
        time: `${new Date().toISOString().split('.')[0]}Z`
      }
    };
  };
}

function secondInactiveCharging(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        cleanId,
        fullCleanType: '',
        msg: 'CURRENT-STATE',
        state: 'INACTIVE_CHARGING',
        time: `${new Date().toISOString().split('.')[0]}Z`
      }
    };
  };
}

module.exports = {
  initialiseClean,
  fullCleanInitiated,
  firstMapGlobal,
  firstFullCleanRunning,
  secondFullCleanRunning,
  thirdFullCleanRunning,
  secondMapGlobal,
  firstMapGrid,
  firstMapData,
  secondMapGrid,
  thirdMapGlobal,
  secondMapData,
  fullCleanCharging,
  fullCleanFinished,
  firstInactiveCharging,
  secondInactiveCharging
};
