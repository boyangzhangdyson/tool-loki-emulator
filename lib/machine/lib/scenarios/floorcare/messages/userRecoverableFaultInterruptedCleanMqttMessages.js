function initialiseClean(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 100,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: 'scheduled',
        globalPosition: [
          0,
          0
        ],
        msg: 'STATE-CHANGE',
        oldState: 'INACTIVE_CHARGED',
        newstate: 'FULL_CLEAN_INITIATED',
        time: `${new Date().toISOString().split('.')[0]}Z` // remove milliseconds
      }
    };
  };
}

function fullCleanInitiated(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 100,
        cleanDuration: 1,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: 'scheduled',
        globalPosition: [
          0,
          0
        ],
        msg: 'STATE-CHANGE',
        oldState: 'FULL_CLEAN_INITIATED',
        newstate: 'FULL_CLEAN_RUNNING',
        time: `${new Date().toISOString().split('.')[0]}Z`
      }
    };
  };
}

function firstMapGlobal(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        angle: -180,
        cleanId,
        gridID: '1',
        msg: 'MAP-GLOBAL',
        time: `${new Date().toISOString().split('.')[0]}Z`,
        x: 0,
        y: 0,
      }
    };
  };
}

function firstFullCleanRunning(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 95,
        cleanDuration: 3,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: 'scheduled',
        globalPosition: [
          -318,
          177
        ],
        msg: 'CURRENT-STATE',
        state: 'FULL_CLEAN_RUNNING',
        time: `${new Date().toISOString().split('.')[0]}Z`
      }
    };
  };
}

function secondFullCleanRunning(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 85,
        cleanDuration: 4,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: 'scheduled',
        globalPosition: [
          -371,
          1403
        ],
        msg: 'CURRENT-STATE',
        state: 'FULL_CLEAN_RUNNING',
        time: `${new Date().toISOString().split('.')[0]}Z`
      }
    };
  };
}

function thirdFullCleanRunning(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 75,
        cleanDuration: 5,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: 'scheduled',
        globalPosition: [
          -2390,
          2581
        ],
        msg: 'CURRENT-STATE',
        state: 'FULL_CLEAN_RUNNING',
        time: `${new Date().toISOString().split('.')[0]}Z`
      }
    };
  };
}

function secondMapGlobal(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        angle: 90,
        cleanId,
        gridID: '2',
        msg: 'MAP-GLOBAL',
        time: `${new Date().toISOString().split('.')[0]}Z`,
        x: -2586,
        y: 2580,
      }
    };
  };
}


function firstMapData(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        cleanId,
        data: {
          content: 'eJztfcuO7Dpy7fx8RWOPayC+yfMrB3vQ7j6wDRhtw7725ML/bkasUFZm6hHKKmVKVTuwaj9WKVMiF4NBRpCS/v+Pv/3bn3/9x59///H7H38Mb4YV/Hz7zSRah0mkwiRSYRKpMIlUmEQqliVyN5j/7fWRb4sliaZSLOPxy37km4c1yLxEjwj0eMG3nXHvZvlCEj167sOl+uoSvUCkPXzRsRI9XaitI9qexftiIh07on0JqfbwRWcS6AkivV6iZwu0u0gmkUn0HImeWaRtZzOJdpJo6ay7CjQn0XNbbevZHr/qkwR6tUSPnG3PhvmmEumfNonUzx8m0TPd9cfOdkKJrl3fngJ9VKL5771IoC3p/Vdb0bZvvkygV0t0Ghe8r0T7Dr9fTJ79JNpavS9nQVsk2iLP1gp+wU6mS7RdIDfzveszfFOJHhFoD9yKexqcS6JT2tSZJJqzqhNgP1/0bUXaYx3tF5XoaHFOL9HR0phEJtEvIZGJpEp0tDCnEun8Eh0ukkn0IYmOlsQkMol+CYnOJtIpJTqXSCeV6ExCfVuJ9pP520s0f6bt5z5YoH1XQLZLdP+7w7c2fFSixy1mrWrr1T6tQHtLtL7Is17tkwnzPIk+g1MKtI8vOrwSx0n0Whs6LUyil0j0zWXab8H68KqcX6JvK5JJZBKdSaLDq3KERK+fX58Se20HPbwix0m0Va7DK3J2iQ6vxjNhEqkwiVSYRCoee5batSi/iECPP27uWpRfQiB7It8GmEQqTCIVJpEKk0iFSaTCJFJhEqkwiVSYRCpMIhUmkQqTSIVJpMIkUmESqTCJVJhEKkwiFSaRCpNIhUmkwiRSYRKpMIlUmEQqTCIVJpEKk0iFSaTCJFJhEqkwiVSYRCpMIhUmkQqTSIVJpMIkUmESqTCJVJhEKkwiFSaRCpNIhUmkwiRSYRKpMIlUmEQqTCIVJpEKk0iFSaTCJFJhEqkwiVSYRCpMIhUmkQqTSIVJpMIkUmESqTCJVJhEKkwiFSaRip8/3/7y249//6f/+vM//+fPv//4/Q8TbB1mUypMIhUmkYqjJPpCj9D8qEQff96s/qza7Wd8yVNvpxLNP3B3nyeoa9+9P3L/uSX2VJF0ibbh4xLth28g0XMFMonOI9EzC2cSmUR7FM4kOoFMT5BrH3d9Pol2FMkkeliiZxfnVQI9TaLnF+eLS/SKwrxOot1EepfoNYUxiU4k0U4CvUv0muK8TiCT6AiJXtPRvqBA55XoIy/W3F2cqUQfF2p/GxpLtF/z7CDR49V4nkSPfOflEo3LN2ewotNK9LGKPUuivdZevoREjwikSfQCYbZK9JEqfeZcL636qyV6r+J9dbctVn5RiR4VaA+c8JWZSxIdIc9J7ersEp1AqL1WY7+tQF9DopNZ0dFimEQmkUn0KwpkEplE+0t0tBgm0dcUySR6SKKjhTCJvqZAX0OiE1nReUUyib6SRFtXQH9pic4o0OkkGi1pH7H2OMuhAj1/BcQkUqTYQ+qDBXq2FZlEn5Jo6xW+lUTv31m+4HzF13zWt5Loc9U5jSRnl+hwQc4s0WlhEr1Yom8p0rJEHxHoW4pkEr1YosOrYxKdTCIb0Uyiz0v0kVzP4dV5rUSDSfQMib6lSCbRJyWyrra7RIdX5/USXVfdJHoQ7o59W4HsAaobYBKpMIlUmEQqTCIVJpEKk0iFSaTCJFJhEqkwiVSYRCpMIhUmkQqTSIVJpMIkUmESqTCJVJhEKkwiFSaRCpNIhUmkwiRSYRKpMIlUmEQqTCIVJpEKk0iFSaTCJFJhEqkwiVSYRCpMIhUmkQqTSIVJpMIkUmESqTCJVJhEKkwiFSaRCpNIhUmkwiRSYRKpMIlUmEQqTCIVJpEKk0iFSaTCJFJhEqkwiVSYRCpMIhUmkQqTSIVJpMIkUmESqfj58+0vv/3497/97b//41///PuP3/8wwdZhNqXCJFJhEqkwiVSYRCpMIhUmkYq5NzceXqhz4ftI9LRyT1+R+uilziKpSaSU4YnPTf58RzvDQ51fLNFHind8dZ74oPJjJNpboOt3mOx+7iMk2r0SnyqPiu8n0e54tURncO4PYg+JHvv0nhK9RO7XW9Gexb9+Hcfu0oz4HhI91Z72iPS3F2+bnB+rrvvEd1dxPonc3Z/DcT6JrmfKj539SXitL9r2SXc1Vz5coH0i/a3f2PrJa/s5gUhzkf5jIeFjEu15vsMkeh8btld96+e2WtHhslxD90Vagbd7jM/Zx2GeaYu73iLSlipuE2EqxcEvrdPfufdKiV5e/S3Y8lrCLdW/DgVuv/WCEOG50F7XvO0077b2mfc6viAk3VeiZ86Qlr9/wtdlLkl0TPFGWU4l0pnW9E8hyBRnkmgJBwtnEqkwiVSYRCq+gkQH4/wSHT7KmUQqTCIVX0Eic9cKzIpUmEQqTCIV5otUnFiiw4t2lnKsS3QGkQ4vxT6562fi8FIcKdG2nR+HN9MeEr1nmbVtL8vnu19cfLQMp5Zo7x2z17tCTmBDr+9oy53rBGLMY12i/S50okWfR/Eqib4w9pLoG8tpVqTCJFJhHU2FSaRin0j/Sw7mW7GXRIdX5HkwiVScP6V2OPbb6/htoY1oXzi22gtaen+bRN9awC37rk2iHST61jArUmG+SIVJpGJbR7tl8zchfFuZPrqmP3fH2OGVeQ7Ov+3hcJhEKkwiFSaRCpNIhUmkwiRSYRKpMIlUmEQqTCIVJpEKk0iFSaTCJFJhEqkwiVSYRCpMIhUmkQqTSIVJpMIkUmESqTCJVJhEKkwiFSaRCpNIhUmkwiRSYRKpMIlUmEQqTCIVJpEKk0iFSaTCJFJhEqkwiVSYRCpMIhUmkQqTSIVJpMIkUmESqTCJVJhEKkwiFSaRCpNIhUmkwiRSYRKpMIlUmEQqTCIVJpEKk0iFSaTCJFJhEqkwiVSYRCp+/nz7y28//vsf//jr//zrP//1n/7tzx+//2GarWNqVt/2IWkfxbVE6c11hcKbi/w3/eLw8j2Ip7Xvu07tzeU37958fPOJ/3b0m3Z0CTfjFc08ytXFqiRTGN5CeAuR/x7oN/33mmQnssdPNvqDuiW+WlfNv4XyFoe36Ojv/v/+G89H16R4VWG3KvdxC/iQet1QeoUD6xX9W6xvKdPfkdUMfNQ9tcB7Po6zfc4WPqThxf4CX7BXu5diGOgf0pGF8dwp50/ySfu97flxv/5/3bQrtrGvryFB70yyX5PkdIPjf6kIjiyNpIkLlvNJq554hbSbb5i29rzB7O+KWFo2xXtLfUza2VPclX3FZqceI+7n6GbbfbaKz/CtrDCr18/eL3Nz7c0Su7fFc+D7ug3PuhS3ozNcsqN7oXe96GuV3ua+Zw0OP4+4mxWll8t4U9edLztV+7OeQ6nItnOsmN32AXcdaYvg93/2ufRU9Il1PTS3+LzkW61PHzbWcDXYzhvY0p8drj2j+p33fGz68HnNL5dfOoPe+JoM0zF4bqY1Z/RPMvSrIj06C97ByFfnm1LjpZafDbvn9F+Y5t/Pom49y3MNfd4UNsyKdtBcHU78QtsvxOvLmY+VWOCaYvS8aoGnmfpKLZanoZ+V/LHvXzf9soXcN8G18qthQr2Zq1zPm540exnJQ0myT04PVT++0GIrnjBOWuDKarTizpn2k6foU9HfdV9+38+nTX2LP5803OqoL776otOdj1RLPFeD6aRi30hUbOkujXHprPHqZ85rfrYRNjr4226uTHjvZJsYrVrou2K894onOPv3dphNeVy68PXP/DjwidjrsWZ87/jrl7ydrvR/Fjz3crwGve9O+3Au6gOtsZQauTjY65+FqdAnEpQfGxRWXfvyv1OzHvv6bM5COtV1n3t+71AK425/lkapz/SQlcZc/fraNaeB6lJ5rnv9klneDEYPp7IfbZC1PnudaFqfsD0+flxPB1T3M2uO42i31phzbfN+vrmeP+O4pyHcs5aZNiQ6l/7cp+SrPk+cHRKmMwX6UcfZmRa6HQcXus1tV5mWa94Z1Mks5y5ged5qqpYd1RNY01B3Uxtdut31GtXoTi5yPJa6mxse59t3mrK4C+5unMNYuZsw4C4CftoC/GoqdWmKflerqY/b7qNu1rlGK1zLvS13oqXhcmbEu01sTNIbi3WDF76LlJ+1r2Qp3zqZgszP3qf6PJCwvlsZu3iUa9c+66Dmopf1pYkZ0SfnvzfBLXkGd/X3rphNyk6mIbNln50waQ5yeHcXdyY7m6lbneduCwbnXbHb9NnnhSjb2mUxSbQ255lvF3X2OxOt3djyjWMBn3qiaXdZj2iWeoxT+tYTg5WtLTMJ92fHG62DL20qmvn20qx2rmcu9NTJvHUxe7scDtwN/VtihJe1yNy4uTDxXFnuWQ/75+cRj/5ZyMNv6C7LAcGlSvfR9HP3xm1sGHVguE8v3Rd2Zf+dpsyWP0vu5MHhZS3qLDPTw52DlO1NooYe7+Z0G4CtZMguQ7eWTXjQfWwJu/Qx/zYF/6QYZI8+ceedFu1kNsep5tZmf+b9g2ojN/1vtk/Nda1Ddq6vjxKzceySccxuq1jPMM/+LKWdlczc7PCrzP9eiruV2JU8/OhTFo3hdmFLX1iZ/VlYbVnPSA/3feTEIitLT8oAc0kBbFlCnP2Z7c3rLX+bKjm7vGsrrFsGj/Wl8VvMn2HD2uKndtK/UMolObafclmmdawuk68Pxa8OM9cF/JwMH8OCDX92D/shyh0B97ZhkffUmr3W3u6u/Nkd/C/X7LNO7unKnXV0+NxQ+yLxTml2n5kFfkal9783q3c6R7ctUFlT8H0CdzuZm1Pmcs1BX3z7Wla4MbZeyEJO73GNE7og1mTP73Tv9XQrOX3t6otfx1FuzB0tda7Z+17ThM4v2M9uap/uHZ9uIofstxf+EnpvTZYudsOlu2HjhC5sY1nam7+2R+k2Qrz57jcQXS3w7NrpVcZzmgOd7vCaKizJi+XdetdnkE9efff8sm9Zq1ks78Lu+usc/0zWf7LXcf4unuldVDO7xuOluSb3AJ1f+I/uUV1d+Jzk42d+PZOpn1lnu4i5uDJ5n/Mf2+C+oTbu9DysJT56U8+2taq19aqFhfuZLXp3awJzveL+VLO38E9+ddIZ+kdD2U2rWI91j8UteTftMh0eJms5S73o/t+5jU9ftDn0Ba4t48TK9rulXvQ+wM+lyJe6z70/O7RDPNAG6051JUpbXDge1InMrR+bH1OoqEu3il7/564wB2xf+bDwa55zNs0w87yHpXhA2Xg6nQlh4/jihOq2Xa//3pr2OI/mM/e3AdfRl/bladg7+9SX+x0WtxsoFsOGu9ac3Oh7nNYfV3s6bUaCQHl61DSZs/Qso7t9FTdaLd6zfpe6mNzFfpjMD0w1VaFX9sTduf4xFbmSiJhL1rRLa06fxHCXh4uLCcyDBf7Uc+Y++OWl/NpM+vHSksOdgO/Z4/HPwdieJZgqM81qf/RWztkU8ZrZnUA5Xc+HZtNzfuzzC323qxZfCh9TcmmB7OwLLqdS0k+G6I03OJ4iw3AaKWfusth6K+IpcgOvEnJLdno+KNp8A+HxWeGXyrnhbpdpbD9RyG0xzu/nLhcn84uPTNW1HH/MOgnamsi6nnc/pqe2srEi5+zP6kh0YDLjNWIqw/qSmCs/87Z5dPLzVXpuXz67yKP+TNfAvu+k87N6fuzP/aLXNx3tP9j575V65M9U5e87WG233ZuZfZp5PMXSauDcsQcSJF8WM9IuP8/+fVljstxxsxPk/re/pA9Y3jd8/Vy3u19Onznq7vX+xUYpbYPk9JfTJ+bGidSHPPTgOAlXd/VOfjm7ZPaUl7WcEkt3OS1tQL/75eyC2HPeJHRKrN9Yd7uOgP/PLHBNtT5+6fAIuZYxecjvd5qV3OJMt2qeFCaRCpNIhUmkwiRSYRKpMIlUmEQqTCIVJpEKk0iFSaTCJFJhEqkwiVSYRCpMIhUmkQqTSIVJpMIkUmESqTCJVJhEKkwiFSaRCpNIhUmkwiRSYRKpMIlUmEQqTCIVJpEKk0iFSaTCJFJhEqkwiVSYRCpMIhUmkQqTSIVJpMIkUmESqTCJVJhEKkwiFSaRCpNIhUmkwiRSYRKpMIlUmEQqTCIVJpEKk0jFz59vf/ntx3/89f/9y4/f//jD5bfi31r++faHK/TfkEojUolEFx2RxiSl1IkfmLRSiDgipYVKxBOp3nkigUlITCKTHCKRxKTgBHz92hyfmkvQBlynlyC8Ncdl81ycFgJ/h4vTIp8gDPyx7KjUwTEpqV2OOF+TE+bfXKpDfD+WS23vx4p3w+X0rkjRO3NvrtZWhQ1vrrkawHLrLPgsrHaWB7leZ34IfD0c68q0AbVhVlxGrftZvEM5uyDMWvPQij7pPTdKl5FZbxYoTCxUPwjr34vJB1GfXtCTcxJGTxCqQ7i0jE9DLcLoqappyNJQnRURmpvN1zBegVmt8f2cbbi+XgvjFagsLUY5Zy6dFT+y/BaGYZArZHo8lItVGL0Xx9extuEtuF4aUYJYDWI2xHyuo4LERIkK1tgKiNETp9wg7cfMRy9tRCzwWbrxMIOCwYNVNm05lmPJ79/rxjNaAT1LMKWxpekVSK2MNkEPDfNJrJhZgC11Ro8VS6FJHToLA+w9g4VcpO7E0kUzYqWNVkCseemCxOJQBvROZmLXw1tqnUVcYeBjOXgnrJcsw3aJ9TpkaemB61ekpbnusZvueCy9dcNi6+l+glhzwcGFMIO1du/CDCp1t8Os+wdiiVganCvC4ltyaAfH7Z5ci+Mn/VvyA5eMmOvMs2U5Vql/izWjJ7O1zjJbFrHaWWlJWOmsmyu+Rw/HC9CMjvVf9OYsKDWx5EanyAz9odeWWR68qEussGV5hwfuDb6hVZiJZgFM+koES1V8IzPpK/LYvporLIRYgdYeT/Ur0nPw6MR+8Sasn6h19yafJGkcPFFnZEChirUmMiDxEyRYl3SoCVaeugE5sbrGTJSXT4aLq6ZjIXP7UTmJNdQvM4sDrDyBidVFsJjFvzDLKKcHq3XUs18vDUHskxn0dChLb8AkbUQscR2o/YhBT2pbYvViIb3u/aB7P1bjMB7rV2/wGsS6oTdo1lkkx19xjMy/O7+hMotgYnX9GA1pw2jzxILYUgGDEr3UzLITy2ImtjSASW90zKL0Rg8WnPhdZpH7io9gqYoPYYa6e5Sld7kBLcZMbAllSTJWoSwpsn8JKEuXLMArMpMRFmXJLrBvRVky/CCxrkt1GBsDKViqeOFA6pbuaEfWR/EqXpiPdffCfTNEZm5IfCyBiU/OYBinQ2EWYCFyLMAmArdfb2j2SyGBoe6BW7NGqS1rViPGaWK5M6ltZ6kzjNPE4lsvdJGSxdAZfJ0cK+hj9FTfzip8FilBDD6LFCQG6yF1iV3mKMza2MOJNY8eV8ECZhAFLGIGkcEyeg7K0pqTkaSzNkgvDmCOfaT3YFDXO7CSZOxgBq0dl6V3lSA+kplj63EFDD3cZTCMMi6BwcodyuJh1w5l8dLDURaPuRSx1Jn0cBqo+mQQVufYBhsNeXIWYjLm4Fhynj/JttsybNCxXbcCz0fH6PmzGV7D8fWa1JaVcMPgykjp6YJD4HZx/ELoTqXCAdRJPQI9P7RTuH7nhb7XhJ8eXML4XaKlYfyMoBXNQ+IxRft0ZQM9LrfCBZPQfLRk8YKgUoUK2qTFmtCAKcQgVLyyEypTAylku8wC6bq9wl4mcKAy601CxSFloaivRyF7DcPohJjWLPM2pk6Gu4bvOumng1BMFXtXAcX4F7x8F64dT3ruVHpuFNra2FmJevFbUsg+5Y5wJKDSl6VUPnMrhCYU8xi88tL16Q/XCM+K7RTj8Hg0ihOU7yaMqEQdUc/fJeqJimdobGYuiWvgLkf1a+OHU6dZ3GQj8+0U877AHrxTuIfAz1Emi0T1G/m/ThGcdZqoVBlWF3jU7/Mycbk81HbqxuonKlX3+fXqaCvh6rt+iDxljwOozK+J9gv5iDCEnnTONA3UgvTY4kY0Z2r9GIQi0Iw8yvY2GXgojTzQ9Tbpfk0+3KvvCwZaookoRtrInqVT2DNRChJLH0OFeqKVTTSiw/oKrxRhSL6iO0cYv6+Ic4hWjjdRI37OcqfFS40Claoh8CFKpWowUTlKgVCRUzFFyH05WnEhB9oFaFASNPJEtBcJNEWxWNDK1Sd7Ztqdm1g70R5TylAFipkOPaqXaZ8Oox+BSgwtpepxnoRPTIOMbFKqHkk66b9M8zjHAIUBeylVn3xJDAwqqQQpVYTFkpNJRDMnJ8gFMZUcAoTtsRIGNSfUjVEPaIgSsIAil9AdIyjsubtNUHGbGTTLBI6f0N9jORl5slA0meP3slKTpPHMnihaga5L1EsMPggVNZzQxmemGhENl8QK6CW1AiriJKGlyOwbtI2pBqYS3XgpVRRxpFTxEuWCSu+WUvWDw9VRCY3G7/aJlEQSgd5j0H35eObecXoTubH1mfosAXv/QKcwM6K9H6X3EIWpy6ivHPViKkVoRDSchV5STaCiRgQNUn3KdBDtHgs6g8YxtQWK6vc2AkVyqbcg6GVIZRrhRoh2i+2mUKX16WHXPZAbbQMUAxDRbjk9RhmHclAoSZQyVQNMhT7MFH6Szsw0w8yaUDGkAdTBT1KNmIoheaEBiZIgVKSLQksZlWQqvUxK1UMbSV6ASt5ISuVjkaGcatRnU05iVaYSCI1HY6//1XejBL1So9784kZ6sTuNYTQVpvCiROm52D3AGz/czSz3Gkrky7SgD1KZifZwQubJoGEMxEFTkF4GekmogdYxVmXapDs7ofCE1ApMQ5bpMmhOEvmBlnHCDIp5Pj3+u3eNMqDJus8BlRxHFIq+T4+5ZooW7PNJUDSZQ6n6ZMtHoY6ohM2Olewjd5Q5L8leghiwHO1REpdqAO0tmoXSh3vIf3MU3RktVgrSDw1XbVImSFMHOCB6bDc9sH+IVYoIKu6Y0rBE4YCI9i7XI75x9sx0DCA8Pzq9jjFDAO0T/CbCMQ1ZXDmodKoMWuPYP0HhRKgMTKUKDbRdBhhQ6QlOKEIJL6WSDLWXUjVMXDxK1WcI8IOJa9QDMk4tkENhGnnkJnfDVNJPVSgckEchW5+MjEfJdEIa8zVsOqGO3ZVNJ168M5tOvKQpuSFirpIPYJouKSNutTy08UJMYevjd3MeI1PQMjpr0C6W1Je+W8SzR6GXYQ9UxjmP73ajk2EeVLzVAFoxCXBS5h47idMEDUGGeVCZBGRcqIrVSakq2shJqXozVLErpjIn8ELRRo4L2SfAmPHTh5lKHonmnUQl0RmFQlg8tr7TOki8R28FIrcvWVFHHir5MbEF2keniwF3KvbMptJpHcTFsNNJ4mJozaLTLC6G/VGnmF7AW3WaBslMsXeTaMFzV+/0Ijt7xiyDhAMtyH/JSwyG4hFJOn6VQY/+ojQou+Aio28QKmMGD9WdSsfhgdzTMCcTRKY1+HESQMWueZDBid42MYwLF4WmJn4YVy6qUJela9CbPbrKwziPYSpLGY3fPjM0uEW81qJTpBcD3vLTY2M2/sAzUU/xvSS2Ar3JYcDQhbl1pxKj8mtGOpXMWhJ6STYFGtsozv45xqidYmDD+0k8Rb/vMWqn6OxEydf3gWAM/5jWMSrr0YWnoE3CsB57dEuQ0MILjYNEKZFePSHxAEIpT4EX00iBVqcYX4lmXvLivhATaMA4EDkB2Sn6QixCYfxEqb4Bxk9vrWGKSIMoVT/CyYxHI9L543cTLHY8c4JNEqViyEoOUSpzhpnFghr1kXw8StUvcMixQKsxkixQsryfimTvo5OUmduowOqIUqnGSLKifSusLlYYQw2D1Igtp48EDWcGxXxRjvoxOOTIyvcaciHTIBStkJxQtELyoD3eo1Ml2CTF5FFoofVFdOfEAW2nkC5xuNstA9PlxMFwpxjpEofKnWLsThxId4oOmzjMJqsaxg93nfv0H2XmEN3T/HCkVCpZm0peaByrQA3apx4sTuIXHHVauJclzhR0WsdiJCpkGcYKJipknwRJqRIVsqB3E6VC9ulIej8aZGEoRaGYBiReu/BBFtdSApX1mMSZ6k7T+F0qVajID6ckFFP8xEnnThMnWfCapk7RQxN6Sg8sOdwlShFCd8FRKL0QZsDabuIMq4+yhEOU3pblMEzgxdidYvpI1yWf7BCTJ06EdYqEc+LkXadYWUxwQVHirgQHFT2GicQheqdIOhOlUlETCqVSeaSdU8I7vALGhcQha6dIKqUsFEv641FZQBqPJqQUUxEKD5zgvWNG0jA1oWiUPAhFdj87oUjhE6VSVXizDGce+9RtkO8SbRjK84AKNqSPMzpdj2C5d+cB4jS0EVGSrqFLZs5d+u6euI0ypwU7RRvxkjBRtFGGf04DVuF4qZdoHmtEzd2PcZNlzlT3DgqL5RVdon4Yj1Lk6ZAHJEpxqcOEiiiVSsK/zPk46uteTkXmnWQpK3uhmXPpROnDHnOk8WhAijwHoci4ZXRJcs9c3ygUqVteB+5UYrbxaMEieUY/SgUZ1RxRhR7RjUepgrLKlDEAJVlmyhiekqwzESUlq7RRhM4V68w5ohWatFFEG0lElyNasEkbRW7f1KRRMJSnVup4lIrRMDxlzl51zwe/kfmFWJ1Ko/DLscgvNikkmWgP78N41BGFC+K9Ep06aZTI057s0I8y5khZ1scyZlDZYYUge9DoR8sBhb/Kg9AC59aEYjaSqlB0nMTJDt+n+OjsRaj4uiz04txottbngEG8Gc3lig+j++KZnkcGORXQhO0TiaP9TmEMqQnFto88CG2jPTPNYgxeqLR+ECodNuJCWYwhCYVTzVIqWR3KUqpeaP5wFSrGwOmMPoaItXPqp1OsY49HZXORfLcWpDIzZ4K6l3RcX7lQoxyyUE8UjUKUNuB46Jw5cdBpjeOZeUMOssDj0YIFvJyFInuVE2hFFJOjUOxzyEFo5slJ9kILp6qzEyoVHFCqhhR5akIRhqcqNPJ3k1ShYUEocSH7J2UAqqAJgUniOKXTMBohJVWHBH+VOP7tVOYqDTTLEMNBeqfSNQbQJuOCEyouyAuVvhCElrHDgoqZceog0Jqeh7CgvkqDgqYsxkBZYzdg9Yj2jjHF6hFtLCPqsA2Gdp0x7YMoUS8UUTlR8gYyzSNKu6MSOjtRTzRfTkW0ov/ShYjKPh0qBlMxpCoU+3aoCkyRyaUKMkWZqfqUnpatOzkK9TytzUEoypy9UPHeTuil/4I2TE5QKpqay1DuaLzOyBKmKrSW8P7dEBDgy5lDRG5Erhtire69VEG2j0iZg+wfkRoF2UCSOUfRKSYYuQjFNgoRp49sGGGrULErlDlk+NgyCIWZFScUZla8UGmjILShuaWQBauSRQpZ0EYlCy0c8hClty3KJIEojTKSFC6cKwjRYSJXYLFRJgmFs0yBEn1eaCWKAahwOrJTDEClgGY/UuqwMSeeQZUqtF0ozakk/ToeleG4oHcnVziRXRqoxxSiDkLRgpXThr3F0ILj0Yo5ElEKkWQbBVEKoGpp8l1eyKktjB+myWrDlIkoDdCylaLyewQ7FTVgk31k5/pWWF0fuzmPVGF1fcrLzV29UMw2axAKN1KjUDQ3UYrUaP3k6qjsOaicCwq9ddm3V36pYafwz0RpNi57DiocVJEBiGghisQgUYo9HeLQyhnkThGHVvb8FMRwQFT5PZOdoi/UIBR9ofIY2in6QuX1g26h6AsVL1kuXtSIoD3Q5O8moWj9moVi/K1FKNLrtYJmjL+14kKSgqsVxWiiBsysSAquwsyKbF+osNjSxJA4FRbqIGpwoqxTNxaDlOyTgGE8SkGQbF8gShM/2lUhlOJlh/XdCs9QaYUXxWCaPLuvWvFhWd2vTSjydW0QCgNusMmaMCw2JxRxWcNAUCV91zAQVFnsl6NNNtE2DCJ93GO/0QKoDIsNI12T3EiLQtElG0a6bvtshA1+o7snXIi9Snc5aAUcJQ/ENtlYye42YAyNB+vIiwJEm1DMJ/mFz8QlWub3aFPqxcsOzYEXAqKX6JI/TzwGf/39IF5KTt+7LXcmuXiQkKIV0N5w7b2kQcICqUeQqX3jtHqPReEP5MNRNqQ1NpBOMeo2NpAYA8ynZdAIA2nspDvFWNHYhXeK7GDjYaczUZfTzDHJRLhFUJlyNnYekbYyVaGUMgmYkjR2Hj3oxSBMzUi09zwnlOKkhNSaFDKXcN2quQYe7aWCWXYZtSoU2bLGk6zu7sUEoBUl2d370T4F4d4jrdAc5gLy4eYxH5cPdwuHdHJUtkOPR2XzwHhUlunHM1+ZXuHbFdhpjUd7bOauvlsxCONoD8WRWcR3O82XNiKaMCi1BJqlRuxok5NsCjUZUY+Rs3nQIH3LgcqUpPEMtEfXWEBsg1CEm43HqERJ6dFbEHUw/9rwYR+COEtQtBF8WCIvXOFomQZkU2oSKr4z4cxBRpLxKKZRGFiSl62ZNQhFG1WP7zZsZ6pOKKpQpQoNtxkUVCFI1FuqUGw6KigzzZPHOQgdlbgWE5ZOUd+ShMrMNwpFQregUQItTv7ErIqoR/XHozLLoKOVKO7noBkZU1Sf7wIhCv9XeHGyU8Q9fFdIpwEjCd8WQhQDKaKxFMZom8eoTtG7Ecl1KokojvMozAnjUUpMJkz9Mg+VKcjm0swvhO7jSONpMlGqfpGoN1Ok3qmkQHh3RO/5kqhIQiV7lN54O7hMhfi+DKKSiqDgg2gp8uHgaZu8RFSZ8hadSnYhCb2Em7R1N8qG0cz57ETLjQPUAL1EzLRfN8q+0NyEYpm3DKAeKzDFgQYkk4oXKjPuILTw2FiiUJlic56GEolJbINpD+sl3gIVi3VCZU4t1y3I8Yylknufxg9XMW/5cEVafTzaxJ5xoSTuWopBLZqE0v79hF5WOAffKdJUhXPwKWU/WixTWcksTqhEgYNQbArNDVR2geYqVHIAvGpE8eMY2US6iUFuSyJKdzF0tzJSuo1BbkwiSvcxBOx9Go8GbA/IDVSWPalU9OGYs0gHiqkf33ZGd0dg6leCUKlvFIo9ZiUJvcQ9oJe4J/LNFFgZKAlUlgELZ9k7xdSvcA6+UzEzTtj3MQKDdYmgXiL1IBSjM98dR1Qi9UFoRhanCZW8cgWVO5I6Tb0ViiQ5Mt9Y0WkeU03JE5X8H++3TLWHDRJfgiLQKQ40YpgoXqj42AD6njAARQ+Vo31cR+gWQR0KWZJQsUle+uhUbDKDBjF+/jAFyJzyKbjlY5AQuQShEhPjpo/uNnhpC1Xo9JIgYVol2SD3rlQsfGW5saVhaYuOMpWUHj7sxjy63Okiu5vkzM5Ll8R1XcDyVHG4ESZgxanIfTFBPL8cjUPwV0ejGBJvc81O7p8oTih5pJ//+39ZkFQv',
          'content-encoding': 'gzip',
          'content-type': 'application/json'
        },
        gridID: '1',
        msg: 'MAP-DATA',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function firstMapGrid(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        anchor: [16, 72],
        cleanId,
        gridID: '1',
        height: 144,
        msg: 'MAP-GRID',
        resolution: 43,
        time: `${new Date().toISOString().split('.')[0]}Z`,
        width: 144
      }
    };
  };
}

function fourthFullCleanRunning(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 65,
        cleanDuration: 9,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: 'scheduled',
        globalPosition: [
          -1908,
          1908
        ],
        msg: 'CURRENT-STATE',
        state: 'FULL_CLEAN_RUNNING',
        time: `${new Date().toISOString().split('.')[0]}Z`
      }
    };
  };
}

function fifthFullCleanRunning(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 55,
        cleanDuration: 10,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: 'scheduled',
        globalPosition: [
          -1997,
          3612
        ],
        msg: 'CURRENT-STATE',
        state: 'FULL_CLEAN_RUNNING',
        time: `${new Date().toISOString().split('.')[0]}Z`
      }
    };
  };
}

function sixthFullCleanRunning(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 45,
        cleanDuration: 11,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        fullCleanType: 'scheduled',
        globalPosition: [
          -4682,
          2583
        ],
        msg: 'CURRENT-STATE',
        state: 'FULL_CLEAN_RUNNING',
        time: `${new Date().toISOString().split('.')[0]}Z`
      }
    };
  };
}

function secondMapGrid(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        anchor: [12, 76],
        cleanId,
        gridID: '2',
        height: 144,
        msg: 'MAP-GRID',
        resolution: 43,
        time: `${new Date().toISOString().split('.')[0]}Z`,
        width: 144
      }
    };
  };
}

function thirdMapGlobal(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        angle: 90,
        cleanId,
        gridID: '3',
        msg: 'MAP-GLOBAL',
        time: `${new Date().toISOString().split('.')[0]}Z`,
        x: 80,
        y: 2580,
      }
    };
  };
}

function secondMapData(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        cleanId,
        data: {
          content: 'eJztncuS6ziOhvf1FB1n7YV4J+tVKs6iLxUzHdHR3TEz3ZuJefcBCFCWJUpwZsopW0YgT1X+KdsiP5EgCFLy//74899+/+Pff//Lj19/+224qG3Yz8svimjbFJFoikg0RSSaIhJNEYmmiERTRKIpItEUkWiKSDREZC5rdnjxnsEUkWiKSDRFJJoiEk0RiaaIRFNEoiki0RSRaIpINEUkmiISTRGJpohEU0SiKSLRFJFoikg0RSTaHNH0oCKqpisgoiki0RSRaIpINEUkmiISTRGJpohEU0SiKSLRFJFoikg0RSSaIhJNEYmmiERTRKIpItEUkWgyonlm++0WAK7pfRnQm66SbK2A3APoDSD119E+Ami5tHQycGtLjR8BdPLWJa3GKiRFdDSiU0B6NKITYPoORC8OaRvRfnZ4RRWRIlJET22K6EkQHV5NRfTyiA6v5LMjOryKz4/o5SHNc9eKaBORtiIBkQISECkgAZECEhA9CpAiUkSKSBEpIkWkiJ7IFJEiem5Eh1dOESmiZzFFpIieGdHhVdsT0VUqoG9AdNIds3siOmFLeiSik0DaRqQ7ZgdpHa3/V0XUqeBbQ1pfarx94RtDuhfR3BSRIlJEn0K0vMVTES0QfWTy8LaI7re3AfRoRIdX70hEW5AOr9S+9pVHGbwFoP0RHV6h/W1fRIdX5xG2J6LDK/MY+9pjVd4A0NefPHN6QPvERYdX4rH2OURv4oXIPoPoTQb7ZmuItiquiGZVf6PZ2L2I7kPyRpD2vB/t8Mo8P6KTYtr/rsbDq/RoRArpDkTa2RSRIlJEiugJTREpIkWkiJ7BFNGnEK1DestHhG9nHe+v/GkBbeWuT9867jX9BgfRFJFoikg0RSSaIhJNEYmmiET7+fPyh19+/ONP//37f/3797/8+PU3BbZt2qZEmz7C8PDCPKdtf4mT2tB7sMHhRXo26z374fBCPZetfxWYAmO7/wkihxdVET2tKSJFpIgU0TOYIlJE34fo8IIqoic2RbQbojeGpIgUkSJSRM9gikgRKaKnQvS2kBSRIlJEiugZTBF9GNHWPv43NXl/0ZsD0i1Yd5giEk0RiaaIRFNEoiki0RSRaIpINEUkmiISTRGJpohEU0SiKSLRFJFoiki0jyF6y9TaFqJeovbwAn+/Ue66f/DetH9DN/3viWya3v8KohMvCazfj7bXF6YeXsXHINrTDq/i8yN6eUjfgejFMSkiRaSIFNEzmCJSRIpIET2DKaInQXR4NZ8f0UtDUkSKSBGdBtHh1VRED0e0V5b6xIge34peGpJ2tMMRHV5BRaSInsEUkSJSRIroGUwRKSJFpIiewRTRwYgOr54iUkTPYYpIESmil0d0CkiPXgE5vILPj+gEkBSRItoH0XblFJGIaA9MLw3qPkRfB3V4Rb+GaK3y/bcooruqpYi0FS0R9Sq+9pa3RXStvvymrcesng7Q5x+IIUM5BZ7PI7qvvZwAz6MRncQUkWifQbSF54SQPo7orVoQ2p6ITgppHhdJaN7wwfPL0LFf2VMHhx9B1EehiERE99nhVfkORJ/Hc2pYj0B0MkyK6EOI9oR0eMWeH9GJID0O0WlAKaIPItof0+EVVEQHINKO9s2IDq/e/oi0DX0zosMr9+yIDq/asyM6vGKK6DBE+0A6vFKPR3TfQtDbItqu/D2p/8Or9B2I1mHJrzy8QvvbPUuNp638faZfuCOaIhJNEYmmiET7+fPyh19+/OPPf/7XP//6+19+/PqbAts2bVOiKSLRFJFoikg0RSSaIhJNEYmmiERTRKIpItEUkWiKSDRFJJoiEk0RiaaIRFNEoiki0RSRaIpINEUkmiISTRGJpohEU0SiKSLRFJFoikg0RSSaIhJNEYmmiERTRKIpItEUkWiKSDRFJJoiEk0RiaaIRFNEoiki0RSRaIpINEUkmiISTRGJpohEU0SiKSLRthD1HpVxeIG/3z727IdbTPO/nRTgRzraGq61B7McXrnvR3RFdfv/5XEzYju8gl83ddeibbvrw4v3DPa4VqQd7Q4znd9e0L7HFymiFdNW9AFTRKIpIsFePNJ+PKIXB/Rdrejwan7FFJFoiki07/FFLw1JEYmmiETTuEg0ddeiKSLRFJFomrsW7bGIXrz9kD02paaIRFNEgmkrEk0R3WGKaNN0qfEO044m2MvP8JspItG0o4mmiERTRKJ9z7aHl7bHuuvDq7eHPQrRacYzbUV32GMQtX3Yh1dvD9NWJJqOaKJpXCTao/NFJ4CkiERTRKLd+0W7n/v4t0E0vdfsoyc4QZT98Vv22m8fedfh1fyKfe6WvY9W+6Uh6bYH0T6PqNcyXrq1rNlnEa3DOIGDvrV9EV1vGj4Rpj0Rze/iP7xy+9h+7vp0HayZIhLtc4jeZjRD+2zoeM/fTmIfR9TvUKcF9FlEhxf7O00RiXY/orXnzZweWA/R9nOIlvrk7Wod0XUi0Xug0/Idh1flOxFNQV2fcLX9Qfe+7gVN9kXXqm8/Me20HW6PCchp4ZDtNUc7MaQ9E7MnhaS5a9EUkWiKSLTH7XU8vGp7mbYi0RSRaI9AdKJOhqatSDRFJJoiEk0RiaaIRFNEoiki0RSRaIpINEUkmiISTRGJpohE+/nz8odffvzr73//47//+h9//NPffv/x62+/GX8x7mKGS7h5ccA/4QF/WXnFmxm2Lxsu1l+suZh4KeOhghL+iIfCZfqad2aGvJy/OHdxA0IxmZEV/BX+AH/Gg/4yfdWc7VsZIvPm4gFMujhbqUVoQSMxiwfgML5ofN0C8JsZUgvx4vPFVz5Iw1TvFSfQLL4AXja+ckH58IocAc4M0CFLRVK7o62+Hvi54YoNXoCv49d2Wb+dETxToSARg24Mm5JHhkASu+kEnbm0V/eRH16hZwIY8Hf4S4gzeuaG5OxtJ1t0uBdhtwP3muDtP22C6yPHzP/RsDH+qPdbjVRuhtwxTjG3AcubD7mdoLgbEvt5bPzOAV5v5rWcd4X5BOwtefHA0Ju1z2b1Xqf3I66Vg6f7SuHPmWbXRFNEoiki0RSRaIpINEUkmiISTRGJpohEm0bX89tjNbKudjNf60zPlj/vPWFrzMapficFsPx586RAgzamlDqppuXPuyefGrUxfdlJay5/3j7R2bBdl1sXafTljybWR27XtdaV5Zu1f7qsM2FoZnh6y4hr/3R58ZZjn9H2MW2ONxhXe+zGIXWLU4bdMWNtINFRuGHrRiirYYuGfNdAeRkNr4fIOrvgKdly3rU+GdNpLE39l/N7nfRvEFs8HEtzSRPTrKRoikg0RSSaIhJtC9HSfb+lA+/vP1regSWvBCyfpnl45R4GbBkQdW/E2lwKQMJrKy6u/Z3PcrIGOt9V2aq5dnvW5uoAQl9bhfHt7+OZzhjTznf2Xmvb3RK9togwmYGvrtC49vfxXOecWN1uMr+t73J7/toCw00uaG35Zmh/H8910gn+7ZrMrMKdO0aE1YdpgnJtkQeP5svNSc+ac7pduZlXeeje0iStS0xBby8HwSE+Or76vEnR3gLPtNrjquPiDrz7Fn/uWziaHr+vl0zT+C8Kv78m1Kuy2GQ/dmXmr7/3vd2lqBd25tfVpPvbcNcldK9Mb9F06lw2h4W1FcLJauqL+3qEf3f1zZQftr5x2LsdLG/89lWMQ+cyvukU4fZ903jmZb19DQHFiq8u3OPPNOSzt5FIvhkWO5H55CJd4c6vzcsvz1bIK9WeAje3zQvd6GyamG6a3U0T7E4lUy8ounEZ8zb9qtsI6vSwV/WR921NJw1qOrG+0nbz37vrbfSazqMImq/peuiX3O9S8xmLql9p20V3bV7xmgGaZo/8/Pdr5mJMF8WVJ2XQRTwd3lnFr63SLrq7vRngzU0y081/D5NE8HgP+fIBEAM7pHNBHas9Msmdnt4Pl9a+eqv+rfsUiJO2zFsopdOTPxzrdx8DQYP+zJOeCx/aord+NHDsPgmi+zScc8Db7wsmp+n3/jNwThINfbGFrdKL689fotb28rH6Vz3bms08XmeQeHl0d42qn2K38sylE3XY7RDvC+jkR36dYZjYmIB8zfe9Cb9FMmA/Z/QmBDsp3V290pYPPA/Djcz212s1jf56g/ApBpKV1Pguleo9KveMgUynUexTp9nMbZIKXm3yL7qusOngv1KnZebgOnCdJZ+99IZ7Vmk7mj7NzcK3exh2HUyEkKad59XXB5f7QL61GfIq4wuvZi/78X7D40Yqv+txXxLfbWg9WxzdcxiR+L1cOD3lt79L33oI9tno7fzQkY2x44z09n7QzdmnwVN2uz5Z6Q3SMFN2e94A8W7oxtubzBer8U6dtff47y+1vneKULoPm/+K31skWk6Nr//9Bl8ac1fTLKdJlk4Bdu4h633/xgc+dmyAgvN78STLFWH3K13mc44Pu0XJCZ4jYXrDceWbheY3WHzcR0qTtjN5xXHycUfAtvyexDs85h3TuA7PF81iCTynjXP5DYqTjcDbLvT+9rnnAv+xQPs3hV7vmlh8sSL70jDZM32zy3eGtLfNsj+Qn2AgqkRtb2FoekvKEvXtTbL9LemLb/qZ7cDsRpgniZHa/qReGEgNc6OvTu+Lnd3wvT6E3T4m4b7Z44v61nZ/0FoFu6PXzYrMlfLsnu/NQa3Mb7BYnyG9tq/lHO5iiJ7cZ7Z5F/OU883dzyvjHCKabfGWF2pe3wEv2vG0ZXZqffuv//flTZozSr17EbZQ772K9ESs+a7UtX48H/eXrXxxD/Js6Fq7c2bdQZ9kltt3ILzEvzGx73077NKxbMxj+zd/3d5z1zn7y0cda81bWC7r3FC8BX/WLjv3It2OqJ0reoYZ801wN6349tLk2ChXbmdeTVx07pnrDKyzYP0UTXrZl+cg5lVlT9u7yX7jLrmVlMTNFenCfvkopKVyZ/dx37bueUV9737mBb+by7KcY655905i5MU3hdX1hlnQtRinOtmf2U3NKzcgj5dla4DbTLM95g6kAyDP5hDL6Gtey/ENeZNwo7QZtE0cUs/F7H973AGA6VccrpfTiEX9ZlvfO/mldmnWM86j310Z1V78gY3ze51nDXiaqaRDi63w/VuQxKUfe6IhbB3o2reLjz+tCW8s2gr9/6xj1hrUje+4n/yMTlfOQ96b4znFINWn2n3Cbfcxq5OwdnuJbuVWo5vsvV313C9sjWo3G7DyhNtZqLuaHJi+buXWz9ow93+MwFMg7a6MdX9uUsTz5zrdDELdae2V6Kk6fIfoxvJt/1/3IVndHr6Zv58tfJxilJrj3dhusPWvy/wuwnOPsf89BE9kdzPuzT23uM+Hss2dDZvZ98MZ7cdZdhVrSBfz05W1/H6WbCNp8JKZW4HxygC3mhlfy7H08jN3XbnTN+NuWLb26NmZA+7frSy4ntmwd4KnXffB9mYRK0+aHcelMVJbTrUkr3t9uT3X/GFrtrv2aNnrRGL5zQ325ukZi+1U58zFrgOd5GRm2e1ZmnZKYoFkto1sZd3rjKkY+fGIy6zsdJI/n+3fuIw0n+T622dunCyUXf8isMXjYf1tRnY2vx+9xIzk2prlmQZ4+Qvn2oN0h85XgI1JbbPwAb0VxTMFn1//pr4pPL8yxrxda7sT3Ma+irfxch+xjYn+Sfa87s9sbdI+3015uhDvC8DW95WdpkOi7cVrY8/jSboi2n5farucI59pooW25xcA7/LY+qe1fb8q+ctPqH9ae8RXSr905L40/dZt0RSRaIpINEUkmiISTRGJ9vPn5Q+//PjnH//nP3/8+ttvxl4SfiG7D+7npaoEgaAfPCp3SRlUzJYVho3Zssow2CczBFYQQCbr6/v8JVtQwWZWEDalGCMriKNS8YUVBuhDrq8MpKwtrKBk2cWmoGQ5mIEVlCynYlhh+F9crUOsqgzJkipQzmINHytQzuKCZwXlLNEFVjBrH4aQWGFS3ZtIZ8BjuYwK5mCFXxnwMy2cnOsA57PF52EsmRuSj2OpHVQ2jDVyZqBS19o644YyknDGey4LMHMm5MIKpjYAuymYIdqBuES8Ds6awHWHa+QApyEF189Z7/gYXFtnQ2ISCSeWuXGBNuFsKU3hfY02RVY4t2IuoHAVJxYuZ4KyuEwkQGFG19h2DL9myEYuZ4SyeD9wySKUxXM7A4X7w7idgYKy+JLaK6EsgdsZKChLcJmvX4SyhGBryRKpSPUDBSULeUisoGSh+KagZHEguukSoGTRJccKSha51YGCkkWQrKBkMRU+e4CSJW5nidTYskBBXWMcj/khp0opV2WYfKnKMmvKkVimS8ccc6GEZrSmvS+B8qWpDIrPXrBGPqb2mVBbH4tJrMzFJ2j1rCwobskZCfrkU2HlQcEJWUFZUhmawlzO4JuCsmRH5cx4bX2GsrHCFFkcuJwJs2c5R1ZQljLYpqAsUEyuA7QzX6DvsIKyFO4dBdsnNBDqDwXbri85NgW4B0NXrGCbD4MtTBD6Q4BOzAr6ShgSXb+C/SgMpfGEPhaMaTyh/wXDPaBg3wwmxHYsgKIrZgfs08GUShAVlMUa0xSUxdrYFJTFhkoQFPgQaMfZsYKyWLruqKAstsTMCsoCToTLCT4rOO4doKAsLjSeBcoCzXXg94HKxBrm/lX56pOtJUXexjpSxNP6qgrxRBWgkVDvQOVBkU9G5UCRT0ZloQGRT8bPNKDIC+MxVORbUQ3QuKLh8wGXGFLkz8zY1KidoYKuG03m92V0FdTqbB1XYqRWZ+uYE8EVJFZQspjrmGPrWBUT9U1UUJZE7QwVlCX59j5oLzGF9r6EDX3kAu0s5qGRSOjSRkrQPsG3EZeALTlmanWo4OyZej8qOHumVocKzl6Mi6Sg58RCrQ4VdqVQRyBUcPbCrQ4rDqrk9kr8IG51AXtxGsjvooJBfQieywK9Pw3czgJ6hjTA4MhqgMJROwMVsLM6IhHQ2yRD7QxVuiDOwAoHhbGFgAdLxUY+e1VclkiKz159JOKs58ukyM/b6hXzwOerfhCChDrK4LEEDQGGSlYZFJPPWOocmXz1gzlSf7fVD2a4LHwGIAFXtvDZgVJO1Ptt9YM52cTniziM83WofjAnvg7VD4KXd4UVlCUbyyWDq5kzjYagEroDJpGxTUCHy1wyaC8583XI6PkgDCEu1fPlYj2TSBj6BCJRPR+8ja5D9XzQsMLACtzBQFGXrZ6vDBRn2er5ILQx7Ri4gwEcPSvoukOpEYStng+KUkdYN2CPK4bGP1c9XzE0Hrnq+aB/V4Kuer5iqL+76vmKHeoVc9XzFUsjkKueD6pX/aernq/A0DiwgrJYGoFQocMxNYJAhQGbc1yWgqGdj03B2V3yXDLceuYHx++rizienUGpy2AFijM0CeHiEEoOTWM+MPK1QQ2hLQwaZnw7hLMAoZWfdKA4ZzyeKN4djye6Es40TR7VWdYQxFRyrmlXW63zpC1HcVVDAG055hqPc5zlAmsIZ8z09blU71c1hNwQyxI51zSN3lVn1MwSNdTJcZyD5cXEqXcU+WN9qvZ8vnY80KgwHgfXnybHYaQdxvrD5wceRdr5gudWxuUJUNxp+QOPLLW+qAN/fqD6hsBtKLKOXN9IfAI4s9A0fCaMk9RaEuk4UE+tGjc/sber2qKG9tXej9px+SOtssKQZcbz4aq/4/rQSj8MqHS5IzZcnJqFwrLO1JxvR/G2Ys99JZDM5LScZ0mjh3MswcMRWJJjO62fXILnZo/nTYMN3GrxKDJhN4PvhdjLsr+AngctmBDbGpCApP6MEmeCjgZ36sIgrz0aZ43u2qVxSumvfRrnmz7ViaozJAO3NVfnfCmN9SXp25UnGcjJRJbkp11iSdMWl1mWOoNzhSS7aj+wpDbpB5qHglPyLHGSCn7I8XtRYjTDEl8c+byZJZ83seTzRp7hcudtE14/WK5glRQwYvWr5JZo2+w4NgnFAE89yoiSm4qtk27oI6ZJjzJRo7d18g4u0HKfq9KX1uXqi8MVe5U0fcEyV8lXIbIMdMkSfVTgRphZXrGjjBS3INgqbS2VN/RRUF3LR6ssMU2OJooX6CjMcOLko0CW8QrCXH4wVEGUMJkfrKlxMn5UlTSdRzmgzHU2ChLGUTs4CiLwvXi0+fPCkqaSWMEqafaI1a+SnXtkSfEyokOZ2TV4luzpHUuYdvA1qjLVGaWjQhqe9jsqpGle32AkYMGrkhOvCR/MF3j22QnXBQPE7XSiKjN3yUCycPOOLNlfJpY8PGSWqfoNV1jmdlFQAvd2yUi6ockAkmfFKOEK2kABi7csHV0FixESSCLpHclIswuUcH0tT2q9Z0k+h4+ifwr8UVVSDIXnrZLSBljmKilv4ApJT4kDl1ny1U8sg2F3TZKvfmDJV59K5cI4spHkq8+lgoGMx0WYJVh0kzwsQvAIkmYbICGyBGlb0ABhJ0ges1yV3pbqveG8VToC6wJLrkJkyfFIYkme32WWXIVCkgdnYEWSquANS2rA3rKkiBCuAkTKIGmkg2tEksZtH0jyMO3r9MbCQXpvzQiBJK/ia0oIJDlVX3NC0HDYeweSgcYUPBHupQjUgLEYVVK2DgtZX8wt1pCMg+XGQJJaLFa/SporusySJtEusUwjWJSJpouOSwVjKjcGkpYco2PJV5BLlShxhxKX8NtAT0chJGkhGNYoDpQ9QwktJxq+3I4lxbcoB5R8uT1Jz9Gdx9kTSKLhAslAGQQXWVJ3donlSIMkTZ1dYUk0gCRJmkrjHjSUkdozSmgqiScrfDRBUB35KkAhk6dP5o9Knj9qoPeG1BphlWkcyknSoNnemymj3Y4W6gvtvYViOz6Kr+WrX6XlWKVm4yzGCMPkqOcemlna1nFI0rx0PJoIbDvKAy4fbQMuHw2hdUmSY2CDNQIHbNg/I6sCAxCHn1XmMRrFxgBzF3pvrC2nwHA0abFQiNSkRzlGzpiVHQZKzDhK2Q7DOExgPncYOISobgQkDxOpZoKHgZtZqmliGI5paEs1hzxwIpycKkhuSJllqikSV6fKIOnquzpXdgOnxlFiIe14FTDHPaA/Y4mFtONVwOw4dHXDYzemzoc2s6pDOUi+KHXK7AY3XhTMyA+cMKdIBiRHQXXW7AZOmbuaMARJWQKUWCp3jYKwVJ6iIAogQdrI/aguLniOCXFKj5IGel/jZ5C5BTYFS+UpJvQ19nYDhwG+zp9BUhjg6wQaJOVYfJ1BgySv4g3JaB13yYKF5LQ4SiwkpqpY1hUQShN42lYK2kXTtKkrJGU8juVMPBaYOqkCzYOBZZ3T9f24GmKJWDtuPIRs5NNJRx55HH2+KRxpOCoP9HLiYqk84HC8aRqPJ44Pba0O9PwRRZXUPrnu0EzyhAwOtvbKDUoawpWqS2Pkie/FxARfa5Kjz8QTwVzQctuuMo9tu5bSccqxTj2rnkxVoVaOB02eekLoEvM49bR1rSaMqQHUliOBQBR9m7qGOvXF9ZvAYwVr9h6UasDgJ9umcRWHx9I6VUcdfRin7rjm00ZE27S9pj5whQgGQZ794dQfwyc6H6UOcM0ocVLJGKxfm/CixvoUTriVpjndiBpXiwZy/jY3nYfcNK408dhYNa5DmTEVRhqCtNx0rOtUxTedcKWK+QysIyd+2/FrQpD1mA6tGmIPSgajrutSxKNqXLXikQ61xTWtMCY0q4ZRNJV2Png9Rhy5lQe15esz8OsdN0bDug1DqHFbeJva2abjmLqxuG81cErBsOYp2Xicl3Pb+zN4STs5DuPrVTvUPIag9qh51DCsLQVn43EbSpweTxz54PGAGkLwdrxqmlLU+ldNPg15kebljMya52eVd9W8pBGb9i2VT+fP5NXwdg3Sxfqm4fWFR5Sqof0U9B1NJ9TejO/PqMcFANKc6qvnR5058ZKapnlsLX/VaWxvUEfQZUitvqgLj+hD0zykm6ZpgaXyrPrav0F7iAgoivFNc7QcmvYtI8M6uZYqs7iPPl5TYxY3QccxNYZ8QMfRn1lcU42lpRqQL8xFxmimHodObEI7f9WlxTOkHW1LaJ9v28QosW6pw0TlsYHjodw0J1Fyez2nttrxyO25NM3+cTxOS6H1OL6fF6La5zk/+Xx4PXToFp9UXtCBS0s9kva+jQfIFzrkmHokTf4BNVxjnMuNfOEae5fH1A30PNCFx4vQdEnNv+PrAVhLULAOQ+uvpDmXaZqmZWhsX1W7YfSfpJ3nJRHW3H5T02ls76RLW9gi7Qff+hNpar+2lc+Tv6l6QE0zQWubpiAVN0eTpqmhHVjz9MdS/4DDtFhnStPcP9vxQltiqobrEwZeGBvoesL8v9im4XrD0bY4Xdsnhv2haY96XPBH/wia/ctQ/SfmHgwv8KJ/hcGWFy8NHw+8YGn5eOAlS9uOU/zQjsNE8ubzYCYZxvqgdtS/2/kjZwJr+fB4oDAXH6RUdeTtIJlfHymUNalpym/iEzNJ8+aHwDqNG0Ho81KMafL6xEm+qnEvgxu3wlR+yYW2SajyxeFp/LyIex9M28BRr0/y4yaYev1g3hly0wX1uPWltg+YeZnSNO6zCOOGl9p/oHl43p5i8FYimIq2DSp0nMeP9vp83TpE2pi2qaq+P7cNQ65p8qfGsm7bhCy/v23/MazbRh7unzmFwTWNOzUgKm/Hsb458xYoQzwyjzdV4x6QHOl6GeKZebwxNP76zOOLofHa4w4u3zSWl1OLhttr5tyi4fZX2pYaik9wbhNujidqn4bbZ+FNKaVJ+rjC785I9+f//T9GH55j',
          'content-encoding': 'gzip',
          'content-type': 'application/json'
        },
        gridID: '2',
        msg: 'MAP-DATA',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function forthMapGlobal(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        angle: -180,
        cleanId,
        gridID: '4',
        msg: 'MAP-GLOBAL',
        time: `${new Date().toISOString().split('.')[0]}Z`,
        x: -4937,
        y: 3462,
      }
    };
  };
}

function fifthMapGlobal(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        angle: -180,
        cleanId,
        gridID: '6',
        msg: 'MAP-GLOBAL',
        time: `${new Date().toISOString().split('.')[0]}Z`,
        x: -4937,
        y: 2946,
      }
    };
  };
}

function sixthMapGlobal(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        angle: -180,
        cleanId,
        gridID: '5',
        msg: 'MAP-GLOBAL',
        time: `${new Date().toISOString().split('.')[0]}Z`,
        x: -4937,
        y: 3204,
      }
    };
  };
}

function eighthMapGlobal(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        angle: -180,
        cleanId,
        gridID: '7',
        msg: 'MAP-GLOBAL',
        time: `${new Date().toISOString().split('.')[0]}Z`,
        x: -4937,
        y: 2688,
      }
    };
  };
}

function ninthMapGlobal(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        angle: -90,
        cleanId,
        gridID: '9',
        msg: 'MAP-GLOBAL',
        time: `${new Date().toISOString().split('.')[0]}Z`,
        x: -4658,
        y: 2580,
      }
    };
  };
}

function tenthMapGlobal(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        angle: -90,
        cleanId,
        gridID: '8',
        msg: 'MAP-GLOBAL',
        time: `${new Date().toISOString().split('.')[0]}Z`,
        x: -4830,
        y: 2580,
      }
    };
  };
}

function twelfthMapGlobal(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        angle: 90,
        cleanId,
        gridID: '10',
        msg: 'MAP-GLOBAL',
        time: `${new Date().toISOString().split('.')[0]}Z`,
        x: -4121,
        y: 3366,
      }
    };
  };
}

function userRecoverableFault(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        batteryChargeLevel: 45,
        cleanDuration: 22,
        cleanId,
        currentVacuumPowerMode: 'fullPower',
        defaultVacuumPowerMode: 'halfPower',
        faults: {
          AIRWAYS: {
            active: false
          },
          BATTERY: {
            active: false
          },
          BRUSH_BAR_AND_TRACTION: {
            active: true,
            description: '3.6.0',

          },
          CHARGE_STATION: {
            active: false,
          },
          LOST: {
            active: false
          },
          OPTICS: {
            active: false
          },
        },
        fullCleanType: 'scheduled',
        globalPosition: [
          -5056,
          2366,
        ],
        msg: 'STATE-CHANGE',
        newstate: 'FAULT_USER_RECOVERABLE',
        oldstate: 'FULL_CLEAN_RUNNING',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

function thirdMapGrid(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        anchor: [12, 76],
        cleanId,
        gridID: '2',
        height: 144,
        msg: 'MAP-GRID',
        resolution: 43,
        time: `${new Date().toISOString().split('.')[0]}Z`,
        width: 144
      }
    };
  };
}

function thirdMapData(cleanId) {
  return function (machine) {
    return {
      topic: machine.topics.status,
      payload: {
        cleanId,
        data: {
          content: 'eJztnduS47iRhu/nKSb6Whc4H+ZVOvpibE/sOsJhO3bXvtnYd99MZALiASRUXVJRojKyeqb+oiQCH4FEIgFS//vtz3/74/e///GXb799/64uYjv24/KLINo3QTQ0QTQ0QTQ0QTQ0QTQ0QTQ0QTQ0QTQ0QTQ0QTQ0RKQvW3Z48Z7BBNHQBNHQBNHQBNHQBNHQBNHQBNHQBNHQBNHQBNHQBNHQBNHQBNHQBNHQBNHQBNHQBNHQBNHQBNHQloimBwVRMVkBGZogGpogGpogGpogGpogGpogGpogGpogGpogGpogGpogGpogGpogGpogGpogGpogGpogGpogGtoY0TKz/XYLANf0/hjQm66S7K2A3ALoDSD119E+Ami9tHQycFtLjR8BdPLWNVqNFUiC6GhEp4D0aEQnwPQViF4c0j6i+9nhFRVEgkgQPbUJoidBdHg1BdHLIzq8ks+O6PAqPj+il4e0zF0Lol1E0ooGiATQAJEAGiASQANEjwIkiASRIBJEgkgQCaInMkEkiJ4b0eGVE0SC6FlMEAmiZ0Z0eNXuiegqBdAXIDrpjtl7IjphS3okopNA2kckO2bVaB2t/1dB1KngW0PaXmqcv/CNId2KaGmCSBAJop9CtL7FUxCtEH1k8vC2iG63twH0aESHV+9IRHuQDq/Ufe0zjzJ4C0D3R3R4he5v90V0eHUeYfdEdHhlHmOfe6zKGwD6/JNnTg/oPnHR4ZV4rP0cojfxQmQ/g+hNBvtqW4j2Ki6IFlV/o9nYrYhuQ/JGkO55P9rhlXl+RCfFdP+7Gg+v0qMRCaQbEElnE0SCSBAJoic0QSSIBJEgegYTRD+FaBvSWz4ifD/reHvlTwtoL3d9+tZxq8k3OAxNEA1NEA1NEA1NEA1NEA1NEA3tx4/Lr798+8ef/vuP//r3H3/59tt3AbZv0qaGNn2E4eGFeU7b/xInMdV7sMHhRXo26z374fBCPZdtfxWYAGO7/QkihxdVED2tCSJBJIgE0TOYIBJEX4fo8IIKoic2QXQ3RG8MSRAJIkEkiJ7BBJEgEkRPhehtIQkiQSSIBNEzmCD6MKK9ffxvauP9RW8OSLZg3WCCaGiCaGiCaGiCaGiCaGiCaGiCaGiCaGiCaGiCaGiCaGiCaGiCaGiCaGiCaGgfQ/SWqbU9RL1E7eEF/nqj3HX/4K1p/4pu+t8T2TS9/xlEJ14S2L4f7V5fmHp4FR+D6J52eBWfH9HLQ/oKRC+OSRAJIkEkiJ7BBJEgEkSC6BlMED0JosOr+fyIXhqSIBJEgug0iA6vpiB6OKJ7ZalPjOjxreilIUlHOxzR4RUURILoGUwQCSJBJIiewQSRIBJEgugZTBAdjOjw6gkiQfQcJogEkSB6eUSngPToFZDDK/j8iE4ASRAJovsg2q+cIBoiugemlwZ1G6LPgzq8op9DtFX5/lsE0U3VEkTSitaIehXfesvbIrpWf/ymvcesng7Qzz8QYwzlFHh+HtFt7eUEeB6N6CQmiIb2M4hO7JrvgejUY9dXIDq8Oo9HNJ507EM6vDqPRrRX0VEHOy2gPqJe1QXRjRDeENA9EZ0W1iMQnQyTIBJE90QkkL4c0WlArbOOgmgX0f0xHV5BQXQAIuloX4zo8OrdH5G0oS9GdHjlnh3R4VV7dkSHV0wQHYboPpAOr9TjEd22lPi2iPYrf0vq//AqfQWibVjjVx5eofvbLUuNp638bSZfuDM0QTQ0QTQ0QTS0Hz8uv/7y7R9//vO//vnXP/7y7bfvAmzfpE0NTRANTRANTRANTRANTRANTRANTRANTRANTRANTRANTRANTRANTRANTRANTRANTRANTRANTRANTRANTRANTRANTRANTRANTRANTRANTRANTRANTRANTRANTRANTRANTRANTRANTRANTRANTRANTRANTRANTRANTRANTRANTRANTRANTRANTRANTRANTRANTRANTRANbQ9R71EZhxf46+1jz36YY1r+7aQAP9LRtnBtPZjl8Mp9PaIrqvn/18d1w3Z4BT9v4q6Htu+uDy/eM9jjWpF0tBtMd357QfsaXySINkxa0QdMEA1NEA3sxSPtxyN6cUBf1YoOr+ZnTBANTRAN7Wt80UtDEkRDE0RDk7hoaOKuhyaIhiaIhia566E9FtGLtx+yx6bUBNHQBNHApBUNTRDdYIJo12Sp8QaTjjawl5/hVxNEQ5OONjRBNDRBNLSv2fbw0vZYd3149e5hj0J0mvFMWtEN9hhEdR/24dW7h0krGpqMaEOTuGhoj84XnQCSIBqaIBrarV+0+3Mf/zaIpveaffQEJ4iyP37LXv3tI+86vJqfsZ+7Ze+j1X5pSLLtYWg/j6jXMl66tWzZzyLahnECBz23+yK63jR8Ikz3RLS8i//wyt3H7umuT9RypvYZRPMWc1JAnxvRrlBO56Kn9rOh4/ovgqhZ/wErh1fkcfbzE5Dp/iFBNLP5w3kOr8Dj7XZEb9FietZDtP8copE+nW0jukY7vQc6rd9xeFW+EtEU1K0ptOuTsA6v0r1t7IuuVd9/YtppY6N7zNFOC4fsXtPYE0O670z/8Oo8wiR3PTRBNDRBNLTH7XU8vGr3MmlFQxNEQ3sEohN1MjRpRUMTREMTREMTREMTREMTREMTREMTREMTREMTREMTREMTREMTREP78ePy6y/f/vX3v//+77/+x+9/+tsf3377/l27i7YXrS5+9mKPf8ID7rLxijczbF/GX4y7GH3R4ZLboYwS/oiH/GX6mndmhrysu1h7sQqh6MTIMv4Kf4A/40F3mb5qyfatDJE5fXEAJl6sKdQCtKBGzOABOIwvaq9bAX4zQ2o+XFy6uMIHaejivcIEmsEXwMvaK1eUD6/IEeC0gg6ZC5LSHU3x9cDPqis2eAG+jl/bZf12RvB0gYJENLoxbEoOGQJJ7KYTdPpSX91HfniFngmgx9/hLz4s6OkZycXbTrbocCvCbgfuNcH5P2mC2yPHwv/RsNF+xPttRiqzIbfFKXoesLz5kNsJirshsVvGxu8c4PVmXut5l19OwN6SFw8MvVn7YlbvZHrfcG0cPN1XCv+cSXZtaIJoaIJoaIJoaIJoaIJoaIJoaIJoaIJoaNPoenl7rETWxWbztc70bP3z3hO2yqxN9TspgPXPmycFKrSWUuqkmtY/7558qtRa+rKT1lz/vH2is2K7Lreu0ujrH0msN27XtdaN5Zutf7KsM2GoF3h6y4hb/2R5cc6xz2j/mDTHGcbNHrtzSNzilGF3zNgaSGQUrti6Ecpm2CIh3zVQXkfD2yGyzC54Sraed21PxmQaS1P/9fxeJv07xGbZI8klLUyykkMTREMTREMTREPbQ7R232/pwPv7j9Z3YI1XAtZP0zy8cg8Dtg6Iujdi7S4FIOGtFRdb/85nOVkDXe6qrNXcuj1rd3UAoW+twrj693amM8a0y52919p2t0RvLSJMZuCbKzS2/r2d65wTq/km83l919vztxYYZrmgreUbVf/eznXSCf58TWZR4c4dI4PVh2mCcmuRB4+my+ykZ805zVdullVW3VuaRusSU9D7y0FwiI+2V583Kdpb4JlWu606ru7Au23x57aFo+nx23rJNI3/ovD7a0K9Kg+b7MeuzPL1t763uxT1ws78upp0exvuuoTulektmk6dy+6wsLVCOFlNfXFfj/Bvrr6e8sPW14a9+WA589tX0YbOdXzTKcL8fdN45mW9fQkBhxXfXLjHn2nIZ+aRSJoNi53IfHKRrnCX1+bll2cL5I1qT4HrefNCN7qYJsZZs5s1we5UMvaCopnLWLbpV91GUKaHvao33vOaThrUdGJ9pW2Xv3fX2+g1nUcRVF/T9dAvud+l5DNWVb/SNqvuWr3iNQM0zR655e/XzEVLF4WNJ2XQRTwd3kXFr63SrLq7mQ3wepbMtMvf/SQR3O4hXz8AQrFDOhfUVu3GJHV6ej9c2vvqrf5TIE7aMudQcqcnfzjW7z4Gggb9hSc9Fz60VW/9aODYfRJE92k454DX+86yn/vIafq9/wyck0RDn2xhm/TC9vOXqLW9fKz+Wc+2ZQuP1xkkXh7dTaPqT7HbeObSiTrsfoj3CXTjR36dYZjYmYB8zve9Cb9VMuB+zuhNCHZSunf1Sns+8DwMdzLbn6/VNPrrDcKnGEg2UuN3qVTvUblnDGQ6jeI+dVrM3Cap4M0m/6LrCrsO/jN1WmcOrgPXWfLZa294zyrtR9OnuVl4vofhroPJIKSp53n19cH1PpAvbYa8yvjCq9nrfny/4XEnld/1uC+Jbx5aLxZH7zmMjPi9XDg95Xd/l773EOyz0bvzQ0d2xo4z0rv3g27OPg2esrvrk5XeIA0zZXfPGyDeDV27vUl/shrv1Fl7j//+VOt7pwil+7D5z/i9VaLl1Pj632/wqTF3M81ymmTpFGDnHrLe92984GNbAxw4vxdPslwRdr/SZTnn+LBbHDnBcyRMZxw3vlloeYPFx33kaNJ2Jq/YJh83BGzr70m8wWPeMI3r8HzRLNaA57Rxrr9BcbIReN+F3t4+77nAfyzQ/k2h17smVl+syL7UT/ZMz3b5LpD2tln2B/ITDESFqOktDE1vSVmjnt8k29+Svvqmn8UOzG6EeZIYqe5P6oWB1DB3+ur0vtjFDd/bQ9j8MQm3zR5f1LfW+4O2KtgdvWYrMlfKi3u+dwe1vLzBYnuG9Nq+lnO4qyF6cp/Z7l3MU86zu583xjlEtNjiPV6oeX0HvGrH05bZqfX8X//v65s0F5R69yLsob73KtITsea7Urf68XLcX7fy1T3Ii6Fr686ZbQd9kllu34HwEv/OxL737bBrx7Izj+3f/DW/565z9pePOraa92C5rHND8R78Rbvs3Is0H1E7V/QMM+ZZcDet+P7SZGuUG7czbyYuOvfMdQbWRbB+iia97stLEMuqsqft3WS/c5fcRkpidkW6sF8+Cqmp3MV93PPWvayo693PvOK3vCyb/mAr2Oje3/Gisccy6FqNU53sz+Km5o0bkPmy7N/VuJtme8wdSAdAXswh1tHXspbtDWmX8OpxYjttd8vF3P/2uAMA0684XK+nEav6rbe+bzju5Z8XkdksX7Ic1l78iY3Lm50XLXiaqqRDG3vhh8/S6ES75kTD2DbTrW8Ybz+1GXcXbjfj4N6fZ2nhswxeW2R3vux+8tO872a+bh1l7c88zG1Pongxu5LtPu62+8zVSYy7mfFcOtKNfF7N7DzqTu4nINtND2w88nbv8RrLQb/jEbr5yfCApws8Bdjugln3ZznW78y3Fn1/NfjHk/X/Dtadpd3+v+4DtBZytZg3yyd01kROMW4t6erx7tTtZY0F8s6Mt3eD4tzh3v/2gieymxn3otM97sup62K2uwpyN9Jpr5cK2+M89hRjnAxpY5m/n0DbySe8ZFJ3wHhjkNtMmm+lX3qpm5uu3OmbcTdA23oq7XoE6+WC913PrKWf4kHYfbC9OcXGQ2jbuNSitcUwNdpqNqNpzjWT2Jv/bj11dvYMX9VJCWxtNOskEU836d1M1SwS34sM7pTECslih9nGktgZkzPjJyeuE7bTzO1y3j9zGXE1LZ6nIE8Wym5/R9jqybFunqtdzPGbl1iQ3FrOPNMAP/4uuvqMXdX5drCW7tYrH9BbbDxT8Pn5L/GbwnMbY8zbtbYbwe1suXgbL/cR25non2Q77P2ZbU3alxstTxfifQLY9paz03RItHvx2tkOeZKuiHa/77tdz5HPNNFCu+d3A9/lifZPa/f9FuVPP7z+ae0R3zb90pH72uQLuYcmiIYmiIYmiIYmiIYmiIb248fl11++/fP3//nPb799/67NJeLWJuftj0tREQJBpxwqe4kJVEiGFYaNybBKMNhHrTwrCCCjceV97pIMKG8SKwibYgiBFcRRMbvMCgN0lcorPSljMisoWbKhKihZ8lqxgpKlmDUrDP+zLXUIRWUVDakM5cxG87EM5czWO1ZQzhysZwWzdqV8ZIVJdacDnQGPpdwUzMEyv9LjZxo4OdcBzmeyS6qVzKroQiu1hcr6ViOrFZW61NZqq3IjYbVzXBZgZrVPmRVMbQB2VTBDNIq4BLwO1mjPdYdrZAGnJgXXzxpn+RhcW2t8ZBIRJ5apcoE2YU3OVeEtjyYGVji3Yi6gcBUnZC5nhLLYRCRAYUZXm3oMv4HIBC5ngLI4p7hkAcriuJ2Bwu1h3M5AQVlcjvWVUBbP7QwUlMXbxNcvQFm8N6VkkVSg+oGCkvmkIisomc+uKihZUEQ3XjyULNhoWUHJArc6UFCyAJIVlCzEzGf3ULLI7SySai0LFNQ1hHbMqRQLpVSUZvK5KMOsKUdimC4ds8yFEprB6Pq+CMrlqhIoPnvGGrkQ62dCbV3IOrLSFxeh1bMyoLglJyTooouZlQMFJ2QFZYlZVYW5HOWqgrIkS+VMeG1dgrKxwhRZUFzOiNmzlAIrKEtWpiooCxST6wDtzGXoO6ygLJl7R8b2CQ2E+kPGtutyClUBbqXpimVs816ZzAShP3joxKygr3gV6fpl7Ede5coT+pjXuvKE/uc194CMfdNrH+oxD4qumFHYp73OhSAqKIvRuiooizGhKiiL8YUgKPAh0I6TZQVlMXTdUUFZTA6JFZQFnAiXE3yWt9w7QEFZrK88M5QFmqvi94FKxBrm/kW54pONIUXexlhSxNO4ojLxROWhkVDvQOVAkU9GZUGRT0ZloAGRT8bP1KDIC+MxVORbUSloXEHz+YBL8DHwZyZsatTOUEHXDTrx+xK6Cmp1powrIVCrM2XMCeAKIisoWUhlzDFlrAqR+iYqKEukdoYKyhJdfR+0lxB9fV/Eht64QDsLSVUSEV1aowTtE3wbcfHYkkOiVocKzp6o96OCsydqdajg7FnbQAp6TsjU6lBhV/JlBEIFZ8/c6rDioHKqr8QP4lbnsRdHRX4XFQzqyjsuC/T+qLidefQMUcHgyEpB4aidgfLYWS2R8OhtoqZ2hipeEKdnhYNCayHgwWI2gc9eFJclkOKzFx+JOMv5Einy86Z4xaT4fMUPQpBQRhk8FqEhwFDJKoFi8glLnQKTL34wBervpvjBBJeFzwAk4MpmPjtQSpF6vyl+MEUT+XwBh3G+DsUPpsjXofhB8PI2s4KyJG24ZHA1U6LREFREd8AkErYJ6HCJSwbtJSW+Dgk9H4QhxKV4vpSNYxIRQx9PJIrng7fRdSieDxqWV6zAHSiKukzxfFlRnGWK54PQRtdj4A4UOHpW0HVVLhGEKZ4PilJGWKuwx2VN458tni9rGo9s8XzQvwtBWzxf1tTfbfF82ahyxWzxfNnQCGSL54PqFf9pi+fLMDQqVlAWQyMQKnQ4ukQQqDBgs5bLkjG0c6EqOLuNjkuGW8+csvy+sojj2BnksgyWoTiqSggXlc/JV435wMDXBjWEtjBo6PZ2CGcBQi0/aU9xTjseKd5txyNdCaurJo9qDWsIYgo5W7UtrdY60oajuKIhgDYcc7XjHGdZzxrCGT19fcrF+xUNITfEskTOVk2jd9EJNbNEDXWyHOdgeTFx6ixF/lifoh2frx73NCq04+D64+Q4jLSq1R8+3/MoUs/nHbcyLo+H4k7L73lkKfVF7fnzPdXXe25DgXXg+gbi48GZ+arhM2GcpNYSSQdFPbVo3PzE3q5ogxraV30/asvlD7TKCkOWbufDVX/L9aGVfhhQ6XIHbLg4NfOZZZmpWVeP4h3HjvuKJ5nIaVnHkkYPa1mChyOwJFs7LZ+cveNmj+eNynhutXgUmbCbwfdC7GXYX0DPgxZMiE0JSEBSf0aJM0FLgzt1YZDXHo2zRnvt0jildNc+jfNNF8tE1WqSntuaLXO+GFt9Sbp65Ul6cjKBJflpG1nStMUmlrnM4Gwmya7aKZbUJp2ieSg4JccSJ6nghyy/FyVGMyzxxYHPm1jyeSNLPm/gGS533jrhdcpwBYukgBGrXyS3RFNnx6FKKAZ46iYDSm4qpky6oY/oKh3KSI3elMk7uEDDfa5Il2uXKy/2V+xF0vQFy1wkX4XA0tMli/RRnhthYnnFjjJQ3IJgizSlVE7TR0F1DR8tMoc4ORopXqCjMMMJk48CmdsVhLm80lRBlDCZV0aXOBk/qkiazqNUKFOZjYKEcdQoS0EEvhePVn+eWdJUEitYJM0esfpFsnMPLCleRnQoE7sGx5I9vWUJ0w6+RkXGMqO0VEjN035LhdTV62uMBAx4VXLiJeGD+QLHPjviuqCHuJ1OVGTiLulJZm7egSX7y8iSh4fEMha/YTPLVC8KSuBeLxlJq6r0IHlWjBKuoPEUsDjD0tJVMBghgSSSzpIMNLtACdfX8KTWOZbkc/go+ifPH1UkxVB43iIpbYBlLpLyBjaTdJQ4sIklX/3I0mt21yT56nuWfPWpVNa3kY0kX30uFQxkPC7CLMGgm+RhEYJHkDTbAAmRJUhTgwYIO0HymGWLdCYX7w3nLdISWOtZchUCS45HIkvy/Dax5Cpkkjw4AyuSVAWnWVIDdoYlRYRwFSBSBkkjHVwjkjRuO0+Sh2lXpjcGDtJ7S0YIJHkVV1JCIMmpupITgobD3tuT9DSm4IlwL4WnBozFKJKydVjI8mJusZpkUIYbA0lqsVj9ImmuaBNLmkTbyDI2sCgjTRctlwrGVG4MJA05RsuSryCXKlLiDiUu4deBno5CSFJDMKxRUJQ9QwktJ2i+3JYlxbcoFUq+3I6k4+jO4ewJJNGwnqSnDIINLKk728iy0SBJU2ebWRINIEmSptK4Bw1loPaMEppK5MkKH40QVAe+ClDI6OiT+aOi449S9F4fayMsMrahnCQNmvW9iTLa9WimvlDfmym246P4Wr76RRqOVUo2zmCMoCZHHffQxNLUjkOS5qXtaCSw9SgPuHy0Drh81PvaJUm2wAZrBA5Ys39GVhkGIA4/i0wtGsXGAHMXem8oLSfDcDRpsVCIWKVD2SJnzMoqRYkZSylbpdowgflcpTiEKG4EJA8TsWSCleJmFkuaGIZjGtpiySErToSTUwXJDSmxjCVFYstUGSRdfVvmylZxahwlFtK0q4A5boX+jCUW0rSrgNlx6Oqax25Mnas6sypDOUi+KGXKbJVtFwUz8ooT5hTJgOQoqMyareKUuS0JQ5CUJUCJpbLXKAhL5SgKogASpAncj8riguOYEKf0KGmgdyV+BplqYJOxVI5iQldib6s4DHBl/gySwgBXJtAgKcfiygwaJHkVp0kGY7lLZiwkp8VRYiExVcWyrIBQmsDRtlLQNuiqdVkhye04ljPyWKDLpAo0DwaGdYrX9+NqiCFi9bh2ELKRTycdeOSx9Pk6c6RhqTzQy4mLofKAw3G6ajweOT40pTrQ8xuKIql9ct2hmaQJGRxszZUblNT7K1UbW+SJ78XEBF9rks1n4olgLmi4bReZWtsupbSccixTz6InU1WoleVBk6eeELqE1KaepqzV+JYaQG04EvBE0dWpqy9TX1y/8TxWsGbvQakGDH6SqRpXcXgsLVN11MH5NnXHNZ86IpqqzTX1gStEMAjy7A+n/hg+0fkodYBrRpGTSlpj/eqEFzXWJ3PCLVfN6UbUuFqkyPmbVHVSqWpcaeKxsWhch9ItFUYagrRUdSjrVNlVHXGlivko1oETv/X4NSHIuqVDi4bYg5LBqMu6FPEoGleteKRDbXBNy7eEZtEwisZczwevx4gj1fKgNnx9FL/ecmPUrOswhBq3hdepnak6tNSNwX2rnlMKmjVPydpxXs6t70/gJc3kOIyvV21R8xiC2qHmUUOzNhSctePG5zA9HjnyweMeNYTg9XjRNKUo9S+afBryIs3LGYk1z88K76J5SSNU7Woqn86fyKvh7Rqks3FVw+szjyhFQ/vJ6Duqjqidbu9PqNsCAGlO9ZXzo06ceIlV0zy2lL/o2Nob1BF0VrHWF3XmEV1VzUO6rpoWWArPoq/9G7SDiICiGFc1R8u+alczMqyjrakyg/vowzU1ZnATdGipMeQDOjR/ZnBNNeSaakC+MBdp0Uw5Dp1Y+3r+onONZ0hb2pZQP9/UiVFkXVOHkcpjPMdDqWpOoqT6ek5t1eOB23Oumv1jO05LoeU4vp8XournWTf5fHg9dOganxRe0IFzTT2Sdq6OB8gXOmRLPZIm/4AarjHO5RpfuMbOppa6gZ4HOvN44avOsfp3fD0AqwkK1l7V/kqac5m6alqGxvZVtFXNf5K2jpdEWHP7jVXH1t5J57qwRdopV/sTaWq/ppbPkb8pWqGmmaAxVVOQipujSdPU0CjWPP0x1D/gMC3W6Vw19896PNOWmKLh+njFC2OKrifM/7OpGq43HK2L06V9Ytjvq3ao24I/+kfQ7F9U8Z+Ye9C8wIv+FQZbXrzUfNzzgqXh456XLE09TvFDPQ4TydnnwUzSt/qgttS/6/kDZwJL+fC4pzBXJ9aBt4Mkfn2gUFbHqim/iQ9fIs2bHzzr2DaC0OfFEOLk9ZGTfEXjXgbbtsIUftH6ukmo8MXhqX1ewL0Pum7gKNcnurYJplw/mHf6VHVG3ba+lPYBMy+dq8Z9Fr5teCn9B5qH4+0pGm8lgqlo3aBCx3n8qK9P161DpLWum6rK+1PdMGSrJn+qDeu6Tcjw++v2H826buTh/pmiV7Zq3KkBUXk9jvVNibdAaeKReLwpGveApEDXSxPPxOONpvHXJR5fNI3XDndwuaqxvJxa1NxeE+cWNbe/XLfUUHyCcxs/Ox6pfWpun5k3peQq6eMyvzsxXYp+oHNxbend2Nl0uH469BWj4rV0oHmrjKmavDVejaIDeZ+icQdJCCpUjXtI4vVqFs3RvKboAc52bU2kaSG99Jaifd2Aw5o3VOWqE0UbijXEzM0bFE29yZiqeYndVh3CJHrxxvACMtcHJgtW1eNFU366vR/g6+nreaGrvd7SaF6iGdQ8mzDU2z1mIhN7e9IuNY07ZHh05OgX9DW60bS7pi6iY/SMO2poET9UzQv3oUTb3nK+iqNvb+vWgViie2+vy/6seamfZgMewiltJsddIO/B0b13nIiqn++Sv0bzuB1Cc31T1bwNhmYfMDmn6JdnJ6BTW5gu2qq6RQJnM6BTi26Lrhkomh2hL2qzK2jToGNdzMfZlfc1zaT4eI3263H25jwb8569dTue43R2Boc5euPzhdyW/YuG4KauYLDmlJGm8kbeAtmOG4qW2/sNRzd8/jiJZsvx3JYWy+eBcwh68vnJTsqP2tFssJ4vGxOm5cm8Yau+PvNGrHY8tI0MrNtKJ31e1NP3B1U2Vv34v/8HG7eesQ==',
          'content-encoding': 'gzip',
          'content-type': 'application/json'
        },
        gridID: '3',
        msg: 'MAP-DATA',
        time: `${new Date().toISOString().split('.')[0]}Z`,
      }
    };
  };
}

module.exports = {
  initialiseClean,
  fullCleanInitiated,
  firstMapGlobal,
  firstFullCleanRunning,
  secondFullCleanRunning,
  thirdFullCleanRunning,
  secondMapGlobal,
  firstMapData,
  firstMapGrid,
  fourthFullCleanRunning,
  fifthFullCleanRunning,
  sixthFullCleanRunning,
  secondMapGrid,
  thirdMapGlobal,
  secondMapData,
  forthMapGlobal,
  fifthMapGlobal,
  sixthMapGlobal,
  eighthMapGlobal,
  ninthMapGlobal,
  tenthMapGlobal,
  twelfthMapGlobal,
  userRecoverableFault,
  thirdMapGrid,
  thirdMapData,
};
