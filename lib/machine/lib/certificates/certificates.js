const fs = require('fs');
const path = require('path');
const { env } = require('../../../env');
const { decrypt } = require('../crypto/crypto');

const pathToCerts = path.join(__dirname, '..', '..', '..', '..', 'certs');

const getMachineCertificateGroup = (isOtaCert = false) => {
  if (isOtaCert) {
    return 'ota-machines';
  }
  return process.env.TEST_DATA_MODE === 'existing' ? 'owned-machines' : 'freepool-machines';
};

const getCertificates = (ungradedSerial, connectionJourneyType,
  provisioningType, isOtacert = false) => {
  const pkiGrade = env.cloud.grade;
  const clientCertAbsPath = `${pathToCerts}/client-certs/${pkiGrade}/${getMachineCertificateGroup(isOtacert)}/`;
  const serverCertRootCaAbsPathForIot = `${pathToCerts}/server-certs/cp.root.ca/${pkiGrade}/amazonCA/AWS_ROOTCA.crt.pem`;
  const serverCertRootCaAbsPathForLink = `${pathToCerts}/server-certs/cp.root.ca/${pkiGrade}/dysonCA/DysonConnectedProductsRootCA.crt`;

  let machineKeys;
  const caCerts = {
    ca: {
      link: fs.readFileSync(`${serverCertRootCaAbsPathForLink}`),
      awsIot: fs.readFileSync(`${serverCertRootCaAbsPathForIot}`)
    }
  };

  let keyFilename = `${ungradedSerial}.key`;
  let pemFilename = `${ungradedSerial}.pem`;

  // Try to choose the cert based on the provisioning type in case they are different
  const baseNameWithType = `${ungradedSerial}_${provisioningType.toLowerCase()}`;

  if (fs.existsSync(`${clientCertAbsPath}${baseNameWithType}.key`)) {
    keyFilename = `${baseNameWithType}.key`;
    pemFilename = `${baseNameWithType}.pem`;
  }

  if (connectionJourneyType === 'lecOnly') {
    machineKeys = {};
  } else if (pkiGrade.split('.')[2] === 'prod') {
    machineKeys = {
      key: Buffer.from(decrypt(fs.readFileSync(`${clientCertAbsPath}${keyFilename}`).toString())),
      cert: Buffer.from(decrypt(fs.readFileSync(`${clientCertAbsPath}${pemFilename}`).toString())),
    };
  } else {
    machineKeys = {
      key: fs.readFileSync(`${clientCertAbsPath}${keyFilename}`),
      cert: fs.readFileSync(`${clientCertAbsPath}${pemFilename}`)
    };
  }

  const certObject = Object.assign(machineKeys, caCerts);
  return certObject;
};

const getAWSATSca = () => {
  const pkiGrade = env.getCurrentEnvironment().cloud.grade;
  const serverCertRootCaAbsPathForIot = `${pathToCerts}/server-certs/cp.root.ca/${pkiGrade}/amazonCA/AWS_ATS_ROOTCA.crt.pem`;
  return fs.readFileSync(`${serverCertRootCaAbsPathForIot}`);
};

const getAWSca = () => {
  const pkiGrade = env.getCurrentEnvironment().cloud.grade;
  const serverCertRootCaAbsPathForIot = `${pathToCerts}/server-certs/cp.root.ca/${pkiGrade}/amazonCA/AWS_ROOTCA.crt.pem`;
  return fs.readFileSync(`${serverCertRootCaAbsPathForIot}`);
};

const getLinkca = () => {
  const pkiGrade = env.getCurrentEnvironment().cloud.grade;
  const serverCertRootCaAbsPathForLink = `${pathToCerts}/server-certs/cp.root.ca/${pkiGrade}/dysonCA/DysonConnectedProductsRootCA.crt`;
  return fs.readFileSync(`${serverCertRootCaAbsPathForLink}`);
};

module.exports = {
  getCertificates,
  getMachineCertificateGroup,
  getAWSATSca,
  getAWSca,
  getLinkca,
};
