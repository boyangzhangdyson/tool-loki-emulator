const crypto = require('crypto');

const cipherSecretKey = process.env.CIPHER_SECRET_KEY;

const decrypt = (encryptedData) => {
  if (!cipherSecretKey) {
    throw new Error('Unable to read/interpret environment variable CIPHER_SECRET_KEY to decrypt production grade machine cert files. Please check README.md for usage information');
  }
  const encryptionAlgorithm = 'AES-256-CBC';
  const iVector = cipherSecretKey.substr(0, 16);
  const decryptor = crypto.createDecipheriv(encryptionAlgorithm, cipherSecretKey, iVector);
  return decryptor.update(encryptedData, 'base64', 'utf8') + decryptor.final('utf8');
};

module.exports = {
  decrypt
};
