const awsIot = require('aws-iot-device-sdk');
const util = require('util');
const debug = require('debug')('lib:mqtt:aws-iot-client');
const { getAWSATSca, getAWSca } = require('../certificates/certificates');

const getCa = (url) => {
  if (url.includes('ats')) {
    return getAWSATSca();
  }
  return getAWSca();
};
class AwsIotClient {

  constructor(machineIdentity) {
    this.machineIdentity = machineIdentity;
    this.connected = false;
    this.receivedMessages = [];
  }

  connect(connection, timeoutInSec = 30) {
    if (this.connected) {
      debug('Aws Iot Client is already connected to a broker. Skipping connect() call');
      return Promise.resolve(this);
    }
    return new Promise((resolve, reject) => {
      this.mqttClient = awsIot.device({
        host: connection.endpoint,
        clientId: this.machineIdentity.serial,
        protocol: 'mqtts',
        clientCert: this.machineIdentity.certificates.cert,
        caCert: getCa(connection.endpoint),
        privateKey: this.machineIdentity.certificates.key,
        ALPNProtocols: ['x-amzn-mqtt-ca'],
        port: connection.port,
      });

      setTimeout(function () {
        reject(new Error(`Broker connection timed out after ${timeoutInSec} ms`));
      }, timeoutInSec * 1000);

      this.mqttClient.on('connect', () => {
        this.connected = true;
        debug(`AWS IoT MQTT client connection started for ${this.machineIdentity.serial}`);
        resolve(this);
      });

      this.mqttClient.on('reconnect', () => {
        debug(`AWS IoT MQTT client reconnection started for ${this.machineIdentity.serial}`);
        resolve(this);
      });

      this.mqttClient.on('error', (error) => {
        this.connected = false;
        debug(`AWS IoT MQTT client received error: ${error} for ${this.machineIdentity.serial}`);
        reject(new Error('Aws IoT Mqtt Connection Failed.'));
      });

      this.mqttClient.on('close', () => {
        this.connected = false;
        debug(`AWS IoT MQTT client connection closed for ${this.machineIdentity.serial}`);
      });

      this.mqttClient.on('offline', () => {
        this.connected = false;
        debug(`AWS IoT MQTT client connection offline for ${this.machineIdentity.serial}`);
      });

      this.mqttClient.on('end', () => {
        this.connected = false;
        debug(`AWS IoT MQTT client connection ended for ${this.machineIdentity.serial}`);
      });

      this.mqttClient.on('message', (topic, message) => {
        if (!this.receivedMessages[topic]) {
          this.receivedMessages[topic] = [];
        }
        this.receivedMessages[topic].push(JSON.parse(message));
      });
    });
  }

  disconnect() {
    return new Promise((resolve, reject) => {
      const timeout = setTimeout(() => reject(new Error(`Timeout whilst trying to disconnect AWS IoT MQTT client for serial ${this.machineIdentity.serial}`)), 10000);

      this.mqttClient.end(true, (err) => {
        clearTimeout(timeout);
        if (err) {
          reject(new Error(`Error whilst trying to disconnect AWS IoT MQTT client for serial ${this.machineIdentity.serial}: ${err}`));
        } else {
          debug(`Successfully disconnected AWS IoT MQTT client for serial ${this.machineIdentity.serial}`);
          resolve();
        }
      });
    });
  }

  publish(topic, payload) {
    return new Promise((resolve, reject) => {
      this.mqttClient.publish(topic, JSON.stringify(payload), (err) => {
        if (err) {
          debug(`Failed to Publish ${util.inspect(JSON.stringify(payload), false, null)} to ${topic}\n`);
          return reject(err);
        }
        debug(`Published ${util.inspect(JSON.stringify(payload), false, null)} to ${topic}\n`);
        return resolve();
      });
    });
  }

  subscribe(topic = this.topics.command) {
    return new Promise((resolve, reject) => {
      this.mqttClient.subscribe(topic, {
        qos: 0
      }, (err) => {
        if (err) {
          debug(`Error subscribing to ${topic} Topic`);
          return reject(err);
        }
        return resolve();
      });
    });
  }

  addListener(event, handler) {
    this.mqttClient.on(event, handler);
  }

  getReceivedMessage(messageFn) {
    const {
      topic,
      payload
    } = messageFn(this);
    return (this.receivedMessages[topic] || []).find(t => t.msg === payload.msg);
  }

  getAllReceivedMessages(messageFn) {
    const {
      topic,
      payload
    } = messageFn(this);

    return (this.receivedMessages[topic] || []).filter(t => t.msg === payload.msg);
  }

  getAllReceivedMessagesForTopic(topic) {
    return this.receivedMessages[topic] || [];
  }

  clearAllReceivedMessages() {
    this.receivedMessages = {};
  }

}

module.exports = AwsIotClient;
