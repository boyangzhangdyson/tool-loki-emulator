const AwsIotClient = require('./AwsIotClient');
const RabbitmqClient = require('./RabbitmqClient');

const get = (provisioningType) => {
  switch (provisioningType) {
    case 'awsIot':
      return AwsIotClient;
    case 'rabbitMQ':
      return RabbitmqClient;
    default:
      throw new Error(`Unknown provisioning type or endpoint: ${provisioningType}`);
  }
};

module.exports = {
  get,
};
