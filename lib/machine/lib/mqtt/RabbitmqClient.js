const util = require('util');
const mqtt = require('mqtt');
const debug = require('debug')('lib:mqtt:rabbitmq-client');

// TODO: Shouldn't these be in config?
const DEFAULT_REJECT_UNAUTHORIZED = false;
const DEFAULT_KEEPALIVE = 60;
const DEFAULT_PROTOCOL_ID = 'MQIsdp';
const DEFAULT_PROTOCOL_VERSION = 3;
const DEFAULT_RECONNECT_PERIOD = 1000;
const DEFAULT_CLEAN = true;
const DEFAULT_PROTOCOL = 'mqtts';
const DEFAULT_SECURE_PROTOCOL = 'TLSv1_2_client_method';
const DEFAULT_CIPHERS = 'AES256-SHA256';

const { getLinkca } = require('../certificates/certificates');

class RabbitmqClient {

  constructor(machineIdentity) {
    this.machineIdentity = machineIdentity;
    this.receivedMessages = [];
    this.connected = false;
  }

  connect(connection, timeoutInSec = 30) {
    if (this.connected) {
      debug('RabbitMQ Client is already connected to a broker. Skipping connect() call');
      return Promise.resolve(this);
    }
    return new Promise((resolve, reject) => {
      const opts = {
        rejectUnauthorized: DEFAULT_REJECT_UNAUTHORIZED,
        keepalive: DEFAULT_KEEPALIVE,
        protocolId: DEFAULT_PROTOCOL_ID,
        protocolVersion: DEFAULT_PROTOCOL_VERSION,
        reconnectPeriod: DEFAULT_RECONNECT_PERIOD,
        clean: DEFAULT_CLEAN,
        protocol: DEFAULT_PROTOCOL,
        secureProtocol: DEFAULT_SECURE_PROTOCOL,
        ciphers: DEFAULT_CIPHERS,
        ca: getLinkca(),
        clientId: `${this.machineIdentity.serial}-${Math.floor((Math.random() * 1000000) % 1000)}`,
        host: connection.BrokerHostName,
        port: connection.BrokerPort,
        username: connection.TokenId,
        password: connection.TokenPassword
      };

      setTimeout(function () {
        reject(new Error(`Broker connection timed out after ${timeoutInSec} ms`));
      }, timeoutInSec * 1000);

      this.mqttClient = mqtt.connect(opts);

      this.mqttClient.on('connect', () => {
        this.connected = true;
        debug(`RabbitMq MQTT client connection started for ${this.machineIdentity.serial}`);
        resolve(this);
      });

      this.mqttClient.on('reconnect', () => {
        debug(`RabbitMq MQTT client reconnection started for ${this.machineIdentity.serial}`);
        resolve(this);
      });

      this.mqttClient.on('error', (error) => {
        this.connected = false;
        debug(`RabbitMq MQTT client received error: ${error}`);
        reject(new Error('RabbitMq Mqtt Connection Failed.'));
      });

      this.mqttClient.on('offline', () => {
        this.connected = false;
        debug('RabbitMq MQTT client connection offline');
      });

      this.mqttClient.on('end', () => {
        this.connected = false;
        debug('RabbitMq MQTT client connection ended');
      });

      this.mqttClient.on('message', (topic, message) => {
        if (!this.receivedMessages[topic]) {
          this.receivedMessages[topic] = [];
        }
        this.receivedMessages[topic].push(JSON.parse(message));
      });

      this.mqttClient.on('close', () => {
        this.connected = false;
        debug('RabbitMq MQTT client connection closed');
        reject(new Error('Disconnected from broker'));
      });
    });
  }

  disconnect() {
    return new Promise((resolve, reject) => {
      const timeout = setTimeout(() => reject(new Error(`Timeout whilst trying to disconnect RabbitMq MQTT client for serial ${this.machineIdentity.serial}`)), 10000);

      this.mqttClient.end(true, (err) => {
        clearTimeout(timeout);
        if (err) {
          reject(new Error(`Error whilst trying to disconnect RabbitMq MQTT client: ${err}`));
        } else {
          debug(`Successfully disconnected RabbitMq MQTT client for serial ${this.machineIdentity.serial}`);
          resolve();
        }
      });
    });
  }

  publish(topic, payload) {
    return new Promise((resolve, reject) => {
      this.mqttClient.publish(topic, JSON.stringify(payload), (err) => {
        if (err) {
          debug(`Failed to Publish ${util.inspect(JSON.stringify(payload), false, null)} to ${topic}\n`);
          return reject(err);
        }

        debug(`Successfully Published ${util.inspect(JSON.stringify(payload), false, null)} to ${topic}\n`);
        return resolve();
      });
    });
  }

  subscribe(topic = this.topics.command) {
    return new Promise((resolve, reject) => {
      this.mqttClient.subscribe(topic, {
        qos: 0
      }, (err) => {
        if (err) {
          debug(`Error subscribing to ${topic} Topic`);
          return reject(err);
        }
        return resolve();
      });
    });
  }

  addListener(event, handler) {
    this.mqttClient.on(event, handler);
  }

  getReceivedMessage(messageFn) {
    const {
      topic,
      payload
    } = messageFn(this);

    return (this.receivedMessages[topic] || []).find(t => t.msg === payload.msg);
  }

  getAllReceivedMessages(messageFn) {
    const {
      topic,
      payload
    } = messageFn(this);

    return (this.receivedMessages[topic] || []).filter(t => t.msg === payload.msg);
  }

  getAllReceivedMessagesForTopic(topic) {
    return this.receivedMessages[topic] || [];
  }

  clearAllReceivedMessages() {
    this.receivedMessages = {};
  }

}

module.exports = RabbitmqClient;
