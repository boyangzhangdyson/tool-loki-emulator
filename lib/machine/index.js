const Machine = require('./machine');
const machineFactory = require('./machineFactory');

module.exports = {
  Machine,
  machineFactory
};
