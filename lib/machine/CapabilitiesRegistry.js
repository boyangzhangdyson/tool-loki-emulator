// Maintains a registry of capabilities
// Capabilities can be either classes to be instantiated or
// exported modules to execute single functions

class CapabilitiesRegistry {

  constructor(machineIdentity) {
    this.registry = new Map();
    // Clone machineIdentity to avoid copying by reference
    // thus having circular references
    this.machineIdentity = Object.assign({}, machineIdentity);
  }

  add(name, CapabilityClass) {
    this.registry.set(name, { CapabilityClass, instance: null });
  }

  get(name) {
    if (!this.registry.has(name)) {
      throw new Error(`Requested item '${name}' is not present in the capabilities registry for this machine`);
    }

    const registryItem = this.registry.get(name);

    if (!registryItem.instance) {
      if (typeof registryItem.CapabilityClass === 'function') {
        // The object is instantiable
        registryItem.instance = new registryItem.CapabilityClass(this.machineIdentity);
      } else {
        registryItem.instance = registryItem.CapabilityClass;
      }
    }
    return registryItem.instance;
  }

}

module.exports = CapabilitiesRegistry;
