const {
  skusForN223, skusFor520, skusFor527, skusFor276, skusFor552, skusFor475, skusFor358
} = require('../skus');

const allDefinitions = [
  {
    type: '520',
    provisioningType: 'awsIot',
    connectionJourneyType: 'lecHec',
    topics: {
      connection: 'status/connection',
      faults: 'status/faults',
      software: 'status/software',
      auth: 'status/auth',
      log: 'status/log',
      current: 'status/current',
      summary: 'status/summary',
      scheduler: 'status/scheduler',
      command: 'command'
    },
    skus: skusFor520
  },
  {
    type: '527',
    provisioningType: 'awsIot',
    connectionJourneyType: 'lecHec',
    topics: {
      connection: 'status/connection',
      faults: 'status/faults',
      software: 'status/software',
      auth: 'status/auth',
      log: 'status/log',
      current: 'status/current',
      summary: 'status/summary',
      scheduler: 'status/scheduler',
      command: 'command'
    },
    skus: skusFor527
  },
  {
    type: 'N223',
    provisioningType: 'rabbitMQ',
    connectionJourneyType: 'hecOnly',
    topics: {
      status: 'status',
      connection: 'status',
      faults: 'status',
      software: 'status',
      auth: 'status',
      log: 'status',
      current: 'status',
      summary: 'status',
      scheduler: 'status',
      command: 'command'
    },
    skus: skusForN223
  },
  {
    type: '276',
    provisioningType: 'awsIot',
    connectionJourneyType: 'lecHec',
    topics: {
      status: 'status',
      connection: 'status',
      faults: 'status',
      software: 'status',
      auth: 'status',
      log: 'status',
      current: 'status',
      summary: 'status',
      scheduler: 'status',
      command: 'command'
    },
    skus: skusFor276
  },
  {
    type: '475',
    provisioningType: 'rabbitMQ',
    connectionJourneyType: 'hecOnly',
    topics: {
      connection: 'status/connection',
      faults: 'status/faults',
      software: 'status/software',
      auth: 'status/auth',
      log: 'status/log',
      current: 'status/current',
      summary: 'status/summary',
      scheduler: 'status/scheduler',
      command: 'command'
    },
    skus: skusFor475
  },
  {
    type: '552',
    provisioningType: 'awsIot',
    connectionJourneyType: 'lecOnly',
    topics: {
      connection: 'status/connection',
      faults: 'status/faults',
      software: 'status/software',
      auth: 'status/auth',
      log: 'status/log',
      current: 'status/current',
      summary: 'status/summary',
      scheduler: 'status/scheduler',
      command: 'command'
    },
    skus: skusFor552
  },
  {
    type: '358',
    provisioningType: 'awsIot',
    connectionJourneyType: 'hecOnly',
    topics: {
      connection: 'status/connection',
      faults: 'status/faults',
      software: 'status/software',
      auth: 'status/auth',
      log: 'status/log',
      current: 'status/current',
      summary: 'status/summary',
      scheduler: 'status/scheduler',
      command: 'command'
    },
    skus: skusFor358
  }
];

const get = function get(filterFn) {
  if (filterFn) return allDefinitions.find(filterFn);
  return allDefinitions;
};

module.exports = {
  get
};
