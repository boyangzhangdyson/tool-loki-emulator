function generateHelloMessage(machine, message) {
  const helloMessage = JSON.stringify(message);
  return {
    topic: machine.topics.connection,
    payload: JSON.parse(helloMessage),
  };
}

function hello() {
  return function (machine) {
    function getHelloMessageBody(type, serial) {
      switch (type) {
        case '276':
          return {
            msg: 'HELLO', time: `${new Date().toISOString()}`, serial: `${serial}`, version: 'RB02PR.01.03.008.0016', protocol: '1.0.0'
          };
        case 'N223':
          return {
            msg: 'HELLO', time: `${new Date().toISOString()}`, serial: `${serial}`, version: '11.0.7.10', protocol: '1.0.0'
          };
        case '475':
          return {
            msg: 'HELLO', time: `${new Date().toISOString()}`, serialNumber: `${serial}`, model: 475, version: '11.03.08', protocol: '1.0.0', 'mac address': 'C8:FF:77:D2:9E:C6', 'module hardware': '140762-01-07', 'module bootloader': '-.-.-.-', 'module software': '5221', 'module nwp': '2.7.0.0', 'product hardware': '141793-01-06', 'product bootloader': '000000.00.00', 'product software': '000033.00.3', 'reset-source': 'HIB'
          };
        default:
          // For Gen2 devices
          return {
            msg: 'HELLO', time: `${new Date().toISOString()}`, serialNumber: `${serial}`, version: 'ECG2EF.02.06.000.0004', 'recovery package version': 'ECG2EF.02.05.002.0022', 'hardware version': 'SC04.AF05.WF02,protocol:1.0.0', 'mac address': 'C8:FF:77:FF:FF:FA', 'reset-source': 'PWUP'
          };
      }
    }

    return generateHelloMessage(machine, getHelloMessageBody(machine.type, machine.serial));
  };
}

function otaHello(firmwareVersion) {
  return function (machine) {
    const defaultOTAVersion = {
      475: { firmwareVersion: 'Loki.Day0.Target' },
      520: { firmwareVersion: 'Loki.InstantUpgrade.Target' },
    };

    const version = !firmwareVersion
      ? defaultOTAVersion[machine.type].firmwareVersion : firmwareVersion;

    const machineHelloMessage = {
      msg: 'HELLO',
      time: `${new Date().toISOString()}`,
      serialNumber: `${machine.serial}`,
      model: `${machine.type}`,
      version: `${version}`,
      protocol: '1.0.0',
      'mac address': 'C8:FF:77:D2:9E:C6',
      'module hardware': '140762-01-07',
      'module bootloader': '-.-.-.-',
      'module software': '5221',
      'module nwp': '2.7.0.0',
      'product hardware': '141793-01-06',
      'product bootloader': '000000.00.00',
      'product software': '000033.00.3',
      'reset-source': 'HIB'
    };

    return generateHelloMessage(machine, machineHelloMessage);
  };
}

function avu(account) {
  return function (machine) {
    return {
      topic: machine.topics.auth,
      payload: {
        msg: 'AUTHORISE-VERIFIED-USER',
        time: new Date().toISOString(),
        id: account,
        localpwd: 'Ar3lXcGGAlBlY9xhD3dSyjytT+1a/Do5LVT7oLrBVYEq13LDn2/hP/28U29s/Rgy7QpXL91F7she+xHv+d3WcNuqA0WaJ1H5PlZCWx8W4kRyVIg1dgbLA20N6iufftOrW7wxhyn5xPVG6jYGL9XNeQT/iu97SNESCj40zB3jK2BjM8lT+KUoq/EN90fCAIlW'
      }
    };
  };
}

function currentState(overrides) {
  return function (machine) {
    const payload = {
      msg: 'CURRENT-STATE',
      time: new Date().toISOString(),
      'mode-reason': 'PRC',
      'state-reason': 'NONE',
      dial: 'OFF',
      rssi: '40',
      'product-state': {
        fpwr: 'ON',
        fdir: 'ON',
        auto: 'OFF',
        fnsp: '0001',
        qtar: '0001',
        sltm: 'OFF',
        oson: 'OFF',
        nmod: 'OFF',
        rhtm: 'ON',
        fnst: 'FAN',
        filf: '0000',
        ercd: 'NONE',
        wacd: 'NONE'
      },
      scheduler: {
        srsc: '0000',
        dstv: '0000',
        tzid: '0000'
      }
    };

    Object.assign(payload['product-state'], overrides);
    return {
      topic: machine.topics.current,
      payload
    };
  };
}

function stateChange(overrides) {
  return function (machine) {
    const payload = {
      msg: 'STATE-CHANGE',
      time: new Date().toISOString(),
      'mode-reason': 'LAPP',
      'state-reason': 'MODE',
      'product-state': {
        fnst: ['OFF', 'FAN'],
        fnsp: ['0004', '0004'],
        qtar: ['0004', '0004'],
        oson: ['OFF', 'OFF'],
        rhtm: ['ON', 'ON'],
        filf: ['1000', '1000'],
        ercd: ['NONE', 'NONE'],
        nmod: ['ON', 'ON'],
        wacd: ['NONE', 'NONE'],
        hmod: ['HEAT', 'HEAT'],
        hmax: ['2941', '2941'],
        hsta: ['OFF', 'HEAT'],
        ffoc: ['ON', 'ON'],
        tilt: ['OK', 'OK']
      },
      scheduler: {
        srsc: 'ecd8',
        dstv: '0001',
        tzid: '0001'
      }
    };
    switch (machine.type) {
      case '520':
        payload['product-state'].fpwr = ['ON', 'ON'];
        break;
      case '358':
        payload['product-state'].clfr = ['9999', '9999'];
        payload['product-state'].clcr = ['CLNO', 'CLNO'];
        payload['product-state'].cdrr = ['9999', '9999'];
        break;
      default:
        payload['product-state'].fmod = ['FAN', 'FAN'];
    }

    Object.assign(payload['product-state'], overrides);
    return {
      topic: machine.topics.current,
      payload
    };
  };
}

function stateSet(data) {
  return function (machine) {
    return {
      topic: machine.topics.command,
      payload: {
        msg: 'STATE-SET',
        time: new Date().toISOString(),
        'mode-reason': 'ALXA',
        data
      }
    };
  };
}

function faultsChange(overrides) {
  return function (machine) {
    const payload = {
      msg: 'FAULTS-CHANGE',
      time: new Date().toISOString(),
      'product-errors': {
        nvmw: ['OK', 'FAIL']
      },
      'product-warnings': {},
      'module-errors': {},
      'module-warnings': {}
    };

    if (machine.type === '358') {
      var warningAttributes = ['ltem', 'tnke', 'cldu']

      var errorOverrides = {};
      var warningOverides = {};
      Object.keys(overrides).forEach(key => {
        if(warningAttributes.includes(key)){
          warningOverides[key] = overrides[key];
        }else{
          errorOverrides[key] = overrides[key];
        }
      });
      
      Object.assign(payload['product-errors'], errorOverrides);
      Object.assign(payload['product-warnings'], warningOverides);
    } else {
      Object.assign(payload['product-errors'], overrides);
    }

    return {
      topic: machine.topics.faults,
      payload
    };
  };
}

function currentFaults(overrides) {
  return function (machine) {
    const payload = {
      msg: 'CURRENT-FAULTS',
      time: new Date().toISOString(),
      'product-error': {
        fmco: 'OK',
        stto: 'OK',
        hall: 'OK',
        hamp: 'OK',
        stal: 'OK',
        shrt: 'OK',
        cnfg: 'OK',
        wdog: 'OK',
        dsts: 'OK',
        vocs: 'OK',
        't&hs': 'OK',
        wfma: 'OK',
        wfhb: 'OK',
        wfcp: 'OK',
        ibus: 'OK',
        nvmw: 'OK'
      },
      'product-warnings': {
        filt: 'OK'
      },
      'module-errors': {
        szme: 'OK',
        szmw: 'OK',
        szps: 'OK',
        szpe: 'OK',
        szpw: 'OK',
        szed: 'OK',
        lspd: 'OK',
        szpi: 'OK',
        szpp: 'OK',
        szhv: 'OK',
        szbv: 'OK',
        szav: 'OK'
      },
      'module-warnings': {
        srnk: 'OK',
        stac: 'OK',
        strs: 'OK',
        srmi: 'OK',
        srmu: 'OK'
      }
    };

    Object.assign(payload['product-error'], overrides);
    return {
      topic: machine.topics.current,
      payload
    };
  };
}

module.exports = {
  hello,
  otaHello,
  avu,
  currentState,
  stateChange,
  stateSet,
  faultsChange,
  currentFaults
};
