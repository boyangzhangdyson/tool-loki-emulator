const powerModes = {
  N223: [0, 1],
  276: [0, 1, 2, 3]
};

const powerModeMappings = {
  N223: ['fullPower', 'halfPower'],
  276: [0, 1, 2, 3]
};

const getRandomPowerMode = (productType) => {
  const randomIndex = Math.floor(Math.random() * powerModes[productType].length);
  return powerModes[productType][randomIndex];
};

const getPowerModeMapping = (productType, powerMode) => powerModeMappings[productType][powerMode];

module.exports = {
  getRandomPowerMode,
  getPowerModeMapping
};
