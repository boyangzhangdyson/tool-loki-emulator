# Usage

## CLI Features
- Commands, and most arguments and options have autocomplete enabled which can be shown by pressing the `[tab]` key
- Scrolling through history is possible with `[up]` and `[down]` keys

## Scenarios
- Scenarios can be added in the [scenarios](scenarios) directory as a file
- A scenario file should export a function with the signature `(Program) => Promise<void>`
- The program passed in to the function contains the state of the machine and user
- Program API can be found in [bin/program.js](bin/program.js)
- See [scenarios/decreasingFilterLife.js](scenarios/decreasingFilterLife.js) for an example

## Parameters
- Where `<msg>` is mentioned, it refers to the exported name in [lib/messages/machineMessages.js](lib/messages/machineMessages.js)
- Where `<topic>` is mentioned, it refers to the keys of the `topics` object for the machine in [lib/machine/machineDefinitions.js](lib/machine/machineDefinitions.js)
- Where `<scenario>` is mentioned, it refers to the filenames (excluding file extension) of files in the [scenarios](scenarios) directory


## Help Text
```

  Usage: loglevel [options] <level>

  set the log level

  Options:

    --help  output usage information
```


```
  Usage: connect [options]

  Alias: c

  Perform connection journey

  Options:

    --help                      output usage information
    -t, --type <type>           Machine type (default=520)
    -r, --region <region>       Region (default=row)
    -e, --env <env>             Environment (default=dev004)
    --linkAppVersion <version>  Link App version (default=4.1)
    --isOwner                   If set, user will be the owner of the machine (default=false)
    --isOtaMachine              If set, uses OTA configuration for the machine with client cert from ota-machines, overrides --useFreeMachine (default=false)
    --useFreeMachine            If set, uses client certs from freepool-machines, else from owned-machines (default=false)
```


```
  Usage: list [options] <item>

  Alias: l | ls | show

  Prints machine or user information

  Options:

    --help  output usage information
```


```
  Usage: run [options] <scenario>

  Alias: r | load

  Runs a specified scenario within the current context

  Options:

    --help  output usage information
```


```
  Usage: machine send [options] <msg> [params...]

  Alias: ms

  Sends an MQTT message as the machine, params will be passed into matching message function

  Options:

    --help            output usage information
    -v, --toKeyVal    From params "key1 val1 key2 val2 ..." to { key1: val1, key2: val2, ...} (default=true)
    -a, --toKeyArray  From params "key1 val11 val12 key2 val21 val22 ..." to { key1: [val11, val12], key2: [val21, val22], ... } (default=false)
```


```
  Usage: machine sendRaw [options] <jsonString>

  Alias: msr

  Sends an MQTT message as the machine with raw payload specified

  Options:

    --help               output usage information
    -t, --topic <topic>  Topic (default=current)
```


```
  Usage: machine receive [options]

  Alias: mr

  Get messages received by the machine

  Options:

    --help               output usage information
    -t, --topic <topic>  Topic (default=command)
    --timeout <ms>       Timeout in ms (default=5000)
```


```
  Usage: machine watch [options]

  Alias: mw

  Logs messages received by the machine the moment they are received

  Options:

    --help  output usage information
```


```
  Usage: user send [options] <msg> [params...]

  Alias: us

  Sends an MQTT message as the user, params will be passed into matching message function

  Options:

    --help            output usage information
    -v, --toKeyVal    From params "key1 val1 key2 val2 ..." to { key1: val1, key2: val2, ...} (default=true)
    -a, --toKeyArray  From params "key1 val11 val12 key2 val21 val22 ..." to { key1: [val11, val12], key2: [val21, val22], ... } (default=false)
```


```
  Usage: user sendRaw [options] <jsonString>

  Alias: usr

  Sends an MQTT message as the user with raw payload specified

  Options:

    --help               output usage information
    -t, --topic <topic>  Topic (default=command)
```
