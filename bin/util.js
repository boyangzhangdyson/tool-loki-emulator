function padNumber(val) {
  if (Number.isFinite(val)) {
    return val.toString().padStart(4, '0');
  }
  return val;
}

function arrToKeyPaddedVal(arr) {
  const obj = {};
  for (let i = 0; i < arr.length; i += 2) {
    obj[arr[i]] = padNumber(arr[i + 1]);
  }
  return obj;
}

function arrToKeyPaddedArr(arr) {
  const obj = {};
  for (let i = 0; i < arr.length; i += 3) {
    obj[arr[i]] = [padNumber(arr[i + 1]), padNumber(arr[i + 2])];
  }
  return obj;
}

module.exports = {
  arrToKeyPaddedVal,
  arrToKeyPaddedArr
};
