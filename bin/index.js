#!/usr/bin/env node

const vorpal = require('vorpal')();
const vorpalLog = require('vorpal-log');
const { scenarios } = require('../scenarios');
const Program = require('./program');
const {
  arrToKeyPaddedVal,
  arrToKeyPaddedArr
} = require('./util');

vorpal.use(vorpalLog, { printDate: true })
  .history('loki')
  .delimiter('loki >')
  .log('>>> Run `help` to see available commands <<<')
  .show();

// Autocomplete //
const messageNames = ['hello', 'otaHello', 'avu', 'currentState', 'stateChange', 'stateSet', 'faultsChange', 'currentFaults'];
const topicNames = ['connection', 'faults', 'software', 'auth', 'log', 'current', 'summary', 'scheduler', 'command'];
const regionNames = ['cn', 'row'];
const envNames = ['dev004', 'ci', 'si', 'uat', 'ppe', 'stage'];
const machineTypes = ['475', '520'];

// Setup //
const log = vorpal.logger;
log.setFilter('info');
let program;

function isConnected() {
  if (!program || !program.isConnected) {
    log.error('Not yet connected, run `connect` first');
    return false;
  }
  return true;
}

async function execLongRunningFn(fn) {
  const frames = ['-', '\\', '|', '/'];
  let i = 0;
  const timer = setInterval(() => {
    i = (i + 1) % frames.length;
    vorpal.ui.redraw(`In progress... ${frames[i]}`);
  }, 80);

  let result;
  try {
    result = await fn();
  } finally {
    clearInterval(timer);
    vorpal.ui.redraw.clear();
  }
  return result;
}

// Commands //
vorpal
  .command('connect')
  .description('Perform connection journey')
  .alias('c')
  .option('-t, --type <type>', 'Machine type (default=520)', machineTypes)
  .option('-r, --region <region>', 'Region (default=row)', regionNames)
  .option('-e, --env <env>', 'Environment (default=dev004)', envNames)
  .option('--linkAppVersion <version>', 'Link App version (default=4.1)')
  .option('--isOwner', 'If set, user will be the owner of the machine (default=false)')
  .option('--isOtaMachine', 'If set, uses OTA configuration for the machine with client cert from ota-machines, overrides --useFreeMachine (default=false)')
  .option('--useFreeMachine', 'If set, uses client certs from freepool-machines, else from owned-machines (default=false)')
  .types({
    string: ['t', 'type', 'linkAppVersion'],
    boolean: ['useFreeMachine', 'isOtaMachine', 'isOwner']
  })
  .validate(() => {
    if (program && program.isConnected) {
      log.error('Already connected');
      return false;
    }
    return true;
  })
  .action(async (args) => {
    log.debug(args);
    program = new Program(log);
    const opts = Object.assign({
      region: 'row',
      env: 'dev004',
      type: '520',
      linkAppVersion: '4.1',
      useFreeMachine: false,
      isOtaMachine: false,
      isOwner: false
    }, args.options);
    log.info(`Connecting with ${JSON.stringify(opts)}`);
    try {
      await execLongRunningFn(() => program.connect(opts));
      log.info('Connected');
    } catch (e) {
      log.error(e);
    }
  });

vorpal
  .command('list <item>')
  .description('Prints machine or user information')
  .autocomplete(['machine', 'user'])
  .alias('l', 'ls', 'show')
  .validate(isConnected)
  .action(async (args) => {
    let item;
    if (args.item === 'machine') {
      item = program.getMachineDetails();
    } else if (args.item === 'user') {
      item = program.getUserDetails();
    }
    log.info(JSON.stringify(item, null, 2));
    return item;
  });

vorpal
  .command('run <scenario>')
  .description('Runs a specified scenario within the current context')
  .autocomplete(Object.keys(scenarios))
  .alias('r', 'load')
  .action(async (args) => {
    log.debug(args);
    if (!program) {
      program = new Program(log);
    }

    try {
      const scenario = scenarios[args.scenario];
      log.info(`Running scenario ${args.scenario}`);
      await scenario(program);
      log.info(`Finished running scenario ${args.scenario}`);
    } catch (e) {
      log.error(`Encountered error while running scenario ${args.scenario}`);
      log.error(e);
    }
  });

// Machine
vorpal
  .command('machine send <msg> [params...]')
  .description('Sends an MQTT message as the machine, params will be passed into matching message function')
  .autocomplete(messageNames)
  .alias('ms')
  .option('-v, --toKeyVal', 'From params "key1 val1 key2 val2 ..." to { key1: val1, key2: val2, ...} (default=true)')
  .option('-a, --toKeyArray', 'From params "key1 val11 val12 key2 val21 val22 ..." to { key1: [val11, val12], key2: [val21, val22], ... } (default=false)')
  .parse((cmd, args) => (args.includes('-a') || args.includes('--toKeyArray') ? cmd : `${cmd} -v`))
  .validate((args) => {
    if (!isConnected()) {
      return false;
    } else if (args.options.toKeyVal && args.params.length % 2 !== 0) {
      log.error('toKeyVal option specified but number of params not divisible by 2');
      return false;
    } else if (args.options.toKeyArray && args.params.length % 3 !== 0) {
      log.error('toKeyArray option specified but number of params not divisible by 3');
      return false;
    }
    return true;
  })
  .action(async (args) => {
    log.debug(args);
    let data;
    if (args.options.toKeyVal) {
      data = arrToKeyPaddedVal(args.params);
    } else if (args.options.toKeyArray) {
      data = arrToKeyPaddedArr(args.params);
    } else {
      data = args.params;
    }
    log.info(`Machine sending msg: ${args.msg}, data: ${JSON.stringify(data)}`);
    try {
      await execLongRunningFn(() => program.machinePublish(args.msg, data));
      log.info('Machine sent');
    } catch (e) {
      log.error(e);
    }
  });

vorpal
  .command('machine sendRaw <jsonString>')
  .description('Sends an MQTT message as the machine with raw payload specified')
  .alias('msr')
  .option('-t, --topic <topic>', 'Topic (default=current)', topicNames)
  .validate(isConnected)
  .action(async (args) => {
    log.debug(args);
    try {
      const { topic = 'current' } = args.options;
      const payload = JSON.parse(args.jsonString);
      log.info(`Machine sending topic: ${topic}, payload: ${args.jsonString}`);
      await execLongRunningFn(() => program.machineRawPublish(topic, payload));
      log.info('Machine sent');
    } catch (e) {
      log.error(e);
    }
  });

vorpal
  .command('machine receive')
  .description('Get messages received by the machine')
  .alias('mr')
  .option('-t, --topic <topic>', 'Topic (default=command)', topicNames)
  .option('--timeout <ms>', 'Timeout in ms (default=5000)')
  .validate(isConnected)
  .action(async (args) => {
    log.debug(args);
    let received = [];
    try {
      const { topic = 'command', timeout = 5000 } = args.options;
      received = await execLongRunningFn(() =>
        program.machineReceive(topic, timeout));
      log.info(`Machine received ${JSON.stringify(received, null, 2)}`);
    } catch (e) {
      log.error(e);
    }
    return received;
  });

vorpal
  .command('machine watch')
  .description('Logs messages received by the machine the moment they are received')
  .alias('mw')
  .validate(() => isConnected() && !program.hasListenerAdded())
  .action(async (args) => {
    log.debug(args);
    try {
      program.machineAddMessageListener((topic, message) => {
        log.info(`[Machine receive] (${topic}) ${message}`);
      });
      log.info('Machine watching for messages');
    } catch (e) {
      log.error(e);
    }
  });

// User
vorpal
  .command('user send <msg> [params...]')
  .description('Sends an MQTT message as the user, params will be passed into matching message function')
  .autocomplete(messageNames)
  .alias('us')
  .option('-v, --toKeyVal', 'From params "key1 val1 key2 val2 ..." to { key1: val1, key2: val2, ...} (default=true)')
  .option('-a, --toKeyArray', 'From params "key1 val11 val12 key2 val21 val22 ..." to { key1: [val11, val12], key2: [val21, val22], ... } (default=false)')
  .parse((cmd, args) => (args.includes('-a') || args.includes('--toKeyArray') ? cmd : `${cmd} -v`))
  .validate((args) => {
    if (!isConnected()) {
      return false;
    } else if (args.options.toKeyVal && args.params.length % 2 !== 0) {
      log.error('toKeyVal option specified but number of params not divisible by 2');
      return false;
    } else if (args.options.toKeyArray && args.params.length % 3 !== 0) {
      log.error('toKeyArray option specified but number of params not divisible by 3');
      return false;
    }
    return true;
  })
  .action(async (args) => {
    log.debug(args);
    let data;
    if (args.options.toKeyVal) {
      data = arrToKeyPaddedVal(args.params);
    } else if (args.options.toKeyArray) {
      data = arrToKeyPaddedArr(args.params);
    } else {
      data = args.params;
    }
    log.info(`User sending msg: ${args.msg}, data: ${JSON.stringify(data)}`);
    try {
      await execLongRunningFn(() => program.userPublish(args.msg, data));
      log.info('User sent');
    } catch (e) {
      log.error(e);
    }
  });

vorpal
  .command('user sendRaw <jsonString>')
  .description('Sends an MQTT message as the user with raw payload specified')
  .alias('usr')
  .option('-t, --topic <topic>', 'Topic (default=command)', topicNames)
  .validate(isConnected)
  .action(async (args) => {
    log.debug(args);
    try {
      const { topic = 'command' } = args.options;
      const payload = JSON.parse(args.jsonString);
      log.info(`User sending topic: ${topic}, payload: ${args.jsonString}`);
      await execLongRunningFn(() => program.userRawPublish(topic, payload));
      log.info('Sent');
    } catch (e) {
      log.error(e);
    }
  });

module.exports = vorpal;
