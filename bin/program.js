const { getConnectedUserAndMachine } = require('../scenarios');
const messages = require('../lib/messages/machineMessages');
const { poll } = require('../lib/helpers/poll');

function pick(obj, props) {
  const out = {};
  props.forEach((key) => {
    out[key] = obj[key];
  });
  return out;
}

class Program {
  constructor(logger) {
    this.log = logger;
    this.machine = undefined;
    this.user = undefined;
    this.isConnected = false;
    this.isListenerAdded = false;
  }

  async connect(opts) {
    if (this.isConnected) {
      this.log.warn('Already connected, will replace previous machine and user');
    }

    const { machine, user } = await getConnectedUserAndMachine(opts);
    this.machine = machine;
    this.user = user;
    this.isConnected = true;
  }

  hasConnected() {
    if (!this.isConnected) {
      this.log.error('Not yet connected');
      return false;
    }
    return true;
  }

  hasListenerAdded() {
    if (this.isListenerAdded) {
      this.log.error('Listener already added');
      return true;
    }
    return false;
  }

  machineAddMessageListener(handler) {
    if (!this.hasConnected() || this.hasListenerAdded()) {
      return;
    }

    this.machine.addMessageListener(handler);
    this.isListenerAdded = true;
  }

  async machinePublish(msg, data) {
    if (!this.hasConnected()) {
      return;
    }

    const messageFn = messages[msg];
    if (!messageFn) {
      this.log.error(`Could not find ${msg} in lib/messages/machineMessages`);
      return;
    }

    await this.machine.publish(messageFn(data));
  }

  async machineRawPublish(topic, payload) {
    if (!this.hasConnected()) {
      return;
    }

    const messageFn = machine => ({
      topic: machine.topics[topic],
      payload: {
        time: new Date().toISOString(),
        ...payload
      }
    });

    await this.machine.publish(messageFn);
  }

  async machineReceive(topic = 'command', timeoutMs = 5000) {
    if (!this.hasConnected()) {
      return [];
    }

    const self = this;
    const received = await poll(
      async () => this.machine.getAllReceivedMessagesForTopic(self.machine.topics[topic]),
      timeoutMs,
      result => result.length !== 0
    );
    return received;
  }

  async userPublish(msg, data) {
    if (!this.hasConnected()) {
      return;
    }

    const messageFn = messages[msg];
    if (!messageFn) {
      this.log.error(`Could not find ${msg} in lib/messages/machineMessages`);
      return;
    }

    await this.user.publish(messageFn(data), this.machine);
  }

  async userRawPublish(topic = 'command', payload) {
    if (!this.hasConnected()) {
      return;
    }

    const messageFn = machine => ({
      topic: machine.topics[topic],
      payload: {
        time: new Date().toISOString(),
        ...payload
      }
    });

    await this.user.publish(messageFn, this.machine);
  }

  getUserDetails() {
    const props = ['country', 'email', 'password', 'locale', 'firstName', 'lastName', 'honorific', 'contactChoice', 'linkAppVersion', 'account', 'passwordHash', 'authToken', 'apiVersion'];
    return pick(this.user, props);
  }

  getMachineDetails() {
    const props = ['serial', 'ungradedSerial', 'type', 'provisioningType', 'connectionJourneyType', 'topics'];
    return pick(this.machine, props);
  }
}

module.exports = Program;
