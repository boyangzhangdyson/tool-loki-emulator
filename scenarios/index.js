/* eslint-disable global-require, import/no-dynamic-require */
// Disable global and dynamic require to load all scenarios in this directory

const fs = require('fs');
const path = require('path');
const { poll } = require('../lib/helpers/poll');


const scenarios = fs.readdirSync(__dirname)
  .filter(file => path.extname(file) === '.js' && !file.endsWith('index.js') && !file.endsWith('base.js'))
  .map(file => ({ name: path.basename(file, '.js'), scenario: require(path.join(__dirname, file)) }))
  .reduce((obj, { name, scenario }) => ({ [name]: scenario, ...obj }), {});


async function getConnectedUserAndMachine({
  region = 'row',
  env = 'dev004',
  type = '520',
  linkAppVersion = '4.1',
  useFreeMachine = false,
  isOtaMachine = false,
  isOwner = false
}) {
  // Environment variables
  process.env.LINK_ENV = `${region}.${env}`;
  process.env.TEST_DATA_MODE = useFreeMachine ? 'new' : 'existing';

  // Requires
  const { userFactory } = require('../lib/user');
  const { machineFactory } = require('../lib/machine');
  const connectionJourneyHelper = require('../lib/helpers/connectionJourney');


  // Create user
  const user = await userFactory.create({ linkAppVersion });

  // Create machine
  const machine = await machineFactory.create(m => m.type === type, isOtaMachine);

  // Perform connection journey
  await connectionJourneyHelper.perform(machine, user, isOtaMachine);

  // No SLA exists/enforced on time taken to process AVU and update manifest.
  // So, ensure AVU is processed within 'processAVUTimeAllowance' seconds
  // before acting on the machine (use, rename or claiming ownership)
  const readConnectionStatus = () => user.readConnectionStatus(machine);
  const additionalSuccessConditions = connectionStatus => connectionStatus.Status === 'connected';
  const processAVUTimeAllowance = 90;
  await poll(
    readConnectionStatus,
    processAVUTimeAllowance * 1000,
    additionalSuccessConditions
  );

  // Link the user to the machine internally,
  // connect user to brokers and rename machine as user would do in the app
  // OTA machine will require default auto update value to false
  await user.useMachine(machine, !isOtaMachine);

  // Subscribe machine to topics
  if (machine.machineIdentity.connectionJourneyType !== 'lecOnly') {
    await machine.subscribe();
  }

  if (isOwner) {
    await user.takeOwnership(machine);

    // Make sure the machine has been associated properly within 60 seconds
    const secondsTimeout = 60;
    const verifyMachineAssociation = () => user.verifyMachineAssociation(machine);
    await poll(
      verifyMachineAssociation,
      parseInt(secondsTimeout, 10) * 1000,
      result => result === true
    );
  }

  return { machine, user };
}

module.exports = {
  scenarios,
  getConnectedUserAndMachine
};
