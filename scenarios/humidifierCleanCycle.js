const { sleep } = require('../lib/helpers/sleep');

async function humidifierCleanCycle(program) {
  //CleanCycleNearlyDue
  await program.machinePublish('stateChange', { cltr: ['0100', '0036'] });
  program.log.info(`Machine sent stateChange with ${JSON.stringify({ cltr: ['0100', '0036'] })}`);
  await sleep(1);

  //CleanCycleOverdue
  await program.machinePublish('faultsChange', { cldu: ['OK', 'FAIL'] });
  program.log.info(`Machine sent faultsChange with ${JSON.stringify({ cldu: ['OK', 'FAIL'] })}`);
  await sleep(1);

  //CleanCycleCompleted
  await program.machinePublish('stateChange', { clcr: ['CLAC', 'CLCM'] });
  program.log.info(`Machine sent stateChange with ${JSON.stringify({ clcr: ['CLAC', 'CLCM'] })}`);
  await sleep(1);

  //Reset
  await program.machinePublish('stateChange', { cltr: ['0000', '0100'] });
  program.log.info(`Machine sent stateChange with ${JSON.stringify({ cltr: ['0000', '0100'] })}`);
  await sleep(1);
}

module.exports = humidifierCleanCycle;
