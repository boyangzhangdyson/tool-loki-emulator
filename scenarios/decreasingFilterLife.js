/* eslint-disable no-await-in-loop */
// Disable await in loop to send sequentially

const { sleep } = require('../lib/helpers/sleep');


// Sends STATE-CHANGE messages with decreasing filter life with 1s interval
async function decreasingFilterLife(program) {
  const msg = 'stateChange';
  for (let i = 10; i >= 0; i -= 1) {
    const filfVal = i.toString().padStart(4, '0');
    const overrides = { filf: [filfVal, filfVal] };
    await program.machinePublish(msg, overrides);
    program.log.info(`Machine sent ${msg} with ${JSON.stringify(overrides)}`);
    await sleep(1);
  }
}

module.exports = decreasingFilterLife;
