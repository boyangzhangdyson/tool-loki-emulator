# Loki
An interactive command line tool for connecting and controlling fake machines and users.

Loki is a bundle of lies, able to morph into different machines/users. No physical machines and users were harmed in the process of creating this tool.

Loki is based on the cloud [end-to-end tests](https://stash.dyson.global.corp/projects/CTCLD/repos/end-to-end-tests/browse). Interactive CLI built with [Vorpal](http://vorpal.js.org/).


## Run
This will start Loki and drop into a `loki >` prompt from which the commands listed in [USAGE.md](USAGE.md) can be used.

### With Node.js
```
npm install
npm start
```

### From path
```
npm install
npm link
loki
```

### With Docker
```
docker build . -t loki
docker run --rm -it loki
```


## Usage
Loki is an interactive command line tool. Running Loki will start a prompt from which the following commands can be entered.

For more details, see [USAGE.md](USAGE.md)

```
  Commands:

    help [command...]                         Provides help for a given command.
    exit                                      Exits application.
    connect [options]                         Perform connection journey
    list <item>                               Prints machine or user information
    run <scenario>                            Runs a specified scenario within the current context
    machine send [options] <msg> [params...]  Sends an MQTT message as the machine, params will be passed into matching message function
    machine sendRaw [options] <jsonString>    Sends an MQTT message as the machine with raw payload specified
    machine receive [options]                 Get messages received by the machine
    machine watch                             Logs messages received by the machine the moment they are received
    user send [options] <msg> [params...]     Sends an MQTT message as the user, params will be passed into matching message function
    user sendRaw [options] <jsonString>       Sends an MQTT message as the user with raw payload specified
```

### Adding a user/machine/message/scenario
- Define user info in [testUsers.json](testUsers.json), first user with `(region === row && country === GB)` or `(region === cn && country === CN)` in array will be selected
- Define machine in [lib/machine/machineDefinitions.js](lib/machine/machineDefinitions.js) and add certs in [certs/client-certs](certs/client-certs), first machineDefinition with `(type === machine type)` and first cert which matches the sku will be selected
- Define message function in [lib/messages/machineMessages.js](lib/messages/machineMessages.js), the exported name will be used in `<msg>` parameters in commands
- Add a scenario file in [scenarios](scenarios), the file name (excluding file extension) will be used in `<scenario>` parameters in commands

### Differences from E2E test
- Methods added to `Machine`, `User`, `AwsIotClient`, `RabbitmqClient` for adding event listeners and getting received messages
- Some messages removed from [lib/messages/machineMessages.js](lib/messages/machineMessages.js) and message function signature standardised to `(data) => (machine) => ({ topic, payload })`
- Condition added in [lib/env/env.js#getPublicCloudBaseUrl](lib/env/env.js#getPublicCloudBaseUrl) to direct to old api url in dev004
- Condition added in [lib/user/user.js#connect](lib/user/user.js#connect) to direct to old broker url in dev004
- Randomness removed from `MachineFactory` and `UserFactory` when choosing serials/existing users respectively; will use first serial/user found
- Path to certs changed to use dirname instead of cwd in [lib/machine/lib/certificates/certificates.js](lib/machine/lib/certificates/certificates.js) and [lib/user/lib/certificates/certificates.js](lib/user/lib/certificates/certificates.js)
- Device names and client IDs renamed, E2E certs and users removed to prevent conflicts with E2E tests


## Examples
The following commands are executed in the `loki >` prompt after starting.

### Connecting to SI with a 520
```
loki > connect --region row --env si --type 520
```

### Send STATE-CHANGE from machine

* Filter needs replacement for Gen1 device
```
loki > machine send stateChange filf 0 0 --toKeyArray
```

* Cleaning cycle status changed for 358
```
loki > machine send stateChange clcr CLAC CLCM --toKeyArray
```

### Send FAULTS-CHANGE from machine

* Cleaning required for 358
```
loki > machine send faultsChange cldu OK FAIL --toKeyArray
```
* Alerting empty tank status for 358
```
loki > machine send faultsChange tnke OK FAIL --toKeyArray
```

### Send STATE-SET from user
```
loki > user send stateSet fpwr ON
```

### Run the scenario on decreasing filter life for Gen1 devices
```
loki > run decreasingFilterLife
```


### Run the scenario on trigger clean cycle for 358

```
loki > run humidifierCleanCycle
```

### Print out messages received by machine as soon as they are received
```
loki > machine watch
```

### Output logs with debug level and above
```
loki > loglevel debug
```


## Known Issues
- appapi.cpdev004.dyson.com does not exist, redirect already added
- appbroker1.cpdev004.dyson.com does not exist, redirect already added
- Connecting a machine using `awsIot` or `lecOnly` or `lecHec` does not work in dev004, machines in [lib/machine/machineDefinitions.js](lib/machine/machineDefinitions.js) can be changed to use `rabbitMQ` and `hecOnly`
- Updating LEC certificates failure, connection journey will still succeed


## Debugging
1. Run the following command depending on if running on local or in Docker:
    - On local
      ```
      npm run debugLocal
      ```
    - In Docker
      ```
      docker run --rm  -it -p 127.0.0.1:9221:9221 -v ${PWD}:/opt/dyson/loki -v /opt/dyson/loki/node_modules loki debugDocker
      ```
2. Attach the inspector client
    - For VSCode, the configuration is in `.vscode/launch.json`
