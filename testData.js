// Expect an object in the form:
// {
//   row.ci: [],
//   row.si: [].
//   ..
// }
const users = require('./testUsers.json');

module.exports = {
  users,
};
